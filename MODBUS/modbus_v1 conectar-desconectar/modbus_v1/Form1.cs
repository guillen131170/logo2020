﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EasyModbus;

namespace modbus_v1
{
    public partial class Form1 : Form
    {

        private ModbusClient plc;
        private ModbusServer pc;
        private string msg;

        public Form1()
        {
            InitializeComponent();
            plc = new ModbusClient(ip.Text, Convert.ToInt32(port.Text));
        }

        private void on_Click(object sender, EventArgs e)
        {
            if (!plc.Connected)
            {
                plc.Connect();
                text.AppendText(Environment.NewLine);
                text.AppendText("Has pulsado el botón CONECTAR");
                if (plc.Connected)
                {
                    msg = "OK: conectado";
                    MessageBox.Show(msg);
                }
                else
                {
                    msg = "Error: sigues desconectado";
                    MessageBox.Show(msg);
                }
                text.AppendText(Environment.NewLine);
                text.AppendText(msg);
            }
            else
            {
                msg = "Error: no puedes conectarte, ya estás conectado";
                MessageBox.Show(msg);
                text.AppendText(Environment.NewLine);
                text.AppendText(msg);
            }
        }

        private void off_Click(object sender, EventArgs e)
        {
            if (plc.Connected)
            {
                plc.Disconnect();
                text.AppendText(Environment.NewLine);
                text.AppendText("Has pulsado el botón DESCONECTAR");
                if (!plc.Connected)
                {
                    msg = "OK: desconectado";
                    MessageBox.Show(msg);
                }
                else
                {
                    msg = "Error: sigues conectado";
                    MessageBox.Show(msg);
                }
                text.AppendText(Environment.NewLine);
                text.AppendText(msg);
            }
            else
            {
                msg = "Error: no puedes desconectarte, ya estás desconectado";
                MessageBox.Show(msg);
                text.AppendText(Environment.NewLine);
                text.AppendText(msg);
            }
        }
    }
}
