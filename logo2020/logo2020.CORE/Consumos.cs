﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.CORE
{
    /// <summary>
    /// ESTA CLASE SERÁN LOS DATOS DE CONSUMOS
    /// QUE SE GUARDARÁN EN BASE DE DATOS
    /// 
    /// atributos:
    ///     - id - identificador de la dirrección de correo
    ///     - name - nombre del propietario de la dirrección de correo
    ///     - email - cuenta de correo electrónico
    ///     - level - indica el nivel del usuario y sirve para indicar
    ///               a quién se dirige el correo y a quién no
    ///               (VA A DEPENDER DE LA ALARMA DE QUE SE TARTE)
    /// </summary>   
    public class Consumos
    {
        /*################################################################*/
        #region atributos     
        /// <summary>
        /// identificador de dato de consumo
        /// </summary>
        private int id;

        /// <summary>
        /// consumo acumulado total (decilitros)
        /// </summary>
        private int accumulate;

        /// <summary>
        /// consumo de ese día (decilitros)
        /// </summary>
        private int amount;

        /// <summary>
        /// día del dato de consumo
        /// </summary>
        private int _day;

        /// <summary>
        /// mes del dato de consumo
        /// </summary>
        private int _month;

        /// <summary>
        /// año del dato de consumo
        /// </summary>
        private int _year;

        /// <summary>
        /// fecha completa del dato de consumo
        /// año + mes + día
        /// </summary>
        private int _date;
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region /* constructores */

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Consumos() { }

        /// <summary>
        /// Constructor por parámetros
        /// </summary>
        public Consumos(int Accumulate, int Amount, int _Day, int _Mounth, int _Year)
        {
            this.Accumulate = Accumulate;
            this.Amount = Amount;
            this._Day = _Day;
            this._Mounth = _Mounth;
            this._Year = _Year;
            _Date = (((_Year * 100) + _Mounth) * 100) + _Day;
        }

        /// <summary>
        /// Constructor copia
        /// </summary>
        public Consumos(Consumos o)
        {
            Id = o.Id;
            Accumulate = o.Accumulate;
            Amount = o.Amount;
            _Day = o._Day;
            _Mounth = o._Mounth;
            _Year = o._Year;
            _Date = (((_Year * 100) + _Mounth) * 100) + _Day;
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region propiedades
        [Required]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Required]
        public int Accumulate
        {
            get { return accumulate; }
            set { accumulate = value; }
        }

        [Required]
        public int Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        [Required]
        [Range(1, 31)]
        public int _Day
        {
            get { return _day; }
            set { _day = value; }
        }

        [Required]
        [Range(1, 12)]
        public int _Mounth
        {
            get { return _month; }
            set { _month = value; }
        }

        [Required]
        [Range(2020, 2099)]
        public int _Year
        {
            get { return _year; }
            set { _year = value; }
        }

        [Required]
        public int _Date
        {
            get { return _date; }
            set { _date = value; }
        }
        #endregion
        /*################################################################*/
    }
}
