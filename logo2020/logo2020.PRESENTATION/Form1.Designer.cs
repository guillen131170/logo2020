﻿namespace logo2020.PRESENTATION
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.cabecera = new System.Windows.Forms.Panel();
            this.textip = new System.Windows.Forms.TextBox();
            this.panelcabcentral = new System.Windows.Forms.Panel();
            this.pictureBoxBoton = new System.Windows.Forms.PictureBox();
            this.off = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.PanelConnect = new System.Windows.Forms.Panel();
            this.on = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.slotText = new System.Windows.Forms.TextBox();
            this.rackText = new System.Windows.Forms.TextBox();
            this.ipText = new System.Windows.Forms.TextBox();
            this.dnsText = new System.Windows.Forms.TextBox();
            this.labelonoff = new System.Windows.Forms.Label();
            this.pictureon = new System.Windows.Forms.PictureBox();
            this.panel = new System.Windows.Forms.Panel();
            this.panelsubcab = new System.Windows.Forms.Panel();
            this.cabecera.SuspendLayout();
            this.panelcabcentral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBoton)).BeginInit();
            this.PanelConnect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureon)).BeginInit();
            this.SuspendLayout();
            // 
            // cabecera
            // 
            this.cabecera.Controls.Add(this.textip);
            this.cabecera.Controls.Add(this.panelcabcentral);
            this.cabecera.Controls.Add(this.pictureon);
            this.cabecera.Location = new System.Drawing.Point(12, 12);
            this.cabecera.Name = "cabecera";
            this.cabecera.Size = new System.Drawing.Size(636, 134);
            this.cabecera.TabIndex = 0;
            // 
            // textip
            // 
            this.textip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(213)))), ((int)(((byte)(48)))));
            this.textip.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textip.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textip.ForeColor = System.Drawing.Color.Black;
            this.textip.Location = new System.Drawing.Point(13, 58);
            this.textip.Name = "textip";
            this.textip.ReadOnly = true;
            this.textip.Size = new System.Drawing.Size(59, 11);
            this.textip.TabIndex = 22;
            // 
            // panelcabcentral
            // 
            this.panelcabcentral.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelcabcentral.Controls.Add(this.pictureBoxBoton);
            this.panelcabcentral.Controls.Add(this.off);
            this.panelcabcentral.Controls.Add(this.progressBar1);
            this.panelcabcentral.Controls.Add(this.PanelConnect);
            this.panelcabcentral.Controls.Add(this.labelonoff);
            this.panelcabcentral.Location = new System.Drawing.Point(137, 3);
            this.panelcabcentral.Name = "panelcabcentral";
            this.panelcabcentral.Size = new System.Drawing.Size(494, 126);
            this.panelcabcentral.TabIndex = 1;
            // 
            // pictureBoxBoton
            // 
            this.pictureBoxBoton.Image = global::logo2020.PRESENTATION.Properties.Resources.button_Red;
            this.pictureBoxBoton.Location = new System.Drawing.Point(375, 3);
            this.pictureBoxBoton.Name = "pictureBoxBoton";
            this.pictureBoxBoton.Size = new System.Drawing.Size(53, 59);
            this.pictureBoxBoton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxBoton.TabIndex = 1;
            this.pictureBoxBoton.TabStop = false;
            // 
            // off
            // 
            this.off.Enabled = false;
            this.off.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.off.Location = new System.Drawing.Point(375, 64);
            this.off.Name = "off";
            this.off.Size = new System.Drawing.Size(112, 41);
            this.off.TabIndex = 16;
            this.off.Text = "Desconectar";
            this.off.UseVisualStyleBackColor = true;
            this.off.Click += new System.EventHandler(this.off_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(5, 111);
            this.progressBar1.MarqueeAnimationSpeed = 20;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(482, 10);
            this.progressBar1.TabIndex = 15;
            // 
            // PanelConnect
            // 
            this.PanelConnect.Controls.Add(this.on);
            this.PanelConnect.Controls.Add(this.label5);
            this.PanelConnect.Controls.Add(this.label4);
            this.PanelConnect.Controls.Add(this.label3);
            this.PanelConnect.Controls.Add(this.label2);
            this.PanelConnect.Controls.Add(this.slotText);
            this.PanelConnect.Controls.Add(this.rackText);
            this.PanelConnect.Controls.Add(this.ipText);
            this.PanelConnect.Controls.Add(this.dnsText);
            this.PanelConnect.Location = new System.Drawing.Point(5, 3);
            this.PanelConnect.Name = "PanelConnect";
            this.PanelConnect.Size = new System.Drawing.Size(364, 104);
            this.PanelConnect.TabIndex = 14;
            // 
            // on
            // 
            this.on.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.on.Location = new System.Drawing.Point(249, 60);
            this.on.Name = "on";
            this.on.Size = new System.Drawing.Size(112, 41);
            this.on.TabIndex = 10;
            this.on.Text = "Conectar";
            this.on.UseVisualStyleBackColor = true;
            this.on.Click += new System.EventHandler(this.on_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(120, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "SLOT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "RACK";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(38, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "DNS";
            // 
            // slotText
            // 
            this.slotText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slotText.Location = new System.Drawing.Point(173, 77);
            this.slotText.Name = "slotText";
            this.slotText.Size = new System.Drawing.Size(49, 24);
            this.slotText.TabIndex = 4;
            this.slotText.Text = "0";
            // 
            // rackText
            // 
            this.rackText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rackText.Location = new System.Drawing.Point(65, 77);
            this.rackText.Name = "rackText";
            this.rackText.Size = new System.Drawing.Size(49, 24);
            this.rackText.TabIndex = 3;
            this.rackText.Text = "1";
            // 
            // ipText
            // 
            this.ipText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipText.Location = new System.Drawing.Point(65, 40);
            this.ipText.Name = "ipText";
            this.ipText.Size = new System.Drawing.Size(157, 24);
            this.ipText.TabIndex = 2;
            this.ipText.Text = "192.168.0.3";
            // 
            // dnsText
            // 
            this.dnsText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dnsText.Location = new System.Drawing.Point(65, 3);
            this.dnsText.Name = "dnsText";
            this.dnsText.Size = new System.Drawing.Size(296, 24);
            this.dnsText.TabIndex = 1;
            // 
            // labelonoff
            // 
            this.labelonoff.AutoSize = true;
            this.labelonoff.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelonoff.Location = new System.Drawing.Point(425, 18);
            this.labelonoff.Name = "labelonoff";
            this.labelonoff.Size = new System.Drawing.Size(62, 29);
            this.labelonoff.TabIndex = 17;
            this.labelonoff.Text = "OFF";
            // 
            // pictureon
            // 
            this.pictureon.Image = global::logo2020.PRESENTATION.Properties.Resources.plcoff;
            this.pictureon.Location = new System.Drawing.Point(3, 3);
            this.pictureon.Name = "pictureon";
            this.pictureon.Size = new System.Drawing.Size(128, 126);
            this.pictureon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureon.TabIndex = 13;
            this.pictureon.TabStop = false;
            // 
            // panel
            // 
            this.panel.Location = new System.Drawing.Point(12, 152);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1260, 500);
            this.panel.TabIndex = 2;
            // 
            // panelsubcab
            // 
            this.panelsubcab.Location = new System.Drawing.Point(654, 12);
            this.panelsubcab.Name = "panelsubcab";
            this.panelsubcab.Size = new System.Drawing.Size(618, 134);
            this.panelsubcab.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1284, 661);
            this.Controls.Add(this.panelsubcab);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.cabecera);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CONTROL DE BARRILES";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.cabecera.ResumeLayout(false);
            this.cabecera.PerformLayout();
            this.panelcabcentral.ResumeLayout(false);
            this.panelcabcentral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBoton)).EndInit();
            this.PanelConnect.ResumeLayout(false);
            this.PanelConnect.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel cabecera;
        private System.Windows.Forms.PictureBox pictureon;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel panelcabcentral;
        private System.Windows.Forms.Button off;
        private System.Windows.Forms.Panel PanelConnect;
        private System.Windows.Forms.Button on;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox slotText;
        private System.Windows.Forms.TextBox rackText;
        private System.Windows.Forms.TextBox ipText;
        private System.Windows.Forms.TextBox dnsText;
        private System.Windows.Forms.TextBox textip;
        private System.Windows.Forms.PictureBox pictureBoxBoton;
        private System.Windows.Forms.Label labelonoff;
        public System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Panel panelsubcab;
    }
}

