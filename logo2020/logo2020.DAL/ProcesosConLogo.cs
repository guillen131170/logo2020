﻿using logo2020.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.DAL
{
    public class ProcesosConLogo
    {
        /*################################################################*/
        #region ATRIBUTOS DE LA CLASE
        /* Objeto Logo */
        private Logos logo;
        /* Objeto Sharp7 */
        private S7Client client = new S7Client();
        //private int sizeRead = 0;
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region BLOQUES DE DATOS
        private const int BLOCK1024 = 1024;
        private const int BLOCK512 = 512;
        private const int BLOCK256 = 256;
        private const int BLOCK128 = 128;
        private const int BLOCK64 = 64;
        private const int BLOCK32 = 32;
        private const int BLOCK16 = 16;
        private const int BLOCK8 = 8;
        private const int BLOCK4 = 4;
        private const int BLOCK3 = 3;
        private const int BLOCK2 = 2;
        private const int BLOCK1 = 1;

        private byte[] buffer1024 = new byte[BLOCK1024];
        public byte[] buffer512 = new byte[BLOCK512];
        private byte[] buffer256 = new byte[BLOCK256];
        private byte[] buffer128 = new byte[BLOCK128];
        private byte[] buffer64 = new byte[BLOCK64];
        private byte[] buffer32 = new byte[BLOCK32];
        private byte[] buffer16 = new byte[BLOCK16];
        private byte[] buffer8 = new byte[BLOCK8];
        private byte[] buffer4 = new byte[BLOCK4];
        private byte[] buffer3 = new byte[BLOCK3];
        private byte[] buffer2 = new byte[BLOCK2];
        private byte[] buffer1 = new byte[BLOCK1];
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region ESTRUCTURAS DE DATOS
        public int[] Area =
        {
            S7Consts.S7AreaPE,
            S7Consts.S7AreaPA,
            S7Consts.S7AreaMK,
            S7Consts.S7AreaDB,
            S7Consts.S7AreaCT,
            S7Consts.S7AreaTM
        };
        public int[] WordLen =
        {
            S7Consts.S7WLBit,
            S7Consts.S7WLByte,
            S7Consts.S7WLChar,
            S7Consts.S7WLWord,
            S7Consts.S7WLInt,
            S7Consts.S7WLDWord,
            S7Consts.S7WLDInt,
            S7Consts.S7WLReal,
            S7Consts.S7WLCounter,
            S7Consts.S7WLTimer
        };
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region CONSTRUCTORES DE LA CLASE        
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ProcesosConLogo() { }

        /// <summary>
        /// Constructor con 2 parámetros
        /// </summary>
        /// <param name="dns">Nombre de dominio de la dirección de red de Logo</param>
        /// <param name="ip">Dirección de red de Logo</param>
        public ProcesosConLogo(string dns, string ip)
        {
            logo = new Logos(dns, ip);
        }


        /// <summary>
        /// Constructor copia
        /// </summary>
        /// <param name="o">Objeto ProcesosConLogo que se va a copiar</param>
        public ProcesosConLogo(ProcesosConLogo o)
        {
            Logo = o.Logo;
            client = o.client;
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region CONECTAR/DESCONECTAR LOGO

        /// <summary>
        /// Conectar con Logo mediante IP
        /// </summary>
        /// <returns></returns>
        public int connectPlcIP()
        {
            return client.ConnectTo(logo.IP, logo.Rack, logo.Slot);
        }

        /// <summary>
        /// Conectar con Logo mediante DNS
        /// </summary>
        /// <returns></returns>
        public void connectPlcDNS()
        {
            try
            {
                IPHostEntry host = Dns.GetHostEntry(logo.DNS);
                if (host != null)
                {
                    foreach (IPAddress address in host.AddressList)
                    {
                        logo.IP = address.ToString();
                    }
                }
                connectPlcIP();
            }
            catch { }
        }

        /// <summary>
        /// Desconectar del Logo
        /// </summary>
        /// <returns></returns>
        public void disconnectPLc()
        {
            try
            {
                client.Disconnect();
            }
            catch { }
        }
        #endregion
        /*################################################################*/


        #region LECTURAS DE LOGO
        
        /// <summary>
        /// Lee un bloque de 512 bytes del Logo
        /// </summary>
        /// <returns></returns>
        public int LeerBloque512()
        {
            /* Crear instancia Multi Reader */
            S7MultiVar Reader = new S7MultiVar(Client);

            /* Crea la sentencia */
            Reader.Add(Area[3], WordLen[1], 1, 0, BLOCK512, ref buffer512);
            /* Ejecuta y lee */
            int Result = Reader.Read();
            return Result;
        } 
        #endregion


        /*################################################################*/
        #region MÉTODOS GENERALES DE LA CLASE
        /* Leer resultados */
        public string LeerResultado()
        {
            return Client.PduSizeNegotiated.ToString();
        }

        /* Leer resultados */
        public bool EstaConectado()
        {
            return Client.Connected;
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region PROPIEDADES DE LA CLASE
        /// <summary>
        /// Propiedad de Objeto S7Client
        /// </summary>
        public S7Client Client
        {
            get { return client; }
        }

        /// <summary>
        /// Propiedadad de Objeto Logo
        /// </summary>
        public Logos Logo
        {
            get { return logo; }
            set { logo = value; }
        }
        #endregion
        /*################################################################*/
    }
}
