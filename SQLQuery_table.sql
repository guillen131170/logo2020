CREATE DATABASE taller
GO
USE taller
GO
 
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE ordenes(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	
	cliente [nvarchar](20) NOT NULL,
	cod_cliente [nvarchar](3) NOT NULL,	
	
	pds [nvarchar](40) NULL  DEFAULT '',
	cod_pds [nvarchar](20) NULL  DEFAULT '',	
	
	ods_incidencia [nvarchar](20) NULL  DEFAULT '',	
	ods_taller [nvarchar](20) NULL  DEFAULT '',	
	ods_montaje [nvarchar](20) NULL  DEFAULT '',	
	ods_desmontaje [nvarchar](20) NULL  DEFAULT '',	

	objeto_seriado [nvarchar](40) NULL,
	objeto_noseriado [nvarchar](40) NULL,		
	material [nvarchar](20) NULL,	
	ns [nvarchar](20) NULL,	
	sap [nvarchar](20) NULL,	
	
	orden [nvarchar](20) NULL,	
	oferta [nvarchar](20) NULL,	
	precio [nvarchar](20) NULL,	
	
	tipo [nvarchar](10) NOT NULL,
	estado [nvarchar](10) NOT NULL,
	resultado [nvarchar](10) NOT NULL,	
	
	componente int FOREIGN KEY REFERENCES componentes(id)
	)

GO

CREATE TABLE componentes(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	
	nombre [nvarchar](20) NOT NULL,
	cod [nvarchar](3) NOT NULL,
	unidades [nvarchar](3) NOT NULL  DEFAULT 1
	)
	
GO