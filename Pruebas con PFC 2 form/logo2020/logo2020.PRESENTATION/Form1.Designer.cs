﻿namespace logo2020.PRESENTATION
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.cabecera = new System.Windows.Forms.Panel();
            this.textip = new System.Windows.Forms.TextBox();
            this.panelcabcentral = new System.Windows.Forms.Panel();
            this.pictureBoxBoton = new System.Windows.Forms.PictureBox();
            this.off = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.PanelConnect = new System.Windows.Forms.Panel();
            this.on = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.slotText = new System.Windows.Forms.TextBox();
            this.rackText = new System.Windows.Forms.TextBox();
            this.ipText = new System.Windows.Forms.TextBox();
            this.dnsText = new System.Windows.Forms.TextBox();
            this.labelonoff = new System.Windows.Forms.Label();
            this.pictureon = new System.Windows.Forms.PictureBox();
            this.panel = new System.Windows.Forms.Panel();
            this.panelHMI = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.subpanel3b = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.df3 = new System.Windows.Forms.PictureBox();
            this.label25 = new System.Windows.Forms.Label();
            this.FB_3 = new System.Windows.Forms.PictureBox();
            this.FA_3 = new System.Windows.Forms.PictureBox();
            this.v3_1 = new System.Windows.Forms.PictureBox();
            this.b3_3 = new System.Windows.Forms.Label();
            this.v3_2 = new System.Windows.Forms.PictureBox();
            this.b3_2 = new System.Windows.Forms.Label();
            this.v3_3 = new System.Windows.Forms.PictureBox();
            this.b3_1 = new System.Windows.Forms.Label();
            this.v3_paso = new System.Windows.Forms.PictureBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.subpanel3 = new System.Windows.Forms.Panel();
            this.run3 = new System.Windows.Forms.Button();
            this.stop3 = new System.Windows.Forms.Button();
            this.labelbombilla3 = new System.Windows.Forms.Label();
            this.picbombilla3 = new System.Windows.Forms.PictureBox();
            this.picparomarcha3 = new System.Windows.Forms.PictureBox();
            this.picB31 = new System.Windows.Forms.PictureBox();
            this.picB32 = new System.Windows.Forms.PictureBox();
            this.picB33 = new System.Windows.Forms.PictureBox();
            this.jump3 = new System.Windows.Forms.Button();
            this.refresh3 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.subpanel2b = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.df2 = new System.Windows.Forms.PictureBox();
            this.label24 = new System.Windows.Forms.Label();
            this.FB_2 = new System.Windows.Forms.PictureBox();
            this.FA_2 = new System.Windows.Forms.PictureBox();
            this.v2_1 = new System.Windows.Forms.PictureBox();
            this.b2_3 = new System.Windows.Forms.Label();
            this.v2_2 = new System.Windows.Forms.PictureBox();
            this.b2_2 = new System.Windows.Forms.Label();
            this.v2_3 = new System.Windows.Forms.PictureBox();
            this.b2_1 = new System.Windows.Forms.Label();
            this.v2_paso = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.subpanel2 = new System.Windows.Forms.Panel();
            this.run2 = new System.Windows.Forms.Button();
            this.stop2 = new System.Windows.Forms.Button();
            this.labelbombilla2 = new System.Windows.Forms.Label();
            this.picbombilla2 = new System.Windows.Forms.PictureBox();
            this.picparomarcha2 = new System.Windows.Forms.PictureBox();
            this.picB23 = new System.Windows.Forms.PictureBox();
            this.picB22 = new System.Windows.Forms.PictureBox();
            this.picB21 = new System.Windows.Forms.PictureBox();
            this.refresh2 = new System.Windows.Forms.Button();
            this.jump2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.subpanel1 = new System.Windows.Forms.Panel();
            this.picB13 = new System.Windows.Forms.PictureBox();
            this.picB12 = new System.Windows.Forms.PictureBox();
            this.picB11 = new System.Windows.Forms.PictureBox();
            this.labelbombilla1 = new System.Windows.Forms.Label();
            this.run1 = new System.Windows.Forms.Button();
            this.stop1 = new System.Windows.Forms.Button();
            this.picbombilla1 = new System.Windows.Forms.PictureBox();
            this.picparomarcha1 = new System.Windows.Forms.PictureBox();
            this.jump1 = new System.Windows.Forms.Button();
            this.refresh1 = new System.Windows.Forms.Button();
            this.subpanel1b = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.df1 = new System.Windows.Forms.PictureBox();
            this.FA_1 = new System.Windows.Forms.PictureBox();
            this.v1_1 = new System.Windows.Forms.PictureBox();
            this.FB_1 = new System.Windows.Forms.PictureBox();
            this.v1_2 = new System.Windows.Forms.PictureBox();
            this.v1_paso = new System.Windows.Forms.PictureBox();
            this.v1_3 = new System.Windows.Forms.PictureBox();
            this.label39 = new System.Windows.Forms.Label();
            this.b1_3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.b1_2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.b1_1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panelsubcab = new System.Windows.Forms.Panel();
            this.cabecera.SuspendLayout();
            this.panelcabcentral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBoton)).BeginInit();
            this.PanelConnect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureon)).BeginInit();
            this.panel.SuspendLayout();
            this.panelHMI.SuspendLayout();
            this.panel3.SuspendLayout();
            this.subpanel3b.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.df3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_paso)).BeginInit();
            this.subpanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB33)).BeginInit();
            this.panel2.SuspendLayout();
            this.subpanel2b.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.df2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_paso)).BeginInit();
            this.subpanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB21)).BeginInit();
            this.panel1.SuspendLayout();
            this.subpanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picB13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha1)).BeginInit();
            this.subpanel1b.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.df1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_paso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_3)).BeginInit();
            this.SuspendLayout();
            // 
            // cabecera
            // 
            this.cabecera.Controls.Add(this.textip);
            this.cabecera.Controls.Add(this.panelcabcentral);
            this.cabecera.Controls.Add(this.pictureon);
            this.cabecera.Location = new System.Drawing.Point(12, 12);
            this.cabecera.Name = "cabecera";
            this.cabecera.Size = new System.Drawing.Size(636, 134);
            this.cabecera.TabIndex = 0;
            // 
            // textip
            // 
            this.textip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(213)))), ((int)(((byte)(48)))));
            this.textip.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textip.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textip.ForeColor = System.Drawing.Color.Black;
            this.textip.Location = new System.Drawing.Point(13, 58);
            this.textip.Name = "textip";
            this.textip.ReadOnly = true;
            this.textip.Size = new System.Drawing.Size(59, 11);
            this.textip.TabIndex = 22;
            // 
            // panelcabcentral
            // 
            this.panelcabcentral.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelcabcentral.Controls.Add(this.pictureBoxBoton);
            this.panelcabcentral.Controls.Add(this.off);
            this.panelcabcentral.Controls.Add(this.progressBar1);
            this.panelcabcentral.Controls.Add(this.PanelConnect);
            this.panelcabcentral.Controls.Add(this.labelonoff);
            this.panelcabcentral.Location = new System.Drawing.Point(137, 3);
            this.panelcabcentral.Name = "panelcabcentral";
            this.panelcabcentral.Size = new System.Drawing.Size(494, 126);
            this.panelcabcentral.TabIndex = 1;
            // 
            // pictureBoxBoton
            // 
            this.pictureBoxBoton.Image = global::logo2020.PRESENTATION.Properties.Resources.button_Red;
            this.pictureBoxBoton.Location = new System.Drawing.Point(375, 3);
            this.pictureBoxBoton.Name = "pictureBoxBoton";
            this.pictureBoxBoton.Size = new System.Drawing.Size(53, 59);
            this.pictureBoxBoton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxBoton.TabIndex = 1;
            this.pictureBoxBoton.TabStop = false;
            // 
            // off
            // 
            this.off.Enabled = false;
            this.off.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.off.Location = new System.Drawing.Point(375, 64);
            this.off.Name = "off";
            this.off.Size = new System.Drawing.Size(112, 41);
            this.off.TabIndex = 16;
            this.off.Text = "Desconectar";
            this.off.UseVisualStyleBackColor = true;
            this.off.Click += new System.EventHandler(this.off_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(5, 111);
            this.progressBar1.MarqueeAnimationSpeed = 20;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(482, 10);
            this.progressBar1.TabIndex = 15;
            // 
            // PanelConnect
            // 
            this.PanelConnect.Controls.Add(this.on);
            this.PanelConnect.Controls.Add(this.label5);
            this.PanelConnect.Controls.Add(this.label4);
            this.PanelConnect.Controls.Add(this.label3);
            this.PanelConnect.Controls.Add(this.label2);
            this.PanelConnect.Controls.Add(this.slotText);
            this.PanelConnect.Controls.Add(this.rackText);
            this.PanelConnect.Controls.Add(this.ipText);
            this.PanelConnect.Controls.Add(this.dnsText);
            this.PanelConnect.Location = new System.Drawing.Point(5, 3);
            this.PanelConnect.Name = "PanelConnect";
            this.PanelConnect.Size = new System.Drawing.Size(364, 104);
            this.PanelConnect.TabIndex = 14;
            // 
            // on
            // 
            this.on.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.on.Location = new System.Drawing.Point(249, 60);
            this.on.Name = "on";
            this.on.Size = new System.Drawing.Size(112, 41);
            this.on.TabIndex = 10;
            this.on.Text = "Conectar";
            this.on.UseVisualStyleBackColor = true;
            this.on.Click += new System.EventHandler(this.on_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(120, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "SLOT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "RACK";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(38, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "DNS";
            // 
            // slotText
            // 
            this.slotText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slotText.Location = new System.Drawing.Point(173, 77);
            this.slotText.Name = "slotText";
            this.slotText.Size = new System.Drawing.Size(49, 24);
            this.slotText.TabIndex = 4;
            this.slotText.Text = "0";
            // 
            // rackText
            // 
            this.rackText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rackText.Location = new System.Drawing.Point(65, 77);
            this.rackText.Name = "rackText";
            this.rackText.Size = new System.Drawing.Size(49, 24);
            this.rackText.TabIndex = 3;
            this.rackText.Text = "1";
            // 
            // ipText
            // 
            this.ipText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipText.Location = new System.Drawing.Point(65, 40);
            this.ipText.Name = "ipText";
            this.ipText.Size = new System.Drawing.Size(157, 24);
            this.ipText.TabIndex = 2;
            this.ipText.Text = "192.168.8.13";
            // 
            // dnsText
            // 
            this.dnsText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dnsText.Location = new System.Drawing.Point(65, 3);
            this.dnsText.Name = "dnsText";
            this.dnsText.Size = new System.Drawing.Size(296, 24);
            this.dnsText.TabIndex = 1;
            // 
            // labelonoff
            // 
            this.labelonoff.AutoSize = true;
            this.labelonoff.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelonoff.Location = new System.Drawing.Point(425, 18);
            this.labelonoff.Name = "labelonoff";
            this.labelonoff.Size = new System.Drawing.Size(62, 29);
            this.labelonoff.TabIndex = 17;
            this.labelonoff.Text = "OFF";
            // 
            // pictureon
            // 
            this.pictureon.Image = global::logo2020.PRESENTATION.Properties.Resources.plcoff;
            this.pictureon.Location = new System.Drawing.Point(3, 3);
            this.pictureon.Name = "pictureon";
            this.pictureon.Size = new System.Drawing.Size(128, 126);
            this.pictureon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureon.TabIndex = 13;
            this.pictureon.TabStop = false;
            // 
            // panel
            // 
            this.panel.Controls.Add(this.panelHMI);
            this.panel.Location = new System.Drawing.Point(12, 152);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1260, 500);
            this.panel.TabIndex = 2;
            // 
            // panelHMI
            // 
            this.panelHMI.Controls.Add(this.panel3);
            this.panelHMI.Controls.Add(this.panel2);
            this.panelHMI.Controls.Add(this.panel1);
            this.panelHMI.Location = new System.Drawing.Point(3, 3);
            this.panelHMI.Name = "panelHMI";
            this.panelHMI.Size = new System.Drawing.Size(1254, 494);
            this.panelHMI.TabIndex = 33;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.subpanel3b);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.subpanel3);
            this.panel3.Location = new System.Drawing.Point(831, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(395, 471);
            this.panel3.TabIndex = 9;
            // 
            // subpanel3b
            // 
            this.subpanel3b.Controls.Add(this.label23);
            this.subpanel3b.Controls.Add(this.df3);
            this.subpanel3b.Controls.Add(this.label25);
            this.subpanel3b.Controls.Add(this.FB_3);
            this.subpanel3b.Controls.Add(this.FA_3);
            this.subpanel3b.Controls.Add(this.v3_1);
            this.subpanel3b.Controls.Add(this.b3_3);
            this.subpanel3b.Controls.Add(this.v3_2);
            this.subpanel3b.Controls.Add(this.b3_2);
            this.subpanel3b.Controls.Add(this.v3_3);
            this.subpanel3b.Controls.Add(this.b3_1);
            this.subpanel3b.Controls.Add(this.v3_paso);
            this.subpanel3b.Controls.Add(this.label19);
            this.subpanel3b.Controls.Add(this.label22);
            this.subpanel3b.Controls.Add(this.label20);
            this.subpanel3b.Controls.Add(this.label21);
            this.subpanel3b.Location = new System.Drawing.Point(3, 24);
            this.subpanel3b.Name = "subpanel3b";
            this.subpanel3b.Size = new System.Drawing.Size(209, 317);
            this.subpanel3b.TabIndex = 41;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(5, 76);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(65, 18);
            this.label23.TabIndex = 10;
            this.label23.Text = "Detector";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // df3
            // 
            this.df3.Image = ((System.Drawing.Image)(resources.GetObject("df3.Image")));
            this.df3.Location = new System.Drawing.Point(37, 5);
            this.df3.Name = "df3";
            this.df3.Size = new System.Drawing.Size(96, 96);
            this.df3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.df3.TabIndex = 5;
            this.df3.TabStop = false;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.Black;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label25.Location = new System.Drawing.Point(2, 270);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(155, 2);
            this.label25.TabIndex = 33;
            // 
            // FB_3
            // 
            this.FB_3.Location = new System.Drawing.Point(110, 63);
            this.FB_3.Name = "FB_3";
            this.FB_3.Size = new System.Drawing.Size(96, 100);
            this.FB_3.TabIndex = 34;
            this.FB_3.TabStop = false;
            // 
            // FA_3
            // 
            this.FA_3.Image = ((System.Drawing.Image)(resources.GetObject("FA_3.Image")));
            this.FA_3.Location = new System.Drawing.Point(2, 100);
            this.FA_3.Name = "FA_3";
            this.FA_3.Size = new System.Drawing.Size(153, 63);
            this.FA_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FA_3.TabIndex = 32;
            this.FA_3.TabStop = false;
            // 
            // v3_1
            // 
            this.v3_1.Image = ((System.Drawing.Image)(resources.GetObject("v3_1.Image")));
            this.v3_1.Location = new System.Drawing.Point(2, 164);
            this.v3_1.Name = "v3_1";
            this.v3_1.Size = new System.Drawing.Size(45, 46);
            this.v3_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v3_1.TabIndex = 1;
            this.v3_1.TabStop = false;
            // 
            // b3_3
            // 
            this.b3_3.AutoSize = true;
            this.b3_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b3_3.Location = new System.Drawing.Point(127, 273);
            this.b3_3.Name = "b3_3";
            this.b3_3.Size = new System.Drawing.Size(16, 18);
            this.b3_3.TabIndex = 29;
            this.b3_3.Text = "0";
            this.b3_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // v3_2
            // 
            this.v3_2.Image = ((System.Drawing.Image)(resources.GetObject("v3_2.Image")));
            this.v3_2.Location = new System.Drawing.Point(56, 164);
            this.v3_2.Name = "v3_2";
            this.v3_2.Size = new System.Drawing.Size(45, 46);
            this.v3_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v3_2.TabIndex = 2;
            this.v3_2.TabStop = false;
            // 
            // b3_2
            // 
            this.b3_2.AutoSize = true;
            this.b3_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b3_2.Location = new System.Drawing.Point(73, 273);
            this.b3_2.Name = "b3_2";
            this.b3_2.Size = new System.Drawing.Size(16, 18);
            this.b3_2.TabIndex = 28;
            this.b3_2.Text = "0";
            this.b3_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // v3_3
            // 
            this.v3_3.Image = ((System.Drawing.Image)(resources.GetObject("v3_3.Image")));
            this.v3_3.Location = new System.Drawing.Point(110, 164);
            this.v3_3.Name = "v3_3";
            this.v3_3.Size = new System.Drawing.Size(45, 46);
            this.v3_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v3_3.TabIndex = 3;
            this.v3_3.TabStop = false;
            // 
            // b3_1
            // 
            this.b3_1.AutoSize = true;
            this.b3_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b3_1.Location = new System.Drawing.Point(19, 273);
            this.b3_1.Name = "b3_1";
            this.b3_1.Size = new System.Drawing.Size(16, 18);
            this.b3_1.TabIndex = 27;
            this.b3_1.Text = "0";
            this.b3_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // v3_paso
            // 
            this.v3_paso.Image = ((System.Drawing.Image)(resources.GetObject("v3_paso.Image")));
            this.v3_paso.Location = new System.Drawing.Point(161, 164);
            this.v3_paso.Name = "v3_paso";
            this.v3_paso.Size = new System.Drawing.Size(45, 46);
            this.v3_paso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v3_paso.TabIndex = 4;
            this.v3_paso.TabStop = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(113, 213);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(42, 54);
            this.label19.TabIndex = 14;
            this.label19.Text = "V3\r\nBarril\r\n3";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(163, 213);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(43, 36);
            this.label22.TabIndex = 11;
            this.label22.Text = "V5\r\nPaso";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(59, 213);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(42, 54);
            this.label20.TabIndex = 13;
            this.label20.Text = "V2\r\nBarril\r\n2";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(5, 213);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(42, 54);
            this.label21.TabIndex = 12;
            this.label21.Text = "V1\r\nBarril\r\n1";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 3);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(61, 18);
            this.label18.TabIndex = 19;
            this.label18.Text = "LINEA 3";
            // 
            // subpanel3
            // 
            this.subpanel3.Controls.Add(this.run3);
            this.subpanel3.Controls.Add(this.stop3);
            this.subpanel3.Controls.Add(this.labelbombilla3);
            this.subpanel3.Controls.Add(this.picbombilla3);
            this.subpanel3.Controls.Add(this.picparomarcha3);
            this.subpanel3.Controls.Add(this.picB31);
            this.subpanel3.Controls.Add(this.picB32);
            this.subpanel3.Controls.Add(this.picB33);
            this.subpanel3.Controls.Add(this.jump3);
            this.subpanel3.Controls.Add(this.refresh3);
            this.subpanel3.Location = new System.Drawing.Point(214, 24);
            this.subpanel3.Name = "subpanel3";
            this.subpanel3.Size = new System.Drawing.Size(177, 197);
            this.subpanel3.TabIndex = 40;
            // 
            // run3
            // 
            this.run3.FlatAppearance.BorderSize = 0;
            this.run3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.run3.Location = new System.Drawing.Point(94, 57);
            this.run3.Name = "run3";
            this.run3.Size = new System.Drawing.Size(80, 32);
            this.run3.TabIndex = 16;
            this.run3.UseVisualStyleBackColor = true;
            // 
            // stop3
            // 
            this.stop3.FlatAppearance.BorderSize = 0;
            this.stop3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stop3.Location = new System.Drawing.Point(94, 20);
            this.stop3.Name = "stop3";
            this.stop3.Size = new System.Drawing.Size(80, 32);
            this.stop3.TabIndex = 15;
            this.stop3.UseVisualStyleBackColor = true;
            // 
            // labelbombilla3
            // 
            this.labelbombilla3.AutoSize = true;
            this.labelbombilla3.Location = new System.Drawing.Point(3, 15);
            this.labelbombilla3.Name = "labelbombilla3";
            this.labelbombilla3.Size = new System.Drawing.Size(35, 13);
            this.labelbombilla3.TabIndex = 42;
            this.labelbombilla3.Text = "label1";
            // 
            // picbombilla3
            // 
            this.picbombilla3.Location = new System.Drawing.Point(5, 26);
            this.picbombilla3.Name = "picbombilla3";
            this.picbombilla3.Size = new System.Drawing.Size(48, 48);
            this.picbombilla3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbombilla3.TabIndex = 41;
            this.picbombilla3.TabStop = false;
            // 
            // picparomarcha3
            // 
            this.picparomarcha3.Location = new System.Drawing.Point(39, 10);
            this.picparomarcha3.Name = "picparomarcha3";
            this.picparomarcha3.Size = new System.Drawing.Size(69, 84);
            this.picparomarcha3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picparomarcha3.TabIndex = 40;
            this.picparomarcha3.TabStop = false;
            // 
            // picB31
            // 
            this.picB31.Location = new System.Drawing.Point(6, 100);
            this.picB31.Name = "picB31";
            this.picB31.Size = new System.Drawing.Size(48, 48);
            this.picB31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB31.TabIndex = 37;
            this.picB31.TabStop = false;
            // 
            // picB32
            // 
            this.picB32.Location = new System.Drawing.Point(60, 100);
            this.picB32.Name = "picB32";
            this.picB32.Size = new System.Drawing.Size(48, 48);
            this.picB32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB32.TabIndex = 38;
            this.picB32.TabStop = false;
            // 
            // picB33
            // 
            this.picB33.Location = new System.Drawing.Point(114, 100);
            this.picB33.Name = "picB33";
            this.picB33.Size = new System.Drawing.Size(48, 48);
            this.picB33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB33.TabIndex = 39;
            this.picB33.TabStop = false;
            // 
            // jump3
            // 
            this.jump3.FlatAppearance.BorderSize = 0;
            this.jump3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jump3.Location = new System.Drawing.Point(92, 154);
            this.jump3.Name = "jump3";
            this.jump3.Size = new System.Drawing.Size(80, 32);
            this.jump3.TabIndex = 17;
            this.jump3.UseVisualStyleBackColor = true;
            // 
            // refresh3
            // 
            this.refresh3.FlatAppearance.BorderSize = 0;
            this.refresh3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refresh3.Location = new System.Drawing.Point(6, 154);
            this.refresh3.Name = "refresh3";
            this.refresh3.Size = new System.Drawing.Size(80, 32);
            this.refresh3.TabIndex = 18;
            this.refresh3.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.subpanel2b);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.subpanel2);
            this.panel2.Location = new System.Drawing.Point(430, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(395, 471);
            this.panel2.TabIndex = 8;
            // 
            // subpanel2b
            // 
            this.subpanel2b.Controls.Add(this.label17);
            this.subpanel2b.Controls.Add(this.df2);
            this.subpanel2b.Controls.Add(this.label24);
            this.subpanel2b.Controls.Add(this.FB_2);
            this.subpanel2b.Controls.Add(this.FA_2);
            this.subpanel2b.Controls.Add(this.v2_1);
            this.subpanel2b.Controls.Add(this.b2_3);
            this.subpanel2b.Controls.Add(this.v2_2);
            this.subpanel2b.Controls.Add(this.b2_2);
            this.subpanel2b.Controls.Add(this.v2_3);
            this.subpanel2b.Controls.Add(this.b2_1);
            this.subpanel2b.Controls.Add(this.v2_paso);
            this.subpanel2b.Controls.Add(this.label13);
            this.subpanel2b.Controls.Add(this.label16);
            this.subpanel2b.Controls.Add(this.label14);
            this.subpanel2b.Controls.Add(this.label15);
            this.subpanel2b.Location = new System.Drawing.Point(4, 24);
            this.subpanel2b.Name = "subpanel2b";
            this.subpanel2b.Size = new System.Drawing.Size(209, 317);
            this.subpanel2b.TabIndex = 38;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 18);
            this.label17.TabIndex = 10;
            this.label17.Text = "Detector";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // df2
            // 
            this.df2.Image = ((System.Drawing.Image)(resources.GetObject("df2.Image")));
            this.df2.Location = new System.Drawing.Point(38, 4);
            this.df2.Name = "df2";
            this.df2.Size = new System.Drawing.Size(96, 96);
            this.df2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.df2.TabIndex = 5;
            this.df2.TabStop = false;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.Black;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label24.Location = new System.Drawing.Point(1, 269);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(155, 2);
            this.label24.TabIndex = 32;
            // 
            // FB_2
            // 
            this.FB_2.Location = new System.Drawing.Point(111, 62);
            this.FB_2.Name = "FB_2";
            this.FB_2.Size = new System.Drawing.Size(96, 100);
            this.FB_2.TabIndex = 33;
            this.FB_2.TabStop = false;
            // 
            // FA_2
            // 
            this.FA_2.Image = ((System.Drawing.Image)(resources.GetObject("FA_2.Image")));
            this.FA_2.Location = new System.Drawing.Point(3, 99);
            this.FA_2.Name = "FA_2";
            this.FA_2.Size = new System.Drawing.Size(153, 63);
            this.FA_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FA_2.TabIndex = 31;
            this.FA_2.TabStop = false;
            // 
            // v2_1
            // 
            this.v2_1.Image = ((System.Drawing.Image)(resources.GetObject("v2_1.Image")));
            this.v2_1.Location = new System.Drawing.Point(3, 163);
            this.v2_1.Name = "v2_1";
            this.v2_1.Size = new System.Drawing.Size(45, 46);
            this.v2_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v2_1.TabIndex = 1;
            this.v2_1.TabStop = false;
            // 
            // b2_3
            // 
            this.b2_3.AutoSize = true;
            this.b2_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b2_3.Location = new System.Drawing.Point(127, 272);
            this.b2_3.Name = "b2_3";
            this.b2_3.Size = new System.Drawing.Size(16, 18);
            this.b2_3.TabIndex = 29;
            this.b2_3.Text = "0";
            this.b2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // v2_2
            // 
            this.v2_2.Image = ((System.Drawing.Image)(resources.GetObject("v2_2.Image")));
            this.v2_2.Location = new System.Drawing.Point(57, 163);
            this.v2_2.Name = "v2_2";
            this.v2_2.Size = new System.Drawing.Size(45, 46);
            this.v2_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v2_2.TabIndex = 2;
            this.v2_2.TabStop = false;
            // 
            // b2_2
            // 
            this.b2_2.AutoSize = true;
            this.b2_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b2_2.Location = new System.Drawing.Point(74, 272);
            this.b2_2.Name = "b2_2";
            this.b2_2.Size = new System.Drawing.Size(16, 18);
            this.b2_2.TabIndex = 28;
            this.b2_2.Text = "0";
            this.b2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // v2_3
            // 
            this.v2_3.Image = ((System.Drawing.Image)(resources.GetObject("v2_3.Image")));
            this.v2_3.Location = new System.Drawing.Point(111, 163);
            this.v2_3.Name = "v2_3";
            this.v2_3.Size = new System.Drawing.Size(45, 46);
            this.v2_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v2_3.TabIndex = 3;
            this.v2_3.TabStop = false;
            // 
            // b2_1
            // 
            this.b2_1.AutoSize = true;
            this.b2_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b2_1.Location = new System.Drawing.Point(20, 272);
            this.b2_1.Name = "b2_1";
            this.b2_1.Size = new System.Drawing.Size(16, 18);
            this.b2_1.TabIndex = 27;
            this.b2_1.Text = "0";
            this.b2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // v2_paso
            // 
            this.v2_paso.Image = ((System.Drawing.Image)(resources.GetObject("v2_paso.Image")));
            this.v2_paso.Location = new System.Drawing.Point(162, 163);
            this.v2_paso.Name = "v2_paso";
            this.v2_paso.Size = new System.Drawing.Size(45, 46);
            this.v2_paso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v2_paso.TabIndex = 4;
            this.v2_paso.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(114, 212);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 54);
            this.label13.TabIndex = 14;
            this.label13.Text = "V3\r\nBarril\r\n3";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(164, 212);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 36);
            this.label16.TabIndex = 11;
            this.label16.Text = "V5\r\nPaso";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(60, 212);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 54);
            this.label14.TabIndex = 13;
            this.label14.Text = "V2\r\nBarril\r\n2";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(6, 212);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 54);
            this.label15.TabIndex = 12;
            this.label15.Text = "V1\r\nBarril\r\n1";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 18);
            this.label12.TabIndex = 19;
            this.label12.Text = "LINEA 2";
            // 
            // subpanel2
            // 
            this.subpanel2.Controls.Add(this.run2);
            this.subpanel2.Controls.Add(this.stop2);
            this.subpanel2.Controls.Add(this.labelbombilla2);
            this.subpanel2.Controls.Add(this.picbombilla2);
            this.subpanel2.Controls.Add(this.picparomarcha2);
            this.subpanel2.Controls.Add(this.picB23);
            this.subpanel2.Controls.Add(this.picB22);
            this.subpanel2.Controls.Add(this.picB21);
            this.subpanel2.Controls.Add(this.refresh2);
            this.subpanel2.Controls.Add(this.jump2);
            this.subpanel2.Location = new System.Drawing.Point(214, 24);
            this.subpanel2.Name = "subpanel2";
            this.subpanel2.Size = new System.Drawing.Size(177, 197);
            this.subpanel2.TabIndex = 37;
            // 
            // run2
            // 
            this.run2.FlatAppearance.BorderSize = 0;
            this.run2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.run2.Location = new System.Drawing.Point(96, 56);
            this.run2.Name = "run2";
            this.run2.Size = new System.Drawing.Size(80, 32);
            this.run2.TabIndex = 16;
            this.run2.UseVisualStyleBackColor = true;
            // 
            // stop2
            // 
            this.stop2.FlatAppearance.BorderSize = 0;
            this.stop2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stop2.Location = new System.Drawing.Point(96, 19);
            this.stop2.Name = "stop2";
            this.stop2.Size = new System.Drawing.Size(80, 32);
            this.stop2.TabIndex = 15;
            this.stop2.UseVisualStyleBackColor = true;
            // 
            // labelbombilla2
            // 
            this.labelbombilla2.AutoSize = true;
            this.labelbombilla2.Location = new System.Drawing.Point(2, 16);
            this.labelbombilla2.Name = "labelbombilla2";
            this.labelbombilla2.Size = new System.Drawing.Size(35, 13);
            this.labelbombilla2.TabIndex = 39;
            this.labelbombilla2.Text = "label1";
            // 
            // picbombilla2
            // 
            this.picbombilla2.Location = new System.Drawing.Point(5, 26);
            this.picbombilla2.Name = "picbombilla2";
            this.picbombilla2.Size = new System.Drawing.Size(48, 48);
            this.picbombilla2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbombilla2.TabIndex = 38;
            this.picbombilla2.TabStop = false;
            // 
            // picparomarcha2
            // 
            this.picparomarcha2.Location = new System.Drawing.Point(39, 10);
            this.picparomarcha2.Name = "picparomarcha2";
            this.picparomarcha2.Size = new System.Drawing.Size(69, 84);
            this.picparomarcha2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picparomarcha2.TabIndex = 37;
            this.picparomarcha2.TabStop = false;
            // 
            // picB23
            // 
            this.picB23.Location = new System.Drawing.Point(114, 100);
            this.picB23.Name = "picB23";
            this.picB23.Size = new System.Drawing.Size(48, 48);
            this.picB23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB23.TabIndex = 36;
            this.picB23.TabStop = false;
            // 
            // picB22
            // 
            this.picB22.Location = new System.Drawing.Point(60, 100);
            this.picB22.Name = "picB22";
            this.picB22.Size = new System.Drawing.Size(48, 48);
            this.picB22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB22.TabIndex = 35;
            this.picB22.TabStop = false;
            // 
            // picB21
            // 
            this.picB21.Location = new System.Drawing.Point(6, 100);
            this.picB21.Name = "picB21";
            this.picB21.Size = new System.Drawing.Size(48, 48);
            this.picB21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB21.TabIndex = 34;
            this.picB21.TabStop = false;
            // 
            // refresh2
            // 
            this.refresh2.FlatAppearance.BorderSize = 0;
            this.refresh2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refresh2.Image = ((System.Drawing.Image)(resources.GetObject("refresh2.Image")));
            this.refresh2.Location = new System.Drawing.Point(1, 154);
            this.refresh2.Name = "refresh2";
            this.refresh2.Size = new System.Drawing.Size(80, 32);
            this.refresh2.TabIndex = 18;
            this.refresh2.UseVisualStyleBackColor = true;
            // 
            // jump2
            // 
            this.jump2.FlatAppearance.BorderSize = 0;
            this.jump2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jump2.Image = ((System.Drawing.Image)(resources.GetObject("jump2.Image")));
            this.jump2.Location = new System.Drawing.Point(87, 154);
            this.jump2.Name = "jump2";
            this.jump2.Size = new System.Drawing.Size(80, 32);
            this.jump2.TabIndex = 17;
            this.jump2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.subpanel1);
            this.panel1.Controls.Add(this.subpanel1b);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Location = new System.Drawing.Point(29, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(395, 471);
            this.panel1.TabIndex = 7;
            // 
            // subpanel1
            // 
            this.subpanel1.Controls.Add(this.picB13);
            this.subpanel1.Controls.Add(this.picB12);
            this.subpanel1.Controls.Add(this.picB11);
            this.subpanel1.Controls.Add(this.labelbombilla1);
            this.subpanel1.Controls.Add(this.run1);
            this.subpanel1.Controls.Add(this.stop1);
            this.subpanel1.Controls.Add(this.picbombilla1);
            this.subpanel1.Controls.Add(this.picparomarcha1);
            this.subpanel1.Controls.Add(this.jump1);
            this.subpanel1.Controls.Add(this.refresh1);
            this.subpanel1.Location = new System.Drawing.Point(214, 24);
            this.subpanel1.Name = "subpanel1";
            this.subpanel1.Size = new System.Drawing.Size(177, 197);
            this.subpanel1.TabIndex = 32;
            // 
            // picB13
            // 
            this.picB13.Location = new System.Drawing.Point(113, 100);
            this.picB13.Name = "picB13";
            this.picB13.Size = new System.Drawing.Size(48, 48);
            this.picB13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB13.TabIndex = 22;
            this.picB13.TabStop = false;
            // 
            // picB12
            // 
            this.picB12.Location = new System.Drawing.Point(59, 100);
            this.picB12.Name = "picB12";
            this.picB12.Size = new System.Drawing.Size(48, 48);
            this.picB12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB12.TabIndex = 21;
            this.picB12.TabStop = false;
            // 
            // picB11
            // 
            this.picB11.Location = new System.Drawing.Point(5, 100);
            this.picB11.Name = "picB11";
            this.picB11.Size = new System.Drawing.Size(48, 48);
            this.picB11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB11.TabIndex = 20;
            this.picB11.TabStop = false;
            // 
            // labelbombilla1
            // 
            this.labelbombilla1.AutoSize = true;
            this.labelbombilla1.Location = new System.Drawing.Point(2, 16);
            this.labelbombilla1.Name = "labelbombilla1";
            this.labelbombilla1.Size = new System.Drawing.Size(35, 13);
            this.labelbombilla1.TabIndex = 19;
            this.labelbombilla1.Text = "label1";
            // 
            // run1
            // 
            this.run1.FlatAppearance.BorderSize = 0;
            this.run1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.run1.Location = new System.Drawing.Point(95, 56);
            this.run1.Name = "run1";
            this.run1.Size = new System.Drawing.Size(80, 32);
            this.run1.TabIndex = 16;
            this.run1.UseVisualStyleBackColor = true;
            // 
            // stop1
            // 
            this.stop1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.stop1.FlatAppearance.BorderSize = 0;
            this.stop1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.stop1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.stop1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stop1.Location = new System.Drawing.Point(95, 19);
            this.stop1.Name = "stop1";
            this.stop1.Size = new System.Drawing.Size(80, 32);
            this.stop1.TabIndex = 15;
            this.stop1.UseVisualStyleBackColor = true;
            // 
            // picbombilla1
            // 
            this.picbombilla1.Location = new System.Drawing.Point(5, 26);
            this.picbombilla1.Name = "picbombilla1";
            this.picbombilla1.Size = new System.Drawing.Size(48, 48);
            this.picbombilla1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbombilla1.TabIndex = 1;
            this.picbombilla1.TabStop = false;
            // 
            // picparomarcha1
            // 
            this.picparomarcha1.Location = new System.Drawing.Point(39, 10);
            this.picparomarcha1.Name = "picparomarcha1";
            this.picparomarcha1.Size = new System.Drawing.Size(69, 84);
            this.picparomarcha1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picparomarcha1.TabIndex = 0;
            this.picparomarcha1.TabStop = false;
            // 
            // jump1
            // 
            this.jump1.FlatAppearance.BorderSize = 0;
            this.jump1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jump1.Image = ((System.Drawing.Image)(resources.GetObject("jump1.Image")));
            this.jump1.Location = new System.Drawing.Point(86, 154);
            this.jump1.Name = "jump1";
            this.jump1.Size = new System.Drawing.Size(80, 32);
            this.jump1.TabIndex = 17;
            this.jump1.UseVisualStyleBackColor = true;
            // 
            // refresh1
            // 
            this.refresh1.FlatAppearance.BorderSize = 0;
            this.refresh1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refresh1.Image = ((System.Drawing.Image)(resources.GetObject("refresh1.Image")));
            this.refresh1.Location = new System.Drawing.Point(3, 154);
            this.refresh1.Name = "refresh1";
            this.refresh1.Size = new System.Drawing.Size(80, 32);
            this.refresh1.TabIndex = 18;
            this.refresh1.UseVisualStyleBackColor = true;
            // 
            // subpanel1b
            // 
            this.subpanel1b.Controls.Add(this.label6);
            this.subpanel1b.Controls.Add(this.df1);
            this.subpanel1b.Controls.Add(this.FA_1);
            this.subpanel1b.Controls.Add(this.v1_1);
            this.subpanel1b.Controls.Add(this.FB_1);
            this.subpanel1b.Controls.Add(this.v1_2);
            this.subpanel1b.Controls.Add(this.v1_paso);
            this.subpanel1b.Controls.Add(this.v1_3);
            this.subpanel1b.Controls.Add(this.label39);
            this.subpanel1b.Controls.Add(this.b1_3);
            this.subpanel1b.Controls.Add(this.label7);
            this.subpanel1b.Controls.Add(this.b1_2);
            this.subpanel1b.Controls.Add(this.label8);
            this.subpanel1b.Controls.Add(this.b1_1);
            this.subpanel1b.Controls.Add(this.label9);
            this.subpanel1b.Controls.Add(this.label10);
            this.subpanel1b.Location = new System.Drawing.Point(4, 24);
            this.subpanel1b.Name = "subpanel1b";
            this.subpanel1b.Size = new System.Drawing.Size(209, 317);
            this.subpanel1b.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(5, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Detector";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // df1
            // 
            this.df1.Image = ((System.Drawing.Image)(resources.GetObject("df1.Image")));
            this.df1.Location = new System.Drawing.Point(37, 5);
            this.df1.Name = "df1";
            this.df1.Size = new System.Drawing.Size(96, 96);
            this.df1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.df1.TabIndex = 5;
            this.df1.TabStop = false;
            // 
            // FA_1
            // 
            this.FA_1.Image = ((System.Drawing.Image)(resources.GetObject("FA_1.Image")));
            this.FA_1.Location = new System.Drawing.Point(2, 100);
            this.FA_1.Name = "FA_1";
            this.FA_1.Size = new System.Drawing.Size(153, 63);
            this.FA_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FA_1.TabIndex = 30;
            this.FA_1.TabStop = false;
            // 
            // v1_1
            // 
            this.v1_1.Image = ((System.Drawing.Image)(resources.GetObject("v1_1.Image")));
            this.v1_1.Location = new System.Drawing.Point(2, 166);
            this.v1_1.Name = "v1_1";
            this.v1_1.Size = new System.Drawing.Size(45, 46);
            this.v1_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v1_1.TabIndex = 1;
            this.v1_1.TabStop = false;
            // 
            // FB_1
            // 
            this.FB_1.Location = new System.Drawing.Point(110, 63);
            this.FB_1.Name = "FB_1";
            this.FB_1.Size = new System.Drawing.Size(96, 100);
            this.FB_1.TabIndex = 29;
            this.FB_1.TabStop = false;
            // 
            // v1_2
            // 
            this.v1_2.Image = ((System.Drawing.Image)(resources.GetObject("v1_2.Image")));
            this.v1_2.Location = new System.Drawing.Point(56, 166);
            this.v1_2.Name = "v1_2";
            this.v1_2.Size = new System.Drawing.Size(45, 46);
            this.v1_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v1_2.TabIndex = 2;
            this.v1_2.TabStop = false;
            // 
            // v1_paso
            // 
            this.v1_paso.Image = ((System.Drawing.Image)(resources.GetObject("v1_paso.Image")));
            this.v1_paso.Location = new System.Drawing.Point(161, 166);
            this.v1_paso.Name = "v1_paso";
            this.v1_paso.Size = new System.Drawing.Size(45, 46);
            this.v1_paso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v1_paso.TabIndex = 4;
            this.v1_paso.TabStop = false;
            // 
            // v1_3
            // 
            this.v1_3.Image = ((System.Drawing.Image)(resources.GetObject("v1_3.Image")));
            this.v1_3.Location = new System.Drawing.Point(110, 166);
            this.v1_3.Name = "v1_3";
            this.v1_3.Size = new System.Drawing.Size(45, 46);
            this.v1_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v1_3.TabIndex = 3;
            this.v1_3.TabStop = false;
            // 
            // label39
            // 
            this.label39.BackColor = System.Drawing.Color.Black;
            this.label39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label39.Location = new System.Drawing.Point(4, 270);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(155, 2);
            this.label39.TabIndex = 27;
            // 
            // b1_3
            // 
            this.b1_3.AutoSize = true;
            this.b1_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b1_3.Location = new System.Drawing.Point(125, 273);
            this.b1_3.Name = "b1_3";
            this.b1_3.Size = new System.Drawing.Size(16, 18);
            this.b1_3.TabIndex = 26;
            this.b1_3.Text = "0";
            this.b1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(163, 215);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 36);
            this.label7.TabIndex = 11;
            this.label7.Text = "V5\r\nPaso";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // b1_2
            // 
            this.b1_2.AutoSize = true;
            this.b1_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b1_2.Location = new System.Drawing.Point(73, 273);
            this.b1_2.Name = "b1_2";
            this.b1_2.Size = new System.Drawing.Size(16, 18);
            this.b1_2.TabIndex = 25;
            this.b1_2.Text = "0";
            this.b1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(5, 215);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 54);
            this.label8.TabIndex = 12;
            this.label8.Text = "V1\r\nBarril\r\n1";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // b1_1
            // 
            this.b1_1.AutoSize = true;
            this.b1_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b1_1.Location = new System.Drawing.Point(19, 273);
            this.b1_1.Name = "b1_1";
            this.b1_1.Size = new System.Drawing.Size(16, 18);
            this.b1_1.TabIndex = 24;
            this.b1_1.Text = "0";
            this.b1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(59, 215);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 54);
            this.label9.TabIndex = 13;
            this.label9.Text = "V2\r\nBarril\r\n2";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(113, 215);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 54);
            this.label10.TabIndex = 14;
            this.label10.Text = "V3\r\nBarril\r\n3";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 18);
            this.label11.TabIndex = 19;
            this.label11.Text = "LINEA 1";
            // 
            // panelsubcab
            // 
            this.panelsubcab.Location = new System.Drawing.Point(654, 12);
            this.panelsubcab.Name = "panelsubcab";
            this.panelsubcab.Size = new System.Drawing.Size(618, 134);
            this.panelsubcab.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1284, 661);
            this.Controls.Add(this.panelsubcab);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.cabecera);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CONTROL DE BARRILES";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.cabecera.ResumeLayout(false);
            this.cabecera.PerformLayout();
            this.panelcabcentral.ResumeLayout(false);
            this.panelcabcentral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBoton)).EndInit();
            this.PanelConnect.ResumeLayout(false);
            this.PanelConnect.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureon)).EndInit();
            this.panel.ResumeLayout(false);
            this.panelHMI.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.subpanel3b.ResumeLayout(false);
            this.subpanel3b.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.df3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_paso)).EndInit();
            this.subpanel3.ResumeLayout(false);
            this.subpanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB33)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.subpanel2b.ResumeLayout(false);
            this.subpanel2b.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.df2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_paso)).EndInit();
            this.subpanel2.ResumeLayout(false);
            this.subpanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB21)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.subpanel1.ResumeLayout(false);
            this.subpanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picB13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha1)).EndInit();
            this.subpanel1b.ResumeLayout(false);
            this.subpanel1b.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.df1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_paso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel cabecera;
        private System.Windows.Forms.PictureBox pictureon;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel panelcabcentral;
        private System.Windows.Forms.Button off;
        private System.Windows.Forms.Panel PanelConnect;
        private System.Windows.Forms.Button on;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox slotText;
        private System.Windows.Forms.TextBox rackText;
        private System.Windows.Forms.TextBox ipText;
        private System.Windows.Forms.TextBox dnsText;
        private System.Windows.Forms.TextBox textip;
        private System.Windows.Forms.PictureBox pictureBoxBoton;
        private System.Windows.Forms.Label labelonoff;
        public System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Panel panelsubcab;
        private System.Windows.Forms.Panel panelHMI;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel subpanel3b;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.PictureBox df3;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.PictureBox FB_3;
        private System.Windows.Forms.PictureBox FA_3;
        private System.Windows.Forms.PictureBox v3_1;
        private System.Windows.Forms.Label b3_3;
        private System.Windows.Forms.PictureBox v3_2;
        private System.Windows.Forms.Label b3_2;
        private System.Windows.Forms.PictureBox v3_3;
        private System.Windows.Forms.Label b3_1;
        private System.Windows.Forms.PictureBox v3_paso;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel subpanel3;
        private System.Windows.Forms.Button run3;
        private System.Windows.Forms.Button stop3;
        private System.Windows.Forms.Label labelbombilla3;
        private System.Windows.Forms.PictureBox picbombilla3;
        private System.Windows.Forms.PictureBox picparomarcha3;
        private System.Windows.Forms.PictureBox picB31;
        private System.Windows.Forms.PictureBox picB32;
        private System.Windows.Forms.PictureBox picB33;
        private System.Windows.Forms.Button jump3;
        private System.Windows.Forms.Button refresh3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel subpanel2b;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox df2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.PictureBox FB_2;
        private System.Windows.Forms.PictureBox FA_2;
        private System.Windows.Forms.PictureBox v2_1;
        private System.Windows.Forms.Label b2_3;
        private System.Windows.Forms.PictureBox v2_2;
        private System.Windows.Forms.Label b2_2;
        private System.Windows.Forms.PictureBox v2_3;
        private System.Windows.Forms.Label b2_1;
        private System.Windows.Forms.PictureBox v2_paso;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel subpanel2;
        private System.Windows.Forms.Button run2;
        private System.Windows.Forms.Button stop2;
        private System.Windows.Forms.Label labelbombilla2;
        private System.Windows.Forms.PictureBox picbombilla2;
        private System.Windows.Forms.PictureBox picparomarcha2;
        private System.Windows.Forms.PictureBox picB23;
        private System.Windows.Forms.PictureBox picB22;
        private System.Windows.Forms.PictureBox picB21;
        private System.Windows.Forms.Button refresh2;
        private System.Windows.Forms.Button jump2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel subpanel1;
        private System.Windows.Forms.PictureBox picB13;
        private System.Windows.Forms.PictureBox picB12;
        private System.Windows.Forms.PictureBox picB11;
        private System.Windows.Forms.Label labelbombilla1;
        private System.Windows.Forms.Button run1;
        private System.Windows.Forms.Button stop1;
        private System.Windows.Forms.PictureBox picbombilla1;
        private System.Windows.Forms.PictureBox picparomarcha1;
        private System.Windows.Forms.Button jump1;
        private System.Windows.Forms.Button refresh1;
        private System.Windows.Forms.Panel subpanel1b;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox df1;
        private System.Windows.Forms.PictureBox FA_1;
        private System.Windows.Forms.PictureBox v1_1;
        private System.Windows.Forms.PictureBox FB_1;
        private System.Windows.Forms.PictureBox v1_2;
        private System.Windows.Forms.PictureBox v1_paso;
        private System.Windows.Forms.PictureBox v1_3;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label b1_3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label b1_2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label b1_1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}

