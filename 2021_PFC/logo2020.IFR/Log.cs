﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.IFR
{
    /// <summary>
    /// Clase para registrar loggs
    /// </summary>
    public static class Log
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
        (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Escribe el error en el log
        /// </summary>
        /// <param name="message">texto del mensaje</param>
        /// <param name="ex">excepción producida</param>
        public static void LogError(string message, Exception ex = null)
        {
            log.Error(message, ex);
        }

        /// <summary>
        /// Escribe la advertencia en el log
        /// </summary>
        /// <param name="message">texto del mensaje</param>
        /// <param name="ex">excepción producida</param>
        public static void LogWarn(string message, Exception ex = null)
        {
            log.Warn(message, ex);
        }

        /// <summary>
        /// Escribe la información en el log
        /// </summary>
        /// <param name="message">texto del mensaje</param>
        public static void LogInfo(string message)
        {
            log.Info(message);
        }
    }
}
