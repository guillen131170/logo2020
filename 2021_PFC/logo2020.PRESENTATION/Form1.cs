﻿using log4net;
using logo2020.APPLICATION;
using logo2020.CORE;
using logo2020.IFR;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace logo2020.PRESENTATION
{
    #region PANTALLA HMI
    /// <summary>
    /// FORM QUE REPRESENTA LA PANTALLA HMI
    /// </summary>
    public partial class Form1 : Form
    {
        /*################################################################*/
        #region ATRIBUTOS DE LA CLASE
        private bool conectadoLogo;
        private bool conectadoBD;
        public ProcesosConLogo procesosconlogo;
        log4net.ILog log;
        public Hilos hilo;

        #region CONTADORES PARA TIMER DE RED
        static bool T_RED = false;
        #endregion
        #region CONTADORES PARA TIMER DE AVANCE 1,2 Y 3
        static int T_ESPERA_jUMP = 1;
        int i_jump1 = 0;
        int i_jump2 = 0;
        int i_jump3 = 0;
        #endregion
        #region CONTADORES PARA TIMER DE REPONER 1,2 Y 3
        static int T_ESPERA_REFRESH = 1;
        int i_refresh1 = 0;
        int i_refresh2 = 0;
        int i_refresh3 = 0;
        #endregion
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region CARGAR COMPONENTES Y MOSTRAR PANTALLA PRINCIPAL       
        #region INICIA PANTALLA PRINCIPAL
        /// <summary>
        /// INICIO
        /// </summary>
        public Form1()
        {
            InitializeComponent();
        }
        #endregion

        #region INICIA COMPONENTES DE PANTALLA PRINCIPAL
        /// <summary>
        /// Carga los componentes de la ventana principal
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            // COMPRUEBA SI HAY BASE DE DATOS
            ProcesosConSQL sql = new ProcesosConSQL();
            if (sql.verificar()) conectadoBD = true;
            else conectadoBD = false;

            // CONFIGURA LOG4NET
            log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

            progressBar1.Style = ProgressBarStyle.Blocks;
            conectadoLogo = false;
            panelHMI.Visible = false;
            panelsubcab.Visible = false;
            off.Enabled = false;
            textip.Visible = false;
            textpdu.Visible = false;
            pictureon.Image = Properties.Resources.plcoff;
            if (wifi())
            {
                pictureBoxBoton.Image = Properties.Resources.SIGNAL2;
            }
            else
            {
                pictureBoxBoton.Image = Properties.Resources.SIGNAL3_2;
            }
            string iplocal = GetUserIP();
            if (iplocal != null)
            {
                iptextlocal.Text = iplocal;
            }
            //tipodato.SelectedIndex = 0;
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            checkBox3.Checked = false;
            checkBox1.Image = Properties.Resources.int_offoff;
            checkBox2.Image = Properties.Resources.int_offoff;
            checkBox3.Image = Properties.Resources.int_offoff;
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = false;
        }
        #endregion
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region CONECTAR/DESCONECTAR LOGO      
        #region CONECTAR
        /// <summary>
        /// Conectar con Logo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void on_Click(object sender, EventArgs e)
        {
            #region COMPRUENA CAMPOS DNS - IP
            /* Comprueba que los campos no están en blanco */
            if (dnsText.Text.Equals("") && ipText.Text.Equals(""))
            {
                MessageBox.Show("Rellene los campos de forma correcta", "Advertencia",
                                      MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            #endregion

            #region CREA UN OBJETO PARA OPERAR CON LOGO
            /*Crea un objeto de plc*/
            procesosconlogo = new ProcesosConLogo(dnsText.Text, ipText.Text);
            #endregion

            #region BUSCA EL LOGO
            #region BUSCA CON DNS
            if (!dnsText.Text.Equals(""))
            {
                try
                {
                    procesosconlogo.connectPlcDNS();
                }
                catch (Exception ex)
                {
                    if (conectadoBD)
                        Log.LogWarn("ERROR AL ESTABLECER  COMUNICACIÓN CON LOGO EN " + procesosconlogo.Logo.DNS + " - " + ex.Message);
                }

                if (!procesosconlogo.EstaConectado() && !ipText.Text.Equals(""))
                {
                    try
                    {
                        procesosconlogo.connectPlcIP();
                    }
                    catch (Exception ex)
                    {
                        if (conectadoBD)
                            Log.LogWarn("ERROR AL ESTABLECER  COMUNICACIÓN CON LOGO EN " + procesosconlogo.Logo.DNS + " - " + ex.Message);
                    }
                }
            }
            #endregion
            #region BUSCA CON IP
            else
            {
                if (!ipText.Text.Equals(""))
                {
                    try
                    {
                        procesosconlogo.connectPlcIP();
                    }
                    catch (Exception ex)
                    {
                        if (conectadoBD)
                            Log.LogWarn("ERROR AL ESTABLECER  COMUNICACIÓN CON LOGO EN " + procesosconlogo.Logo.IP + " - " + ex.Message);
                    }
                }
            }
            #endregion
            #endregion

            #region RESULTADO DE LA BÚSQUEDA DE UN LOGO
            #region OK - LOGO ENCONTRADO
            if (procesosconlogo.EstaConectado())
            {
                try
                {
                    checkBox1.Image = Properties.Resources.int_off;
                    checkBox2.Image = Properties.Resources.int_off;
                    checkBox3.Image = Properties.Resources.int_off;
                    //hora = minuto = segundo = 0;
                    //timer1.Start();
                    Q4.Image = Properties.Resources.LUZ_AMARILLA;
                    Q14.Image = Properties.Resources.LUZ_AMARILLA;
                    Q24.Image = Properties.Resources.LUZ_AMARILLA;
                    progressBar1.Style = ProgressBarStyle.Marquee;
                    conectadoLogo = true;
                    panelHMI.Visible = true;
                    //PanelConnect.Enabled = false;
                    panelsubcab.Visible = true;
                    ipText.Enabled = false;
                    dnsText.Enabled = false;
                    rackText.Enabled = false;
                    slotText.Enabled = false;
                    pictureon.Image = Properties.Resources.plcon;
                    off.Enabled = true;
                    on.Enabled = false;
                    textip.Visible = true;
                    textip.Text = procesosconlogo.Logo.IP;
                    textpdu.Visible = true;
                    pictureBoxBoton.Image = Properties.Resources.SIGNAL2_1;
                    //picturepc.Image = Properties.Resources.Laptop_ON;
                    labelonoff.Text = "ON";
                    labelonoff.ForeColor = Color.Green;
                    hilo = new Hilos(procesosconlogo, this, ref conectadoLogo, ref conectadoBD, textDump);
                    if (conectadoBD)
                        log.Info("SE HA ESTABLECIDO COMUNICACIÓN CON LOGO EN " + procesosconlogo.Logo.IP + "" + procesosconlogo.Logo.DNS);
                    escribirDisplayB("SE HA ESTABLECIDO COMUNICACIÓN CON LOGO EN " + procesosconlogo.Logo.IP + "" + procesosconlogo.Logo.DNS);
                    T_RED = true;
                    timered.Start();
                }
                catch (Exception ex)
                {
                    if (conectadoBD)
                        Log.LogWarn("ERROR AL DIBUJAR HMI" + ex.Message);
                }
            }
            #endregion
            #region ERROR - LOGO NO EXISTE
            else
            {
                if (conectadoBD)
                    log.Info("NO SE HA PODIDO ESTABLECER COMUNICACIÓN CON LOGO EN " + 
                    procesosconlogo.Logo.IP + "" + procesosconlogo.Logo.DNS);
                MessageBox.Show("NO SE HA PODIDO ESTABLECER COMUNICACIÓN CON LOGO EN " + 
                    procesosconlogo.Logo.IP + "" + procesosconlogo.Logo.DNS);
            }
            #endregion
            #endregion
        }
        #endregion

        #region OBTIENE LA DIRECCIÓN IP LOCAL
        private string GetUserIP()
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                return null;
            }
            else
            {
                try
                {
                    var host = Dns.GetHostEntry(Dns.GetHostName());
                    foreach (var ip in host.AddressList)
                    {
                        if (ip.AddressFamily == AddressFamily.InterNetwork)
                        {
                            return ip.ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    escribirDisplayB("No network adapters with an IPv4 address in the system! " + ex);
                }
                return null;
            }
        } 
        #endregion

        #region DESCONECTAR
        /// <summary>
        /// Desconecta el Logo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void off_Click(object sender, EventArgs e)
        {
            try
            {
                checkBox1.Checked = false;
                checkBox2.Checked = false;
                checkBox3.Checked = false;
                checkBox1.Image = Properties.Resources.int_offoff;
                checkBox2.Image = Properties.Resources.int_offoff;
                checkBox3.Image = Properties.Resources.int_offoff;
                panel1.Visible = false;
                panel2.Visible = false;
                panel3.Visible = false;
                hilo.AbortaHilo();
                T_RED = false;
                Q1.Image = Properties.Resources.LUZ_ROJA_OFF;
                Q2.Image = Properties.Resources.LUZ_ROJA_OFF;
                Q3.Image = Properties.Resources.LUZ_ROJA_OFF;
                Q4.Image = Properties.Resources.LUZ_ROJA_OFF;
                Q5.Image = Properties.Resources.LUZ_ROJA_OFF;
                Q11.Image = Properties.Resources.LUZ_ROJA_OFF;
                Q12.Image = Properties.Resources.LUZ_ROJA_OFF;
                Q13.Image = Properties.Resources.LUZ_ROJA_OFF;
                Q14.Image = Properties.Resources.LUZ_ROJA_OFF;
                Q15.Image = Properties.Resources.LUZ_ROJA_OFF;
                Q21.Image = Properties.Resources.LUZ_ROJA_OFF;
                Q22.Image = Properties.Resources.LUZ_ROJA_OFF;
                Q23.Image = Properties.Resources.LUZ_ROJA_OFF;
                Q24.Image = Properties.Resources.LUZ_ROJA_OFF;
                Q25.Image = Properties.Resources.LUZ_ROJA_OFF;
                textip.Text = "";
                textip.Visible = false;
                textpdu.Visible = false;
                procesosconlogo.disconnectPLc();
                progressBar1.Style = ProgressBarStyle.Blocks;
                conectadoLogo = false;
                panelHMI.Visible = false;
                ipText.Enabled = true;
                dnsText.Enabled = true;
                rackText.Enabled = true;
                slotText.Enabled = true;
                panelsubcab.Visible = false;
                pictureon.Image = Properties.Resources.plcoff;
                off.Enabled = false;
                on.Enabled = true;
                if (wifi())
                {
                    pictureBoxBoton.Image = Properties.Resources.SIGNAL2;
                }
                else
                {
                    pictureBoxBoton.Image = Properties.Resources.SIGNAL3_2;
                }
                labelonoff.Text = "OFF";
                labelonoff.ForeColor = Color.Black;
                if (conectadoBD)
                    log.Info("SE HA CERRADO LA COMUNICACIÓN CON LOGO EN " +
                    procesosconlogo.Logo.IP + "" + procesosconlogo.Logo.DNS);
                escribirDisplayB("SE HA CERRADO LA COMUNICACIÓN CON LOGO EN " +
                    procesosconlogo.Logo.IP + "" + procesosconlogo.Logo.DNS);
            }
            catch (Exception ex)
            {
                if (conectadoBD)
                    Log.LogWarn("ERROR AL CERRAR LA COMUNICACIÓN CON LOGO: off_Click(object sender, EventArgs e)" + ex.Message);
            }
        }
        #endregion
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region /*DIBUJA PANTALLA HDMI*/
        #region ELECTROVÁLVULAS 1-2-3: ESTOS MÉTODOS SON LLAMADOS DESDE EL HILO DE LA CONEXIÓN
        #region LÍNEA DE BARRILES NÚMERO 1 PARA HILO: ELECTROVÁLVULAS 1-2-3 
        /// <summary>
        /// Esta función comprueba y dibuja las electrovávulas 1, 2 y 3 de
        /// la línea de barriles número 1
        /// </summary>
        public void dibujaEVA123_1()
        {
            int barril_activo;
            int barril_contenido;

            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    if (procesosconlogo.EstaConectado())
                    {
                        #region ELECTROVÁLVULA 1, 2 Y 3 DE LINEA NÚMERO 1
                        barril_activo = procesosconlogo.activo(Constantes.BARRILACTIVO_LINEA1_BYTE_1);
                        switch (barril_activo)
                        {
                            #region BARRIL NÚMERO 1
                            case 1:
                                v1_1.Image = Properties.Resources.valveon;
                                v1_2.Image = Properties.Resources.valveoff;
                                v1_3.Image = Properties.Resources.valveoff;
                                Q1.Image = Properties.Resources.LUZ_VERDE_ON;
                                Q2.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q3.Image = Properties.Resources.LUZ_ROJA_OFF;
                                {
                                    FA_1.Image = Properties.Resources.fondoA1;
                                }

                                /*comprueba contenido de barriles*/
                                #region comprueba contenido de barriles
                                picB11.Image = Properties.Resources.green;
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_2);
                                if (barril_contenido == 0)
                                {
                                    picB12.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB12.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_3);
                                if (barril_contenido == 0)
                                {
                                    picB13.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB13.Image = null;
                                }
                                #endregion

                                break;
                            #endregion

                            #region BARRIL NÚMERO 2
                            case 2:
                                v1_1.Image = Properties.Resources.valveoff;
                                v1_2.Image = Properties.Resources.valveon;
                                v1_3.Image = Properties.Resources.valveoff;
                                Q1.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q2.Image = Properties.Resources.LUZ_VERDE_ON;
                                Q3.Image = Properties.Resources.LUZ_ROJA_OFF;
                                {
                                    FA_1.Image = Properties.Resources.fondoA2;
                                }

                                /*comprueba contenido de barriles*/
                                #region comprueba contenido de barriles
                                picB12.Image = Properties.Resources.green;
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_1);
                                if (barril_contenido == 0)
                                {
                                    picB11.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB11.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_3);
                                if (barril_contenido == 0)
                                {
                                    picB13.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB13.Image = null;
                                }
                                #endregion

                                break;
                            #endregion

                            #region BARRIL NÚMERO 3
                            case 3:
                                v1_1.Image = Properties.Resources.valveoff;
                                v1_2.Image = Properties.Resources.valveoff;
                                v1_3.Image = Properties.Resources.valveon;
                                Q1.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q2.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q3.Image = Properties.Resources.LUZ_VERDE_ON;
                                {
                                    FA_1.Image = Properties.Resources.fondoA3;
                                }

                                /*comprueba contenido de barriles*/
                                #region comprueba contenido de barriles
                                picB13.Image = Properties.Resources.green;
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_1);
                                if (barril_contenido == 0)
                                {
                                    picB11.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB11.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_2);
                                if (barril_contenido == 0)
                                {
                                    picB12.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB12.Image = null;
                                }
                                #endregion

                                break;
                            #endregion

                            #region POR DEFECTO
                            default:
                                v1_1.Image = Properties.Resources.valveoff;
                                v1_2.Image = Properties.Resources.valveoff;
                                v1_3.Image = Properties.Resources.valveoff;
                                FA_1.Image = Properties.Resources.fondoA4;
                                Q1.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q2.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q3.Image = Properties.Resources.LUZ_ROJA_OFF;
                                /*comprueba contenido de barriles*/
                                #region comprueba contenido de barriles
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_1);
                                if (barril_contenido == 0)
                                {
                                    picB11.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB11.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_2);
                                if (barril_contenido == 0)
                                {
                                    picB12.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB12.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_3);
                                if (barril_contenido == 0)
                                {
                                    picB13.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB13.Image = null;
                                }
                                #endregion
                                break;
                                #endregion
                        }

                        #region comprueba contenido de barriles
                        int contador = 0;
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_1);
                        if (barril_contenido == 0) { contador++; }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_2);
                        if (barril_contenido == 0) { contador++; }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_3);
                        if (barril_contenido == 0) { contador++; }
                        #endregion
                        if ((contador > 1) && (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA5_LINEA1_BYTE) == 0)) jump1.Enabled = true;
                        else jump1.Enabled = false;
                        #endregion
                    }
                }));
            }
        }
        #endregion

        #region LÍNEA DE BARRILES NÚMERO 2 PARA HILO: ELECTROVÁLVULAS 1-2-3 
        /// <summary>
        /// Esta función comprueba y dibuja las electrovávulas 1, 2 y 3 de
        /// la línea de barriles número 2
        /// </summary>
        public void dibujaEVA123_2()
        {
            int barril_activo;
            int barril_contenido;

            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    if (procesosconlogo.EstaConectado())
                    {
                        #region ELECTROVÁLVULA 1, 2 y 3  LINEA NÚMERO 2
                        barril_activo = procesosconlogo.activo(Constantes.BARRILACTIVO_LINEA2_BYTE_1);
                        switch (barril_activo)
                        {
                            #region BARRIL NÚMERO 1
                            case 1:
                                v2_1.Image = Properties.Resources.valveon;
                                v2_2.Image = Properties.Resources.valveoff;
                                v2_3.Image = Properties.Resources.valveoff;
                                Q11.Image = Properties.Resources.LUZ_VERDE_ON;
                                Q12.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q13.Image = Properties.Resources.LUZ_ROJA_OFF;
                                {
                                    FA_2.Image = Properties.Resources.fondoA1;
                                }

                                /*comprueba contenido de barriles*/
                                #region comprueba contenido de barriles
                                picB21.Image = Properties.Resources.green;
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_2);
                                if (barril_contenido == 0)
                                {
                                    picB22.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB22.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_3);
                                if (barril_contenido == 0)
                                {
                                    picB23.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB23.Image = null;
                                }
                                #endregion

                                break;
                            #endregion

                            #region BARRIL NÚMERO 2
                            case 2:
                                v2_1.Image = Properties.Resources.valveoff;
                                v2_2.Image = Properties.Resources.valveon;
                                v2_3.Image = Properties.Resources.valveoff;
                                Q11.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q12.Image = Properties.Resources.LUZ_VERDE_ON;
                                Q13.Image = Properties.Resources.LUZ_ROJA_OFF;
                                {
                                    FA_2.Image = Properties.Resources.fondoA2;
                                }

                                /*comprueba contenido de barriles*/
                                #region comprueba contenido de barriles
                                picB22.Image = Properties.Resources.green;
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_1);
                                if (barril_contenido == 0)
                                {
                                    picB21.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB21.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_3);
                                if (barril_contenido == 0)
                                {
                                    picB23.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB23.Image = null;
                                }
                                #endregion

                                break;
                            #endregion

                            #region BARRIL NÚMERO 3
                            case 3:
                                v2_1.Image = Properties.Resources.valveoff;
                                v2_2.Image = Properties.Resources.valveoff;
                                v2_3.Image = Properties.Resources.valveon;
                                Q11.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q12.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q13.Image = Properties.Resources.LUZ_VERDE_ON;
                                {
                                    FA_2.Image = Properties.Resources.fondoA3;
                                }

                                /*comprueba contenido de barriles*/
                                #region comprueba contenido de barriles
                                picB23.Image = Properties.Resources.green;
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_1);
                                if (barril_contenido == 0)
                                {
                                    picB21.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB21.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_2);
                                if (barril_contenido == 0)
                                {
                                    picB22.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB22.Image = null;
                                }
                                #endregion

                                break;
                            #endregion

                            #region POR DEFECTO
                            default:
                                v2_1.Image = Properties.Resources.valveoff;
                                v2_2.Image = Properties.Resources.valveoff;
                                v2_3.Image = Properties.Resources.valveoff;
                                FA_2.Image = Properties.Resources.fondoA4;
                                Q11.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q12.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q13.Image = Properties.Resources.LUZ_ROJA_OFF;
                                /*comprueba contenido de barriles*/
                                #region comprueba contenido de barriles
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_1);
                                if (barril_contenido == 0)
                                {
                                    picB21.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB21.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_2);
                                if (barril_contenido == 0)
                                {
                                    picB22.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB22.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_3);
                                if (barril_contenido == 0)
                                {
                                    picB23.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB23.Image = null;
                                }
                                #endregion

                                break;
                                #endregion
                        }
                        #region comprueba contenido de barriles
                        int contador = 0;
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_1);
                        if (barril_contenido == 0) { contador++; }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_2);
                        if (barril_contenido == 0) { contador++; }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_3);
                        if (barril_contenido == 0) { contador++; }
                        #endregion
                        if ((contador > 1) && (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA5_LINEA1_BYTE) == 0)) jump2.Enabled = true;
                        else jump2.Enabled = false;
                        #endregion
                    }
                }));
            }
        }
        #endregion

        #region LÍNEA DE BARRILES NÚMERO 3 PARA HILO: ELECTROVÁLVULAS 1-2-3 
        public void dibujaEVA123_3()
        {
            int barril_activo;
            int barril_contenido;

            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    if (procesosconlogo.EstaConectado())
                    {
                        #region ELECTROVÁLVULA 1, 2 y 3  LINEA NÚMERO 3
                        barril_activo = procesosconlogo.activo(Constantes.BARRILACTIVO_LINEA3_BYTE_1);
                        switch (barril_activo)
                        {
                            #region BARRIL NÚMERO 1
                            case 1:
                                v3_1.Image = Properties.Resources.valveon;
                                v3_2.Image = Properties.Resources.valveoff;
                                v3_3.Image = Properties.Resources.valveoff;
                                Q21.Image = Properties.Resources.LUZ_VERDE_ON;
                                Q22.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q23.Image = Properties.Resources.LUZ_ROJA_OFF;
                                {
                                    FA_3.Image = Properties.Resources.fondoA1;
                                }

                                /*comprueba contenido de barriles*/
                                #region comprueba contenido de barriles
                                picB31.Image = Properties.Resources.green;
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_2);
                                if (barril_contenido == 0)
                                {
                                    picB32.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB32.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_3);
                                if (barril_contenido == 0)
                                {
                                    picB33.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB33.Image = null;
                                }
                                #endregion

                                break;
                            #endregion

                            #region BARRIL NÚMERO 2
                            case 2:
                                v3_1.Image = Properties.Resources.valveoff;
                                v3_2.Image = Properties.Resources.valveon;
                                v3_3.Image = Properties.Resources.valveoff;
                                Q21.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q22.Image = Properties.Resources.LUZ_VERDE_ON;
                                Q23.Image = Properties.Resources.LUZ_ROJA_OFF;
                                {
                                    FA_3.Image = Properties.Resources.fondoA2;
                                }

                                /*comprueba contenido de barriles*/
                                #region comprueba contenido de barriles
                                picB32.Image = Properties.Resources.green;
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_1);
                                if (barril_contenido == 0)
                                {
                                    picB31.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB31.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_3);
                                if (barril_contenido == 0)
                                {
                                    picB33.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB33.Image = null;
                                }
                                #endregion

                                break;
                            #endregion

                            #region BARRIL NÚMERO 3
                            case 3:
                                v3_1.Image = Properties.Resources.valveoff;
                                v3_2.Image = Properties.Resources.valveoff;
                                v3_3.Image = Properties.Resources.valveon;
                                Q21.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q22.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q23.Image = Properties.Resources.LUZ_VERDE_ON;
                                {
                                    FA_3.Image = Properties.Resources.fondoA3;
                                }

                                /*comprueba contenido de barriles*/
                                #region comprueba contenido de barriles
                                picB33.Image = Properties.Resources.green;
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_1);
                                if (barril_contenido == 0)
                                {
                                    picB31.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB31.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_2);
                                if (barril_contenido == 0)
                                {
                                    picB32.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB32.Image = null;
                                }
                                #endregion

                                break;
                            #endregion

                            #region POR DEFECTO
                            default:
                                v3_1.Image = Properties.Resources.valveoff;
                                v3_2.Image = Properties.Resources.valveoff;
                                v3_3.Image = Properties.Resources.valveoff;
                                FA_3.Image = Properties.Resources.fondoA4;
                                Q21.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q22.Image = Properties.Resources.LUZ_ROJA_OFF;
                                Q23.Image = Properties.Resources.LUZ_ROJA_OFF;
                                /*comprueba contenido de barriles*/
                                #region comprueba contenido de barriles
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_1);
                                if (barril_contenido == 0)
                                {
                                    picB31.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB31.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_2);
                                if (barril_contenido == 0)
                                {
                                    picB32.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB32.Image = null;
                                }
                                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_3);
                                if (barril_contenido == 0)
                                {
                                    picB33.Image = Properties.Resources.blue;
                                }
                                else
                                {
                                    picB33.Image = null;
                                }
                                #endregion

                                break;
                                #endregion
                        }
                        #region comprueba contenido de barriles
                        int contador = 0;
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_1);
                        if (barril_contenido == 0) { contador++; }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_2);
                        if (barril_contenido == 0) { contador++; }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_3);
                        if (barril_contenido == 0) { contador++; }
                        #endregion
                        if ((contador > 1) && (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA5_LINEA1_BYTE) == 0)) jump3.Enabled = true;
                        else jump3.Enabled = false;
                        #endregion
                    }
                }));
            }
        }
        #endregion
        #endregion

        #region ELECTROVÁLVULAS 1-2-3: ESTOS MÉTODOS SON LLAMADOS DESDE EL FORMULARIO
        #region LÍNEA DE BARRILES NÚMERO 1B PARA FORMULARIO: ELECTROVÁLVULAS 1-2-3 
        /// <summary>
        /// Esta función comprueba y dibuja las electrovávulas 1, 2 y 3 de
        /// la línea de barriles número 1
        /// </summary>
        public void dibujaEVA123_1B()
        {
            int barril_activo;
            int barril_contenido;

            if (procesosconlogo.EstaConectado())
            {
                #region ELECTROVÁLVULA 1, 2 Y 3 DE LINEA NÚMERO 1
                barril_activo = procesosconlogo.activo(Constantes.BARRILACTIVO_LINEA1_BYTE_1);
                switch (barril_activo)
                {
                    #region BARRIL NÚMERO 1
                    case 1:
                        v1_1.Image = Properties.Resources.valveon;
                        v1_2.Image = Properties.Resources.valveoff;
                        v1_3.Image = Properties.Resources.valveoff;
                        Q1.Image = Properties.Resources.LUZ_VERDE_ON;
                        Q2.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q3.Image = Properties.Resources.LUZ_ROJA_OFF;
                        {
                            FA_1.Image = Properties.Resources.fondoA1;
                        }

                        /*comprueba contenido de barriles*/
                        #region comprueba contenido de barriles
                        picB11.Image = Properties.Resources.green;
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_2);
                        if (barril_contenido == 0)
                        {
                            picB12.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB12.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_3);
                        if (barril_contenido == 0)
                        {
                            picB13.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB13.Image = null;
                        }
                        #endregion

                        break;
                    #endregion

                    #region BARRIL NÚMERO 2
                    case 2:
                        v1_1.Image = Properties.Resources.valveoff;
                        v1_2.Image = Properties.Resources.valveon;
                        v1_3.Image = Properties.Resources.valveoff;
                        Q1.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q2.Image = Properties.Resources.LUZ_VERDE_ON;
                        Q3.Image = Properties.Resources.LUZ_ROJA_OFF;
                        {
                            FA_1.Image = Properties.Resources.fondoA2;
                        }

                        /*comprueba contenido de barriles*/
                        #region comprueba contenido de barriles
                        picB12.Image = Properties.Resources.green;
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_1);
                        if (barril_contenido == 0)
                        {
                            picB11.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB11.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_3);
                        if (barril_contenido == 0)
                        {
                            picB13.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB13.Image = null;
                        }
                        #endregion

                        break;
                    #endregion

                    #region BARRIL NÚMERO 3
                    case 3:
                        v1_1.Image = Properties.Resources.valveoff;
                        v1_2.Image = Properties.Resources.valveoff;
                        v1_3.Image = Properties.Resources.valveon;
                        Q1.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q2.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q3.Image = Properties.Resources.LUZ_VERDE_ON;
                        {
                            FA_1.Image = Properties.Resources.fondoA3;
                        }

                        /*comprueba contenido de barriles*/
                        #region comprueba contenido de barriles
                        picB13.Image = Properties.Resources.green;
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_1);
                        if (barril_contenido == 0)
                        {
                            picB11.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB11.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_2);
                        if (barril_contenido == 0)
                        {
                            picB12.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB12.Image = null;
                        }
                        #endregion

                        break;
                    #endregion

                    #region POR DEFECTO
                    default:
                        v1_1.Image = Properties.Resources.valveoff;
                        v1_2.Image = Properties.Resources.valveoff;
                        v1_3.Image = Properties.Resources.valveoff;
                        FA_1.Image = Properties.Resources.fondoA4;
                        Q1.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q2.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q3.Image = Properties.Resources.LUZ_ROJA_OFF;
                        /*comprueba contenido de barriles*/
                        #region comprueba contenido de barriles
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_1);
                        if (barril_contenido == 0)
                        {
                            picB11.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB11.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_2);
                        if (barril_contenido == 0)
                        {
                            picB12.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB12.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_3);
                        if (barril_contenido == 0)
                        {
                            picB13.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB13.Image = null;
                        }
                        #endregion

                        break;
                        #endregion
                }

                #region comprueba contenido de barriles
                int contador = 0;
                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_1);
                if (barril_contenido == 0) { contador++; }
                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_2);
                if (barril_contenido == 0) { contador++; }
                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_3);
                if (barril_contenido == 0) { contador++; }
                #endregion
                if ((contador > 1) && (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA5_LINEA1_BYTE) == 0)) jump1.Enabled = true;
                else jump1.Enabled = false;
                #endregion
            }
        }
        #endregion

        #region LÍNEA DE BARRILES NÚMERO 2B PARA FORMULARIO: ELECTROVÁLVULAS 1-2-3 
        /// <summary>
        /// Esta función comprueba y dibuja las electrovávulas 1, 2 y 3 de
        /// la línea de barriles número 2
        /// </summary>
        public void dibujaEVA123_2B()
        {
            int barril_activo;
            int barril_contenido;

            if (procesosconlogo.EstaConectado())
            {
                #region ELECTROVÁLVULA 1, 2 y 3  LINEA NÚMERO 2
                barril_activo = procesosconlogo.activo(Constantes.BARRILACTIVO_LINEA2_BYTE_1);
                switch (barril_activo)
                {
                    #region BARRIL NÚMERO 1
                    case 1:
                        v2_1.Image = Properties.Resources.valveon;
                        v2_2.Image = Properties.Resources.valveoff;
                        v2_3.Image = Properties.Resources.valveoff;
                        Q11.Image = Properties.Resources.LUZ_VERDE_ON;
                        Q12.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q13.Image = Properties.Resources.LUZ_ROJA_OFF;
                        {
                            FA_2.Image = Properties.Resources.fondoA1;
                        }

                        /*comprueba contenido de barriles*/
                        #region comprueba contenido de barriles
                        picB21.Image = Properties.Resources.green;
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_2);
                        if (barril_contenido == 0)
                        {
                            picB22.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB22.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_3);
                        if (barril_contenido == 0)
                        {
                            picB23.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB23.Image = null;
                        }
                        #endregion

                        break;
                    #endregion

                    #region BARRIL NÚMERO 2
                    case 2:
                        v2_1.Image = Properties.Resources.valveoff;
                        v2_2.Image = Properties.Resources.valveon;
                        v2_3.Image = Properties.Resources.valveoff;
                        Q11.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q12.Image = Properties.Resources.LUZ_VERDE_ON;
                        Q13.Image = Properties.Resources.LUZ_ROJA_OFF;
                        {
                            FA_2.Image = Properties.Resources.fondoA2;
                        }

                        /*comprueba contenido de barriles*/
                        #region comprueba contenido de barriles
                        picB22.Image = Properties.Resources.green;
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_1);
                        if (barril_contenido == 0)
                        {
                            picB21.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB21.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_3);
                        if (barril_contenido == 0)
                        {
                            picB23.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB23.Image = null;
                        }
                        #endregion

                        break;
                    #endregion

                    #region BARRIL NÚMERO 3
                    case 3:
                        v2_1.Image = Properties.Resources.valveoff;
                        v2_2.Image = Properties.Resources.valveoff;
                        v2_3.Image = Properties.Resources.valveon;
                        Q11.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q12.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q13.Image = Properties.Resources.LUZ_VERDE_ON;
                        {
                            FA_2.Image = Properties.Resources.fondoA3;
                        }

                        /*comprueba contenido de barriles*/
                        #region comprueba contenido de barriles
                        picB23.Image = Properties.Resources.green;
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_1);
                        if (barril_contenido == 0)
                        {
                            picB21.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB21.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_2);
                        if (barril_contenido == 0)
                        {
                            picB22.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB22.Image = null;
                        }
                        #endregion

                        break;
                    #endregion

                    #region POR DEFECTO
                    default:
                        v2_1.Image = Properties.Resources.valveoff;
                        v2_2.Image = Properties.Resources.valveoff;
                        v2_3.Image = Properties.Resources.valveoff;
                        FA_2.Image = Properties.Resources.fondoA4;
                        Q11.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q12.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q13.Image = Properties.Resources.LUZ_ROJA_OFF;
                        /*comprueba contenido de barriles*/
                        #region comprueba contenido de barriles
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_1);
                        if (barril_contenido == 0)
                        {
                            picB21.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB21.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_2);
                        if (barril_contenido == 0)
                        {
                            picB22.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB22.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_3);
                        if (barril_contenido == 0)
                        {
                            picB23.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB23.Image = null;
                        }
                        #endregion

                        break;
                        #endregion
                }
                #region comprueba contenido de barriles
                int contador = 0;
                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_1);
                if (barril_contenido == 0) { contador++; }
                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_2);
                if (barril_contenido == 0) { contador++; }
                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_3);
                if (barril_contenido == 0) { contador++; }
                #endregion
                if ((contador > 1) && (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA5_LINEA1_BYTE) == 0)) jump2.Enabled = true;
                else jump2.Enabled = false;
                #endregion
            }
        }
        #endregion

        #region LÍNEA DE BARRILES NÚMERO 3B PARA FORMULARIO: ELECTROVÁLVULAS 1-2-3 
        public void dibujaEVA123_3B()
        {
            int barril_activo;
            int barril_contenido;

            if (procesosconlogo.EstaConectado())
            {
                #region ELECTROVÁLVULA 1, 2 y 3  LINEA NÚMERO 3
                barril_activo = procesosconlogo.activo(Constantes.BARRILACTIVO_LINEA3_BYTE_1);
                switch (barril_activo)
                {
                    #region BARRIL NÚMERO 1
                    case 1:
                        v3_1.Image = Properties.Resources.valveon;
                        v3_2.Image = Properties.Resources.valveoff;
                        v3_3.Image = Properties.Resources.valveoff;
                        Q21.Image = Properties.Resources.LUZ_VERDE_ON;
                        Q22.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q23.Image = Properties.Resources.LUZ_ROJA_OFF;
                        {
                            FA_3.Image = Properties.Resources.fondoA1;
                        }

                        /*comprueba contenido de barriles*/
                        #region comprueba contenido de barriles
                        picB31.Image = Properties.Resources.green;
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_2);
                        if (barril_contenido == 0)
                        {
                            picB32.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB32.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_3);
                        if (barril_contenido == 0)
                        {
                            picB33.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB33.Image = null;
                        }
                        #endregion

                        break;
                    #endregion

                    #region BARRIL NÚMERO 2
                    case 2:
                        v3_1.Image = Properties.Resources.valveoff;
                        v3_2.Image = Properties.Resources.valveon;
                        v3_3.Image = Properties.Resources.valveoff;
                        Q21.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q22.Image = Properties.Resources.LUZ_VERDE_ON;
                        Q23.Image = Properties.Resources.LUZ_ROJA_OFF;
                        {
                            FA_3.Image = Properties.Resources.fondoA2;
                        }

                        /*comprueba contenido de barriles*/
                        #region comprueba contenido de barriles
                        picB32.Image = Properties.Resources.green;
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_1);
                        if (barril_contenido == 0)
                        {
                            picB31.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB31.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_3);
                        if (barril_contenido == 0)
                        {
                            picB33.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB33.Image = null;
                        }
                        #endregion

                        break;
                    #endregion

                    #region BARRIL NÚMERO 3
                    case 3:
                        v3_1.Image = Properties.Resources.valveoff;
                        v3_2.Image = Properties.Resources.valveoff;
                        v3_3.Image = Properties.Resources.valveon;
                        Q21.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q22.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q23.Image = Properties.Resources.LUZ_VERDE_ON;
                        {
                            FA_3.Image = Properties.Resources.fondoA3;
                        }

                        /*comprueba contenido de barriles*/
                        #region comprueba contenido de barriles
                        picB33.Image = Properties.Resources.green;
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_1);
                        if (barril_contenido == 0)
                        {
                            picB31.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB31.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_2);
                        if (barril_contenido == 0)
                        {
                            picB32.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB32.Image = null;
                        }
                        #endregion

                        break;
                    #endregion

                    #region POR DEFECTO
                    default:
                        v3_1.Image = Properties.Resources.valveoff;
                        v3_2.Image = Properties.Resources.valveoff;
                        v3_3.Image = Properties.Resources.valveoff;
                        FA_3.Image = Properties.Resources.fondoA4;
                        Q21.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q22.Image = Properties.Resources.LUZ_ROJA_OFF;
                        Q23.Image = Properties.Resources.LUZ_ROJA_OFF;
                        /*comprueba contenido de barriles*/
                        #region comprueba contenido de barriles
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_1);
                        if (barril_contenido == 0)
                        {
                            picB31.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB31.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_2);
                        if (barril_contenido == 0)
                        {
                            picB32.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB32.Image = null;
                        }
                        barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_3);
                        if (barril_contenido == 0)
                        {
                            picB33.Image = Properties.Resources.blue;
                        }
                        else
                        {
                            picB33.Image = null;
                        }
                        #endregion

                        break;
                        #endregion
                }
                #region comprueba contenido de barriles
                int contador = 0;
                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_1);
                if (barril_contenido == 0) { contador++; }
                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_2);
                if (barril_contenido == 0) { contador++; }
                barril_contenido = procesosconlogo.lecturaContenido(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_3);
                if (barril_contenido == 0) { contador++; }
                #endregion
                if ((contador > 1) && (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA5_LINEA1_BYTE) == 0)) jump3.Enabled = true;
                else jump3.Enabled = false;
                #endregion
            }
        }
        #endregion 
        #endregion

        #region LÍNEA DE BARRILES NÚMERO 1: ELECTROVÁLVULA 5
        public void dibujaEVA5_1()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    if (procesosconlogo.EstaConectado())
                    {
                        /*comprueba pasos - electroválvula 5*/
                        #region comprueba pasos - electroválvula 5

                        if (procesosconlogo.lecturaV5(Constantes.ELECTROVALVULA5_LINEA1_BYTE) == 0)
                        {
                            v1_paso.Image = Properties.Resources.valveon;
                            stop1.Enabled = true;
                            run1.Enabled = false;
                            picbombilla1.Image = Properties.Resources.on1;
                            picparomarcha1.Image = Properties.Resources.PALANCA_ON;
                            labelbombilla1.Text = "En MARCHA";
                            //FB_1.Image = Properties.Resources.fondoB;
                            Q5.Image = Properties.Resources.LUZ_VERDE_ON;
                        }
                        else
                        {
                            v1_paso.Image = Properties.Resources.valveoff;
                            stop1.Enabled = false;
                            run1.Enabled = true;
                            picbombilla1.Image = Properties.Resources.off1;
                            picparomarcha1.Image = Properties.Resources.PALANCA_OFF;
                            labelbombilla1.Text = "En PARO";
                            //FB_1.Image = Properties.Resources.fondoB4;
                            Q5.Image = Properties.Resources.LUZ_ROJA_OFF;
                        }
                        #endregion
                    }
                }));
            }
        }
        #endregion

        #region LÍNEA DE BARRILES NÚMERO 2: ELECTROVÁLVULA 5
        public void dibujaEVA5_2()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    if (procesosconlogo.EstaConectado())
                    {
                        /*comprueba pasos - electroválvula 15*/
                        #region comprueba pasos - electroválvula 15
                        if (procesosconlogo.lecturaV5(Constantes.ELECTROVALVULA15_LINEA2_BYTE) == 0)
                        {
                            v2_paso.Image = Properties.Resources.valveon;
                            stop2.Enabled = true;
                            run2.Enabled = false;
                            picbombilla2.Image = Properties.Resources.on1;
                            picparomarcha2.Image = Properties.Resources.PALANCA_ON;
                            labelbombilla2.Text = "En MARCHA";
                            //FB_2.Image = Properties.Resources.fondoB;
                            Q15.Image = Properties.Resources.LUZ_VERDE_ON;
                        }
                        else
                        {
                            v2_paso.Image = Properties.Resources.valveoff;
                            stop2.Enabled = false;
                            run2.Enabled = true;
                            picbombilla2.Image = Properties.Resources.off1;
                            picparomarcha2.Image = Properties.Resources.PALANCA_OFF;
                            labelbombilla2.Text = "En PARO";
                            //FB_2.Image = Properties.Resources.fondoB4;
                            Q15.Image = Properties.Resources.LUZ_ROJA_OFF;
                        }
                        #endregion
                    }
                }));
            }
        }
        #endregion

        #region LÍNEA DE BARRILES NÚMERO 3: ELECTROVÁLVULA 5
        public void dibujaEVA5_3()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    if (procesosconlogo.EstaConectado())
                    {
                        /*comprueba pasos - electroválvula 25*/
                        #region comprueba pasos - electroválvula 25                    
                        if (procesosconlogo.lecturaV5(Constantes.ELECTROVALVULA25_LINEA3_BYTE) == 0)
                        {
                            v3_paso.Image = Properties.Resources.valveon;
                            stop3.Enabled = true;
                            run3.Enabled = false;
                            picbombilla3.Image = Properties.Resources.on1;
                            picparomarcha3.Image = Properties.Resources.PALANCA_ON;
                            labelbombilla3.Text = "En MARCHA";
                            //FB_3.Image = Properties.Resources.fondoB;
                            Q25.Image = Properties.Resources.LUZ_VERDE_ON;
                        }
                        else
                        {
                            v3_paso.Image = Properties.Resources.valveoff;
                            stop3.Enabled = false;
                            run3.Enabled = true;
                            picbombilla3.Image = Properties.Resources.off1;
                            picparomarcha3.Image = Properties.Resources.PALANCA_OFF;
                            labelbombilla3.Text = "En PARO";
                            //FB_3.Image = Properties.Resources.fondoB4;
                            Q25.Image = Properties.Resources.LUZ_ROJA_OFF;
                        }
                        #endregion
                    }
                }));
            }
        }
        #endregion

        #region LÍNEA DE BARRILES NÚMERO 1: DETECTOR
        public void dibujaDetector1()
        {

            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    if (procesosconlogo.EstaConectado())
                    {
                        /*comprueba detectores*/
                        #region comprueba detectores
                        if (procesosconlogo.lecturaDetector(Constantes.DETECTOR_LINEA1_BYTE) == 0)
                        {
                            df1.Image = Properties.Resources.cellaron;
                            FB_1.Image = Properties.Resources.fondoB;
                        }
                        else
                        {
                            df1.Image = Properties.Resources.cellaroff;
                            FB_1.Image = Properties.Resources.fondoB4;
                        }
                        #endregion
                    }
                }));
            }
        }
        #endregion

        #region LÍNEA DE BARRILES NÚMERO 2: DETECTOR
        public void dibujaDetector2()
        {

            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    if (procesosconlogo.EstaConectado())
                    {
                        /*comprueba detectores2*/
                        #region comprueba detectores2
                        if (procesosconlogo.lecturaDetector(Constantes.DETECTOR_LINEA2_BYTE) == 0)
                        {
                            df2.Image = Properties.Resources.cellaron;
                            FB_2.Image = Properties.Resources.fondoB;
                        }
                        else
                        {
                            df2.Image = Properties.Resources.cellaroff;
                            FB_2.Image = Properties.Resources.fondoB4;
                        }
                        #endregion
                    }
                }));
            }
        }
        #endregion

        #region LÍNEA DE BARRILES NÚMERO 3: DETECTOR
        public void dibujaDetector3()
        {

            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    if (procesosconlogo.EstaConectado())
                    {
                        /*comprueba detectores3*/
                        #region comprueba detectores3
                        if (procesosconlogo.lecturaDetector(Constantes.DETECTOR_LINEA3_BYTE) == 0)
                        {
                            df3.Image = Properties.Resources.cellaron;
                            FB_3.Image = Properties.Resources.fondoB;
                        }
                        else
                        {
                            df3.Image = Properties.Resources.cellaroff;
                            FB_3.Image = Properties.Resources.fondoB4;
                        }
                        #endregion
                    }
                }));
            }
        }
        #endregion

        #region LINEA 1: PARO/MARCHA
        public void ParoMarcha_1()
        {
            try
            {
                if (InvokeRequired)
                {
                    Invoke(new Action(() =>
                    {
                        #region COMPRUEBA QUE ESTÁ EN MARCHA Y LO PARA
                        if (procesosconlogo.EstaConectado())
                        {
                            if (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA5_LINEA1_BYTE) == 1)
                            {

                                v1_paso.Image = Properties.Resources.valveoff;
                                stop1.Enabled = false;
                                run1.Enabled = true;
                                picbombilla1.Image = Properties.Resources.off1;
                                picparomarcha1.Image = Properties.Resources.PALANCA_OFF;
                                labelbombilla1.Text = "PARADO";
                                FB_1.Image = Properties.Resources.fondoB4;
                            }
                            else
                            {
                                v1_paso.Image = Properties.Resources.valveon;
                                stop1.Enabled = true;
                                run1.Enabled = false;
                                picbombilla1.Image = Properties.Resources.on1;
                                picparomarcha1.Image = Properties.Resources.PALANCA_ON;
                                labelbombilla1.Text = "EN MARCHA";
                                FB_1.Image = Properties.Resources.fondoB;
                            }
                        }
                        #endregion
                    }));
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region LINEA 2: PARO/MARCHA
        public void ParoMarcha_2()
        {
            try
            {
                if (InvokeRequired)
                {
                    Invoke(new Action(() =>
                    {
                        #region COMPRUEBA QUE ESTÁ EN MARCHA Y LO PARA
                        if (procesosconlogo.EstaConectado())
                        {
                            if (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA15_LINEA2_BYTE) == 1)
                            {
                                v2_paso.Image = Properties.Resources.valveoff;
                                stop2.Enabled = false;
                                run2.Enabled = true;
                                picbombilla2.Image = Properties.Resources.off1;
                                picparomarcha2.Image = Properties.Resources.PALANCA_OFF;
                                labelbombilla2.Text = "PARADO";
                                FB_2.Image = Properties.Resources.fondoB4;
                            }
                            else
                            {
                                v2_paso.Image = Properties.Resources.valveon;
                                stop2.Enabled = true;
                                run2.Enabled = false;
                                picbombilla2.Image = Properties.Resources.on1;
                                picparomarcha2.Image = Properties.Resources.PALANCA_ON;
                                labelbombilla2.Text = "EN MARCHA";
                                FB_2.Image = Properties.Resources.fondoB;
                            }
                        }
                        #endregion
                    }));
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region LINEA 3: PARO/MARCHA
        public void ParoMarcha_3()
        {
            try
            {
                if (InvokeRequired)
                {
                    Invoke(new Action(() =>
                    {
                    #region COMPRUEBA QUE ESTÁ EN MARCHA Y LO PARA
                    if (procesosconlogo.EstaConectado())
                    {
                            if (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA25_LINEA3_BYTE) == 1)
                            {
                                try
                                {
                                    v3_paso.Image = Properties.Resources.valveoff;
                                    stop3.Enabled = false;
                                    run3.Enabled = true;
                                    picbombilla3.Image = Properties.Resources.off1;
                                    picparomarcha3.Image = Properties.Resources.PALANCA_OFF;
                                    labelbombilla3.Text = "PARADO";
                                    FB_3.Image = Properties.Resources.fondoB4;
                                } catch { }
                            }
                            else
                            {
                                try
                                {
                                    v3_paso.Image = Properties.Resources.valveon;
                                    stop3.Enabled = true;
                                    run3.Enabled = false;
                                    picbombilla3.Image = Properties.Resources.on1;
                                    picparomarcha3.Image = Properties.Resources.PALANCA_ON;
                                    labelbombilla3.Text = "EN MARCHA";
                                    FB_3.Image = Properties.Resources.fondoB;
                                } catch { }
                            }
                        }
                        #endregion
                    }));
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region LITROS DE LINEAS DE BARRILES NÚMEROS 1, 2 Y 3
        public void dibujaLitros()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    if (procesosconlogo.EstaConectado())
                    {
                        #region RECOGE LOS DATOS
                        float[,] barril = new float[3, 3];

                        #region LINEA 1
                        /* Barril 1 Linea 1 */
                        barril[0, 0] = (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL1_LINEA1_BYTE_3]) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL1_LINEA1_BYTE_2] << 8) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL1_LINEA1_BYTE_1] << 16) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL1_LINEA1_BYTE_0] << 24);
                        /* Barril 2 Linea 1 */
                        barril[0, 1] = (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL2_LINEA1_BYTE_3]) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL2_LINEA1_BYTE_2] << 8) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL2_LINEA1_BYTE_1] << 16) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL2_LINEA1_BYTE_0] << 24);
                        /* Barril 3 Linea 1 */
                        barril[0, 2] = (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL3_LINEA1_BYTE_3]) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL3_LINEA1_BYTE_2] << 8) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL3_LINEA1_BYTE_1] << 16) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL3_LINEA1_BYTE_0] << 24);
                        #endregion

                        #region LINEA 2
                        /* Barril 1 Linea 2 */
                        barril[1, 0] = (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL1_LINEA2_BYTE_3]) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL1_LINEA2_BYTE_2] << 8) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL1_LINEA2_BYTE_1] << 16) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL1_LINEA2_BYTE_0] << 24);
                        /* Barril 2 Linea 2 */
                        barril[1, 1] = (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL2_LINEA2_BYTE_3]) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL2_LINEA2_BYTE_2] << 8) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL2_LINEA2_BYTE_1] << 16) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL2_LINEA2_BYTE_0] << 24);
                        /* Barril 3 Linea 2 */
                        barril[1, 2] = (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL3_LINEA2_BYTE_3]) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL3_LINEA2_BYTE_2] << 8) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL3_LINEA2_BYTE_1] << 16) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL3_LINEA2_BYTE_0] << 24);
                        #endregion

                        #region LINEA 3
                        /* Barril 1 Linea 3 */
                        barril[2, 0] = (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL1_LINEA3_BYTE_3]) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL1_LINEA3_BYTE_2] << 8) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL1_LINEA3_BYTE_1] << 16) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL1_LINEA3_BYTE_0] << 24);
                        /* Barril 2 Linea 3 */
                        barril[2, 1] = (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL2_LINEA3_BYTE_3]) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL2_LINEA3_BYTE_2] << 8) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL2_LINEA3_BYTE_1] << 16) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL2_LINEA3_BYTE_0] << 24);
                        /* Barril 3 Linea 3 */
                        barril[2, 2] = (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL3_LINEA3_BYTE_3]) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL3_LINEA3_BYTE_2] << 8) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL3_LINEA3_BYTE_1] << 16) |
                                      (procesosconlogo.buffer512[Constantes.N_LITROS_BARRIL3_LINEA3_BYTE_0] << 24);
                        #endregion
                        #endregion

                        #region MUESTRA LOS RESULTADOS
                        #region LINEA NÚMERO 1: BARRILES 1, 2 Y 3
                        b1_1.Text = ((barril[0, 0] / 10) + "," + ((barril[0, 0] % 10)) + "Lts").ToString();
                        b1_2.Text = ((barril[0, 1] / 10) + "," + ((barril[0, 1] % 10)) + "Lts").ToString();
                        b1_3.Text = ((barril[0, 2] / 10) + "," + ((barril[0, 2] % 10)) + "Lts").ToString();
                        #endregion

                        #region LINEA NÚMERO 2: BARRILES 1, 2 Y 3
                        b2_1.Text = ((barril[1, 0] / 10) + "," + ((barril[1, 0] % 10)) + "Lts").ToString();
                        b2_2.Text = ((barril[1, 1] / 10) + "," + ((barril[1, 2] % 10)) + "Lts").ToString();
                        b2_3.Text = ((barril[1, 2] / 10) + "," + ((barril[1, 2] % 10)) + "Lts").ToString();
                        #endregion

                        #region LINEA NÚMERO 3: BARRILES 1, 2 Y 3
                        b3_1.Text = ((barril[2, 0] / 10) + "," + ((barril[2, 0] % 10)) + "Lts").ToString();
                        b3_2.Text = ((barril[2, 1] / 10) + "," + ((barril[2, 1] % 10)) + "Lts").ToString();
                        b3_3.Text = ((barril[2, 2] / 10) + "," + ((barril[2, 2] % 10)) + "Lts").ToString();
                        #endregion
                        #endregion

                        #region DIBUJA LOS BARRILES
                        #region LINEA NÚMERO 1: BARRILES 1, 2 Y 3
                        /*BARRIL 1*/
                        if (picB11.Image != null)
                        {
                            p11b1.Visible = true;
                            b1_1.Visible = true;
                            p12b1.Height = ((int)(barril[0, 0] / 10)) * 3;
                            p12b1.Top = 4 + (90 - p12b1.Height);
                        }
                        else
                        {
                            b1_1.Visible = false;
                            p11b1.Visible = false;
                        }
                        /*BARRIL 2*/
                        if (picB12.Image != null)
                        {
                            p11b2.Visible = true;
                            b1_2.Visible = true;
                            p12b2.Height = ((int)(barril[0, 1] / 10)) * 3;
                            p12b2.Top = 4 + (90 - p12b2.Height);
                        }
                        else
                        {
                            b1_2.Visible = false;
                            p11b2.Visible = false;
                        }
                        /*BARRIL 3*/
                        if (picB13.Image != null)
                        {
                            p11b3.Visible = true;
                            b1_3.Visible = true;
                            p12b3.Height = ((int)(barril[0, 2] / 10)) * 3;
                            p12b3.Top = 4 + (90 - p12b3.Height);
                        }
                        else
                        {
                            b1_3.Visible = false;
                            p11b3.Visible = false;
                        }
                        #endregion

                        #region LINEA NÚMERO 2: BARRILES 1, 2 Y 3
                        /*BARRIL 1*/
                        if (picB21.Image != null)
                        {
                            p21b1.Visible = true;
                            b2_1.Visible = true;
                            p22b1.Height = ((int)(barril[1, 0] / 10)) * 3;
                            p22b1.Top = 4 + (90 - p22b1.Height);
                        }
                        else
                        {
                            b2_1.Visible = false;
                            p21b1.Visible = false;
                        }
                        /*BARRIL 2*/
                        if (picB22.Image != null)
                        {
                            p21b2.Visible = true;
                            b2_2.Visible = true;
                            p22b2.Height = ((int)(barril[1, 1] / 10)) * 3;
                            p22b2.Top = 4 + (90 - p22b2.Height);
                        }
                        else
                        {
                            b2_2.Visible = false;
                            p21b2.Visible = false;
                        }
                        /*BARRIL 3*/
                        if (picB23.Image != null)
                        {
                            p21b3.Visible = true;
                            b2_3.Visible = true;
                            p22b3.Height = ((int)(barril[1, 2] / 10)) * 3;
                            p22b3.Top = 4 + (90 - p22b3.Height);
                        }
                        else
                        {
                            b2_3.Visible = false;
                            p21b3.Visible = false;
                        }
                        #endregion

                        #region LINEA NÚMERO 3: BARRILES 1, 2 Y 3
                        /*BARRIL 1*/
                        if (picB31.Image != null)
                        {
                            p31b1.Visible = true;
                            b3_1.Visible = true;
                            p32b1.Height = ((int)(barril[2, 0] / 10)) * 3;
                            p32b1.Top = 4 + (90 - p32b1.Height);
                        }
                        else
                        {
                            b3_1.Visible = false;
                            p31b1.Visible = false;
                        }
                        /*BARRIL 2*/
                        if (picB32.Image != null)
                        {
                            p31b2.Visible = true;
                            b3_2.Visible = true;
                            p32b2.Height = ((int)(barril[2, 1] / 10)) * 3;
                            p32b2.Top = 4 + (90 - p32b2.Height);
                        }
                        else
                        {
                            b3_2.Visible = false;
                            p31b2.Visible = false;
                        }
                        /*BARRIL 3*/
                        if (picB33.Image != null)
                        {
                            p31b3.Visible = true;
                            b3_3.Visible = true;
                            p32b3.Height = ((int)(barril[2, 2] / 10)) * 3;
                            p32b3.Top = 4 + (90 - p32b3.Height);
                        }
                        else
                        {
                            b3_3.Visible = false;
                            p31b3.Visible = false;
                        }
                        #endregion
                        #endregion
                    }
                }));
            }
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region VENTAS: LITROS POR LINEA
        public void dibujaConsumo2(int a, int b, int c)
        {
            #region CONTADOR DE BARRILES 1, 2 Y 3
            /*Esta variable sirve para almacenar los contadores de cada linea de barriles*/
            //int[] contador_barriles = new int[4];
            #endregion

            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    if (procesosconlogo.EstaConectado())
                    {
                        #region MUESTRA LOS LITROS VENDIDOS DE CADA UNA DE LAS LÍNEAS
                        vendido1.Text = ((a / 10) + "," + ((a % 10)) + "Lts").ToString();
                        vendido2.Text = ((b / 10) + "," + ((b % 10)) + "Lts").ToString();
                        vendido3.Text = ((c / 10) + "," + ((c % 10)) + "Lts").ToString();
                        #endregion
                    }
                }));
            }
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region CONSUMOS: ACUMULADO Y ÚLTIMO REGISTRO
        public void dibujaConsumo(int[] contador_barriles)
        {
            #region CONTADOR DE BARRILES 1, 2 Y 3
            /*Esta variable sirve para almacenar los contadores de cada linea de barriles*/
            //int[] contador_barriles = new int[4];
            #endregion

            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    if (procesosconlogo.EstaConectado())
                    {
                        #region REALIZA EL RECUENTO DE BARRILES DE CADA UNA DE LAS LÍNEAS
                        acumulado1.Text = " " + contador_barriles[0] + " BARRILES";
                        acumulado2.Text = " " + contador_barriles[1] + " BARRILES";
                        acumulado3.Text = " " + contador_barriles[2] + " BARRILES";
                        #endregion
                    }
                }));
            }
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region ESCRIBE MENSAJES EN EL DISPLAY       
        /// <summary>
        /// MENSAJES DESDE HILO
        /// </summary>
        /// <param name="s"></param>
        public void escribirDisplay(string s)
        {
            #region FUNCIÓN
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    textDump.Text = s;
                }));
            }
            #endregion
        }

        /// <summary>
        /// MENSAJES DESDE FORMULARIO
        /// </summary>
        /// <param name="s"></param>
        public void escribirDisplayB(string s)
        {
            textDump.Text = s;
        }

        /// <summary>
        /// MENSAJES PDU
        /// </summary>
        /// <param name="s"></param>
        public void escribirPlc(string s)
        {
            #region FUNCIÓN
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    textpdu.Text = s;
                }));
            }
            #endregion
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region /*PARO - MARCHA*/
        #region PARO GRIFO 1
        /// <summary>
        /// Para el grifo número 1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void stop1_Click_1(object sender, EventArgs e)
        {
            #region TRY-CATCH
            try
            {
                #region COMPRUEBA QUE ESTÁ EN MARCHA Y LO PARA
                if (procesosconlogo.EstaConectado())
                {
                    if (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA5_LINEA1_BYTE) == 0)
                    {
                        if (procesosconlogo.paro(Constantes.ELECTROVALVULA5_LINEA1_BYTE) == 1)
                        {
                            v1_paso.Image = Properties.Resources.valveoff;
                            Q5.Image = Properties.Resources.LUZ_ROJA_OFF;
                            stop1.Enabled = false;
                            run1.Enabled = true;
                            picbombilla1.Image = Properties.Resources.off1;
                            picparomarcha1.Image = Properties.Resources.PALANCA_OFF;
                            labelbombilla1.Text = "PARADO";
                            FB_1.Image = Properties.Resources.fondoB4;
                        }
                    }
                }
                #endregion
                else
                {
                    escribirDisplayB("PLC DESCONECTADO");
                }
            }
            catch (Exception ex)
            {
                escribirDisplayB("ErrorCheck: " + ex.Message);
            }
            #endregion
        }
        #endregion

        #region MARCHA GRIFO 1
        /// <summary>
        /// Marcha el grifo número 1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void run1_Click(object sender, EventArgs e)
        {
            #region TRY-CATCH
            try
            {
                #region COMPRUEBA QUE ESTÁ EN MARCHA Y LO PARA
                if (procesosconlogo.EstaConectado())
                {
                    if (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA5_LINEA1_BYTE) == 1)
                    {
                        if (procesosconlogo.marcha(Constantes.ELECTROVALVULA5_LINEA1_BYTE) == 0)
                        {
                            v1_paso.Image = Properties.Resources.valveon;
                            Q5.Image = Properties.Resources.LUZ_VERDE_ON;
                            stop1.Enabled = true;
                            run1.Enabled = false;
                            picbombilla1.Image = Properties.Resources.on1;
                            picparomarcha1.Image = Properties.Resources.PALANCA_ON;
                            labelbombilla1.Text = "EN MARCHA";
                            FB_1.Image = Properties.Resources.fondoB;
                        }
                    }
                }
                #endregion
                else
                {
                    escribirDisplayB("PLC DESCONECTADO");
                }
            }
            catch (Exception ex)
            {
                escribirDisplayB("ErrorCheck: " + ex.Message);
            }
            #endregion
        }
        #endregion

        #region PARO GRIFO 2
        /// <summary>
        /// Para el grifo número 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void stop2_Click(object sender, EventArgs e)
        {
            #region TRY-CATCH
            try
            {
                #region COMPRUEBA QUE ESTÁ EN MARCHA Y LO PARA
                if (procesosconlogo.EstaConectado())
                {
                    if (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA15_LINEA2_BYTE) == 0)
                    {
                        if (procesosconlogo.paro(Constantes.ELECTROVALVULA15_LINEA2_BYTE) == 1)
                        {
                            v2_paso.Image = Properties.Resources.valveoff;
                            Q15.Image = Properties.Resources.LUZ_ROJA_OFF;
                            stop2.Enabled = false;
                            run2.Enabled = true;
                            picbombilla2.Image = Properties.Resources.off1;
                            picparomarcha2.Image = Properties.Resources.PALANCA_OFF;
                            labelbombilla2.Text = "PARADO";
                            FB_2.Image = Properties.Resources.fondoB4;
                        }
                    }
                }
                #endregion
                else
                {
                    escribirDisplayB("PLC DESCONECTADO");
                }
            }
            catch (Exception ex)
            {
                escribirDisplayB("ErrorCheck: " + ex.Message);
            }
            #endregion
        }
        #endregion

        #region MARCHA GRIFO 2
        /// <summary>
        /// Marcha el grifo número 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void run2_Click_1(object sender, EventArgs e)
        {
            #region TRY-CATCH
            try
            {
                #region COMPRUEBA QUE ESTÁ EN MARCHA Y LO PARA
                if (procesosconlogo.EstaConectado())
                {
                    if (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA15_LINEA2_BYTE) == 1)
                    {
                        if (procesosconlogo.marcha(Constantes.ELECTROVALVULA15_LINEA2_BYTE) == 0)
                        {
                            v2_paso.Image = Properties.Resources.valveon;
                            Q15.Image = Properties.Resources.LUZ_VERDE_ON;
                            stop2.Enabled = true;
                            run2.Enabled = false;
                            picbombilla2.Image = Properties.Resources.on1;
                            picparomarcha2.Image = Properties.Resources.PALANCA_ON;
                            labelbombilla2.Text = "EN MARCHA";
                            FB_2.Image = Properties.Resources.fondoB;
                        }
                    }
                }
                #endregion
                else
                {
                    escribirDisplayB("PLC DESCONECTADO");
                }
            }
            catch (Exception ex)
            {
                escribirDisplayB("ErrorCheck: " + ex.Message);
            }
            #endregion
        }
        #endregion

        #region PARO GRIFO 3
        /// <summary>
        /// Para el grifo número 3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void stop3_Click_1(object sender, EventArgs e)
        {
            #region TRY-CATCH
            try
            {
                #region COMPRUEBA QUE ESTÁ EN MARCHA Y LO PARA
                if (procesosconlogo.EstaConectado())
                {
                    if (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA25_LINEA3_BYTE) == 0)
                    {
                        if (procesosconlogo.paro(Constantes.ELECTROVALVULA25_LINEA3_BYTE) == 1)
                        {
                            v3_paso.Image = Properties.Resources.valveoff;
                            Q25.Image = Properties.Resources.LUZ_ROJA_OFF;
                            stop3.Enabled = false;
                            run3.Enabled = true;
                            picbombilla3.Image = Properties.Resources.off1;
                            picparomarcha3.Image = Properties.Resources.PALANCA_OFF;
                            labelbombilla3.Text = "PARADO";
                            FB_3.Image = Properties.Resources.fondoB4;
                        }
                    }
                }
                #endregion
                else
                {
                    escribirDisplayB("PLC DESCONECTADO");
                }
            }
            catch (Exception ex)
            {
                escribirDisplayB("ErrorCheck: " + ex.Message);
            }
            #endregion
        }
        #endregion

        #region MARCHA GRIFO 3
        /// <summary>
        /// Marcha el grifo número 3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void run3_Click_1(object sender, EventArgs e)
        {
            #region TRY-CATCH
            try
            {
                #region COMPRUEBA QUE ESTÁ EN MARCHA Y LO PARA
                if (procesosconlogo.EstaConectado())
                {
                    if (procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA25_LINEA3_BYTE) == 1)
                    {
                        if (procesosconlogo.marcha(Constantes.ELECTROVALVULA25_LINEA3_BYTE) == 0)
                        {
                            v3_paso.Image = Properties.Resources.valveon;
                            Q25.Image = Properties.Resources.LUZ_VERDE_ON;
                            stop3.Enabled = true;
                            run3.Enabled = false;
                            picbombilla3.Image = Properties.Resources.on1;
                            picparomarcha3.Image = Properties.Resources.PALANCA_ON;
                            labelbombilla3.Text = "EN MARCHA";
                            FB_3.Image = Properties.Resources.fondoB;
                        }
                    }
                }
                #endregion
                else
                {
                    escribirDisplayB("PLC DESCONECTADO");
                }
            }
            catch (Exception ex)
            {
                escribirDisplayB("ErrorCheck: " + ex.Message);
            }
            #endregion
        }
        #endregion
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region /*AVANCE BARRIL*/
        #region AVANCE BARRIL GRIFO 1
        /// <summary>
        /// Avanza Barril de Grifo 1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void jump1_Click_1(object sender, EventArgs e)
        {
            #region TRY-CATCH
            try
            {
                if (procesosconlogo.EstaConectado())
                {
                    #region FUNCIÓN QUE DEVUELVE EL NÚMERO DE BARRILES LLENOS
                   if ((procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA5_LINEA1_BYTE) == 0) && 
                        (CuentaBarriles(Constantes.N_BARRILESLLENOS_LINEA1_BYTE_1, Constantes.N_BARRILESLLENOS_LINEA1_BYTE_2, Constantes.N_BARRILESLLENOS_LINEA1_BYTE_3) > 1))
                    {
                        procesosconlogo.avance(311);
                        int barril_activo = procesosconlogo.activo(Constantes.BARRILACTIVO_LINEA1_BYTE_1);
                        switch (barril_activo)
                        {
                            case 1:
                                v1_1.Image = Properties.Resources.valveon;
                                v1_2.Image = Properties.Resources.valveoff;
                                v1_3.Image = Properties.Resources.valveoff;
                                break;
                            case 2:
                                v1_1.Image = Properties.Resources.valveoff;
                                v1_2.Image = Properties.Resources.valveon;
                                v1_3.Image = Properties.Resources.valveoff;
                                break;
                            case 3:
                                v1_1.Image = Properties.Resources.valveoff;
                                v1_2.Image = Properties.Resources.valveoff;
                                v1_3.Image = Properties.Resources.valveon;
                                break;
                            default:
                                v1_1.Image = Properties.Resources.valveoff;
                                v1_2.Image = Properties.Resources.valveoff;
                                v1_3.Image = Properties.Resources.valveoff;
                                break;
                        }
                        progressBarB1.Visible = true;
                        progressBarB1.Style = ProgressBarStyle.Marquee;
                        jump1.Visible = false;
                        timer1_jump1.Start();
                        refresh1.Visible = false;
                        timer1_refresh1.Start();
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                escribirDisplayB("ErrorCheck: " + ex.Message);
            }
            #endregion
        }
        #endregion

        #region AVANCE BARRIL GRIFO 2
        /// <summary>
        /// Avanza Barril de Grifo 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void jump2_Click_1(object sender, EventArgs e)
        {
            #region TRY-CATCH
            try
            {
                if (procesosconlogo.EstaConectado())
                {
                    #region FUNCIÓN QUE DEVUELVE EL NÚMERO DE BARRILES LLENOS
                   if ((procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA15_LINEA2_BYTE) == 0) &&
                        (CuentaBarriles(Constantes.N_BARRILESLLENOS_LINEA2_BYTE_1, Constantes.N_BARRILESLLENOS_LINEA2_BYTE_2, Constantes.N_BARRILESLLENOS_LINEA2_BYTE_3) > 1))
                    {
                        procesosconlogo.avance(321);
                        int barril_activo = procesosconlogo.activo(Constantes.BARRILACTIVO_LINEA2_BYTE_1);

                        switch (barril_activo)
                        {
                            case 1:
                                v2_1.Image = Properties.Resources.valveon;
                                v2_2.Image = Properties.Resources.valveoff;
                                v2_3.Image = Properties.Resources.valveoff;
                                break;
                            case 2:
                                v2_1.Image = Properties.Resources.valveoff;
                                v2_2.Image = Properties.Resources.valveon;
                                v2_3.Image = Properties.Resources.valveoff;
                                break;
                            case 3:
                                v2_1.Image = Properties.Resources.valveoff;
                                v2_2.Image = Properties.Resources.valveoff;
                                v2_3.Image = Properties.Resources.valveon;
                                break;
                            default:
                                v2_1.Image = Properties.Resources.valveoff;
                                v2_2.Image = Properties.Resources.valveoff;
                                v2_3.Image = Properties.Resources.valveoff;
                                break;
                        }
                        progressBarB2.Visible = true;
                        progressBarB2.Style = ProgressBarStyle.Marquee;
                        jump2.Visible = false;
                        timer2_jump2.Start();
                        refresh2.Visible = false;
                        timer2_refresh2.Start();
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                escribirDisplayB("ErrorCheck: " + ex.Message);
            }
            #endregion
        }
        #endregion

        #region AVANCE BARRIL GRIDO 3
        /// <summary>
        /// Avanza Barril de Grifo 3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void jump3_Click_1(object sender, EventArgs e)
        {
            #region TRY-CATCH
            try
            {
                if (procesosconlogo.EstaConectado())
                {
                    #region FUNCIÓN QUE DEVUELVE EL NÚMERO DE BARRILES LLENOS
                    if ((procesosconlogo.lercturaMarchaParo(Constantes.ELECTROVALVULA25_LINEA3_BYTE) == 0) &&
                        (CuentaBarriles(Constantes.N_BARRILESLLENOS_LINEA3_BYTE_1, Constantes.N_BARRILESLLENOS_LINEA3_BYTE_2, Constantes.N_BARRILESLLENOS_LINEA3_BYTE_3) > 1))
                    {
                        procesosconlogo.avance(331);
                        int barril_activo = procesosconlogo.activo(Constantes.BARRILACTIVO_LINEA3_BYTE_1);

                        switch (barril_activo)
                        {
                            case 1:
                                v3_1.Image = Properties.Resources.valveon;
                                v3_2.Image = Properties.Resources.valveoff;
                                v3_3.Image = Properties.Resources.valveoff;
                                break;
                            case 2:
                                v3_1.Image = Properties.Resources.valveoff;
                                v3_2.Image = Properties.Resources.valveon;
                                v3_3.Image = Properties.Resources.valveoff;
                                break;
                            case 3:
                                v3_1.Image = Properties.Resources.valveoff;
                                v3_2.Image = Properties.Resources.valveoff;
                                v3_3.Image = Properties.Resources.valveon;
                                break;
                            default:
                                v3_1.Image = Properties.Resources.valveoff;
                                v3_2.Image = Properties.Resources.valveoff;
                                v3_3.Image = Properties.Resources.valveoff;
                                break;
                        }
                        progressBarB3.Visible = true;
                        progressBarB3.Style = ProgressBarStyle.Marquee;
                        jump3.Visible = false;
                        timer3_jump3.Start();
                        refresh3.Visible = false;
                        timer3_refresh3.Start();
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                escribirDisplayB("ErrorCheck: " + ex.Message);
            }
            #endregion
        }
        #endregion

        #region COMPRUEBA Y CUENTA BARRILES LLENOS
        /// <summary>
        /// Cuenta cuantos barriles hay en la linea
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public int CuentaBarriles(int a, int b, int c)
        {
            int barril_contenido = 0; //¿barril vacío?
            int contador = 0; //cuenta los barriles vacíos de la línea

            barril_contenido = procesosconlogo.lecturaContenido(a);
            if (barril_contenido == 0) { contador++; }
            barril_contenido = procesosconlogo.lecturaContenido(b);
            if (barril_contenido == 0) { contador++; }
            barril_contenido = procesosconlogo.lecturaContenido(c);
            if (barril_contenido == 0) { contador++; }
            return contador;
        }
        #endregion
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region REPOSICION BARRILES
        #region REPOSICION BARRILES GRIFO 1
        /// <summary>
        /// Repone Barriles de Grifo 1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void refresh1_Click_1(object sender, EventArgs e)
        {
            if (procesosconlogo.EstaConectado())
            {
                procesosconlogo.reponer(313);
                dibujaEVA123_1B();
                progressBarB1.Visible = true;
                progressBarB1.Style = ProgressBarStyle.Marquee;
                refresh1.Visible = false;
                timer1_refresh1.Start();
                jump1.Visible = false;
                timer1_jump1.Start();
            }
        }
        #endregion

        #region REPOSICION BARRILES GRIFO 2
        /// <summary>
        /// Repone Barriles de Grifo 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void refresh2_Click_1(object sender, EventArgs e)
        {
            if (procesosconlogo.EstaConectado())
            {
                procesosconlogo.reponer(323);
                dibujaEVA123_2B();
                progressBarB2.Visible = true;
                progressBarB2.Style = ProgressBarStyle.Marquee;
                refresh2.Visible = false;
                timer2_refresh2.Start();
                jump2.Visible = false;
                timer2_jump2.Start();
            }
        }
        #endregion

        #region REPOSICION BARRILES GRIFO 3
        /// <summary>
        /// Repone Barriles de Grifo 3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void refresh3_Click_1(object sender, EventArgs e)
        {
            if (procesosconlogo.EstaConectado())
            {
                procesosconlogo.reponer(333);
                dibujaEVA123_3B();
                progressBarB3.Visible = true;
                progressBarB3.Style = ProgressBarStyle.Marquee;
                refresh3.Visible = false;
                timer3_refresh3.Start();
                jump3.Visible = false;
                timer3_jump3.Start();
            }
        }
        #endregion
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region ALARMAS
        #region ALARMA SIN CO2 - BOTELLA 1
        /// <summary>
        /// SALTA ALARMA CO2 CON VALOR 1 == VALOR_OK_ALARMA_CO2
        /// EN LA DIRECCIÓN DE MEMORIA 208 == ALARMA_CO2_BYTE
        /// </summary>
        public void dibujaAlarmaCO2()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    #region Alarmas co2
                    if (procesosconlogo.EstaConectado()) 
                    {
                        int CO2 = procesosconlogo.buffer512[Constantes.ALARMA_CO2_BYTE];

                        #region CO2
                        if (CO2 == Constantes.VALOR_OK_ALARMA_CO2)
                        {
                            picCO2.Image = Properties.Resources.green;
                            labelCO2resultado.Text = "OK";
                            labelCO2resultado.ForeColor = Color.Green;
                        }
                        else
                        {
                            picCO2.Image = Properties.Resources.red;
                            labelCO2resultado.Text = "SIN CO2";
                            labelCO2resultado.ForeColor = Color.Red;
                        }
                        #endregion
                    }
                    #endregion
                }));
            }
        }
        #endregion

        #region ALARMA COMPRESOR AIRE AVERIADO
        public void dibujaAlarmaCA()
        {

            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    #region Alarmas ca
                    if (procesosconlogo.EstaConectado())
                    {
                        int CA = procesosconlogo.buffer512[Constantes.ALARMA_COMPRESORAIRE_BYTE];

                        #region COMPRESOR AIRE
                        if (CA == 1)
                        {
                            picCA.Image = Properties.Resources.green;
                            labelCAresultado.Text = "OK";
                            labelCAresultado.ForeColor = Color.Green;
                        }
                        else
                        {
                            picCA.Image = Properties.Resources.red;
                            labelCAresultado.Text = "AVERÍA";
                            labelCAresultado.ForeColor = Color.Red;
                        }
                        #endregion
                    }
                    #endregion
                }));
            }
        }
        #endregion

        #region ALARMA PUERTA DE CÁMARA ABIERTA
        public void dibujaAlarmaPuerta()
        {

            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    #region Alarmas puerta
                    if (procesosconlogo.EstaConectado())
                    {
                        int PUERTA = procesosconlogo.buffer512[Constantes.ALARMA_PUERTAANIERTA_BYTE];

                        #region PUERTA
                        if (PUERTA == 1)
                        {
                            picPU.Image = Properties.Resources.green;
                            labelPUresultado.Text = "OK";
                            labelPUresultado.ForeColor = Color.Green;
                        }
                        else
                        {
                            picPU.Image = Properties.Resources.red;
                            labelPUresultado.Text = "ABIERTA";
                            labelPUresultado.ForeColor = Color.Red;
                        }
                        #endregion
                    }
                    #endregion
                }));
            }
        }
        #endregion

        #region ALARMA FUSIBLE FUNDIDO
        public void dibujaAlarmaFusible()
        {

            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    #region Alarmas fusible
                    if (procesosconlogo.EstaConectado())
                    {
                        int FUSIBLE = procesosconlogo.buffer512[250];

                        #region FUSIBLE
                        if (true) //(FUSIBLE == 1)
                        {
                            picFU.Image = Properties.Resources.green;
                            labelFUresultado.Text = "OK";
                            labelFUresultado.ForeColor = Color.Green;
                        }
                        else
                        {
                            picFU.Image = Properties.Resources.red;
                            labelFUresultado.Text = "CAMBIAR";
                            labelFUresultado.ForeColor = Color.Red;
                        }
                        #endregion
                    }
                    #endregion
                }));
            }
        }
        #endregion

        #endregion
        /*################################################################*/


        /*################################################################*/
        #region PROPIEDADES
        /// <summary>
        /// Indica si hay conexión establecida con Logo
        /// </summary>
        public bool ConectadoLogo
        {
            get { return conectadoLogo; }
            set { conectadoLogo = value; }
        }

        /// <summary>
        /// Indica si hay conexión con Base de Datos
        /// </summary>
        public bool ConectadoBD
        {
            get { return conectadoBD; }
            set { conectadoBD = value; }
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region TIMER's
        #region TIMER AVANCE 1
        /// <summary>
        /// Timer para avance barril 1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_jump1_Tick(object sender, EventArgs e)
        {
            if (i_jump1 < T_ESPERA_jUMP) i_jump1++;
            else
            {
                jump1.Visible = true;
                i_jump1 = 0;
                progressBarB1.Style = ProgressBarStyle.Blocks;
                progressBarB1.Visible = false;
                timer1_jump1.Stop();
            }
        }
        #endregion

        #region TIMER AVANCE 2
        /// <summary>
        /// Timer para avance barril 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer2_jump2_Tick(object sender, EventArgs e)
        {
            if (i_jump2 < T_ESPERA_jUMP) i_jump2++;
            else
            {
                jump2.Visible = true;
                i_jump2 = 0;
                progressBarB2.Style = ProgressBarStyle.Blocks;
                progressBarB2.Visible = false;
                timer2_jump2.Stop();
            }
        }
        #endregion

        #region TIMER AVANCE 3
        /// <summary>
        /// Timer para avance barril 3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer3_jump3_Tick(object sender, EventArgs e)
        {
            if (i_jump3 < T_ESPERA_jUMP) i_jump3++;
            else
            {
                jump3.Visible = true;
                i_jump3 = 0;
                progressBarB3.Style = ProgressBarStyle.Blocks;
                progressBarB3.Visible = false;
                timer3_jump3.Stop();
            }
        }
        #endregion

        #region TIMER REPONER 1
        /// <summary>
        /// Timer para reponer barril 1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_refresh1_Tick(object sender, EventArgs e)
        {
            if (i_refresh1 < T_ESPERA_REFRESH) i_refresh1++;
            else
            {
                refresh1.Visible = true;
                i_refresh1 = 0;
                timer1_refresh1.Stop();
            }
        }
        #endregion

        #region TIMER REPONER 2
        /// <summary>
        /// Timer para reponer barril 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer2_refresh2_Tick(object sender, EventArgs e)
        {
            if (i_refresh2 < T_ESPERA_REFRESH) i_refresh2++;
            else
            {
                refresh2.Visible = true;
                i_refresh2 = 0;
                timer2_refresh2.Stop();
            }
        }
        #endregion

        #region TIMER REPONER 3
        /// <summary>
        /// Timer para reponer barril 3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer3_refresh3_Tick(object sender, EventArgs e)
        {
            if (i_refresh3 < T_ESPERA_REFRESH) i_refresh3++;
            else
            {
                refresh3.Visible = true;
                i_refresh3 = 0;
                timer3_refresh3.Stop();
            }
        }
        #endregion

        #region TIMER RED
        private void timered_Tick(object sender, EventArgs e)
        {
            if (T_RED)
            {
                var seed = Environment.TickCount;
                var random = new Random(seed);
                //var value = random.Next(0, 3);
                switch (random.Next(0, 3))
                {
                    case 0:
                        pictureBoxBoton.Image = Properties.Resources.SIGNAL2_1;
                        break;
                    case 1:
                        pictureBoxBoton.Image = Properties.Resources.SIGNAL2_2;
                        break;
                    case 2:
                        pictureBoxBoton.Image = Properties.Resources.SIGNAL2_3;
                        break;
                    default:
                        pictureBoxBoton.Image = Properties.Resources.SIGNAL2_1;
                        break;
                }
            }
            else
            {
                pictureBoxBoton.Image = Properties.Resources.SIGNAL2;
                timered.Stop();
            }
        }
        #endregion
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region PREGUNTA SI HAY CONEXIÓN A INTERNET
        /// <summary>
        /// PRIMERO COMPRUEBA SI ESTÁ CONECTADO A RED LOCAL ... 
        /// Y SEGUNDO COMPRUEBA SI HAY SALIDA A INTERNET
        /// </summary>
        /// <returns></returns>
        public bool wifi()
        {
            bool Estado = false;
            bool RedActiva = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            //SI ESTÁ CONECTADO A RED LOCAL ... COMPRUEBA SALIDA A INTERNET
            if (RedActiva)
            {
                System.Uri Url = new System.Uri("https://www.google.com/");
                System.Net.WebRequest WebRequest;
                WebRequest = System.Net.WebRequest.Create(Url);
                System.Net.WebResponse objetoResp;
                try
                {
                    objetoResp = WebRequest.GetResponse();
                    Estado = true;
                    objetoResp.Close();
                }
                catch (Exception ex)
                {
                    if (conectadoBD)
                        Log.LogWarn("SIN CONEXIÓN A INTERNET: " + ex.Message);
                }
                WebRequest = null;
            }

            return Estado;
        }
        #endregion
        /*################################################################*/


        #region CONTROLES DEL MENU PRINCIPAL
        #region VERIFICA CONEXIÓN CON BASE DE DATOS
        private void verificarConexiónBDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormVerificarBD f =
                 new FormVerificarBD(ref conectadoBD);
            f.ShowDialog();
        }
        #endregion

        #region ABRE UN FORMULARIO PARA ESCRIBIR INFORMACIÓN EN LOGO
        private void enviarDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormEscribirEnLogo f =
               new FormEscribirEnLogo(procesosconlogo);
            f.ShowDialog();
        }
        #endregion

        #region CERRAR APLICACIÓN
        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            conectadoLogo = false;
            procesosconlogo.disconnectPLc();
            hilo.AbortaHilo();
            Thread.Sleep(1000);
            Application.Exit();
        }
        #endregion

        #endregion


        #region OCULTAR O MOSTRAR PANELES
        #region GRIFO 1
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                checkBox1.Image = Properties.Resources.int_on;
                panel1.Visible = true;
            }
            else
            {
                checkBox1.Image = Properties.Resources.int_off;
                panel1.Visible = false;
            }
        }
        #endregion

        #region GRIFO 2
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                checkBox2.Image = Properties.Resources.int_on;
                panel2.Visible = true;
            }
            else
            {
                checkBox2.Image = Properties.Resources.int_off;
                panel2.Visible = false;
            }
        }
        #endregion

        #region GRIFO 3
        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked == true)
            {
                checkBox3.Image = Properties.Resources.int_on;
                panel3.Visible = true;
            }
            else
            {
                checkBox3.Image = Properties.Resources.int_off;
                panel3.Visible = false;
            }
        }  
        #endregion
        #endregion
    }
    #endregion
}
