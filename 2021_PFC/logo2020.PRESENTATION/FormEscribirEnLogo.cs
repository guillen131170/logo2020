﻿using logo2020.APPLICATION;
using logo2020.CORE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.PRESENTATION
{
    public partial class FormEscribirEnLogo : Form
    {
        public ProcesosConLogo procesosconlogo;

        #region CREAR Y MOSTRAR EL FORMULARIO
        #region CREAR EL FORMULARIO
        public FormEscribirEnLogo(ProcesosConLogo p)
        {
            InitializeComponent();
            procesosconlogo = p;
        }
        #endregion

        #region CARGAR EL FORMULARIO
        private void FormEscribirEnLogo_Load(object sender, EventArgs e)
        {
            nombre.SelectedIndex = 0;
            direccion.SelectedIndex = nombre.SelectedIndex;
            admin_address.Text = direccion.SelectedItem.ToString();
            tipodato.SelectedIndex = NumIndex();
        }
        #endregion 

        private int NumIndex()
        {
            int valor = 0;
            switch (nombre.SelectedIndex)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                    valor = 2;
                    break;
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                    valor = 0;
                    break;
                default:
                    valor = 0;
                    break;
            }
            return valor;
        }

        private int Valor_Actual()
        {
            int valor = 1;
            switch (tipodato.SelectedIndex)
            {
                case 0:
                    valor = 1;
                    break;
                case 1:
                    valor = 2;
                    break;
                case 2:
                    valor = 4;
                    break;
                default:
                    valor = 1;
                    break;
            }
            return valor;
        }
        #endregion


        #region ENVIAR LA INFORMACIÓN Y ESCRIBIR EN LOGO
        private void admin_enviar_Click(object sender, EventArgs e)
        {
            int start = 0;
            int value = 0;
            Generales general = new Generales();

            try
            {
                start = Convert.ToInt32(admin_address.Text);
                value = Convert.ToInt32(admin_value.Text);
            }
            catch (Exception) { }

            if (!admin_address.Text.Equals("") && !admin_value.Text.Equals(""))
            {
                switch (tipodato.SelectedIndex)
                {
                    case 0: //BYTE
                        byte[] buffer_byte = new byte[1];
                        try
                        {
                            buffer_byte[0] = (byte)value;
                            procesosconlogo.writeByte(start, buffer_byte[0]);
                        }
                        catch (Exception) { }
                        break;
                    case 1: //WORD
                        byte[] buffer_word = new byte[2];
                        try
                        {
                            buffer_word = general.IntToArrayByte2(value);
                            procesosconlogo.writeWord(start, buffer_word[0], buffer_word[1]);
                        }
                        catch (Exception) { }
                        break;
                    case 2: //DWORD
                        byte[] buffer_dword = new byte[4];
                        try
                        {
                            buffer_dword = general.IntToArrayByte4(value);
                            procesosconlogo.writeDword(start, buffer_dword[0], buffer_dword[1], buffer_dword[2], buffer_dword[3]);
                        }
                        catch (Exception) { }
                        break;
                    default:
                        break;
                }
            }
        } 
        #endregion


        #region CERRAR ESTE FORMULARIO
        private void cerrarEscribirLogo_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion


        private void nombre_SelectionChangeCommitted(object sender, EventArgs e)
        {
            direccion.SelectedIndex = nombre.SelectedIndex;
            tipodato.SelectedIndex = NumIndex();
            admin_address.Text = direccion.SelectedItem.ToString();
        }

        private void direccion_SelectionChangeCommitted(object sender, EventArgs e)
        {
            nombre.SelectedIndex = direccion.SelectedIndex;
            tipodato.SelectedIndex = NumIndex();
            admin_address.Text = direccion.SelectedItem.ToString();
        }

        private void admin_address_TextChanged(object sender, EventArgs e)
        {
            for (int i=0; i<direccion.Items.Count; i++)
            {
                if (admin_address.Text.Equals(direccion.Items[i].ToString()))
                {
                    direccion.SelectedIndex = i;
                    nombre.SelectedIndex = direccion.SelectedIndex;
                    tipodato.SelectedIndex = NumIndex();
                }
            }          
        }
    }
}
