﻿using logo2020.APPLICATION;
using logo2020.CORE;
using logo2020.IFR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.PRESENTATION
{
    public class Hilos
    {
        /*################################################################*/
        #region Atributos
        /* Hilo para Conexión */
        private Thread hilo;

        /* PLC del hilo */
        private ProcesosConLogo proceso;
        private ProcesosConSQL sql;
        private Form1 f;
        private TextBox display;
        private bool conectadoLogo;
        private bool conectadoBD;
        /*Array que va a contener los valores tipo byte de la memoria de Logo*/
        private byte[] array = new byte[512];
        /*FORMATO AAAAMMDD*/
        private int fecha;

        private int[] barrilActivo = new int[3];
        private int[] consumoAnterior = new int[3];
        private int[] barrilActivoAuxiliar = new int[3];
        private int[] consumoAuxiliar = new int[3];
        private int[] consumoAcumulado = new int[3];
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region /* Constructores de Hilo */
        /// <summary>
        /// Crea el hilo que mantiene la conexión con Logo
        /// </summary>
        /// <param name="_proceso">OPERACIONES CON LOGO</param>
        /// <param name="_form">PANTALLA HMI</param>
        public Hilos(ProcesosConLogo _proceso, Form1 _form, ref bool _conectado, ref bool _bd, TextBox _display)
        {
            //------------------------------------------------
            #region INICIA LOS OBJETOS: Form Y ProcesosConLogo
            f = _form;
            display = _display;
            proceso = _proceso;
            sql = new ProcesosConSQL();
            ConectadoLogo = _conectado;
            ConectadoBD = _bd;

            /*Inicia fecha para comprobar consumos*/
            DateTime fechaActual = DateTime.Today;
            int fecha = (fechaActual.Year * 10000) +
                         (fechaActual.Month * 100) +
                         (fechaActual.Day);
            #endregion
            //------------------------------------------------

            //------------------------------------------------
            #region INICIA EL ARRAY DE BYTES CON VALORES 255
            for (int i = 0; i < 512; i++)
            {
                array[i] = 255;
            } 
            #endregion
            //------------------------------------------------

            //------------------------------------------------
            #region INICIO DEL HILO
            /*DELEGADO*/
            ThreadStart delegado = new ThreadStart(CorrerProceso);
            /*HILO*/
            hilo = new Thread(delegado);
            /*INICIO DE HILO*/ 
            hilo.Start();
            #endregion
            //------------------------------------------------
        }   
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region Proceso del hilo
        /// <summary>
        /// Todas las tareas que se realizan dentro del hilo
        /// </summary>
        private void CorrerProceso()
        {
            //------------------------------------------------
            #region OPERACIONES ANTERIORES AL INICIO DEL BUCLE DEL HILO - PREPARAR HILO
            if (ConectadoBD)
                Log.LogInfo(DateTime.Now + " SE HA INICIADO UN SUBPROCESO CON EL LOGO");
            f.escribirDisplay(DateTime.Now + " SE HA INICIADO UN SUBPROCESO CON EL LOGO");

            #region CONTADOR DE BARRILES 1, 2 Y 3
            /*Esta variable sirve para almacenar los contadores de cada linea de barriles*/
            int[] contador_barriles = new int[4];
            #endregion


            #region LECTURA DE MEMORIA DE LOGO Y DIBUJO DE HMI
            #region LECTURA DE MEMORIA DE LOGO
            try
            {
                proceso.LeerBloque512();
            }
            catch (Exception ex)
            {
                if (ConectadoBD)
                    Log.LogWarn("ERROR AL LEER EN MEMORIA DE LOGO - LeerBloque512(): " + ex.Message);
            }
            #endregion

            #region INICIA EL ARRAY DE BYTES CON VALORES DE LA LECTURA
            for (int i = 0; i < 512; i++)
            {
                array[i] = proceso.buffer512[i];
            }
            #endregion

            /*
            #region ASIGNA VALORES INICIALES PARA MOSTRAR CONSUMOS
            #region OBTIENE LOS BARRILES ACTIVOS DE CADA LINEA
            barrilActivo[0] = proceso.activo(Constantes.BARRILACTIVO_LINEA1_BYTE_1);
            barrilActivo[1] = proceso.activo(Constantes.BARRILACTIVO_LINEA2_BYTE_1);
            barrilActivo[2] = proceso.activo(Constantes.BARRILACTIVO_LINEA3_BYTE_1);
            #endregion

            #region OBTIENE EL CONTENIDO EN LITROS DE CADA BARRIL ACTIVO
            for (int i = 0; i < 3; i++)
            {
                consumoAnterior[i] = ObtenerLitros(i);
                consumoAcumulado[i] = 0;
            }
            #endregion
            #endregion
            */

            #region COMPRUEBA SI EXISTE UN REGISTRO DE CONSUMO EN EL DÍA ACTUAL  Y LO GRABA           
            #region OBTIENE LA FECHA ACTUAL EN EL FORMATO ADECUADO
            DateTime fechaActual = DateTime.Today;
            int _fecha = (fechaActual.Year * 10000) +
                         (fechaActual.Month * 100) +
                         (fechaActual.Day);
            #endregion
            #region COMPRUEBA SI EXISTE UN REGISTRO DE CONSUMO EN EL DÍA ACTUAL
            bool existeregistro = false;
            if (conectadoBD)
            {
                if (sql.comprobarConsumo(_fecha))
                {
                    existeregistro = true;
                }
            }
            #endregion
            #endregion

            #region DIBUJA HMI
            f.dibujaEVA123_1();
            f.dibujaEVA123_2();
            f.dibujaEVA123_3();
            f.dibujaEVA5_1();
            f.dibujaEVA5_2();
            f.dibujaEVA5_3();
            f.dibujaDetector1();
            f.dibujaDetector2();
            f.dibujaDetector3();
            f.ParoMarcha_1();
            f.ParoMarcha_2();
            f.ParoMarcha_3();
            f.dibujaAlarmaCO2();
            f.dibujaAlarmaCA();
            f.dibujaAlarmaPuerta();
            f.dibujaAlarmaFusible();
            f.dibujaLitros();
            #endregion
            #endregion 
            #endregion
            //------------------------------------------------


            //------------------------------------------------
            #region OPERACIONES DURANTE EL BUCLE DEL HILO - INICIO HILO
            while (ConectadoLogo && proceso.EstaConectado())
            {
                #region BUCLE DEL HILO
                #region MIENTRAS EXISTA CONEXION CON LOGO    
                int valor_alarma_co2_botella1_auxiliar = 0;

                #region LECTURA DE MEMORIA DE LOGO
                try
                {
                    proceso.LeerBloque512();
                    f.escribirPlc(proceso.Client.ExecutionTime.ToString() + " MS");
                }
                catch (Exception ex)
                {
                    if (ConectadoBD)
                        Log.LogWarn("ERROR AL LEER EN MEMORIA DE LOGO - LeerBloque512(): " + ex.Message);
                }
                #endregion

                #region REALIZA EL RECUENTO DE BARRILES DE CADA UNA DE LAS LÍNEAS
                #region CONTADOR DE LINEA 1
                contador_barriles[0] = (proceso.buffer512[Constantes.N_BARRILES_LINEA1_BYTE_4]) |
                                        (proceso.buffer512[Constantes.N_BARRILES_LINEA1_BYTE_3] << 8) |
                                         (proceso.buffer512[Constantes.N_BARRILES_LINEA1_BYTE_2] << 16) |
                                          (proceso.buffer512[Constantes.N_BARRILES_LINEA1_BYTE_1] << 24);
                #endregion
                #region CONTADOR DE LINEA 2
                contador_barriles[1] = (proceso.buffer512[Constantes.N_BARRILES_LINEA2_BYTE_4]) |
                                        (proceso.buffer512[Constantes.N_BARRILES_LINEA2_BYTE_3] << 8) |
                                         (proceso.buffer512[Constantes.N_BARRILES_LINEA2_BYTE_2] << 16) |
                                          (proceso.buffer512[Constantes.N_BARRILES_LINEA2_BYTE_1] << 24);
                #endregion
                #region CONTADOR DE LINEA 3
                contador_barriles[2] = (proceso.buffer512[Constantes.N_BARRILES_LINEA3_BYTE_4]) |
                                        (proceso.buffer512[Constantes.N_BARRILES_LINEA3_BYTE_3] << 8) |
                                         (proceso.buffer512[Constantes.N_BARRILES_LINEA3_BYTE_2] << 16) |
                                          (proceso.buffer512[Constantes.N_BARRILES_LINEA3_BYTE_1] << 24);
                #endregion
                f.dibujaConsumo(contador_barriles);
                #endregion

                #region comprueba - electroválvulas 1,2 y 3
                if (array[Constantes.BARRILACTIVO_LINEA1_BYTE_1] != proceso.buffer512[Constantes.BARRILACTIVO_LINEA1_BYTE_1] ||
                    array[Constantes.BARRILACTIVO_LINEA1_BYTE_2] != proceso.buffer512[Constantes.BARRILACTIVO_LINEA1_BYTE_2] ||
                    array[Constantes.BARRILACTIVO_LINEA1_BYTE_3] != proceso.buffer512[Constantes.BARRILACTIVO_LINEA1_BYTE_3])
                {
                    f.dibujaEVA123_1();
                    array[Constantes.BARRILACTIVO_LINEA1_BYTE_1] = proceso.buffer512[Constantes.BARRILACTIVO_LINEA1_BYTE_1];
                    array[Constantes.BARRILACTIVO_LINEA1_BYTE_2] = proceso.buffer512[Constantes.BARRILACTIVO_LINEA1_BYTE_2];
                    array[Constantes.BARRILACTIVO_LINEA1_BYTE_3] = proceso.buffer512[Constantes.BARRILACTIVO_LINEA1_BYTE_3];
                }
                #endregion
                #region comprueba barriles pinchados - linea 1
                if (array[Constantes.N_BARRILESLLENOS_LINEA1_BYTE_1] != proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA1_BYTE_1] ||
                    array[Constantes.N_BARRILESLLENOS_LINEA1_BYTE_2] != proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA1_BYTE_2] ||
                    array[Constantes.N_BARRILESLLENOS_LINEA1_BYTE_3] != proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA1_BYTE_3])
                {
                    f.dibujaEVA123_1();
                    array[Constantes.N_BARRILESLLENOS_LINEA1_BYTE_1] = proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA1_BYTE_1];
                    array[Constantes.N_BARRILESLLENOS_LINEA1_BYTE_2] = proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA1_BYTE_2];
                    array[Constantes.N_BARRILESLLENOS_LINEA1_BYTE_3] = proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA1_BYTE_3];
                }
                #endregion

                #region comprueba - electroválvulas 11,12 y 13
                if (array[Constantes.BARRILACTIVO_LINEA2_BYTE_1] != proceso.buffer512[Constantes.BARRILACTIVO_LINEA2_BYTE_1] ||
                    array[Constantes.BARRILACTIVO_LINEA2_BYTE_2] != proceso.buffer512[Constantes.BARRILACTIVO_LINEA2_BYTE_2] ||
                    array[Constantes.BARRILACTIVO_LINEA2_BYTE_3] != proceso.buffer512[Constantes.BARRILACTIVO_LINEA2_BYTE_3])
                {
                    f.dibujaEVA123_2();
                    array[Constantes.BARRILACTIVO_LINEA2_BYTE_1] = proceso.buffer512[Constantes.BARRILACTIVO_LINEA2_BYTE_1];
                    array[Constantes.BARRILACTIVO_LINEA2_BYTE_2] = proceso.buffer512[Constantes.BARRILACTIVO_LINEA2_BYTE_2];
                    array[Constantes.BARRILACTIVO_LINEA2_BYTE_3] = proceso.buffer512[Constantes.BARRILACTIVO_LINEA2_BYTE_3];
                }
                #endregion
                #region comprueba barriles pinchados - linea 2
                if (array[Constantes.N_BARRILESLLENOS_LINEA2_BYTE_1] != proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA2_BYTE_1] ||
                    array[Constantes.N_BARRILESLLENOS_LINEA2_BYTE_2] != proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA2_BYTE_2] ||
                    array[Constantes.N_BARRILESLLENOS_LINEA2_BYTE_3] != proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA2_BYTE_3])
                {
                    f.dibujaEVA123_2();
                    array[Constantes.N_BARRILESLLENOS_LINEA2_BYTE_1] = proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA2_BYTE_1];
                    array[Constantes.N_BARRILESLLENOS_LINEA2_BYTE_2] = proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA2_BYTE_2];
                    array[Constantes.N_BARRILESLLENOS_LINEA2_BYTE_3] = proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA2_BYTE_3];
                }
                #endregion

                #region comprueba - electroválvulas 21,22 y 23
                if (array[Constantes.BARRILACTIVO_LINEA3_BYTE_1] != proceso.buffer512[Constantes.BARRILACTIVO_LINEA3_BYTE_1] ||
                    array[Constantes.BARRILACTIVO_LINEA3_BYTE_2] != proceso.buffer512[Constantes.BARRILACTIVO_LINEA3_BYTE_2] ||
                    array[Constantes.BARRILACTIVO_LINEA3_BYTE_3] != proceso.buffer512[Constantes.BARRILACTIVO_LINEA3_BYTE_3])
                {
                    f.dibujaEVA123_3();
                    array[Constantes.BARRILACTIVO_LINEA3_BYTE_1] = proceso.buffer512[Constantes.BARRILACTIVO_LINEA3_BYTE_1];
                    array[Constantes.BARRILACTIVO_LINEA3_BYTE_2] = proceso.buffer512[Constantes.BARRILACTIVO_LINEA3_BYTE_2];
                    array[Constantes.BARRILACTIVO_LINEA3_BYTE_3] = proceso.buffer512[Constantes.BARRILACTIVO_LINEA3_BYTE_3];
                }
                #endregion
                #region comprueba barriles pinchados - linea 3
                if (array[Constantes.N_BARRILESLLENOS_LINEA3_BYTE_1] != proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA3_BYTE_1] ||
                    array[Constantes.N_BARRILESLLENOS_LINEA3_BYTE_2] != proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA3_BYTE_2] ||
                    array[Constantes.N_BARRILESLLENOS_LINEA3_BYTE_3] != proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA3_BYTE_3])
                {
                    f.dibujaEVA123_3();
                    array[Constantes.N_BARRILESLLENOS_LINEA3_BYTE_1] = proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA3_BYTE_1];
                    array[Constantes.N_BARRILESLLENOS_LINEA3_BYTE_2] = proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA3_BYTE_2];
                    array[Constantes.N_BARRILESLLENOS_LINEA3_BYTE_3] = proceso.buffer512[Constantes.N_BARRILESLLENOS_LINEA3_BYTE_3];
                }
                #endregion

                #region comprueba - electroválvulas 5
                if (array[Constantes.ELECTROVALVULA5_LINEA1_BYTE] != proceso.buffer512[Constantes.ELECTROVALVULA5_LINEA1_BYTE])
                {
                    f.dibujaEVA5_1();
                    array[Constantes.ELECTROVALVULA5_LINEA1_BYTE] = proceso.buffer512[Constantes.ELECTROVALVULA5_LINEA1_BYTE];
                }
                #endregion

                #region comprueba - electroválvulas 15
                if (array[Constantes.ELECTROVALVULA15_LINEA2_BYTE] != proceso.buffer512[Constantes.ELECTROVALVULA15_LINEA2_BYTE])
                {
                    f.dibujaEVA5_2();
                    array[Constantes.ELECTROVALVULA15_LINEA2_BYTE] = proceso.buffer512[Constantes.ELECTROVALVULA15_LINEA2_BYTE];
                }
                #endregion

                #region comprueba - electroválvulas 25
                if (array[Constantes.ELECTROVALVULA25_LINEA3_BYTE] != proceso.buffer512[Constantes.ELECTROVALVULA25_LINEA3_BYTE])
                {
                    f.dibujaEVA5_3();
                    array[Constantes.ELECTROVALVULA25_LINEA3_BYTE] = proceso.buffer512[Constantes.ELECTROVALVULA25_LINEA3_BYTE];
                }
                #endregion

                #region comprueba - detectores1
                if (array[Constantes.DETECTOR_LINEA1_BYTE] != proceso.buffer512[Constantes.DETECTOR_LINEA1_BYTE])
                {
                    f.dibujaDetector1();
                    array[Constantes.DETECTOR_LINEA1_BYTE] = proceso.buffer512[Constantes.DETECTOR_LINEA1_BYTE];
                }
                #endregion

                #region comprueba - detectores2
                if (array[Constantes.DETECTOR_LINEA2_BYTE] != proceso.buffer512[Constantes.DETECTOR_LINEA2_BYTE])
                {
                    f.dibujaDetector2();
                    array[Constantes.DETECTOR_LINEA2_BYTE] = proceso.buffer512[Constantes.DETECTOR_LINEA2_BYTE];
                }
                #endregion

                #region comprueba - detectores3
                if (array[Constantes.DETECTOR_LINEA3_BYTE] != proceso.buffer512[Constantes.DETECTOR_LINEA3_BYTE])
                {
                    f.dibujaDetector3();
                    array[Constantes.DETECTOR_LINEA3_BYTE] = proceso.buffer512[Constantes.DETECTOR_LINEA3_BYTE];
                }
                #endregion

                #region PARO/MARCHA
                #region PARO/MARCHA: LINEA 1
                f.ParoMarcha_1();
                /*
                if (array[310] != proceso.buffer512[310])
                {
                    f.ParoMarcha_1();
                    array[310] = proceso.buffer512[310];
                }*/
                #endregion

                #region PARO/MARCHA: LINEA 2
                f.ParoMarcha_2();
                /*
                if (array[320] != proceso.buffer512[320])
                {
                    f.ParoMarcha_2();
                    array[320] = proceso.buffer512[320];
                }*/
                #endregion

                #region PARO/MARCHA: LINEA 3
                f.ParoMarcha_3();
                /*
                if (array[330] != proceso.buffer512[330])
                {
                    f.ParoMarcha_3();
                    array[330] = proceso.buffer512[330];
                }*/

                #endregion 
                #endregion

                #region COMPRUEBA ALARMA CO2
                //array[Constantes.ALARMA_CO2_BYTE] = 5;
                if (array[Constantes.ALARMA_CO2_BYTE] != proceso.buffer512[Constantes.ALARMA_CO2_BYTE])
                {
                    f.dibujaAlarmaCO2();
                    array[Constantes.ALARMA_CO2_BYTE] = proceso.buffer512[Constantes.ALARMA_CO2_BYTE];
                    #region ALARMA CO2 - ENVIA EMAIL
                    /* Comprueba el valor del registro de la botella número 1 */
                    /* Los valores del registro son:
                     * A- Funcionamiento OK - BOTELLA LLENA - VALOR 0
                     * B- Funcionamiento ERROR - BOTELLA VACIA - VALOR 1 */
                    if (array[Constantes.ALARMA_CO2_BYTE] != Constantes.VALOR_OK_ALARMA_CO2)
                    {
                        /* Este registro sirve para comprobar si ya se realizó el envío de un email
                             * Tiene dos valores:
                             * 1- Valor 0 significa: no se ha enviado email
                             * 2- Valor 2, significa: ya se ha enviado un email */
                        if (valor_alarma_co2_botella1_auxiliar == 0)
                        {
                            /* ENVÍO DE CORREO */
                            ProcesosConCorreo envio = new ProcesosConCorreo("ALARMA BOTELLA 1 DE CO2",
                                                                            "BOTELLA 1 DE CO2 VACÍA",
                                                                            "guillen131170@hotmail.com");
                            envio.enviarCorreo();
                            //Cambia el valor de valor_alarma_co2_botella1_auxiliar
                            //para no repetir envío de CC
                            valor_alarma_co2_botella1_auxiliar = 2;
                        }
                    }
                    else if ((array[Constantes.ALARMA_CO2_BYTE] == Constantes.VALOR_OK_ALARMA_CO2) &&
                             (valor_alarma_co2_botella1_auxiliar != 0))
                    {
                        /* Cambia el valor del registro del PLC
                        * para repetir el proceso*/
                        valor_alarma_co2_botella1_auxiliar = 0;
                    }
                    else
                    {
                        /* Cambia el valor del registro del PLC
                        * para repetir el proceso*/
                        valor_alarma_co2_botella1_auxiliar = 0;
                    }
                    #endregion
                }
                #endregion

                #region comprueba - alarma ca
                if (array[Constantes.ALARMA_COMPRESORAIRE_BYTE] != proceso.buffer512[Constantes.ALARMA_COMPRESORAIRE_BYTE])
                {
                    f.dibujaAlarmaCA();
                    array[Constantes.ALARMA_COMPRESORAIRE_BYTE] = proceso.buffer512[Constantes.ALARMA_COMPRESORAIRE_BYTE];
                }
                #endregion

                #region comprueba - alarma puerta
                if (array[Constantes.ALARMA_PUERTAANIERTA_BYTE] != proceso.buffer512[Constantes.ALARMA_PUERTAANIERTA_BYTE])
                {
                    f.dibujaAlarmaPuerta();
                    array[Constantes.ALARMA_PUERTAANIERTA_BYTE] = proceso.buffer512[Constantes.ALARMA_PUERTAANIERTA_BYTE];
                }
                #endregion

                #region comprueba - alarma fusible
                /*
                if (array[250] != proceso.buffer512[250])
                {
                    f.dibujaAlarmaFusible();
                    array[250] = proceso.buffer512[250];
                }
                */
                #endregion

                #region DIBUJA LITROS
                f.dibujaLitros();
                #endregion

                /*
                #region ASIGNA VALORES INICIALES PARA MOSTRAR CONSUMOS
                #region OBTIENE LOS BARRILES ACTIVOS DE CADA LINEA
                barrilActivoAuxiliar[0] = proceso.activo(Constantes.BARRILACTIVO_LINEA1_BYTE_1);
                barrilActivoAuxiliar[1] = proceso.activo(Constantes.BARRILACTIVO_LINEA2_BYTE_1);
                barrilActivoAuxiliar[2] = proceso.activo(Constantes.BARRILACTIVO_LINEA3_BYTE_1);
                #endregion

                #region OBTIENE EL CONTENIDO EN LITROS DE CADA BARRIL ACTIVO
                for (int i = 0; i < 3; i++) consumoAuxiliar[i] = ObtenerLitros(i);
                #endregion

                for (int i = 0; i < 3; i++)
                {
                    if (barrilActivo[i] != barrilActivoAuxiliar[i])
                    {
                        barrilActivo[i] = barrilActivoAuxiliar[i];
                        consumoAcumulado[i] += consumoAnterior[i];
                        consumoAnterior[i] = consumoAuxiliar[i];
                    }
                }

                f.dibujaConsumo2(consumoAnterior[0] - consumoAuxiliar[0] + consumoAcumulado[0], 
                                 consumoAnterior[1] - consumoAuxiliar[1] + consumoAcumulado[1],
                                 consumoAnterior[2] - consumoAuxiliar[2] + consumoAcumulado[2]);
                #endregion
                */

                #region ACTUALIZA EL ARRAY DE BYTES CON VALORES DE LA LECTURA
                for (int i = 0; i < 512; i++)
                {
                    array[i] = proceso.buffer512[i];
                }
                #endregion

                #region COMPRUEBA SI EXISTE UN REGISTRO DE CONSUMO EN EL DÍA ACTUAL  Y LO GRABA
                #region OBTIENE LA FECHA ACTUAL EN EL FORMATO ADECUADO
                fechaActual = DateTime.Today;
                _fecha = (fechaActual.Year * 10000) +
                             (fechaActual.Month * 100) +
                             (fechaActual.Day);
                #endregion
                #region SI HA CAMBIADO LA FECHA O EL REGISTRO DEL DÍA NO EXISTE...
                if (conectadoBD)
                {
                    if ((_fecha > fecha) || !existeregistro)
                    {
                        #region SI NO EXISTE REGISTRO DE ESE DÍA...
                        if (!sql.comprobarConsumo(_fecha))
                        {
                            int a = contador_barriles[0];
                            int b = contador_barriles[1];
                            int c = contador_barriles[2];
                            Consumos aux = null;
                            Consumos consumo = new Consumos();

                            #region SI HAY UN REGISTRO ANTERIOR...
                            if (!sql.tablaVacia())
                            {
                                aux = sql.recuperarConsumo();
                                consumo =
                                     new Consumos(a, b, c,
                                                  a - aux.Acumulado1, b - aux.Acumulado2, c - aux.Acumulado3,
                                                  _fecha);
                            }
                            #endregion
                            #region SI NO HAY UN REGISTRO ANTERIOR...
                            else
                            {
                                consumo = new Consumos(a, b, c, a, b, c, _fecha);
                            }
                            #endregion

                            sql.grabarConsumo(consumo);
                            if (ConectadoBD)
                                Log.LogInfo(DateTime.Now + " SE HA REGISTRADO EN BD EL CONSUMO DIARIO: ");
                            f.escribirDisplay(DateTime.Now + " SE HA REGISTRADO EN BD EL CONSUMO DIARIO: ");
                            existeregistro = true;
                            fecha = _fecha;
                        }
                        #endregion
                    }
                }
                #endregion
                #endregion

                /*DUERME EL HILO X MILÉSIMAS DE SEGUNDO*/
                Thread.Sleep(1000);
                #endregion

                #region SI SE PIERDE CONEXION CON LOGO INTENTA CONECTAR
                if (ConectadoLogo && !proceso.EstaConectado())
                {
                    do
                    {
                        try
                        {
                            proceso.connectPlcIP();
                        }
                        catch (Exception) { }
                    } while (ConectadoLogo && !proceso.EstaConectado());
                }
                #endregion 
                #endregion
            }
            #endregion
            //------------------------------------------------


            //------------------------------------------------
            #region OPERACIONES POSTERIORES AL BUCLE DEL HILO - FIN DEL HILO
            #region GRABA LOG
            if (ConectadoBD)
                Log.LogInfo(DateTime.Now + " SE HA DETENIDO EL SUBPROCESO ACTUAL CON EL LOGO");
            f.escribirDisplay(DateTime.Now + " SE HA DETENIDO EL SUBPROCESO ACTUAL CON EL LOGO");
            #endregion
            #region DETIENE EL HILO
            /*Detiene y elimina el hilo*/
            hilo.Abort(); 
            #endregion
            #endregion
            //------------------------------------------------
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region FUNCIONES GENERALES DEL HILO       
        /// <summary>
        /// Obtiene los litros del barril indicado
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        private int ObtenerLitros(int i)
        {
            int valor = 0;
            switch (barrilActivo[i])
            {
                case 1:
                    valor = (proceso.buffer512[3 + (i * 28)]) |
                                 (proceso.buffer512[2 + (i * 28)] << 8) |
                                 (proceso.buffer512[1 + (i * 28)] << 16) |
                                 (proceso.buffer512[0 + (i * 28)] << 24);
                    break;
                case 2:
                    valor = (proceso.buffer512[7 + (i * 28)]) |
                                 (proceso.buffer512[6 + (i * 28)] << 8) |
                                 (proceso.buffer512[5 + (i * 28)] << 16) |
                                 (proceso.buffer512[4 + (i * 28)] << 24);
                    break;
                case 3:
                    valor = (proceso.buffer512[11 + (i * 28)]) |
                                 (proceso.buffer512[10 + (i * 28)] << 8) |
                                 (proceso.buffer512[9 + (i * 28)] << 16) |
                                 (proceso.buffer512[8 + (i * 28)] << 24);
                    break;
                default:
                    valor = 0;
                    break;
            }
            return valor;
        }

        /// <summary>
        /// Aborta el hilo
        /// </summary>
        public void AbortaHilo()
        {
            hilo.Abort();
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region PROPIEDADES
        public bool ConectadoLogo { get => conectadoLogo; set => conectadoLogo = value; }

        public bool ConectadoBD { get => conectadoBD; set => conectadoBD = value; }
        #endregion
        /*################################################################*/
    }
}
