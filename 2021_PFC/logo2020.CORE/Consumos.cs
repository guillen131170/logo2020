﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.CORE
{
    /// <summary>
    /// ESTA CLASE SERÁN LOS DATOS DE CONSUMOS
    /// QUE SE GUARDARÁN EN BASE DE DATOS
    /// 
    /// atributos:
    ///     - id - identificador de la dirrección de correo
    ///     - name - nombre del propietario de la dirrección de correo
    ///     - email - cuenta de correo electrónico
    ///     - level - indica el nivel del usuario y sirve para indicar
    ///               a quién se dirige el correo y a quién no
    ///               (VA A DEPENDER DE LA ALARMA DE QUE SE TARTE)
    /// </summary>   
    public class Consumos
    {
        /*################################################################*/
        #region atributos     
        /// <summary>
        /// identificador de dato de consumo
        /// </summary>
        private int id;

        /// <summary>
        /// consumo acumulado total (decilitros)
        /// </summary>
        private int acumulado;

        /// <summary>
        /// consumo acumulado linea 1 (decilitros)
        /// </summary>
        private int acumulado1;

        /// <summary>
        /// consumo acumulado linea 2 (decilitros)
        /// </summary>
        private int acumulado2;

        /// <summary>
        /// consumo acumulado linea 3 (decilitros)
        /// </summary>
        private int acumulado3;

        /// <summary>
        /// consumo de ese día (decilitros)
        /// </summary>
        private int vendido;

        /// <summary>
        /// consumo de ese día linea 1 (decilitros)
        /// </summary>
        private int vendido1;

        /// <summary>
        /// consumo de ese día linea 2 (decilitros)
        /// </summary>
        private int vendido2;

        /// <summary>
        /// consumo de ese día linea 3 (decilitros)
        /// </summary>
        private int vendido3;

        /// <summary>
        /// fecha completa del dato de consumo
        /// año + mes + día
        /// </summary>
        private int fecha;
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region /* constructores */

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Consumos() { }

        /// <summary>
        /// Constructor por parámetros
        /// </summary>
        public Consumos(int _acumulado, int _acumulado1, int _acumulado2, int _acumulado3, 
                        int _vendido, int _vendido1, int _vendido2, int _vendido3, 
                        int _Day, int _Mounth, int _Year)
        {
            this.Acumulado = _acumulado;
            this.Acumulado1 = _acumulado1;
            this.Acumulado2 = _acumulado2;
            this.Acumulado3 = _acumulado3;
            this.Vendido = _vendido;
            this.Vendido1 = _vendido1;
            this.Vendido2 = _vendido2;
            this.Vendido3 = _vendido3;
            Fecha = (((_Year * 100) + _Mounth) * 100) + _Day;
        }

        /// <summary>
        /// Constructor por parámetros
        /// </summary>
        public Consumos(int _acumulado, int _acumulado1, int _acumulado2, int _acumulado3,
                        int _vendido, int _vendido1, int _vendido2, int _vendido3,
                        int _fecha)
        {
            this.Acumulado = _acumulado;
            this.Acumulado1 = _acumulado1;
            this.Acumulado2 = _acumulado2;
            this.Acumulado3 = _acumulado3;
            this.Vendido = _vendido;
            this.Vendido1 = _vendido1;
            this.Vendido2 = _vendido2;
            this.Vendido3 = _vendido3;
            Fecha = _fecha;
        }

        /// <summary>
        /// Constructor por parámetros
        /// </summary>
        public Consumos(int _acumulado1, int _acumulado2, int _acumulado3,
                        int _vendido1, int _vendido2, int _vendido3,
                        int _fecha)
        {
            this.Acumulado = _acumulado1 + _acumulado2 + _acumulado3;
            this.Acumulado1 = _acumulado1;
            this.Acumulado2 = _acumulado2;
            this.Acumulado3 = _acumulado3;
            this.Vendido = _vendido1 + _vendido2 + _vendido3;
            this.Vendido1 = _vendido1;
            this.Vendido2 = _vendido2;
            this.Vendido3 = _vendido3;
            Fecha = _fecha;
        }

        /// <summary>
        /// Constructor copia
        /// </summary>
        public Consumos(Consumos o)
        {
            Id = o.Id;
            this.Acumulado = o.Acumulado;
            this.Acumulado1 = o.Acumulado1;
            this.Acumulado2 = o.Acumulado2;
            this.Acumulado3 = o.Acumulado3;
            this.Vendido = o.Vendido;
            this.Vendido1 = o.Vendido1;
            this.Vendido2 = o.Vendido2;
            this.Vendido3 = o.Vendido3;
            Fecha = o.Fecha;
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region propiedades
        [Required]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Required]
        public int Acumulado
        {
            get { return acumulado; }
            set { acumulado = value; }
        }

        [Required]
        public int Acumulado1
        {
            get { return acumulado1; }
            set { acumulado1 = value; }
        }

        [Required]
        public int Acumulado2
        {
            get { return acumulado2; }
            set { acumulado2 = value; }
        }

        [Required]
        public int Acumulado3
        {
            get { return acumulado3; }
            set { acumulado3 = value; }
        }

        [Required]
        public int Vendido
        {
            get { return vendido; }
            set { vendido = value; }
        }

        [Required]
        public int Vendido1
        {
            get { return vendido1; }
            set { vendido1 = value; }
        }

        [Required]
        public int Vendido2
        {
            get { return vendido2; }
            set { vendido2 = value; }
        }

        [Required]
        public int Vendido3
        {
            get { return vendido3; }
            set { vendido3 = value; }
        }

        [Required]
        public int Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        #endregion
        /*################################################################*/
    }
}
