
USE PFC_2020
GO
 
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE Consumos(
id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
acumulado int NOT NULL,
acumulado1 int NOT NULL,
acumulado2 int NOT NULL,
acumulado3 int NOT NULL,
vendido int NOT NULL,
vendido1 int NOT NULL,
vendido2 int NOT NULL,
vendido3 int NOT NULL,
fecha int NOT NULL
)

GO
