﻿using logo2020.CORE;
using logo2020.IFR;
using System;
using System.Configuration;
using System.Data.SqlClient;

namespace logo2020.DAL
{
    public class SqlTrans
    {
        #region ATRIBUTOS
        /// <summary>
        /// Cadena de conexión
        /// </summary>
        private string cadenaconexion;
        //"server=DESKTOP-2M3VDIP\\SQLEXPRESS; database=pfc_2020 ; integrated security = true";    
        /// <summary>
        /// Objeto para CONECTAR con db
        /// </summary>
        public SqlConnection conexion;
        /// <summary>
        /// Objeto para CONSULTAR con db
        /// </summary>
        SqlCommand cmd;
        /// <summary>
        /// Objeto que guarada RESULTADO  de las consultas
        /// </summary>
        private SqlDataReader dr;
        #endregion


        #region CONSTRUCTORES
        #region CONSTRUCTOR POR DEFECTO
        /// <summary>
        /// Constructor por defecto - sin parámetros
        /// </summary>
        public SqlTrans()
        {
            try
            {
                //cadenaconexion = 
                conexion = new SqlConnection(CadenaConexion);
            }
            catch (Exception ex)
            {
                Log.LogError("ERROR SQL EN CONSTRUCTOR POR DEFECTO - SqlTrans(): " + ex.Message);
            }
        }
        #endregion
        #endregion


        #region COMPRUEBA SI HAY REGISTRADO UN CONSUMO EN LA FECHA INDICADA    
        /// <summary>
        /// Comprueba si hay algún consumo registrado
        /// en la fecha indicada en el parámetro
        /// </summary>
        /// <param name="_fecha">fecha a comprobar</param>
        /// <returns>verdadero o falso</returns>
        public bool comprobar(int _fecha)
        {
            bool valor = false;
            try
            {
                #region PREPARA LA CONSULTA A DB
                conexion.Open();
                string query =
                    "select Count(*) from Consumos " +
                    "where fecha = @FECHA";
                cmd = new SqlCommand(query, conexion);
                cmd.Parameters.Add(new SqlParameter("FECHA", _fecha));
                #endregion

                if ((Int32)cmd.ExecuteScalar() > 0) valor = true;               
            }
            catch (Exception ex)
            {
                Log.LogError("ERROR SQL EN FUNCIÓN - comprobar(int _fecha): " + ex.Message);
            }
            finally
            {
                conexion.Close();
            }
            return valor;
        }
        #endregion


        #region COMPRUEBA SI LA TABLA ESTA VACIA    
        /// <summary>
        /// Comprueba si la tabla esta vacía
        /// </summary>
        /// <param name="_fecha">fecha a comprobar</param>
        /// <returns>verdadero o falso</returns>
        public bool vacio()
        {
            bool valor = true;
            try
            {
                #region PREPARA LA CONSULTA A DB
                conexion.Open();
                string query =
                    "select Count(*) from Consumos";
                cmd = new SqlCommand(query, conexion);
                #endregion

                if ((Int32)cmd.ExecuteScalar() > 0) valor = false;
            }
            catch (Exception ex)
            {
                Log.LogError("ERROR SQL EN FUNCIÓN vacio(): " + ex.Message);
            }
            finally
            {
                conexion.Close();
            }
            return valor;
        }
        #endregion


        #region GRABA UN CONSUMO EN LA FECHA INDICADA    
        /// <summary>
        /// Graba un consumo
        /// en la fecha indicada en el parámetro
        /// </summary>
        /// <param name="_fecha">fecha a grabar</param>
        /// <returns>verdadero o falso</returns>
        public bool grabar(Consumos c)
        {
            bool valor = false;
            try
            {
                #region PREPARA LA CONSULTA A DB
                conexion.Open();
                string query =
                    "INSERT INTO Consumos(acumulado,acumulado1,acumulado2,acumulado3," +
                                          "vendido,vendido1,vendido2,vendido3," +
                                          "fecha) " +
                    "VALUES(@ACUMULADO,@ACUMULADO1,@ACUMULADO2,@ACUMULADO3," +
                    "@VENDIDO,@VENDIDO1,@VENDIDO2,@VENDIDO3," +
                    "@FECHA)";
                cmd = new SqlCommand(query, conexion);
                cmd.Parameters.Add(new SqlParameter("ACUMULADO", c.Acumulado));
                cmd.Parameters.Add(new SqlParameter("ACUMULADO1", c.Acumulado1));
                cmd.Parameters.Add(new SqlParameter("ACUMULADO2", c.Acumulado2));
                cmd.Parameters.Add(new SqlParameter("ACUMULADO3", c.Acumulado3));
                cmd.Parameters.Add(new SqlParameter("VENDIDO", c.Vendido));
                cmd.Parameters.Add(new SqlParameter("VENDIDO1", c.Vendido1));
                cmd.Parameters.Add(new SqlParameter("VENDIDO2", c.Vendido2));
                cmd.Parameters.Add(new SqlParameter("VENDIDO3", c.Vendido3));
                cmd.Parameters.Add(new SqlParameter("FECHA", c.Fecha));
                #endregion

                if ((Int32)cmd.ExecuteNonQuery() > 0) valor = true;
            }
            catch (Exception ex)
            {
                Log.LogError("ERROR SQL EN FUNCIÓN grabar(Consumos c): " + ex.Message);
            }
            finally
            {
                conexion.Close();
            }
            return valor;
        }
        #endregion


        #region RECUPERA EL ÚLTIMO CONSUMO REGISTRADO
        /// <summary>
        /// Recupera el último consumo de la db
        /// </summary>
        /// <param name="_fecha">fecha a comprobar</param>
        /// <returns>verdadero o falso</returns>
        public Consumos recuperar()
        {
            Consumos registro = null;
            try
            {
                #region PREPARA LA CONSULTA A DB
                conexion.Open();
                string query =
                    "SELECT TOP 1 * " +
                    "FROM Consumos ORDER BY id DESC";
                cmd = new SqlCommand(query, conexion);
                #endregion
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        registro = new Consumos();
                        registro.Id = reader.GetInt32(0);
                        registro.Acumulado = reader.GetInt32(1);
                        registro.Acumulado1 = reader.GetInt32(2);
                        registro.Acumulado2 = reader.GetInt32(3);
                        registro.Acumulado3 = reader.GetInt32(4);
                        registro.Vendido = reader.GetInt32(5);
                        registro.Vendido1 = reader.GetInt32(6);
                        registro.Vendido2 = reader.GetInt32(7);
                        registro.Vendido3 = reader.GetInt32(8);
                        registro.Fecha = reader.GetInt32(9);
                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Log.LogError("ERROR SQL EN FUNCIÓN recuperar(): " + ex.Message);
            }
            finally
            {
                conexion.Close();
            }
            return registro;
        }
        #endregion


        #region ABRIR CONEXIÓN CON DB
        /// <summary>
        /// Función para conectar con base de datos
        /// </summary>
        public void Conectar()
        {
            try
            {
                this.conexion.Open();
            }
            catch (Exception ex)
            {
                Log.LogError("ERROR SQL EN FUNCIÓN Conectar(): " + ex.Message);
            }
        }
        #endregion


        #region CERRAR CONEXIÓN CON DB
        /// <summary>
        /// Función para desconectar con base de datos
        /// </summary>
        public void Desconectar()
        {
            try
            {
                this.conexion.Close();
            }
            catch (Exception ex)
            {
                Log.LogError("ERROR SQL EN FUNCIÓN Desconectar(): " + ex.Message);
            }
        }
        #endregion


        #region VERIFICA LA CONEXIÓN CON BD
        /// <summary>
        /// SI EXISTE DEVUELVE TRUE
        /// ... SI NO EXISTE DEVUELVE FALSE
        /// </summary>
        /// <returns></returns>
        public bool VerificarConexión()
        {
            try
            {
                //abrimos conexión, en caso de no existir dará excepción para caer en el catch
                this.conexion.Open();
                this.conexion.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        #region 2A VERSION - BORRAR
        public bool VerificarConexión2()
        {
            bool valor = false;
            try
            {
                //abrimos conexión, en caso de no existir dará excepción para caer en el catch
                this.conexion.Open();
                valor = true;
            }
            catch { }
            finally
            {
                if (valor) this.conexion.Close();
            }
            return valor;
        }
        #endregion 
        #endregion


        #region RECUPERAR LA CADENA DE CONEXIÓN
        /// <summary>
        /// CON EL NOMBRE DE LA CADENA
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GetConnectionString()
        {
            // ERROR
            string returnValue = null;
            // BUSCA EL NOMBRE
            ConnectionStringSettings settings =
                ConfigurationManager.ConnectionStrings["ConnectionString1"];

            // SI ENCUENTRA EL NOMBRE ...
            if (settings != null)
                returnValue = settings.ConnectionString;

            return returnValue;
        }

        /// <summary>
        /// CON EL NOMBRE DE LA CADENA
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetConnectionStringByName(string name)
        {
            // ERROR
            string returnValue = null;
            // BUSCA EL NOMBRE
            ConnectionStringSettings settings =
                ConfigurationManager.ConnectionStrings[name];

            // SI ENCUENTRA EL NOMBRE ...
            if (settings != null)
                returnValue = settings.ConnectionString;

            return returnValue;
        }

        /// <summary>
        /// CON EL NOMBRE DEL PROVEEDOR
        /// </summary>
        /// <param name="providerName"></param>
        /// <returns></returns>
        static string GetConnectionStringByProvider(string providerName)
        {
            // Return null on failure.
            string returnValue = null;

            // Get the collection of connection strings.
            ConnectionStringSettingsCollection settings =
                ConfigurationManager.ConnectionStrings;

            // Walk through the collection and return the first
            // connection string matching the providerName.
            if (settings != null)
            {
                foreach (ConnectionStringSettings cs in settings)
                {
                    if (cs.ProviderName == providerName)
                        returnValue = cs.ConnectionString;
                    break;
                }
            }
            return returnValue;
        } 
        #endregion


        #region PROPIEDADES     
        /// <summary>
        /// Cadena de conexión para la base de datos 'PFC_2020'
        /// </summary>
        public string CadenaConexion
        {
            get { return GetConnectionString(); }
        }
        #endregion
    }
}
