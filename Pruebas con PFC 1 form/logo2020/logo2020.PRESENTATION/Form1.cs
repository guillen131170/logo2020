﻿using logo2020.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.PRESENTATION
{
    public partial class Form1 : Form
    {
        #region ATRIBUTOS DE LA CLASE
        private bool conectado;
        private ProcesosConLogo procesosconlogo;
        private hmi fhmi;
        #endregion


        #region CARGAR CONTROLES Y MOSTRAR VENTANA PRINCIPAL       
        /// <summary>
        /// INICIO
        /// </summary>
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Carga los componentes de la ventana principal
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            //progressBar1.Visible = false;
            //progressBar1.BackColor = Color.Red;
            progressBar1.Style = ProgressBarStyle.Blocks;
            //conectado = false;
            //PanelHDMI.Visible = false;
            off.Enabled = false;
            textip.Visible = false;
            pictureon.Image = Properties.Resources.plcoff;
        }
        #endregion


        #region CONECTAR/DESCONECTAR LOGO
        private void on_Click(object sender, EventArgs e)
        {
            /* Comprueba que los campos no están en blanco */
            if (dnsText.Text.Equals("") && ipText.Text.Equals(""))
            {
                MessageBox.Show("Rellene los campos de forma correcta", "Advertencia",
                                      MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //Crea un objeto de plc
            procesosconlogo = new ProcesosConLogo(dnsText.Text, ipText.Text);

            //Intenta buscar PLC con DNS
            if (!dnsText.Text.Equals(""))
            {
                try
                {
                    procesosconlogo.connectPlcDNS();
                }
                catch { }

                if (!procesosconlogo.EstaConectado() && !ipText.Text.Equals(""))
                {
                    try
                    {
                        procesosconlogo.connectPlcIP();
                    }
                    catch { }
                }
            }
            else
            {
                if (!ipText.Text.Equals(""))
                {
                    try
                    {
                        procesosconlogo.connectPlcIP();
                    }
                    catch { }
                }
            }

            if (procesosconlogo.EstaConectado())
            {
                //hora = minuto = segundo = 0;
                //timer1.Start();
                //progressBar1.Visible = true;
                progressBar1.Style = ProgressBarStyle.Marquee;
                conectado = true;
                //PanelHDMI.Visible = true;
                PanelConnect.Enabled = false;
                pictureon.Image = Properties.Resources.plcon;
                off.Enabled = true;
                textip.Visible = true;
                textip.Text = procesosconlogo.Logo.IP;
                pictureBoxBoton.Image = Properties.Resources.button_green;
                labelonoff.Text = "ON";
                Hilos hilo = new Hilos(procesosconlogo, this);
            }
            else
            {
                MessageBox.Show("¡NO CONECTADO!");
            }
        }

        private void off_Click(object sender, EventArgs e)
        {
            try
            {
                //timer1.Stop();
                textip.Text = "";
                textip.Visible = false;
                procesosconlogo.disconnectPLc();
                //progressBar1.Visible = false;
                progressBar1.Style = ProgressBarStyle.Blocks;
                conectado = false;
                //PanelHDMI.Visible = false;
                PanelConnect.Enabled = true;
                pictureon.Image = Properties.Resources.plcoff;
                off.Enabled = false;
                pictureBoxBoton.Image = Properties.Resources.button_Red;
                labelonoff.Text = "OFF";
            }
            catch { }
        }
        #endregion


        #region GENERAL
        public void AbrirHMI(object f)
        {
            if (this.panel.Controls.Count > 0)
                this.panel.Controls.RemoveAt(0);
            fhmi = f as hmi;
            fhmi.TopLevel = false;
            fhmi.Dock = DockStyle.Fill;
            //this.panel.Controls.Add(fhmi);
            this.panel.Tag = fhmi;
            //fhmi.Show();
            //return fhmi;
        }

        public void AgregarHMI(object f)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    fhmi = f as hmi;
                    this.panel.Controls.Add(fhmi);
                    fhmi.Show();
                }));
            }
        }

        public void EliminarHMI(object f)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    fhmi = f as hmi;
                    this.panel.Controls.Remove(fhmi);
                    fhmi.Hide();
                }));
            }
        }
        #endregion

        public void dibuja(object f)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    fhmi = f as hmi;
                    
                }));
            }
        }

    }
}
