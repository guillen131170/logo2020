﻿using logo2020.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.PRESENTATION
{
    public class Hilos
    {
        #region Atributos
        /* Hilo para Conexión */
        private Thread hilo;

        /* PLC del hilo */
        private ProcesosConLogo proceso;
        //private hmi h;
        private Form1 f;
        private int[] array = new int[512];
        #endregion


        #region /* Constructores de Hilo */
        public Hilos(ProcesosConLogo _proceso, Form1 _form)
        {
            f = _form;
            //f.AbrirHMI(new hmi());
            /*
            if (_form.panel.Controls.Count > 0)
                _form.panel.Controls.RemoveAt(0);
            form = new hmi() as hmi;
            form.TopLevel = false;
            form.Dock = DockStyle.Fill;
            _form.panel.Controls.Add(form);
            _form.panel.Tag = form;
            form.Show();
            */

            //form = _form.AbrirHMI(new hmi());

            proceso = new ProcesosConLogo(_proceso);
            for (int i = 0; i < 512; i++)
            {
                array[i] = 255;
            }

            /* Delegado */
            ThreadStart delegado = new ThreadStart(CorrerProceso);
            /* Hilo */
            hilo = new Thread(delegado);
            //Iniciamos el hilo 
            hilo.Start();
        }
        #endregion


        #region Proceso del hilo
        private void CorrerProceso()
        {
            #region /*datos del prodeso del hilo*/
            #endregion

            /*
            if (f.panel.Controls.Count > 0)
                f.panel.Controls.RemoveAt(0);
            form = new hmi() as hmi;
            form.TopLevel = false;
            form.Dock = DockStyle.Fill;
            f.AgregarHMI(form);
            //f.panel.Controls.Add(form);
            f.panel.Tag = form;
            //form.Show();
            */
            hmi h = new hmi();
            f.AbrirHMI(h);
            f.AgregarHMI(h);


            /* El hilo estará abierto hasta que cambie el
             * valor Conectado del nodo desde el formulario principal */
            while (proceso.EstaConectado()) /* Comprueba conexión manual del ndo desde el formulario */
            {

                proceso.LeerBloque512();
                
                /*
                #region comprueba - electroválvulas 1,2 y 3
                if (array[214] != proceso.buffer512[214] ||
                    array[215] != proceso.buffer512[215] ||
                    array[216] != proceso.buffer512[216])
                {
                    form.dibujaEVA123();
                    array[214] = proceso.buffer512[214];
                    array[215] = proceso.buffer512[215];
                    array[216] = proceso.buffer512[216];
                }
                #endregion

                #region comprueba - electroválvulas 11,12 y 13
                if (array[224] != proceso.buffer512[224] ||
                    array[225] != proceso.buffer512[225] ||
                    array[226] != proceso.buffer512[226])
                {
                    form.dibujaEVA111213();
                    array[224] = proceso.buffer512[224];
                    array[225] = proceso.buffer512[225];
                    array[226] = proceso.buffer512[226];
                }
                #endregion

                #region comprueba - electroválvulas 21,22 y 23
                if (array[234] != proceso.buffer512[234] ||
                    array[235] != proceso.buffer512[235] ||
                    array[236] != proceso.buffer512[236])
                {
                    form.dibujaEVA212223();
                    array[234] = proceso.buffer512[234];
                    array[235] = proceso.buffer512[235];
                    array[236] = proceso.buffer512[236];
                }
                #endregion

                #region comprueba - electroválvulas 5
                if (array[310] != proceso.buffer512[310])
                {
                    form.dibujaEVA5();
                    array[310] = proceso.buffer512[310];
                }
                #endregion

                #region comprueba - electroválvulas 15
                if (array[320] != proceso.buffer512[320])
                {
                    form.dibujaEVA15();
                    array[320] = proceso.buffer512[320];
                }
                #endregion

                #region comprueba - electroválvulas 25
                if (array[330] != proceso.buffer512[330])
                {
                    form.dibujaEVA25();
                    array[330] = proceso.buffer512[330];
                }
                #endregion

                #region comprueba - detectores1
                if (array[210] != proceso.buffer512[210])
                {
                    form.dibujaDetector1();
                    array[210] = proceso.buffer512[210];
                }
                #endregion

                #region comprueba - detectores2
                if (array[220] != proceso.buffer512[220])
                {
                    form.dibujaDetector2();
                    array[220] = proceso.buffer512[220];
                }
                #endregion

                #region comprueba - detectores3
                if (array[230] != proceso.buffer512[230])
                {
                    form.dibujaDetector3();
                    array[230] = proceso.buffer512[230];
                }
                #endregion

                #region comprueba - alarma co2
                if (array[208] != proceso.buffer512[208])
                {
                    form.dibujaAlarmaCO2();
                    array[208] = proceso.buffer512[208];
                }
                #endregion

                #region comprueba - alarma ca
                if (array[255] != proceso.buffer512[255])
                {
                    form.dibujaAlarmaCA();
                    array[255] = proceso.buffer512[255];
                }
                #endregion

                #region comprueba - alarma puerta
                if (array[209] != proceso.buffer512[209])
                {
                    form.dibujaAlarmaPuerta();
                    array[209] = proceso.buffer512[209];
                }
                #endregion

                #region comprueba - alarma fusible
                if (array[250] != proceso.buffer512[250])
                {
                    form.dibujaAlarmaFusible();
                    array[250] = proceso.buffer512[250];
                }
                #endregion

                form.dibujaLitros();
                */
                Thread.Sleep(1000);

            }
            //f.EliminarHMI(form);
            hilo.Abort();
        }
        #endregion
    }
}
