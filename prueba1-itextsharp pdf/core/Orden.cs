﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core
{
    public class Orden
    {
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #region ATRIBUTOS
        private int id;
        private string cabecera;
        private string cliente;
        private string codcliente;
        private string pds;
        private string codpds;
        private string tipoorden;
        private string ods1;
        private string ods2;
        private string noseriado;
        private int unidades;
        private string seriado;
        private string material;
        private string ns;
        private string sap;
        private string ot;
        private string oferta;
        private float precio;
        #endregion
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #region CONSTRUCTORES
        public Orden() { } 
        #endregion
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #region PROPIEDADES
        public int Id { get => id; set => id = value; }
        public string Cabecera { get => cabecera; set => cabecera = value; }
        public string Cliente { get => cliente; set => cliente = value; }
        public string Codcliente { get => codcliente; set => codcliente = value; }
        public string Pds { get => pds; set => pds = value; }
        public string Codpds { get => codpds; set => codpds = value; }
        public string Tipoorden { get => tipoorden; set => tipoorden = value; }
        public string Ods1 { get => ods1; set => ods1 = value; }
        public string Ods2 { get => ods2; set => ods2 = value; }
        public string Noseriado { get => noseriado; set => noseriado = value; }
        public int Unidades { get => unidades; set => unidades = value; }
        public string Seriado { get => seriado; set => seriado = value; }
        public string Material { get => material; set => material = value; }
        public string Ns { get => ns; set => ns = value; }
        public string Sap { get => sap; set => sap = value; }
        public string Ot { get => ot; set => ot = value; }
        public string Oferta { get => oferta; set => oferta = value; }
        public float Precio { get => precio; set => precio = value; }
        #endregion
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    }
}
