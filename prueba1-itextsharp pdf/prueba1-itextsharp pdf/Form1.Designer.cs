﻿namespace prueba1_itextsharp_pdf
{
    partial class v
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.enviar = new System.Windows.Forms.Button();
            this.cabecera = new System.Windows.Forms.TextBox();
            this.cliente = new System.Windows.Forms.TextBox();
            this.pds = new System.Windows.Forms.TextBox();
            this.codcliente = new System.Windows.Forms.TextBox();
            this.material = new System.Windows.Forms.TextBox();
            this.seriado = new System.Windows.Forms.TextBox();
            this.noseriado = new System.Windows.Forms.TextBox();
            this.codpds = new System.Windows.Forms.TextBox();
            this.precio = new System.Windows.Forms.TextBox();
            this.oferta = new System.Windows.Forms.TextBox();
            this.ot = new System.Windows.Forms.TextBox();
            this.sap = new System.Windows.Forms.TextBox();
            this.ns = new System.Windows.Forms.TextBox();
            this.ods1 = new System.Windows.Forms.TextBox();
            this.ods2 = new System.Windows.Forms.TextBox();
            this.tipoorden = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.unidades = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.c1 = new System.Windows.Forms.TextBox();
            this.c2 = new System.Windows.Forms.TextBox();
            this.c4 = new System.Windows.Forms.TextBox();
            this.c3 = new System.Windows.Forms.TextBox();
            this.c6 = new System.Windows.Forms.TextBox();
            this.c5 = new System.Windows.Forms.TextBox();
            this.c8 = new System.Windows.Forms.TextBox();
            this.c7 = new System.Windows.Forms.TextBox();
            this.d1 = new System.Windows.Forms.TextBox();
            this.d2 = new System.Windows.Forms.TextBox();
            this.d4 = new System.Windows.Forms.TextBox();
            this.d3 = new System.Windows.Forms.TextBox();
            this.d6 = new System.Windows.Forms.TextBox();
            this.d5 = new System.Windows.Forms.TextBox();
            this.d8 = new System.Windows.Forms.TextBox();
            this.d7 = new System.Windows.Forms.TextBox();
            this.d9 = new System.Windows.Forms.TextBox();
            this.c9 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // enviar
            // 
            this.enviar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enviar.Location = new System.Drawing.Point(451, 431);
            this.enviar.Name = "enviar";
            this.enviar.Size = new System.Drawing.Size(414, 41);
            this.enviar.TabIndex = 0;
            this.enviar.Text = "ENVIAR DATOS";
            this.enviar.UseVisualStyleBackColor = true;
            this.enviar.Click += new System.EventHandler(this.enviar_Click);
            // 
            // cabecera
            // 
            this.cabecera.Location = new System.Drawing.Point(215, 29);
            this.cabecera.Name = "cabecera";
            this.cabecera.Size = new System.Drawing.Size(464, 20);
            this.cabecera.TabIndex = 1;
            // 
            // cliente
            // 
            this.cliente.Location = new System.Drawing.Point(215, 55);
            this.cliente.Name = "cliente";
            this.cliente.Size = new System.Drawing.Size(272, 20);
            this.cliente.TabIndex = 2;
            // 
            // pds
            // 
            this.pds.Location = new System.Drawing.Point(215, 81);
            this.pds.Name = "pds";
            this.pds.Size = new System.Drawing.Size(272, 20);
            this.pds.TabIndex = 4;
            // 
            // codcliente
            // 
            this.codcliente.Location = new System.Drawing.Point(577, 55);
            this.codcliente.Name = "codcliente";
            this.codcliente.Size = new System.Drawing.Size(102, 20);
            this.codcliente.TabIndex = 3;
            // 
            // material
            // 
            this.material.Location = new System.Drawing.Point(215, 159);
            this.material.Name = "material";
            this.material.Size = new System.Drawing.Size(102, 20);
            this.material.TabIndex = 8;
            // 
            // seriado
            // 
            this.seriado.Location = new System.Drawing.Point(215, 133);
            this.seriado.Name = "seriado";
            this.seriado.Size = new System.Drawing.Size(464, 20);
            this.seriado.TabIndex = 7;
            // 
            // noseriado
            // 
            this.noseriado.Location = new System.Drawing.Point(215, 107);
            this.noseriado.Name = "noseriado";
            this.noseriado.Size = new System.Drawing.Size(272, 20);
            this.noseriado.TabIndex = 6;
            // 
            // codpds
            // 
            this.codpds.Location = new System.Drawing.Point(577, 81);
            this.codpds.Name = "codpds";
            this.codpds.Size = new System.Drawing.Size(102, 20);
            this.codpds.TabIndex = 5;
            // 
            // precio
            // 
            this.precio.Location = new System.Drawing.Point(215, 368);
            this.precio.Name = "precio";
            this.precio.Size = new System.Drawing.Size(102, 20);
            this.precio.TabIndex = 13;
            // 
            // oferta
            // 
            this.oferta.Location = new System.Drawing.Point(215, 342);
            this.oferta.Name = "oferta";
            this.oferta.Size = new System.Drawing.Size(181, 20);
            this.oferta.TabIndex = 12;
            // 
            // ot
            // 
            this.ot.Location = new System.Drawing.Point(215, 316);
            this.ot.Name = "ot";
            this.ot.Size = new System.Drawing.Size(181, 20);
            this.ot.TabIndex = 11;
            // 
            // sap
            // 
            this.sap.Location = new System.Drawing.Point(215, 211);
            this.sap.Name = "sap";
            this.sap.Size = new System.Drawing.Size(230, 20);
            this.sap.TabIndex = 10;
            // 
            // ns
            // 
            this.ns.Location = new System.Drawing.Point(215, 185);
            this.ns.Name = "ns";
            this.ns.Size = new System.Drawing.Size(230, 20);
            this.ns.TabIndex = 9;
            // 
            // ods1
            // 
            this.ods1.Location = new System.Drawing.Point(215, 264);
            this.ods1.Name = "ods1";
            this.ods1.Size = new System.Drawing.Size(181, 20);
            this.ods1.TabIndex = 14;
            // 
            // ods2
            // 
            this.ods2.Location = new System.Drawing.Point(215, 290);
            this.ods2.Name = "ods2";
            this.ods2.Size = new System.Drawing.Size(181, 20);
            this.ods2.TabIndex = 15;
            // 
            // tipoorden
            // 
            this.tipoorden.FormattingEnabled = true;
            this.tipoorden.Items.AddRange(new object[] {
            "AVERÍA",
            "LIMPIEZA",
            "PREVENTIVO",
            "MONTAJE",
            "DESMONTAJE",
            "CAMBIO"});
            this.tipoorden.Location = new System.Drawing.Point(215, 237);
            this.tipoorden.Name = "tipoorden";
            this.tipoorden.Size = new System.Drawing.Size(181, 21);
            this.tipoorden.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(145, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "CABECERA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(143, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "PROYECTO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(180, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "PDS";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(493, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "COD CLIENTE";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(135, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "NO SERIADO";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(170, 267);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "ODS 1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(135, 240);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "TIPO ORDEN";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(516, 84);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "COD PDS";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(162, 371);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "PRECIO";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(159, 345);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 31;
            this.label10.Text = "OFERTA";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(119, 319);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "ORDEN TALLER";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(181, 214);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "SAP";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(187, 188);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(22, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "NS";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(148, 162);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "MATERIAL";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(154, 136);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 26;
            this.label15.Text = "SERIADO";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(170, 293);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "ODS 2";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(508, 114);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(63, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "UNIDADES";
            // 
            // unidades
            // 
            this.unidades.Location = new System.Drawing.Point(577, 107);
            this.unidades.Name = "unidades";
            this.unidades.Size = new System.Drawing.Size(102, 20);
            this.unidades.TabIndex = 33;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(17, 4);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "CÓDIGO";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(122, 4);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(80, 13);
            this.label19.TabIndex = 35;
            this.label19.Text = "DESCRIPCIÓN";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.d9);
            this.panel1.Controls.Add(this.c9);
            this.panel1.Controls.Add(this.d8);
            this.panel1.Controls.Add(this.d7);
            this.panel1.Controls.Add(this.d6);
            this.panel1.Controls.Add(this.d5);
            this.panel1.Controls.Add(this.d4);
            this.panel1.Controls.Add(this.d3);
            this.panel1.Controls.Add(this.d2);
            this.panel1.Controls.Add(this.d1);
            this.panel1.Controls.Add(this.c8);
            this.panel1.Controls.Add(this.c7);
            this.panel1.Controls.Add(this.c6);
            this.panel1.Controls.Add(this.c5);
            this.panel1.Controls.Add(this.c4);
            this.panel1.Controls.Add(this.c3);
            this.panel1.Controls.Add(this.c2);
            this.panel1.Controls.Add(this.c1);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Location = new System.Drawing.Point(451, 159);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(414, 264);
            this.panel1.TabIndex = 37;
            // 
            // c1
            // 
            this.c1.Location = new System.Drawing.Point(10, 25);
            this.c1.Name = "c1";
            this.c1.Size = new System.Drawing.Size(102, 20);
            this.c1.TabIndex = 37;
            // 
            // c2
            // 
            this.c2.Location = new System.Drawing.Point(10, 51);
            this.c2.Name = "c2";
            this.c2.Size = new System.Drawing.Size(102, 20);
            this.c2.TabIndex = 38;
            // 
            // c4
            // 
            this.c4.Location = new System.Drawing.Point(10, 103);
            this.c4.Name = "c4";
            this.c4.Size = new System.Drawing.Size(102, 20);
            this.c4.TabIndex = 40;
            // 
            // c3
            // 
            this.c3.Location = new System.Drawing.Point(10, 77);
            this.c3.Name = "c3";
            this.c3.Size = new System.Drawing.Size(102, 20);
            this.c3.TabIndex = 39;
            // 
            // c6
            // 
            this.c6.Location = new System.Drawing.Point(10, 155);
            this.c6.Name = "c6";
            this.c6.Size = new System.Drawing.Size(102, 20);
            this.c6.TabIndex = 42;
            // 
            // c5
            // 
            this.c5.Location = new System.Drawing.Point(10, 129);
            this.c5.Name = "c5";
            this.c5.Size = new System.Drawing.Size(102, 20);
            this.c5.TabIndex = 41;
            // 
            // c8
            // 
            this.c8.Location = new System.Drawing.Point(10, 207);
            this.c8.Name = "c8";
            this.c8.Size = new System.Drawing.Size(102, 20);
            this.c8.TabIndex = 44;
            // 
            // c7
            // 
            this.c7.Location = new System.Drawing.Point(10, 181);
            this.c7.Name = "c7";
            this.c7.Size = new System.Drawing.Size(102, 20);
            this.c7.TabIndex = 43;
            // 
            // d1
            // 
            this.d1.Location = new System.Drawing.Point(118, 25);
            this.d1.Name = "d1";
            this.d1.Size = new System.Drawing.Size(284, 20);
            this.d1.TabIndex = 45;
            // 
            // d2
            // 
            this.d2.Location = new System.Drawing.Point(118, 51);
            this.d2.Name = "d2";
            this.d2.Size = new System.Drawing.Size(284, 20);
            this.d2.TabIndex = 46;
            // 
            // d4
            // 
            this.d4.Location = new System.Drawing.Point(118, 103);
            this.d4.Name = "d4";
            this.d4.Size = new System.Drawing.Size(284, 20);
            this.d4.TabIndex = 48;
            // 
            // d3
            // 
            this.d3.Location = new System.Drawing.Point(118, 77);
            this.d3.Name = "d3";
            this.d3.Size = new System.Drawing.Size(284, 20);
            this.d3.TabIndex = 47;
            // 
            // d6
            // 
            this.d6.Location = new System.Drawing.Point(118, 155);
            this.d6.Name = "d6";
            this.d6.Size = new System.Drawing.Size(284, 20);
            this.d6.TabIndex = 50;
            // 
            // d5
            // 
            this.d5.Location = new System.Drawing.Point(118, 129);
            this.d5.Name = "d5";
            this.d5.Size = new System.Drawing.Size(284, 20);
            this.d5.TabIndex = 49;
            // 
            // d8
            // 
            this.d8.Location = new System.Drawing.Point(118, 207);
            this.d8.Name = "d8";
            this.d8.Size = new System.Drawing.Size(284, 20);
            this.d8.TabIndex = 52;
            // 
            // d7
            // 
            this.d7.Location = new System.Drawing.Point(118, 181);
            this.d7.Name = "d7";
            this.d7.Size = new System.Drawing.Size(284, 20);
            this.d7.TabIndex = 51;
            // 
            // d9
            // 
            this.d9.Location = new System.Drawing.Point(118, 233);
            this.d9.Name = "d9";
            this.d9.Size = new System.Drawing.Size(284, 20);
            this.d9.TabIndex = 54;
            // 
            // c9
            // 
            this.c9.Location = new System.Drawing.Point(10, 233);
            this.c9.Name = "c9";
            this.c9.Size = new System.Drawing.Size(102, 20);
            this.c9.TabIndex = 53;
            // 
            // v
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 500);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.unidades);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tipoorden);
            this.Controls.Add(this.ods2);
            this.Controls.Add(this.ods1);
            this.Controls.Add(this.precio);
            this.Controls.Add(this.oferta);
            this.Controls.Add(this.ot);
            this.Controls.Add(this.sap);
            this.Controls.Add(this.ns);
            this.Controls.Add(this.material);
            this.Controls.Add(this.seriado);
            this.Controls.Add(this.noseriado);
            this.Controls.Add(this.codpds);
            this.Controls.Add(this.pds);
            this.Controls.Add(this.codcliente);
            this.Controls.Add(this.cliente);
            this.Controls.Add(this.cabecera);
            this.Controls.Add(this.enviar);
            this.Name = "v";
            this.Text = "ORDEN TALLER";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button enviar;
        private System.Windows.Forms.TextBox cabecera;
        private System.Windows.Forms.TextBox cliente;
        private System.Windows.Forms.TextBox pds;
        private System.Windows.Forms.TextBox codcliente;
        private System.Windows.Forms.TextBox material;
        private System.Windows.Forms.TextBox seriado;
        private System.Windows.Forms.TextBox noseriado;
        private System.Windows.Forms.TextBox codpds;
        private System.Windows.Forms.TextBox precio;
        private System.Windows.Forms.TextBox oferta;
        private System.Windows.Forms.TextBox ot;
        private System.Windows.Forms.TextBox sap;
        private System.Windows.Forms.TextBox ns;
        private System.Windows.Forms.TextBox ods1;
        private System.Windows.Forms.TextBox ods2;
        private System.Windows.Forms.ComboBox tipoorden;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox unidades;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox d9;
        private System.Windows.Forms.TextBox c9;
        private System.Windows.Forms.TextBox d8;
        private System.Windows.Forms.TextBox d7;
        private System.Windows.Forms.TextBox d6;
        private System.Windows.Forms.TextBox d5;
        private System.Windows.Forms.TextBox d4;
        private System.Windows.Forms.TextBox d3;
        private System.Windows.Forms.TextBox d2;
        private System.Windows.Forms.TextBox d1;
        private System.Windows.Forms.TextBox c8;
        private System.Windows.Forms.TextBox c7;
        private System.Windows.Forms.TextBox c6;
        private System.Windows.Forms.TextBox c5;
        private System.Windows.Forms.TextBox c4;
        private System.Windows.Forms.TextBox c3;
        private System.Windows.Forms.TextBox c2;
        private System.Windows.Forms.TextBox c1;
    }
}

