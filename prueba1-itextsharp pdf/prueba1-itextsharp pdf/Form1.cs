﻿using core;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static iTextSharp.text.pdf.PdfCopy;

namespace prueba1_itextsharp_pdf
{
    public partial class v : Form
    {
        /*
         * Para añadir iTextSharp:
         * Dentro de nuestro proyecto
         * En Herramientas ...
         * Dentro del Administrador de paquetes NuGet ...
         * Click en Consola del administrador de paquetes NuGet
         * Insertar comando: Install-Package iTextSharp -Version 5.5.13
         * Esperar que se instale.
         */
        /*
            iTextSharp.text.pdf.PdfPTable aTable = new iTextSharp.text.pdf.PdfPTable(2);
            aTable.AddCell("0.0");
            aTable.AddCell("0.1");
            aTable.AddCell("1.0");
            aTable.AddCell("1.1");
            document.Add(aTable);

            document.Add(new Paragraph("ORDEN NÚMERO " + "ID" + ' ' + "ESTADO" + ' ' + "RESULTADO"));
            document.Add(new Paragraph("NOMBRECLIENTE" + ' ' + "CÓDIGOCLIENTE"));
            document.Add(new Paragraph(" "));
            document.Add(new Paragraph(" "));
            */
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        private Orden orden;
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #region CARGAR COMPONENTES E INICIAR FORMULARIO
        public v()
        {
            InitializeComponent();
        }
        #endregion
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #region PROPIEDADES
        public Orden OT { get => orden; set => orden = value; }
        #endregion
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #region CONTROLES
        #region ENVIAR
        /// <summary>
        /// ENVIAR Y GRABAR DATOS
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void enviar_Click(object sender, EventArgs e)
        {
            Document document = new Document();
            PdfWriter.GetInstance(document,

                          new FileStream("devjoker.pdf",
                                 FileMode.OpenOrCreate));

            document.Open();

            iTextSharp.text.pdf.PdfPTable tabla = new iTextSharp.text.pdf.PdfPTable(1);
            tabla.AddCell("CABECERA");
            tabla.AddCell("ORDEN NÚMERO " + "ID");
            tabla.AddCell("NOMBRECLIENTE" + ' ' + "CÓDIGOCLIENTE");
            document.Add(tabla);
            document.Add(new Paragraph(" "));

            tabla = new iTextSharp.text.pdf.PdfPTable(1);
            tabla.AddCell("PDS/PUNTO DE SERVICIO");
            tabla.AddCell("NOMBREPDS" + ' ' + "CÓDIGOPDS");
            document.Add(tabla);
            document.Add(new Paragraph(" "));


            tabla = new iTextSharp.text.pdf.PdfPTable(1);
            tabla.AddCell("ORDEN ORIGINARIA");
            tabla.AddCell("INCIDENCIA " + "ODSI");
            tabla.AddCell("MONTAJE    " + "ODSM");
            tabla.AddCell("DESMONTAJE " + "ODSD");
            tabla.AddCell("PREVENTIVO " + "ODSI");
            tabla.AddCell("LIMPIEZA " + "ODSI");
            tabla.AddCell("TALLER     " + "ODST");
            document.Add(tabla);
            document.Add(new Paragraph(" "));

            tabla = new iTextSharp.text.pdf.PdfPTable(1);
            tabla.AddCell("EQUIPO/OBJETO");
            tabla.AddCell("NO SERIADO: " + "OBJETO2");
            tabla.AddCell("SERIADO: " + "OBJETO1");
            tabla.AddCell("MATERIAL " + "CÓDIGOMATERIAL");
            tabla.AddCell("NÚMERO SERIE " + "NS");
            tabla.AddCell("NÚMERO EQUIPO SAP " + "SAP");
            document.Add(tabla);
            document.Add(new Paragraph(" "));

            tabla = new iTextSharp.text.pdf.PdfPTable(1);
            tabla.AddCell("ORDEN TALLER");
            tabla.AddCell("ORDEN TALLER  " + "OT");
            tabla.AddCell("OFERTA        " + "O");
            tabla.AddCell("PRESUPUESTO   " + "P");
            document.Add(tabla);

            document.Close();
        }  
        #endregion
        #endregion
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    }
}
