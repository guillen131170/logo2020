﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS60Valladolid.CORE
{
    public class clsUsuarios
    {
        #region ATRIBUTOS
        private int id;
        private string username;
        private string pass;
        private int accesslevel;
        private string email;
        #endregion


        #region DESCRIPTORES DE ACCESO
        public int Id { get; set; }
        public string Username { get; set; }
        public string Pass { get; set; }
        public int Accesslevel { get; set; }
        public string Email { get; set; } 
        #endregion
    }
}
