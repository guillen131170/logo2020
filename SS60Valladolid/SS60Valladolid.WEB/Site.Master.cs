﻿using SS60Valladolid.APPLICATION;
using SS60Valladolid.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SS60Valladolid.WEB
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Obtiene el carrito de compras (si existe)
                //dto_CarritoCompras carritoDeUsuario = Session["Carrito"] as dto_CarritoCompras;
                clsUsuarios usuarioConSesion = Session["Usuario"] as clsUsuarios;
                /*Se verifica que no se esté realizando una doble solicitud*/
                if (!IsPostBack)
                {
                    if (Page.User.Identity.IsAuthenticated || Session["Usuario"] != null)
                    {
                        //Llena la variable de sesión con la información del usuario
                        /*
                        if (Session["Usuario"] == null)
                        {
                            ProcUsuarios gestion = new ProcUsuarios();
                            usuarioConSesion = gestion.obtieneUsuario(Page.User.Identity.Name);
                            Session["Usuario"] = usuarioConSesion;
                        }
                        else
                        {
                            usuarioConSesion = Session["Usuario"] as clsUsuarios;
                        }
                        */

                        //Cuando el usuario está logeado oculta o muestra unos u otros menus
                        sinregistro.Visible = false;
                        comun.Visible = false;
                        //usuario.Visible = LICarrito.Visible = usuarioConSesion.Id_rol == 1;
                        if (usuarioConSesion.Accesslevel == 1)
                        {
                            usuario.Visible = true;
                            administrador.Visible = false;
                        }
                        else if (usuarioConSesion.Accesslevel == 131170)
                        {
                            usuario.Visible = false;
                            administrador.Visible = true;
                            NumCarrito.Visible = true;
                        }
                        //LICarrito.Visible = true;
                        //usuarioConSesion.Id_rol == 1;
                        //administrador.Visible = usuarioConSesion.Id_rol == 2;
                        //usuarioConSesion.Id_rol 1= 2;
                    }
                    else
                    // para que te registres
                    {
                        sinregistro.Visible = true;
                        comun.Visible = usuario.Visible = administrador.Visible = false;
                        NumCarrito.Visible = true;
                        //LICarrito.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                statusMaster.Text = ex.Message;
                statusMasterdiv.Attributes["class"] = "alert alert-danger";
            }
        }
    }
}