﻿using SS60Valladolid.APPLICATION;
using SS60Valladolid.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SS60Valladolid.WEB
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Se verifica que no se esté realizando una doble solicitud
                if (!IsPostBack)
                {
                    //Si el usuario está autenticado muestra el mensaje y oculta el formulario
                    if (User.Identity.IsAuthenticated)
                    {
                        //FailureText.Text = "El Usuario " + User.Identity.GetUserName() + " ha iniciado sesión exitosamente";
                        //ErrorMessage.Visible = true;
                        RegisterHyperLink.NavigateUrl = "Register";
                        RegisterHyperLink.Text = ": " + Email.Text;
                        logeo.Visible = false;
                    }
                    else
                        logeo.Visible = true;
                }
            }
            catch (Exception ex)
            {
                FailureText.Text = ex + " - Error en inicio de sesión";
                ErrorMessage.Visible = true;
            }
        }


        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                clsUsuarios usuarioConSesion = new clsUsuarios();
                ProcUsuarios proceso = new ProcUsuarios();
                var result = proceso.identificaUsuario(Email.Text, Password.Text);

                if (result != 0)
                {
                    usuarioConSesion = proceso.obtieneUsuario(Page.User.Identity.Name);
                    Session["Usuario"] = usuarioConSesion;
                }
                /*
                // Validate the user password
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                // This doen't count login failures towards account lockout
                // To enable password failures to trigger lockout, change to shouldLockout: true
                var result = signinManager.PasswordSignIn(Email.Text, Password.Text, RememberMe.Checked, shouldLockout: false);
                */
                switch (result)
                {
                    case 131170:                       
                        //Response.Redirect("/pgWEB/SolicitudODSPeriodo");
                        Response.Redirect("Inicio");                 
                        break;
                    default:
                        FailureText.Text = "Invalid login attempt";
                        ErrorMessage.Visible = true;
                        break;
/*
                    case SignInStatus.Success:
                        Response.Redirect("/Account/Login");
                        break;
                    case SignInStatus.Failure:
                    default:
                        FailureText.Text = "Invalid login attempt";
                        ErrorMessage.Visible = true;
                        break;
                        */
                }

            }
        }
    }
}