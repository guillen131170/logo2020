﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SS60Valladolid.CORE;
using SS60Valladolid.DAL;

namespace SS60Valladolid.APPLICATION
{
    public class ProcUsuarios
    {
        #region Constructores de la clase
        /// <summary>
        /// <param name=""></param>
        /// </summary>
        public ProcUsuarios()  { }
        #endregion


        #region IDENTIFICACION DE USUARIOS
        public int identificaUsuario(string email, string pass)
        {
            UsuariosSQL accesoDatos = new UsuariosSQL();
            return accesoDatos.indentificar(email, pass);
        }
        #endregion


        #region OBTENER UN USUARIO POR SU NOMBRE
        public clsUsuarios obtieneUsuario(string name)
        {
            UsuariosSQL accesoDatos = new UsuariosSQL();
            return accesoDatos.ObtenerUnoParamUsername(name);
        }
        #endregion
    }
}
