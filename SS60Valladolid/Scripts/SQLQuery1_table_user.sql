USE catalogo
GO
 
/****** Object:  Table [catalogo].[usuarios]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE usuarios(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	username [nvarchar](20) NOT NULL,
	pass [nvarchar](8) NOT NULL,
	accesslevel int NOT NULL,
	email [nvarchar](60) NOT NULL
	)

GO