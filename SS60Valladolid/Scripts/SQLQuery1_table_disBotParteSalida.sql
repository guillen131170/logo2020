USE catalogo
GO
 
/****** Object:  Table [catalogo].[disBotParteSalida]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE disBotParteSalida(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	fecha [int] NOT NULL,
	ns [nvarchar](1024) NOT NULL,
	destino [nvarchar](60) NOT NULL,
	origen [nvarchar](60) NOT NULL,
	transportista [nvarchar](60) NOT NULL,
	cantidad [int] NOT NULL
	)

GO