USE catalogo
GO
 
/****** Object:  Table [catalogo].[otaller]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE otaller(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	tecnico [nvarchar](60) NULL,
	estado [nvarchar](10) NOT NULL DEFAULT 'ABIERTO',
	resultado [nvarchar](12) NOT NULL DEFAULT 'ABIERTO',
	proyecto [nvarchar](12) NOT NULL,
	pds [nvarchar](12) NULL,
	f_trabajo [int] NOT NULL,
	f_repa [int] NOT NULL,
	cliente [nvarchar](60) NULL,
	provincia [nvarchar](60) NULL,
	oaveria [nvarchar](15) NULL,
	omontaje [nvarchar](15) NULL,
	odesmontaje [nvarchar](15) NULL,
	otaller [nvarchar](15) NULL,
	material [nvarchar](80) NOT NULL,
	codmaterial [nvarchar](12) NOT NULL,
	ax [nvarchar](30) NULL,
	sap [nvarchar](30) NULL,
	norden [nvarchar](15) NOT NULL,
	oferta [float] NOT NULL,
	despl [float] NULL,
	mo [float] NULL,
	repuesto [nvarchar](1024) NOT NULL,
	cmi [nvarchar](2) NULL,
	)

GO