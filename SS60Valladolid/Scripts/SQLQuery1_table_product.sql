USE catalogo
GO
 
/****** Object:  Table [catalogo].[producto]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE producto(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	code [nvarchar](20) NOT NULL,
	name [nvarchar](60) NOT NULL,
	description [nvarchar](240) NULL,
	price [float] NOT NULL DEFAULT 0,
	stock [int] NOT NULL DEFAULT 0,
	family [nvarchar](60) NOT NULL,
    machine [nvarchar](120) NOT NULL,
	condition [nvarchar](12) NOT NULL,
	active int NOT NULL DEFAULT 1,
	img [nvarchar](240) NULL
	)

GO