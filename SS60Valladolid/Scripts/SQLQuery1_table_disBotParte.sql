USE catalogo
GO
 
/****** Object:  Table [catalogo].[disBotParte]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE disBotParte(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	f_repa [nvarchar](10) NULL,
	mo [nvarchar](240) NULL,
	origen [nvarchar](30) NOT NULL,
	ns [nvarchar](30) NOT NULL,
	fabricante [nvarchar](12) NOT NULL,
	marca [nvarchar](12) NULL,
	gas [nvarchar](10) NULL,
	modelo [nvarchar](60) NULL,
	estado [nvarchar](10) NOT NULL DEFAULT 'REPARADO',
	img [nvarchar](240) NULL,
	tecnico [nvarchar](60) NOT NULL,
	anyo [int] NULL
	)

GO