USE catalogo
GO
 
/****** Object:  Table [catalogo].[producto]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE entrada(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	product [nvarchar](60) NOT NULL,
	typein [nvarchar](20) NOT NULL,
	datein date NOT NULL,
	supplier [nvarchar](60) NOT NULL,
	units [int] NOT NULL DEFAULT 1
	)

GO