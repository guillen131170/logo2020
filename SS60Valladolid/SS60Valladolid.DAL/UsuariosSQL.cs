﻿using SS60Valladolid.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS60Valladolid.DAL
{
    public class UsuariosSQL : ConnectionSQL
    {
        #region IDENTIFICACION DE USUARIOS
        /// <summary>
        /// Identifica a un usuario
        /// de la base de datos por su email y password
        /// </summary>
        /// <param name="email">direccion de correo electrónico del usuario</param>
        /// <param name="pass">contraseña del usuario</param>
        /// <returns>true SI EXISTE o false si NO EXISTE</returns>
        public int indentificar(string email, string pass)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "SELECT * " +
                "FROM [dbo].[usuarios] where email=@Email AND pass=@Pass";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            /*Utilizamos el valor del parámetro username para la consulta SQL*/
            command.Parameters.Add(new SqlParameter("Email", email));
            command.Parameters.Add(new SqlParameter("Pass", pass));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    resultado = Convert.ToInt32(dr["accesslevel"]);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region OBTENER UN USUARIO POR SU NOMBRE
        /// <summary>
        /// Devuelve la información de un usuario
        /// de la base de datos por su username
        /// <param name="username">objeto del tipo string que tiene
        /// el nombre del usuario que se va a buscar</param>
        /// </summary>
        public clsUsuarios ObtenerUnoParamUsername(string username)
        {
            /*Declaramos un objeto de tipo clsUsuario que guardará la información 
            del usuario de la base de datos, y posteriormente devolverá el objeto
            cuando finalize la función*/
            clsUsuarios usuario = new clsUsuarios();

            /*Cadena para la consulta sql a la base de datos*/
            string query =
                    "SELECT * " +
                    "FROM [dbo].[Usuarios] where username like @username";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            /*Utilizamos el valor del parámetro username para la consulta SQL*/
            command.Parameters.Add(new SqlParameter("username", username));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                DataRow dr = ds.Tables[0].Rows[0];
                //Instanciamos al objeto clsUsuario para llenar sus propiedades
                usuario = new clsUsuarios()
                {
                    Id = Convert.ToInt32(dr["id"]),
                    Username = Convert.ToString(dr["username"]),
                    Pass = Convert.ToString(dr["pass"]),
                    Accesslevel = Convert.ToInt32(dr["accesslevel"]),
                    Email = Convert.ToString(dr["email"])
                };
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                Connection.Close();
            }
            return usuario;
        } 
        #endregion
    }
}
