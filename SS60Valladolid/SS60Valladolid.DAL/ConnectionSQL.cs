﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS60Valladolid.DAL
{
    public class ConnectionSQL
    {
        #region ATRIBUTOS
        private string connectionString;
        private SqlConnection connection; 
        #endregion


        #region CONSTRUCTORES
        public ConnectionSQL()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.ResourceDAL.ConnectionStringSQLLocal;
            Connection = new SqlConnection(ConnectionString);
        }
        #endregion


        #region METODOS
        #region ABRIR CONEXION
        public void abrirConexion()
        {
            try
            {
                Connection.Open();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region CERRAR CONEXION
        public void cerrarConexion()
        {
            try
            {
                Connection.Close();
            }
            catch
            {
                throw;
            }
        }  
        #endregion
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
