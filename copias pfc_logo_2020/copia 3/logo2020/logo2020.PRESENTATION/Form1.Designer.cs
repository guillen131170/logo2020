﻿namespace logo2020.PRESENTATION
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.cabecera = new System.Windows.Forms.Panel();
            this.textip = new System.Windows.Forms.TextBox();
            this.panelcabcentral = new System.Windows.Forms.Panel();
            this.pictureBoxBoton = new System.Windows.Forms.PictureBox();
            this.off = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.PanelConnect = new System.Windows.Forms.Panel();
            this.on = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.slotText = new System.Windows.Forms.TextBox();
            this.rackText = new System.Windows.Forms.TextBox();
            this.ipText = new System.Windows.Forms.TextBox();
            this.dnsText = new System.Windows.Forms.TextBox();
            this.labelonoff = new System.Windows.Forms.Label();
            this.pictureon = new System.Windows.Forms.PictureBox();
            this.panelsubcab = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.registro = new System.Windows.Forms.Label();
            this.acumulado = new System.Windows.Forms.Label();
            this.panelAlarmas = new System.Windows.Forms.Panel();
            this.picCO2 = new System.Windows.Forms.PictureBox();
            this.picFU = new System.Windows.Forms.PictureBox();
            this.picCA = new System.Windows.Forms.PictureBox();
            this.picPU = new System.Windows.Forms.PictureBox();
            this.labelFUresultado = new System.Windows.Forms.Label();
            this.labelCA = new System.Windows.Forms.Label();
            this.labelFU = new System.Windows.Forms.Label();
            this.labelGas = new System.Windows.Forms.Label();
            this.labelPUresultado = new System.Windows.Forms.Label();
            this.labelCO2resultado = new System.Windows.Forms.Label();
            this.labelPU = new System.Windows.Forms.Label();
            this.labelCAresultado = new System.Windows.Forms.Label();
            this.panel = new System.Windows.Forms.Panel();
            this.panelHMI = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.p31b3 = new System.Windows.Forms.Panel();
            this.p32b3 = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.p31b2 = new System.Windows.Forms.Panel();
            this.p32b2 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.p31b1 = new System.Windows.Forms.Panel();
            this.p32b1 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.subpanel3b = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.df3 = new System.Windows.Forms.PictureBox();
            this.FB_3 = new System.Windows.Forms.PictureBox();
            this.FA_3 = new System.Windows.Forms.PictureBox();
            this.v3_1 = new System.Windows.Forms.PictureBox();
            this.v3_2 = new System.Windows.Forms.PictureBox();
            this.v3_3 = new System.Windows.Forms.PictureBox();
            this.v3_paso = new System.Windows.Forms.PictureBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.subpanel3 = new System.Windows.Forms.Panel();
            this.run3 = new System.Windows.Forms.Button();
            this.stop3 = new System.Windows.Forms.Button();
            this.labelbombilla3 = new System.Windows.Forms.Label();
            this.picbombilla3 = new System.Windows.Forms.PictureBox();
            this.picparomarcha3 = new System.Windows.Forms.PictureBox();
            this.picB31 = new System.Windows.Forms.PictureBox();
            this.picB32 = new System.Windows.Forms.PictureBox();
            this.picB33 = new System.Windows.Forms.PictureBox();
            this.jump3 = new System.Windows.Forms.Button();
            this.refresh3 = new System.Windows.Forms.Button();
            this.b3_1 = new System.Windows.Forms.Label();
            this.b3_3 = new System.Windows.Forms.Label();
            this.b3_2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.p11b3 = new System.Windows.Forms.Panel();
            this.p12b3 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.p11b2 = new System.Windows.Forms.Panel();
            this.p12b2 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.p11b1 = new System.Windows.Forms.Panel();
            this.p12b1 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.subpanel1 = new System.Windows.Forms.Panel();
            this.picB13 = new System.Windows.Forms.PictureBox();
            this.picB12 = new System.Windows.Forms.PictureBox();
            this.picB11 = new System.Windows.Forms.PictureBox();
            this.labelbombilla1 = new System.Windows.Forms.Label();
            this.run1 = new System.Windows.Forms.Button();
            this.stop1 = new System.Windows.Forms.Button();
            this.picbombilla1 = new System.Windows.Forms.PictureBox();
            this.picparomarcha1 = new System.Windows.Forms.PictureBox();
            this.jump1 = new System.Windows.Forms.Button();
            this.refresh1 = new System.Windows.Forms.Button();
            this.subpanel1b = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.df1 = new System.Windows.Forms.PictureBox();
            this.FA_1 = new System.Windows.Forms.PictureBox();
            this.v1_1 = new System.Windows.Forms.PictureBox();
            this.FB_1 = new System.Windows.Forms.PictureBox();
            this.v1_2 = new System.Windows.Forms.PictureBox();
            this.v1_paso = new System.Windows.Forms.PictureBox();
            this.v1_3 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.b1_1 = new System.Windows.Forms.Label();
            this.b1_2 = new System.Windows.Forms.Label();
            this.b1_3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.p21b3 = new System.Windows.Forms.Panel();
            this.p22b3 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.p21b2 = new System.Windows.Forms.Panel();
            this.p22b2 = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.p21b1 = new System.Windows.Forms.Panel();
            this.p22b1 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.subpanel2b = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.df2 = new System.Windows.Forms.PictureBox();
            this.FB_2 = new System.Windows.Forms.PictureBox();
            this.FA_2 = new System.Windows.Forms.PictureBox();
            this.v2_1 = new System.Windows.Forms.PictureBox();
            this.v2_2 = new System.Windows.Forms.PictureBox();
            this.v2_3 = new System.Windows.Forms.PictureBox();
            this.v2_paso = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.subpanel2 = new System.Windows.Forms.Panel();
            this.run2 = new System.Windows.Forms.Button();
            this.stop2 = new System.Windows.Forms.Button();
            this.labelbombilla2 = new System.Windows.Forms.Label();
            this.picbombilla2 = new System.Windows.Forms.PictureBox();
            this.picparomarcha2 = new System.Windows.Forms.PictureBox();
            this.picB23 = new System.Windows.Forms.PictureBox();
            this.picB22 = new System.Windows.Forms.PictureBox();
            this.picB21 = new System.Windows.Forms.PictureBox();
            this.refresh2 = new System.Windows.Forms.Button();
            this.jump2 = new System.Windows.Forms.Button();
            this.b2_3 = new System.Windows.Forms.Label();
            this.b2_1 = new System.Windows.Forms.Label();
            this.b2_2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textDump = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cabecera.SuspendLayout();
            this.panelcabcentral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBoton)).BeginInit();
            this.PanelConnect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureon)).BeginInit();
            this.panelsubcab.SuspendLayout();
            this.panelAlarmas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCO2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPU)).BeginInit();
            this.panel.SuspendLayout();
            this.panelHMI.SuspendLayout();
            this.panel3.SuspendLayout();
            this.p31b3.SuspendLayout();
            this.p32b3.SuspendLayout();
            this.p31b2.SuspendLayout();
            this.p32b2.SuspendLayout();
            this.p31b1.SuspendLayout();
            this.p32b1.SuspendLayout();
            this.subpanel3b.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.df3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_paso)).BeginInit();
            this.subpanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB33)).BeginInit();
            this.panel1.SuspendLayout();
            this.p11b3.SuspendLayout();
            this.p12b3.SuspendLayout();
            this.p11b2.SuspendLayout();
            this.p12b2.SuspendLayout();
            this.p11b1.SuspendLayout();
            this.p12b1.SuspendLayout();
            this.subpanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picB13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha1)).BeginInit();
            this.subpanel1b.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.df1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_paso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_3)).BeginInit();
            this.panel2.SuspendLayout();
            this.p21b3.SuspendLayout();
            this.p22b3.SuspendLayout();
            this.p21b2.SuspendLayout();
            this.p22b2.SuspendLayout();
            this.p21b1.SuspendLayout();
            this.p22b1.SuspendLayout();
            this.subpanel2b.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.df2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_paso)).BeginInit();
            this.subpanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB21)).BeginInit();
            this.SuspendLayout();
            // 
            // cabecera
            // 
            this.cabecera.BackColor = System.Drawing.SystemColors.Control;
            this.cabecera.Controls.Add(this.textip);
            this.cabecera.Controls.Add(this.panelcabcentral);
            this.cabecera.Controls.Add(this.pictureon);
            this.cabecera.Location = new System.Drawing.Point(12, 12);
            this.cabecera.Name = "cabecera";
            this.cabecera.Size = new System.Drawing.Size(669, 161);
            this.cabecera.TabIndex = 0;
            // 
            // textip
            // 
            this.textip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(213)))), ((int)(((byte)(48)))));
            this.textip.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textip.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textip.ForeColor = System.Drawing.Color.Black;
            this.textip.Location = new System.Drawing.Point(16, 75);
            this.textip.Name = "textip";
            this.textip.ReadOnly = true;
            this.textip.Size = new System.Drawing.Size(69, 13);
            this.textip.TabIndex = 22;
            // 
            // panelcabcentral
            // 
            this.panelcabcentral.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelcabcentral.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelcabcentral.Controls.Add(this.pictureBoxBoton);
            this.panelcabcentral.Controls.Add(this.off);
            this.panelcabcentral.Controls.Add(this.progressBar1);
            this.panelcabcentral.Controls.Add(this.PanelConnect);
            this.panelcabcentral.Controls.Add(this.labelonoff);
            this.panelcabcentral.Location = new System.Drawing.Point(166, 5);
            this.panelcabcentral.Name = "panelcabcentral";
            this.panelcabcentral.Size = new System.Drawing.Size(500, 153);
            this.panelcabcentral.TabIndex = 1;
            // 
            // pictureBoxBoton
            // 
            this.pictureBoxBoton.Image = global::logo2020.PRESENTATION.Properties.Resources.button_Red;
            this.pictureBoxBoton.Location = new System.Drawing.Point(378, 3);
            this.pictureBoxBoton.Name = "pictureBoxBoton";
            this.pictureBoxBoton.Size = new System.Drawing.Size(53, 59);
            this.pictureBoxBoton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxBoton.TabIndex = 1;
            this.pictureBoxBoton.TabStop = false;
            // 
            // off
            // 
            this.off.Enabled = false;
            this.off.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.off.Location = new System.Drawing.Point(378, 75);
            this.off.Name = "off";
            this.off.Size = new System.Drawing.Size(112, 41);
            this.off.TabIndex = 16;
            this.off.Text = "Desconectar";
            this.off.UseVisualStyleBackColor = true;
            this.off.Click += new System.EventHandler(this.off_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(3, 138);
            this.progressBar1.MarqueeAnimationSpeed = 20;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(482, 10);
            this.progressBar1.TabIndex = 15;
            // 
            // PanelConnect
            // 
            this.PanelConnect.BackColor = System.Drawing.SystemColors.Highlight;
            this.PanelConnect.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelConnect.Controls.Add(this.on);
            this.PanelConnect.Controls.Add(this.label5);
            this.PanelConnect.Controls.Add(this.label4);
            this.PanelConnect.Controls.Add(this.label3);
            this.PanelConnect.Controls.Add(this.label2);
            this.PanelConnect.Controls.Add(this.slotText);
            this.PanelConnect.Controls.Add(this.rackText);
            this.PanelConnect.Controls.Add(this.ipText);
            this.PanelConnect.Controls.Add(this.dnsText);
            this.PanelConnect.Location = new System.Drawing.Point(8, 3);
            this.PanelConnect.Name = "PanelConnect";
            this.PanelConnect.Size = new System.Drawing.Size(364, 129);
            this.PanelConnect.TabIndex = 14;
            // 
            // on
            // 
            this.on.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.on.Location = new System.Drawing.Point(245, 71);
            this.on.Name = "on";
            this.on.Size = new System.Drawing.Size(112, 41);
            this.on.TabIndex = 10;
            this.on.Text = "Conectar";
            this.on.UseVisualStyleBackColor = true;
            this.on.Click += new System.EventHandler(this.on_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(116, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "SLOT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "RACK";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(34, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "DNS";
            // 
            // slotText
            // 
            this.slotText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slotText.Location = new System.Drawing.Point(169, 88);
            this.slotText.Name = "slotText";
            this.slotText.Size = new System.Drawing.Size(49, 24);
            this.slotText.TabIndex = 4;
            this.slotText.Text = "0";
            // 
            // rackText
            // 
            this.rackText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rackText.Location = new System.Drawing.Point(61, 88);
            this.rackText.Name = "rackText";
            this.rackText.Size = new System.Drawing.Size(49, 24);
            this.rackText.TabIndex = 3;
            this.rackText.Text = "1";
            // 
            // ipText
            // 
            this.ipText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipText.Location = new System.Drawing.Point(61, 51);
            this.ipText.Name = "ipText";
            this.ipText.Size = new System.Drawing.Size(157, 24);
            this.ipText.TabIndex = 2;
            this.ipText.Text = "192.168.8.13";
            // 
            // dnsText
            // 
            this.dnsText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dnsText.Location = new System.Drawing.Point(61, 14);
            this.dnsText.Name = "dnsText";
            this.dnsText.Size = new System.Drawing.Size(296, 24);
            this.dnsText.TabIndex = 1;
            // 
            // labelonoff
            // 
            this.labelonoff.AutoSize = true;
            this.labelonoff.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelonoff.Location = new System.Drawing.Point(428, 18);
            this.labelonoff.Name = "labelonoff";
            this.labelonoff.Size = new System.Drawing.Size(62, 29);
            this.labelonoff.TabIndex = 17;
            this.labelonoff.Text = "OFF";
            // 
            // pictureon
            // 
            this.pictureon.Image = global::logo2020.PRESENTATION.Properties.Resources.plcoff;
            this.pictureon.Location = new System.Drawing.Point(3, 5);
            this.pictureon.Name = "pictureon";
            this.pictureon.Size = new System.Drawing.Size(157, 153);
            this.pictureon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureon.TabIndex = 13;
            this.pictureon.TabStop = false;
            // 
            // panelsubcab
            // 
            this.panelsubcab.BackColor = System.Drawing.SystemColors.Control;
            this.panelsubcab.Controls.Add(this.textDump);
            this.panelsubcab.Controls.Add(this.label35);
            this.panelsubcab.Controls.Add(this.registro);
            this.panelsubcab.Controls.Add(this.acumulado);
            this.panelsubcab.Controls.Add(this.panelAlarmas);
            this.panelsubcab.Location = new System.Drawing.Point(684, 12);
            this.panelsubcab.Name = "panelsubcab";
            this.panelsubcab.Size = new System.Drawing.Size(588, 161);
            this.panelsubcab.TabIndex = 4;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(380, 5);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(72, 24);
            this.label35.TabIndex = 48;
            this.label35.Text = "TOTAL";
            // 
            // registro
            // 
            this.registro.AutoSize = true;
            this.registro.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.registro.Location = new System.Drawing.Point(380, 42);
            this.registro.Name = "registro";
            this.registro.Size = new System.Drawing.Size(84, 24);
            this.registro.TabIndex = 47;
            this.registro.Text = "ACTUAL";
            // 
            // acumulado
            // 
            this.acumulado.AutoSize = true;
            this.acumulado.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.acumulado.Location = new System.Drawing.Point(458, 5);
            this.acumulado.Name = "acumulado";
            this.acumulado.Size = new System.Drawing.Size(20, 24);
            this.acumulado.TabIndex = 46;
            this.acumulado.Text = "0";
            // 
            // panelAlarmas
            // 
            this.panelAlarmas.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelAlarmas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelAlarmas.Controls.Add(this.picCO2);
            this.panelAlarmas.Controls.Add(this.picFU);
            this.panelAlarmas.Controls.Add(this.picCA);
            this.panelAlarmas.Controls.Add(this.picPU);
            this.panelAlarmas.Controls.Add(this.labelFUresultado);
            this.panelAlarmas.Controls.Add(this.labelCA);
            this.panelAlarmas.Controls.Add(this.labelFU);
            this.panelAlarmas.Controls.Add(this.labelGas);
            this.panelAlarmas.Controls.Add(this.labelPUresultado);
            this.panelAlarmas.Controls.Add(this.labelCO2resultado);
            this.panelAlarmas.Controls.Add(this.labelPU);
            this.panelAlarmas.Controls.Add(this.labelCAresultado);
            this.panelAlarmas.Location = new System.Drawing.Point(17, 86);
            this.panelAlarmas.Name = "panelAlarmas";
            this.panelAlarmas.Size = new System.Drawing.Size(554, 72);
            this.panelAlarmas.TabIndex = 45;
            // 
            // picCO2
            // 
            this.picCO2.Location = new System.Drawing.Point(482, 11);
            this.picCO2.Name = "picCO2";
            this.picCO2.Size = new System.Drawing.Size(48, 48);
            this.picCO2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCO2.TabIndex = 33;
            this.picCO2.TabStop = false;
            // 
            // picFU
            // 
            this.picFU.Location = new System.Drawing.Point(354, 11);
            this.picFU.Name = "picFU";
            this.picFU.Size = new System.Drawing.Size(48, 48);
            this.picFU.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFU.TabIndex = 42;
            this.picFU.TabStop = false;
            // 
            // picCA
            // 
            this.picCA.Location = new System.Drawing.Point(98, 11);
            this.picCA.Name = "picCA";
            this.picCA.Size = new System.Drawing.Size(48, 48);
            this.picCA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCA.TabIndex = 36;
            this.picCA.TabStop = false;
            // 
            // picPU
            // 
            this.picPU.Location = new System.Drawing.Point(226, 11);
            this.picPU.Name = "picPU";
            this.picPU.Size = new System.Drawing.Size(48, 48);
            this.picPU.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPU.TabIndex = 39;
            this.picPU.TabStop = false;
            // 
            // labelFUresultado
            // 
            this.labelFUresultado.AutoSize = true;
            this.labelFUresultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFUresultado.Location = new System.Drawing.Point(293, 42);
            this.labelFUresultado.Name = "labelFUresultado";
            this.labelFUresultado.Size = new System.Drawing.Size(14, 13);
            this.labelFUresultado.TabIndex = 44;
            this.labelFUresultado.Text = "_";
            // 
            // labelCA
            // 
            this.labelCA.AutoSize = true;
            this.labelCA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCA.Location = new System.Drawing.Point(22, 11);
            this.labelCA.Name = "labelCA";
            this.labelCA.Size = new System.Drawing.Size(66, 26);
            this.labelCA.TabIndex = 37;
            this.labelCA.Text = "Compresor\r\nAire";
            this.labelCA.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelFU
            // 
            this.labelFU.AutoSize = true;
            this.labelFU.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFU.Location = new System.Drawing.Point(293, 13);
            this.labelFU.Name = "labelFU";
            this.labelFU.Size = new System.Drawing.Size(47, 13);
            this.labelFU.TabIndex = 43;
            this.labelFU.Text = "Fusible";
            this.labelFU.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelGas
            // 
            this.labelGas.AutoSize = true;
            this.labelGas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGas.Location = new System.Drawing.Point(418, 11);
            this.labelGas.Name = "labelGas";
            this.labelGas.Size = new System.Drawing.Size(46, 26);
            this.labelGas.TabIndex = 34;
            this.labelGas.Text = "Botella\r\nCO2";
            this.labelGas.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPUresultado
            // 
            this.labelPUresultado.AutoSize = true;
            this.labelPUresultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPUresultado.Location = new System.Drawing.Point(166, 42);
            this.labelPUresultado.Name = "labelPUresultado";
            this.labelPUresultado.Size = new System.Drawing.Size(14, 13);
            this.labelPUresultado.TabIndex = 41;
            this.labelPUresultado.Text = "_";
            // 
            // labelCO2resultado
            // 
            this.labelCO2resultado.AutoSize = true;
            this.labelCO2resultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCO2resultado.Location = new System.Drawing.Point(418, 42);
            this.labelCO2resultado.Name = "labelCO2resultado";
            this.labelCO2resultado.Size = new System.Drawing.Size(14, 13);
            this.labelCO2resultado.TabIndex = 35;
            this.labelCO2resultado.Text = "_";
            // 
            // labelPU
            // 
            this.labelPU.AutoSize = true;
            this.labelPU.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPU.Location = new System.Drawing.Point(166, 13);
            this.labelPU.Name = "labelPU";
            this.labelPU.Size = new System.Drawing.Size(49, 26);
            this.labelPU.TabIndex = 40;
            this.labelPU.Text = "Puerta\r\nCámara";
            this.labelPU.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelCAresultado
            // 
            this.labelCAresultado.AutoSize = true;
            this.labelCAresultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCAresultado.Location = new System.Drawing.Point(22, 42);
            this.labelCAresultado.Name = "labelCAresultado";
            this.labelCAresultado.Size = new System.Drawing.Size(14, 13);
            this.labelCAresultado.TabIndex = 38;
            this.labelCAresultado.Text = "_";
            // 
            // panel
            // 
            this.panel.Controls.Add(this.panelHMI);
            this.panel.Location = new System.Drawing.Point(12, 179);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1260, 470);
            this.panel.TabIndex = 5;
            // 
            // panelHMI
            // 
            this.panelHMI.Controls.Add(this.panel3);
            this.panelHMI.Controls.Add(this.panel1);
            this.panelHMI.Controls.Add(this.panel2);
            this.panelHMI.Location = new System.Drawing.Point(3, 4);
            this.panelHMI.Name = "panelHMI";
            this.panelHMI.Size = new System.Drawing.Size(1254, 462);
            this.panelHMI.TabIndex = 33;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Sienna;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.label25);
            this.panel3.Controls.Add(this.p31b3);
            this.panel3.Controls.Add(this.p31b2);
            this.panel3.Controls.Add(this.p31b1);
            this.panel3.Controls.Add(this.subpanel3b);
            this.panel3.Controls.Add(this.subpanel3);
            this.panel3.Controls.Add(this.b3_1);
            this.panel3.Controls.Add(this.b3_3);
            this.panel3.Controls.Add(this.b3_2);
            this.panel3.Location = new System.Drawing.Point(831, 11);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(395, 441);
            this.panel3.TabIndex = 9;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Sienna;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(276, 397);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 18);
            this.label24.TabIndex = 45;
            this.label24.Text = "LINEA";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Sienna;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(321, 353);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(69, 73);
            this.label25.TabIndex = 46;
            this.label25.Text = "3";
            // 
            // p31b3
            // 
            this.p31b3.BackColor = System.Drawing.Color.Silver;
            this.p31b3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p31b3.Controls.Add(this.p32b3);
            this.p31b3.Location = new System.Drawing.Point(156, 326);
            this.p31b3.Name = "p31b3";
            this.p31b3.Size = new System.Drawing.Size(70, 100);
            this.p31b3.TabIndex = 44;
            // 
            // p32b3
            // 
            this.p32b3.BackColor = System.Drawing.Color.Gold;
            this.p32b3.Controls.Add(this.label33);
            this.p32b3.Location = new System.Drawing.Point(4, 4);
            this.p32b3.Name = "p32b3";
            this.p32b3.Size = new System.Drawing.Size(60, 90);
            this.p32b3.TabIndex = 34;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(4, 32);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(52, 26);
            this.label33.TabIndex = 39;
            this.label33.Text = "BARRIL\r\n3";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p31b2
            // 
            this.p31b2.BackColor = System.Drawing.Color.Silver;
            this.p31b2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p31b2.Controls.Add(this.p32b2);
            this.p31b2.Location = new System.Drawing.Point(80, 326);
            this.p31b2.Name = "p31b2";
            this.p31b2.Size = new System.Drawing.Size(70, 100);
            this.p31b2.TabIndex = 43;
            // 
            // p32b2
            // 
            this.p32b2.BackColor = System.Drawing.Color.Gold;
            this.p32b2.Controls.Add(this.label34);
            this.p32b2.Location = new System.Drawing.Point(4, 4);
            this.p32b2.Name = "p32b2";
            this.p32b2.Size = new System.Drawing.Size(60, 90);
            this.p32b2.TabIndex = 34;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(4, 32);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(52, 26);
            this.label34.TabIndex = 39;
            this.label34.Text = "BARRIL\r\n2";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p31b1
            // 
            this.p31b1.BackColor = System.Drawing.Color.Silver;
            this.p31b1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p31b1.Controls.Add(this.p32b1);
            this.p31b1.Location = new System.Drawing.Point(4, 326);
            this.p31b1.Name = "p31b1";
            this.p31b1.Size = new System.Drawing.Size(70, 100);
            this.p31b1.TabIndex = 42;
            // 
            // p32b1
            // 
            this.p32b1.BackColor = System.Drawing.Color.Gold;
            this.p32b1.Controls.Add(this.label30);
            this.p32b1.Location = new System.Drawing.Point(4, 4);
            this.p32b1.Name = "p32b1";
            this.p32b1.Size = new System.Drawing.Size(60, 90);
            this.p32b1.TabIndex = 34;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(4, 32);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(52, 26);
            this.label30.TabIndex = 38;
            this.label30.Text = "BARRIL\r\n1";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // subpanel3b
            // 
            this.subpanel3b.BackColor = System.Drawing.SystemColors.Control;
            this.subpanel3b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.subpanel3b.Controls.Add(this.label23);
            this.subpanel3b.Controls.Add(this.df3);
            this.subpanel3b.Controls.Add(this.FB_3);
            this.subpanel3b.Controls.Add(this.FA_3);
            this.subpanel3b.Controls.Add(this.v3_1);
            this.subpanel3b.Controls.Add(this.v3_2);
            this.subpanel3b.Controls.Add(this.v3_3);
            this.subpanel3b.Controls.Add(this.v3_paso);
            this.subpanel3b.Controls.Add(this.label19);
            this.subpanel3b.Controls.Add(this.label22);
            this.subpanel3b.Controls.Add(this.label20);
            this.subpanel3b.Controls.Add(this.label21);
            this.subpanel3b.Location = new System.Drawing.Point(4, 24);
            this.subpanel3b.Name = "subpanel3b";
            this.subpanel3b.Size = new System.Drawing.Size(209, 272);
            this.subpanel3b.TabIndex = 41;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(5, 76);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(65, 18);
            this.label23.TabIndex = 10;
            this.label23.Text = "Detector";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // df3
            // 
            this.df3.Image = ((System.Drawing.Image)(resources.GetObject("df3.Image")));
            this.df3.Location = new System.Drawing.Point(37, 5);
            this.df3.Name = "df3";
            this.df3.Size = new System.Drawing.Size(96, 96);
            this.df3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.df3.TabIndex = 5;
            this.df3.TabStop = false;
            // 
            // FB_3
            // 
            this.FB_3.Location = new System.Drawing.Point(110, 63);
            this.FB_3.Name = "FB_3";
            this.FB_3.Size = new System.Drawing.Size(96, 100);
            this.FB_3.TabIndex = 34;
            this.FB_3.TabStop = false;
            // 
            // FA_3
            // 
            this.FA_3.Image = ((System.Drawing.Image)(resources.GetObject("FA_3.Image")));
            this.FA_3.Location = new System.Drawing.Point(2, 100);
            this.FA_3.Name = "FA_3";
            this.FA_3.Size = new System.Drawing.Size(153, 63);
            this.FA_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FA_3.TabIndex = 32;
            this.FA_3.TabStop = false;
            // 
            // v3_1
            // 
            this.v3_1.Image = ((System.Drawing.Image)(resources.GetObject("v3_1.Image")));
            this.v3_1.Location = new System.Drawing.Point(2, 164);
            this.v3_1.Name = "v3_1";
            this.v3_1.Size = new System.Drawing.Size(45, 46);
            this.v3_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v3_1.TabIndex = 1;
            this.v3_1.TabStop = false;
            // 
            // v3_2
            // 
            this.v3_2.Image = ((System.Drawing.Image)(resources.GetObject("v3_2.Image")));
            this.v3_2.Location = new System.Drawing.Point(56, 164);
            this.v3_2.Name = "v3_2";
            this.v3_2.Size = new System.Drawing.Size(45, 46);
            this.v3_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v3_2.TabIndex = 2;
            this.v3_2.TabStop = false;
            // 
            // v3_3
            // 
            this.v3_3.Image = ((System.Drawing.Image)(resources.GetObject("v3_3.Image")));
            this.v3_3.Location = new System.Drawing.Point(110, 164);
            this.v3_3.Name = "v3_3";
            this.v3_3.Size = new System.Drawing.Size(45, 46);
            this.v3_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v3_3.TabIndex = 3;
            this.v3_3.TabStop = false;
            // 
            // v3_paso
            // 
            this.v3_paso.Image = ((System.Drawing.Image)(resources.GetObject("v3_paso.Image")));
            this.v3_paso.Location = new System.Drawing.Point(161, 164);
            this.v3_paso.Name = "v3_paso";
            this.v3_paso.Size = new System.Drawing.Size(45, 46);
            this.v3_paso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v3_paso.TabIndex = 4;
            this.v3_paso.TabStop = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(113, 213);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(25, 18);
            this.label19.TabIndex = 14;
            this.label19.Text = "V3\r";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(163, 213);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(43, 36);
            this.label22.TabIndex = 11;
            this.label22.Text = "V5\r\nPaso";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(59, 213);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(25, 18);
            this.label20.TabIndex = 13;
            this.label20.Text = "V2";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(5, 213);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(25, 18);
            this.label21.TabIndex = 12;
            this.label21.Text = "V1";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // subpanel3
            // 
            this.subpanel3.BackColor = System.Drawing.Color.Gold;
            this.subpanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.subpanel3.Controls.Add(this.run3);
            this.subpanel3.Controls.Add(this.stop3);
            this.subpanel3.Controls.Add(this.labelbombilla3);
            this.subpanel3.Controls.Add(this.picbombilla3);
            this.subpanel3.Controls.Add(this.picparomarcha3);
            this.subpanel3.Controls.Add(this.picB31);
            this.subpanel3.Controls.Add(this.picB32);
            this.subpanel3.Controls.Add(this.picB33);
            this.subpanel3.Controls.Add(this.jump3);
            this.subpanel3.Controls.Add(this.refresh3);
            this.subpanel3.Location = new System.Drawing.Point(214, 24);
            this.subpanel3.Name = "subpanel3";
            this.subpanel3.Size = new System.Drawing.Size(177, 197);
            this.subpanel3.TabIndex = 40;
            // 
            // run3
            // 
            this.run3.FlatAppearance.BorderSize = 0;
            this.run3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.run3.Image = ((System.Drawing.Image)(resources.GetObject("run3.Image")));
            this.run3.Location = new System.Drawing.Point(94, 57);
            this.run3.Name = "run3";
            this.run3.Size = new System.Drawing.Size(80, 32);
            this.run3.TabIndex = 16;
            this.run3.UseVisualStyleBackColor = true;
            // 
            // stop3
            // 
            this.stop3.FlatAppearance.BorderSize = 0;
            this.stop3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stop3.Image = ((System.Drawing.Image)(resources.GetObject("stop3.Image")));
            this.stop3.Location = new System.Drawing.Point(94, 20);
            this.stop3.Name = "stop3";
            this.stop3.Size = new System.Drawing.Size(80, 32);
            this.stop3.TabIndex = 15;
            this.stop3.UseVisualStyleBackColor = true;
            // 
            // labelbombilla3
            // 
            this.labelbombilla3.AutoSize = true;
            this.labelbombilla3.Location = new System.Drawing.Point(3, 15);
            this.labelbombilla3.Name = "labelbombilla3";
            this.labelbombilla3.Size = new System.Drawing.Size(35, 13);
            this.labelbombilla3.TabIndex = 42;
            this.labelbombilla3.Text = "label1";
            // 
            // picbombilla3
            // 
            this.picbombilla3.Location = new System.Drawing.Point(5, 26);
            this.picbombilla3.Name = "picbombilla3";
            this.picbombilla3.Size = new System.Drawing.Size(48, 48);
            this.picbombilla3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbombilla3.TabIndex = 41;
            this.picbombilla3.TabStop = false;
            // 
            // picparomarcha3
            // 
            this.picparomarcha3.Location = new System.Drawing.Point(39, 10);
            this.picparomarcha3.Name = "picparomarcha3";
            this.picparomarcha3.Size = new System.Drawing.Size(69, 84);
            this.picparomarcha3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picparomarcha3.TabIndex = 40;
            this.picparomarcha3.TabStop = false;
            // 
            // picB31
            // 
            this.picB31.Location = new System.Drawing.Point(6, 100);
            this.picB31.Name = "picB31";
            this.picB31.Size = new System.Drawing.Size(48, 48);
            this.picB31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB31.TabIndex = 37;
            this.picB31.TabStop = false;
            // 
            // picB32
            // 
            this.picB32.Location = new System.Drawing.Point(60, 100);
            this.picB32.Name = "picB32";
            this.picB32.Size = new System.Drawing.Size(48, 48);
            this.picB32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB32.TabIndex = 38;
            this.picB32.TabStop = false;
            // 
            // picB33
            // 
            this.picB33.Location = new System.Drawing.Point(114, 100);
            this.picB33.Name = "picB33";
            this.picB33.Size = new System.Drawing.Size(48, 48);
            this.picB33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB33.TabIndex = 39;
            this.picB33.TabStop = false;
            // 
            // jump3
            // 
            this.jump3.FlatAppearance.BorderSize = 0;
            this.jump3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jump3.Image = global::logo2020.PRESENTATION.Properties.Resources.BAVANCE;
            this.jump3.Location = new System.Drawing.Point(92, 154);
            this.jump3.Name = "jump3";
            this.jump3.Size = new System.Drawing.Size(80, 32);
            this.jump3.TabIndex = 17;
            this.jump3.UseVisualStyleBackColor = true;
            // 
            // refresh3
            // 
            this.refresh3.FlatAppearance.BorderSize = 0;
            this.refresh3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refresh3.Image = global::logo2020.PRESENTATION.Properties.Resources.BREPONER;
            this.refresh3.Location = new System.Drawing.Point(6, 154);
            this.refresh3.Name = "refresh3";
            this.refresh3.Size = new System.Drawing.Size(80, 32);
            this.refresh3.TabIndex = 18;
            this.refresh3.UseVisualStyleBackColor = true;
            // 
            // b3_1
            // 
            this.b3_1.AutoSize = true;
            this.b3_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b3_1.Location = new System.Drawing.Point(4, 305);
            this.b3_1.Name = "b3_1";
            this.b3_1.Size = new System.Drawing.Size(16, 18);
            this.b3_1.TabIndex = 27;
            this.b3_1.Text = "0";
            this.b3_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // b3_3
            // 
            this.b3_3.AutoSize = true;
            this.b3_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b3_3.Location = new System.Drawing.Point(158, 305);
            this.b3_3.Name = "b3_3";
            this.b3_3.Size = new System.Drawing.Size(16, 18);
            this.b3_3.TabIndex = 29;
            this.b3_3.Text = "0";
            this.b3_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // b3_2
            // 
            this.b3_2.AutoSize = true;
            this.b3_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b3_2.Location = new System.Drawing.Point(82, 305);
            this.b3_2.Name = "b3_2";
            this.b3_2.Size = new System.Drawing.Size(16, 18);
            this.b3_2.TabIndex = 28;
            this.b3_2.Text = "0";
            this.b3_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Sienna;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.p11b3);
            this.panel1.Controls.Add(this.p11b2);
            this.panel1.Controls.Add(this.p11b1);
            this.panel1.Controls.Add(this.subpanel1);
            this.panel1.Controls.Add(this.subpanel1b);
            this.panel1.Controls.Add(this.b1_1);
            this.panel1.Controls.Add(this.b1_2);
            this.panel1.Controls.Add(this.b1_3);
            this.panel1.Location = new System.Drawing.Point(29, 11);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(395, 441);
            this.panel1.TabIndex = 7;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Sienna;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(287, 397);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 18);
            this.label11.TabIndex = 19;
            this.label11.Text = "LINEA";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Sienna;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(321, 353);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 73);
            this.label1.TabIndex = 36;
            this.label1.Text = "1";
            // 
            // p11b3
            // 
            this.p11b3.BackColor = System.Drawing.Color.Silver;
            this.p11b3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p11b3.Controls.Add(this.p12b3);
            this.p11b3.Location = new System.Drawing.Point(156, 326);
            this.p11b3.Name = "p11b3";
            this.p11b3.Size = new System.Drawing.Size(70, 100);
            this.p11b3.TabIndex = 35;
            // 
            // p12b3
            // 
            this.p12b3.BackColor = System.Drawing.Color.Gold;
            this.p12b3.Controls.Add(this.label28);
            this.p12b3.Location = new System.Drawing.Point(4, 4);
            this.p12b3.Name = "p12b3";
            this.p12b3.Size = new System.Drawing.Size(60, 90);
            this.p12b3.TabIndex = 34;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(4, 32);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(52, 26);
            this.label28.TabIndex = 38;
            this.label28.Text = "BARRIL\r\n3";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p11b2
            // 
            this.p11b2.BackColor = System.Drawing.Color.Silver;
            this.p11b2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p11b2.Controls.Add(this.p12b2);
            this.p11b2.Location = new System.Drawing.Point(80, 326);
            this.p11b2.Name = "p11b2";
            this.p11b2.Size = new System.Drawing.Size(70, 100);
            this.p11b2.TabIndex = 34;
            // 
            // p12b2
            // 
            this.p12b2.BackColor = System.Drawing.Color.Gold;
            this.p12b2.Controls.Add(this.label27);
            this.p12b2.Location = new System.Drawing.Point(4, 4);
            this.p12b2.Name = "p12b2";
            this.p12b2.Size = new System.Drawing.Size(60, 90);
            this.p12b2.TabIndex = 34;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(4, 32);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(52, 26);
            this.label27.TabIndex = 38;
            this.label27.Text = "BARRIL\r\n2";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p11b1
            // 
            this.p11b1.BackColor = System.Drawing.Color.Silver;
            this.p11b1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p11b1.Controls.Add(this.p12b1);
            this.p11b1.Location = new System.Drawing.Point(4, 326);
            this.p11b1.Name = "p11b1";
            this.p11b1.Size = new System.Drawing.Size(70, 100);
            this.p11b1.TabIndex = 33;
            // 
            // p12b1
            // 
            this.p12b1.BackColor = System.Drawing.Color.Gold;
            this.p12b1.Controls.Add(this.label26);
            this.p12b1.Location = new System.Drawing.Point(4, 4);
            this.p12b1.Name = "p12b1";
            this.p12b1.Size = new System.Drawing.Size(60, 90);
            this.p12b1.TabIndex = 34;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(4, 32);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 26);
            this.label26.TabIndex = 37;
            this.label26.Text = "BARRIL\r\n1";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // subpanel1
            // 
            this.subpanel1.BackColor = System.Drawing.Color.Gold;
            this.subpanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.subpanel1.Controls.Add(this.picB13);
            this.subpanel1.Controls.Add(this.picB12);
            this.subpanel1.Controls.Add(this.picB11);
            this.subpanel1.Controls.Add(this.labelbombilla1);
            this.subpanel1.Controls.Add(this.run1);
            this.subpanel1.Controls.Add(this.stop1);
            this.subpanel1.Controls.Add(this.picbombilla1);
            this.subpanel1.Controls.Add(this.picparomarcha1);
            this.subpanel1.Controls.Add(this.jump1);
            this.subpanel1.Controls.Add(this.refresh1);
            this.subpanel1.Location = new System.Drawing.Point(214, 24);
            this.subpanel1.Name = "subpanel1";
            this.subpanel1.Size = new System.Drawing.Size(177, 197);
            this.subpanel1.TabIndex = 32;
            // 
            // picB13
            // 
            this.picB13.Location = new System.Drawing.Point(113, 100);
            this.picB13.Name = "picB13";
            this.picB13.Size = new System.Drawing.Size(48, 48);
            this.picB13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB13.TabIndex = 22;
            this.picB13.TabStop = false;
            // 
            // picB12
            // 
            this.picB12.Location = new System.Drawing.Point(59, 100);
            this.picB12.Name = "picB12";
            this.picB12.Size = new System.Drawing.Size(48, 48);
            this.picB12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB12.TabIndex = 21;
            this.picB12.TabStop = false;
            // 
            // picB11
            // 
            this.picB11.Location = new System.Drawing.Point(5, 100);
            this.picB11.Name = "picB11";
            this.picB11.Size = new System.Drawing.Size(48, 48);
            this.picB11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB11.TabIndex = 20;
            this.picB11.TabStop = false;
            // 
            // labelbombilla1
            // 
            this.labelbombilla1.AutoSize = true;
            this.labelbombilla1.Location = new System.Drawing.Point(2, 16);
            this.labelbombilla1.Name = "labelbombilla1";
            this.labelbombilla1.Size = new System.Drawing.Size(35, 13);
            this.labelbombilla1.TabIndex = 19;
            this.labelbombilla1.Text = "label1";
            // 
            // run1
            // 
            this.run1.FlatAppearance.BorderSize = 0;
            this.run1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.run1.Image = ((System.Drawing.Image)(resources.GetObject("run1.Image")));
            this.run1.Location = new System.Drawing.Point(95, 56);
            this.run1.Name = "run1";
            this.run1.Size = new System.Drawing.Size(80, 32);
            this.run1.TabIndex = 16;
            this.run1.UseVisualStyleBackColor = true;
            // 
            // stop1
            // 
            this.stop1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.stop1.FlatAppearance.BorderSize = 0;
            this.stop1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.stop1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.stop1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stop1.Image = ((System.Drawing.Image)(resources.GetObject("stop1.Image")));
            this.stop1.Location = new System.Drawing.Point(95, 19);
            this.stop1.Name = "stop1";
            this.stop1.Size = new System.Drawing.Size(80, 32);
            this.stop1.TabIndex = 15;
            this.stop1.UseVisualStyleBackColor = true;
            // 
            // picbombilla1
            // 
            this.picbombilla1.Location = new System.Drawing.Point(5, 26);
            this.picbombilla1.Name = "picbombilla1";
            this.picbombilla1.Size = new System.Drawing.Size(48, 48);
            this.picbombilla1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbombilla1.TabIndex = 1;
            this.picbombilla1.TabStop = false;
            // 
            // picparomarcha1
            // 
            this.picparomarcha1.Location = new System.Drawing.Point(39, 10);
            this.picparomarcha1.Name = "picparomarcha1";
            this.picparomarcha1.Size = new System.Drawing.Size(69, 84);
            this.picparomarcha1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picparomarcha1.TabIndex = 0;
            this.picparomarcha1.TabStop = false;
            // 
            // jump1
            // 
            this.jump1.FlatAppearance.BorderSize = 0;
            this.jump1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jump1.Image = ((System.Drawing.Image)(resources.GetObject("jump1.Image")));
            this.jump1.Location = new System.Drawing.Point(86, 154);
            this.jump1.Name = "jump1";
            this.jump1.Size = new System.Drawing.Size(80, 32);
            this.jump1.TabIndex = 17;
            this.jump1.UseVisualStyleBackColor = true;
            // 
            // refresh1
            // 
            this.refresh1.FlatAppearance.BorderSize = 0;
            this.refresh1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refresh1.Image = ((System.Drawing.Image)(resources.GetObject("refresh1.Image")));
            this.refresh1.Location = new System.Drawing.Point(3, 154);
            this.refresh1.Name = "refresh1";
            this.refresh1.Size = new System.Drawing.Size(80, 32);
            this.refresh1.TabIndex = 18;
            this.refresh1.UseVisualStyleBackColor = true;
            // 
            // subpanel1b
            // 
            this.subpanel1b.BackColor = System.Drawing.SystemColors.Control;
            this.subpanel1b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.subpanel1b.Controls.Add(this.label6);
            this.subpanel1b.Controls.Add(this.df1);
            this.subpanel1b.Controls.Add(this.FA_1);
            this.subpanel1b.Controls.Add(this.v1_1);
            this.subpanel1b.Controls.Add(this.FB_1);
            this.subpanel1b.Controls.Add(this.v1_2);
            this.subpanel1b.Controls.Add(this.v1_paso);
            this.subpanel1b.Controls.Add(this.v1_3);
            this.subpanel1b.Controls.Add(this.label7);
            this.subpanel1b.Controls.Add(this.label8);
            this.subpanel1b.Controls.Add(this.label9);
            this.subpanel1b.Controls.Add(this.label10);
            this.subpanel1b.Location = new System.Drawing.Point(4, 24);
            this.subpanel1b.Name = "subpanel1b";
            this.subpanel1b.Size = new System.Drawing.Size(209, 272);
            this.subpanel1b.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(5, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Detector";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // df1
            // 
            this.df1.Image = ((System.Drawing.Image)(resources.GetObject("df1.Image")));
            this.df1.Location = new System.Drawing.Point(37, 5);
            this.df1.Name = "df1";
            this.df1.Size = new System.Drawing.Size(96, 96);
            this.df1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.df1.TabIndex = 5;
            this.df1.TabStop = false;
            // 
            // FA_1
            // 
            this.FA_1.Image = ((System.Drawing.Image)(resources.GetObject("FA_1.Image")));
            this.FA_1.Location = new System.Drawing.Point(2, 100);
            this.FA_1.Name = "FA_1";
            this.FA_1.Size = new System.Drawing.Size(153, 63);
            this.FA_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FA_1.TabIndex = 30;
            this.FA_1.TabStop = false;
            // 
            // v1_1
            // 
            this.v1_1.Image = ((System.Drawing.Image)(resources.GetObject("v1_1.Image")));
            this.v1_1.Location = new System.Drawing.Point(2, 166);
            this.v1_1.Name = "v1_1";
            this.v1_1.Size = new System.Drawing.Size(45, 46);
            this.v1_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v1_1.TabIndex = 1;
            this.v1_1.TabStop = false;
            // 
            // FB_1
            // 
            this.FB_1.Location = new System.Drawing.Point(110, 63);
            this.FB_1.Name = "FB_1";
            this.FB_1.Size = new System.Drawing.Size(96, 100);
            this.FB_1.TabIndex = 29;
            this.FB_1.TabStop = false;
            // 
            // v1_2
            // 
            this.v1_2.Image = ((System.Drawing.Image)(resources.GetObject("v1_2.Image")));
            this.v1_2.Location = new System.Drawing.Point(56, 166);
            this.v1_2.Name = "v1_2";
            this.v1_2.Size = new System.Drawing.Size(45, 46);
            this.v1_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v1_2.TabIndex = 2;
            this.v1_2.TabStop = false;
            // 
            // v1_paso
            // 
            this.v1_paso.Image = ((System.Drawing.Image)(resources.GetObject("v1_paso.Image")));
            this.v1_paso.Location = new System.Drawing.Point(161, 166);
            this.v1_paso.Name = "v1_paso";
            this.v1_paso.Size = new System.Drawing.Size(45, 46);
            this.v1_paso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v1_paso.TabIndex = 4;
            this.v1_paso.TabStop = false;
            // 
            // v1_3
            // 
            this.v1_3.Image = ((System.Drawing.Image)(resources.GetObject("v1_3.Image")));
            this.v1_3.Location = new System.Drawing.Point(110, 166);
            this.v1_3.Name = "v1_3";
            this.v1_3.Size = new System.Drawing.Size(45, 46);
            this.v1_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v1_3.TabIndex = 3;
            this.v1_3.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(163, 215);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 36);
            this.label7.TabIndex = 11;
            this.label7.Text = "V5\r\nPaso";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(5, 215);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 18);
            this.label8.TabIndex = 12;
            this.label8.Text = "V1";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(59, 215);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 18);
            this.label9.TabIndex = 13;
            this.label9.Text = "V2\r";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(113, 215);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 18);
            this.label10.TabIndex = 14;
            this.label10.Text = "V3\r\n";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // b1_1
            // 
            this.b1_1.AutoSize = true;
            this.b1_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b1_1.Location = new System.Drawing.Point(6, 305);
            this.b1_1.Name = "b1_1";
            this.b1_1.Size = new System.Drawing.Size(16, 18);
            this.b1_1.TabIndex = 24;
            this.b1_1.Text = "0";
            this.b1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // b1_2
            // 
            this.b1_2.AutoSize = true;
            this.b1_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b1_2.Location = new System.Drawing.Point(82, 305);
            this.b1_2.Name = "b1_2";
            this.b1_2.Size = new System.Drawing.Size(16, 18);
            this.b1_2.TabIndex = 25;
            this.b1_2.Text = "0";
            this.b1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // b1_3
            // 
            this.b1_3.AutoSize = true;
            this.b1_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b1_3.Location = new System.Drawing.Point(158, 305);
            this.b1_3.Name = "b1_3";
            this.b1_3.Size = new System.Drawing.Size(16, 18);
            this.b1_3.TabIndex = 26;
            this.b1_3.Text = "0";
            this.b1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Sienna;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.p21b3);
            this.panel2.Controls.Add(this.p21b2);
            this.panel2.Controls.Add(this.p21b1);
            this.panel2.Controls.Add(this.subpanel2b);
            this.panel2.Controls.Add(this.subpanel2);
            this.panel2.Controls.Add(this.b2_3);
            this.panel2.Controls.Add(this.b2_1);
            this.panel2.Controls.Add(this.b2_2);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Location = new System.Drawing.Point(430, 11);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(395, 441);
            this.panel2.TabIndex = 8;
            // 
            // p21b3
            // 
            this.p21b3.BackColor = System.Drawing.Color.Silver;
            this.p21b3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p21b3.Controls.Add(this.p22b3);
            this.p21b3.Location = new System.Drawing.Point(156, 326);
            this.p21b3.Name = "p21b3";
            this.p21b3.Size = new System.Drawing.Size(70, 100);
            this.p21b3.TabIndex = 41;
            // 
            // p22b3
            // 
            this.p22b3.BackColor = System.Drawing.Color.Gold;
            this.p22b3.Controls.Add(this.label32);
            this.p22b3.Location = new System.Drawing.Point(4, 4);
            this.p22b3.Name = "p22b3";
            this.p22b3.Size = new System.Drawing.Size(60, 90);
            this.p22b3.TabIndex = 34;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(4, 32);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(52, 26);
            this.label32.TabIndex = 38;
            this.label32.Text = "BARRIL\r\n3";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p21b2
            // 
            this.p21b2.BackColor = System.Drawing.Color.Silver;
            this.p21b2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p21b2.Controls.Add(this.p22b2);
            this.p21b2.Location = new System.Drawing.Point(80, 326);
            this.p21b2.Name = "p21b2";
            this.p21b2.Size = new System.Drawing.Size(70, 100);
            this.p21b2.TabIndex = 40;
            // 
            // p22b2
            // 
            this.p22b2.BackColor = System.Drawing.Color.Gold;
            this.p22b2.Controls.Add(this.label31);
            this.p22b2.Location = new System.Drawing.Point(4, 4);
            this.p22b2.Name = "p22b2";
            this.p22b2.Size = new System.Drawing.Size(60, 90);
            this.p22b2.TabIndex = 34;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(4, 32);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(52, 26);
            this.label31.TabIndex = 38;
            this.label31.Text = "BARRIL\r\n2";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p21b1
            // 
            this.p21b1.BackColor = System.Drawing.Color.Silver;
            this.p21b1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p21b1.Controls.Add(this.p22b1);
            this.p21b1.Location = new System.Drawing.Point(4, 326);
            this.p21b1.Name = "p21b1";
            this.p21b1.Size = new System.Drawing.Size(70, 100);
            this.p21b1.TabIndex = 39;
            // 
            // p22b1
            // 
            this.p22b1.BackColor = System.Drawing.Color.Gold;
            this.p22b1.Controls.Add(this.label29);
            this.p22b1.Location = new System.Drawing.Point(4, 4);
            this.p22b1.Name = "p22b1";
            this.p22b1.Size = new System.Drawing.Size(60, 90);
            this.p22b1.TabIndex = 34;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(4, 32);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(52, 26);
            this.label29.TabIndex = 38;
            this.label29.Text = "BARRIL\r\n1";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // subpanel2b
            // 
            this.subpanel2b.BackColor = System.Drawing.SystemColors.Control;
            this.subpanel2b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.subpanel2b.Controls.Add(this.label17);
            this.subpanel2b.Controls.Add(this.df2);
            this.subpanel2b.Controls.Add(this.FB_2);
            this.subpanel2b.Controls.Add(this.FA_2);
            this.subpanel2b.Controls.Add(this.v2_1);
            this.subpanel2b.Controls.Add(this.v2_2);
            this.subpanel2b.Controls.Add(this.v2_3);
            this.subpanel2b.Controls.Add(this.v2_paso);
            this.subpanel2b.Controls.Add(this.label13);
            this.subpanel2b.Controls.Add(this.label16);
            this.subpanel2b.Controls.Add(this.label14);
            this.subpanel2b.Controls.Add(this.label15);
            this.subpanel2b.Location = new System.Drawing.Point(4, 24);
            this.subpanel2b.Name = "subpanel2b";
            this.subpanel2b.Size = new System.Drawing.Size(209, 272);
            this.subpanel2b.TabIndex = 38;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 18);
            this.label17.TabIndex = 10;
            this.label17.Text = "Detector";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // df2
            // 
            this.df2.Image = ((System.Drawing.Image)(resources.GetObject("df2.Image")));
            this.df2.Location = new System.Drawing.Point(38, 4);
            this.df2.Name = "df2";
            this.df2.Size = new System.Drawing.Size(96, 96);
            this.df2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.df2.TabIndex = 5;
            this.df2.TabStop = false;
            // 
            // FB_2
            // 
            this.FB_2.Location = new System.Drawing.Point(111, 62);
            this.FB_2.Name = "FB_2";
            this.FB_2.Size = new System.Drawing.Size(96, 100);
            this.FB_2.TabIndex = 33;
            this.FB_2.TabStop = false;
            // 
            // FA_2
            // 
            this.FA_2.Image = ((System.Drawing.Image)(resources.GetObject("FA_2.Image")));
            this.FA_2.Location = new System.Drawing.Point(3, 99);
            this.FA_2.Name = "FA_2";
            this.FA_2.Size = new System.Drawing.Size(153, 63);
            this.FA_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FA_2.TabIndex = 31;
            this.FA_2.TabStop = false;
            // 
            // v2_1
            // 
            this.v2_1.Image = ((System.Drawing.Image)(resources.GetObject("v2_1.Image")));
            this.v2_1.Location = new System.Drawing.Point(3, 163);
            this.v2_1.Name = "v2_1";
            this.v2_1.Size = new System.Drawing.Size(45, 46);
            this.v2_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v2_1.TabIndex = 1;
            this.v2_1.TabStop = false;
            // 
            // v2_2
            // 
            this.v2_2.Image = ((System.Drawing.Image)(resources.GetObject("v2_2.Image")));
            this.v2_2.Location = new System.Drawing.Point(57, 163);
            this.v2_2.Name = "v2_2";
            this.v2_2.Size = new System.Drawing.Size(45, 46);
            this.v2_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v2_2.TabIndex = 2;
            this.v2_2.TabStop = false;
            // 
            // v2_3
            // 
            this.v2_3.Image = ((System.Drawing.Image)(resources.GetObject("v2_3.Image")));
            this.v2_3.Location = new System.Drawing.Point(111, 163);
            this.v2_3.Name = "v2_3";
            this.v2_3.Size = new System.Drawing.Size(45, 46);
            this.v2_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v2_3.TabIndex = 3;
            this.v2_3.TabStop = false;
            // 
            // v2_paso
            // 
            this.v2_paso.Image = ((System.Drawing.Image)(resources.GetObject("v2_paso.Image")));
            this.v2_paso.Location = new System.Drawing.Point(162, 163);
            this.v2_paso.Name = "v2_paso";
            this.v2_paso.Size = new System.Drawing.Size(45, 46);
            this.v2_paso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v2_paso.TabIndex = 4;
            this.v2_paso.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(114, 212);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(25, 18);
            this.label13.TabIndex = 14;
            this.label13.Text = "V3\r\n";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(164, 212);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 36);
            this.label16.TabIndex = 11;
            this.label16.Text = "V5\r\nPaso";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(60, 212);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(25, 18);
            this.label14.TabIndex = 13;
            this.label14.Text = "V2\r";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(6, 212);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 18);
            this.label15.TabIndex = 12;
            this.label15.Text = "V1\r\n";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // subpanel2
            // 
            this.subpanel2.BackColor = System.Drawing.Color.Gold;
            this.subpanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.subpanel2.Controls.Add(this.run2);
            this.subpanel2.Controls.Add(this.stop2);
            this.subpanel2.Controls.Add(this.labelbombilla2);
            this.subpanel2.Controls.Add(this.picbombilla2);
            this.subpanel2.Controls.Add(this.picparomarcha2);
            this.subpanel2.Controls.Add(this.picB23);
            this.subpanel2.Controls.Add(this.picB22);
            this.subpanel2.Controls.Add(this.picB21);
            this.subpanel2.Controls.Add(this.refresh2);
            this.subpanel2.Controls.Add(this.jump2);
            this.subpanel2.Location = new System.Drawing.Point(214, 24);
            this.subpanel2.Name = "subpanel2";
            this.subpanel2.Size = new System.Drawing.Size(177, 197);
            this.subpanel2.TabIndex = 37;
            // 
            // run2
            // 
            this.run2.FlatAppearance.BorderSize = 0;
            this.run2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.run2.Image = ((System.Drawing.Image)(resources.GetObject("run2.Image")));
            this.run2.Location = new System.Drawing.Point(96, 56);
            this.run2.Name = "run2";
            this.run2.Size = new System.Drawing.Size(80, 32);
            this.run2.TabIndex = 16;
            this.run2.UseVisualStyleBackColor = true;
            // 
            // stop2
            // 
            this.stop2.FlatAppearance.BorderSize = 0;
            this.stop2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stop2.Image = ((System.Drawing.Image)(resources.GetObject("stop2.Image")));
            this.stop2.Location = new System.Drawing.Point(96, 19);
            this.stop2.Name = "stop2";
            this.stop2.Size = new System.Drawing.Size(80, 32);
            this.stop2.TabIndex = 15;
            this.stop2.UseVisualStyleBackColor = true;
            // 
            // labelbombilla2
            // 
            this.labelbombilla2.AutoSize = true;
            this.labelbombilla2.Location = new System.Drawing.Point(2, 16);
            this.labelbombilla2.Name = "labelbombilla2";
            this.labelbombilla2.Size = new System.Drawing.Size(35, 13);
            this.labelbombilla2.TabIndex = 39;
            this.labelbombilla2.Text = "label1";
            // 
            // picbombilla2
            // 
            this.picbombilla2.Location = new System.Drawing.Point(5, 26);
            this.picbombilla2.Name = "picbombilla2";
            this.picbombilla2.Size = new System.Drawing.Size(48, 48);
            this.picbombilla2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbombilla2.TabIndex = 38;
            this.picbombilla2.TabStop = false;
            // 
            // picparomarcha2
            // 
            this.picparomarcha2.Location = new System.Drawing.Point(39, 10);
            this.picparomarcha2.Name = "picparomarcha2";
            this.picparomarcha2.Size = new System.Drawing.Size(69, 84);
            this.picparomarcha2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picparomarcha2.TabIndex = 37;
            this.picparomarcha2.TabStop = false;
            // 
            // picB23
            // 
            this.picB23.Location = new System.Drawing.Point(114, 100);
            this.picB23.Name = "picB23";
            this.picB23.Size = new System.Drawing.Size(48, 48);
            this.picB23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB23.TabIndex = 36;
            this.picB23.TabStop = false;
            // 
            // picB22
            // 
            this.picB22.Location = new System.Drawing.Point(60, 100);
            this.picB22.Name = "picB22";
            this.picB22.Size = new System.Drawing.Size(48, 48);
            this.picB22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB22.TabIndex = 35;
            this.picB22.TabStop = false;
            // 
            // picB21
            // 
            this.picB21.Location = new System.Drawing.Point(6, 100);
            this.picB21.Name = "picB21";
            this.picB21.Size = new System.Drawing.Size(48, 48);
            this.picB21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB21.TabIndex = 34;
            this.picB21.TabStop = false;
            // 
            // refresh2
            // 
            this.refresh2.FlatAppearance.BorderSize = 0;
            this.refresh2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refresh2.Image = ((System.Drawing.Image)(resources.GetObject("refresh2.Image")));
            this.refresh2.Location = new System.Drawing.Point(1, 154);
            this.refresh2.Name = "refresh2";
            this.refresh2.Size = new System.Drawing.Size(80, 32);
            this.refresh2.TabIndex = 18;
            this.refresh2.UseVisualStyleBackColor = true;
            // 
            // jump2
            // 
            this.jump2.FlatAppearance.BorderSize = 0;
            this.jump2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jump2.Image = ((System.Drawing.Image)(resources.GetObject("jump2.Image")));
            this.jump2.Location = new System.Drawing.Point(87, 154);
            this.jump2.Name = "jump2";
            this.jump2.Size = new System.Drawing.Size(80, 32);
            this.jump2.TabIndex = 17;
            this.jump2.UseVisualStyleBackColor = true;
            // 
            // b2_3
            // 
            this.b2_3.AutoSize = true;
            this.b2_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b2_3.Location = new System.Drawing.Point(158, 305);
            this.b2_3.Name = "b2_3";
            this.b2_3.Size = new System.Drawing.Size(16, 18);
            this.b2_3.TabIndex = 29;
            this.b2_3.Text = "0";
            this.b2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // b2_1
            // 
            this.b2_1.AutoSize = true;
            this.b2_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b2_1.Location = new System.Drawing.Point(6, 305);
            this.b2_1.Name = "b2_1";
            this.b2_1.Size = new System.Drawing.Size(16, 18);
            this.b2_1.TabIndex = 27;
            this.b2_1.Text = "0";
            this.b2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // b2_2
            // 
            this.b2_2.AutoSize = true;
            this.b2_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b2_2.Location = new System.Drawing.Point(82, 305);
            this.b2_2.Name = "b2_2";
            this.b2_2.Size = new System.Drawing.Size(16, 18);
            this.b2_2.TabIndex = 28;
            this.b2_2.Text = "0";
            this.b2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Sienna;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(276, 397);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 18);
            this.label12.TabIndex = 42;
            this.label12.Text = "LINEA";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Sienna;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(321, 353);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(69, 73);
            this.label18.TabIndex = 43;
            this.label18.Text = "2";
            // 
            // textDump
            // 
            this.textDump.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textDump.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textDump.ForeColor = System.Drawing.Color.Gold;
            this.textDump.Location = new System.Drawing.Point(17, 5);
            this.textDump.MaxLength = 0;
            this.textDump.Multiline = true;
            this.textDump.Name = "textDump";
            this.textDump.ReadOnly = true;
            this.textDump.Size = new System.Drawing.Size(357, 77);
            this.textDump.TabIndex = 49;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1284, 661);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.panelsubcab);
            this.Controls.Add(this.cabecera);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CONTROL DE BARRILES";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.cabecera.ResumeLayout(false);
            this.cabecera.PerformLayout();
            this.panelcabcentral.ResumeLayout(false);
            this.panelcabcentral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBoton)).EndInit();
            this.PanelConnect.ResumeLayout(false);
            this.PanelConnect.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureon)).EndInit();
            this.panelsubcab.ResumeLayout(false);
            this.panelsubcab.PerformLayout();
            this.panelAlarmas.ResumeLayout(false);
            this.panelAlarmas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCO2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPU)).EndInit();
            this.panel.ResumeLayout(false);
            this.panelHMI.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.p31b3.ResumeLayout(false);
            this.p32b3.ResumeLayout(false);
            this.p32b3.PerformLayout();
            this.p31b2.ResumeLayout(false);
            this.p32b2.ResumeLayout(false);
            this.p32b2.PerformLayout();
            this.p31b1.ResumeLayout(false);
            this.p32b1.ResumeLayout(false);
            this.p32b1.PerformLayout();
            this.subpanel3b.ResumeLayout(false);
            this.subpanel3b.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.df3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_paso)).EndInit();
            this.subpanel3.ResumeLayout(false);
            this.subpanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB33)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.p11b3.ResumeLayout(false);
            this.p12b3.ResumeLayout(false);
            this.p12b3.PerformLayout();
            this.p11b2.ResumeLayout(false);
            this.p12b2.ResumeLayout(false);
            this.p12b2.PerformLayout();
            this.p11b1.ResumeLayout(false);
            this.p12b1.ResumeLayout(false);
            this.p12b1.PerformLayout();
            this.subpanel1.ResumeLayout(false);
            this.subpanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picB13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha1)).EndInit();
            this.subpanel1b.ResumeLayout(false);
            this.subpanel1b.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.df1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_paso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_3)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.p21b3.ResumeLayout(false);
            this.p22b3.ResumeLayout(false);
            this.p22b3.PerformLayout();
            this.p21b2.ResumeLayout(false);
            this.p22b2.ResumeLayout(false);
            this.p22b2.PerformLayout();
            this.p21b1.ResumeLayout(false);
            this.p22b1.ResumeLayout(false);
            this.p22b1.PerformLayout();
            this.subpanel2b.ResumeLayout(false);
            this.subpanel2b.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.df2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_paso)).EndInit();
            this.subpanel2.ResumeLayout(false);
            this.subpanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB21)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel cabecera;
        private System.Windows.Forms.PictureBox pictureon;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel panelcabcentral;
        private System.Windows.Forms.Button off;
        private System.Windows.Forms.Panel PanelConnect;
        private System.Windows.Forms.Button on;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox slotText;
        private System.Windows.Forms.TextBox rackText;
        private System.Windows.Forms.TextBox ipText;
        private System.Windows.Forms.TextBox dnsText;
        private System.Windows.Forms.TextBox textip;
        private System.Windows.Forms.PictureBox pictureBoxBoton;
        private System.Windows.Forms.Label labelonoff;
        private System.Windows.Forms.Panel panelsubcab;
        public System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Panel panelHMI;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel subpanel3b;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.PictureBox df3;
        private System.Windows.Forms.PictureBox FB_3;
        private System.Windows.Forms.PictureBox FA_3;
        private System.Windows.Forms.PictureBox v3_1;
        private System.Windows.Forms.Label b3_3;
        private System.Windows.Forms.PictureBox v3_2;
        private System.Windows.Forms.Label b3_2;
        private System.Windows.Forms.PictureBox v3_3;
        private System.Windows.Forms.Label b3_1;
        private System.Windows.Forms.PictureBox v3_paso;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel subpanel3;
        private System.Windows.Forms.Button run3;
        private System.Windows.Forms.Button stop3;
        private System.Windows.Forms.Label labelbombilla3;
        private System.Windows.Forms.PictureBox picbombilla3;
        private System.Windows.Forms.PictureBox picparomarcha3;
        private System.Windows.Forms.PictureBox picB31;
        private System.Windows.Forms.PictureBox picB32;
        private System.Windows.Forms.PictureBox picB33;
        private System.Windows.Forms.Button jump3;
        private System.Windows.Forms.Button refresh3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel subpanel2b;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox df2;
        private System.Windows.Forms.PictureBox FB_2;
        private System.Windows.Forms.PictureBox FA_2;
        private System.Windows.Forms.PictureBox v2_1;
        private System.Windows.Forms.Label b2_3;
        private System.Windows.Forms.PictureBox v2_2;
        private System.Windows.Forms.Label b2_2;
        private System.Windows.Forms.PictureBox v2_3;
        private System.Windows.Forms.Label b2_1;
        private System.Windows.Forms.PictureBox v2_paso;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel subpanel2;
        private System.Windows.Forms.Button run2;
        private System.Windows.Forms.Button stop2;
        private System.Windows.Forms.Label labelbombilla2;
        private System.Windows.Forms.PictureBox picbombilla2;
        private System.Windows.Forms.PictureBox picparomarcha2;
        private System.Windows.Forms.PictureBox picB23;
        private System.Windows.Forms.PictureBox picB22;
        private System.Windows.Forms.PictureBox picB21;
        private System.Windows.Forms.Button refresh2;
        private System.Windows.Forms.Button jump2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel subpanel1;
        private System.Windows.Forms.PictureBox picB13;
        private System.Windows.Forms.PictureBox picB12;
        private System.Windows.Forms.PictureBox picB11;
        private System.Windows.Forms.Label labelbombilla1;
        private System.Windows.Forms.Button run1;
        private System.Windows.Forms.Button stop1;
        private System.Windows.Forms.PictureBox picbombilla1;
        private System.Windows.Forms.PictureBox picparomarcha1;
        private System.Windows.Forms.Button jump1;
        private System.Windows.Forms.Button refresh1;
        private System.Windows.Forms.Panel subpanel1b;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox df1;
        private System.Windows.Forms.PictureBox FA_1;
        private System.Windows.Forms.PictureBox v1_1;
        private System.Windows.Forms.PictureBox FB_1;
        private System.Windows.Forms.PictureBox v1_2;
        private System.Windows.Forms.PictureBox v1_paso;
        private System.Windows.Forms.PictureBox v1_3;
        private System.Windows.Forms.Label b1_3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label b1_2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label b1_1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelFUresultado;
        private System.Windows.Forms.Label labelFU;
        private System.Windows.Forms.PictureBox picFU;
        private System.Windows.Forms.Label labelPUresultado;
        private System.Windows.Forms.Label labelPU;
        private System.Windows.Forms.PictureBox picPU;
        private System.Windows.Forms.Label labelCAresultado;
        private System.Windows.Forms.Label labelCA;
        private System.Windows.Forms.PictureBox picCA;
        private System.Windows.Forms.Label labelCO2resultado;
        private System.Windows.Forms.Label labelGas;
        private System.Windows.Forms.PictureBox picCO2;
        private System.Windows.Forms.Panel panelAlarmas;
        private System.Windows.Forms.Panel p11b1;
        private System.Windows.Forms.Panel p12b1;
        private System.Windows.Forms.Panel p11b2;
        private System.Windows.Forms.Panel p12b2;
        private System.Windows.Forms.Panel p11b3;
        private System.Windows.Forms.Panel p12b3;
        private System.Windows.Forms.Panel p31b3;
        private System.Windows.Forms.Panel p32b3;
        private System.Windows.Forms.Panel p31b2;
        private System.Windows.Forms.Panel p32b2;
        private System.Windows.Forms.Panel p31b1;
        private System.Windows.Forms.Panel p32b1;
        private System.Windows.Forms.Panel p21b3;
        private System.Windows.Forms.Panel p22b3;
        private System.Windows.Forms.Panel p21b2;
        private System.Windows.Forms.Panel p22b2;
        private System.Windows.Forms.Panel p21b1;
        private System.Windows.Forms.Panel p22b1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label registro;
        private System.Windows.Forms.Label acumulado;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textDump;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}

