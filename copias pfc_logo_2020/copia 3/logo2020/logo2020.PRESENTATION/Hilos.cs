﻿using logo2020.APPLICATION;
using logo2020.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.PRESENTATION
{
    public class Hilos
    {
        /*################################################################*/
        #region Atributos
        /* Hilo para Conexión */
        private Thread hilo;

        /* PLC del hilo */
        private ProcesosConLogo proceso;
        private ProcesosConSQL sql;
        private Form1 f;
        private TextBox display;
        private bool conectado;
        /*Array que va a contener los valores tipo byte de la memoria de Logo*/
        private byte[] array = new byte[512];
        /*FORMATO AAAAMMDD*/
        private int fecha;
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region /* Constructores de Hilo */
        /// <summary>
        /// Crea el hilo que mantiene la conexión con Logo
        /// </summary>
        /// <param name="_proceso">OPERACIONES CON LOGO</param>
        /// <param name="_form">PANTALLA HMI</param>
        public Hilos(ProcesosConLogo _proceso, Form1 _form, bool _conectado, TextBox _display)
        {
            //------------------------------------------------
            #region INICIA LOS OBJETOS: Form Y ProcesosConLogo
            f = _form;
            display = _display;
            proceso = _proceso;
            sql = new ProcesosConSQL(display);
            Conectado = _conectado;

            /*Inicia fecha para comprobar consumos*/
            DateTime fechaActual = DateTime.Today;
            int fecha = (fechaActual.Year * 10000) +
                         (fechaActual.Month * 100) +
                         (fechaActual.Day);
            #endregion
            //------------------------------------------------

            //------------------------------------------------
            #region INICIA EL ARRAY DE BYTES CON VALORES 255
            for (int i = 0; i < 512; i++)
            {
                array[i] = 255;
            } 
            #endregion
            //------------------------------------------------

            //------------------------------------------------
            #region INICIO DEL HILO
            /*DELEGADO*/
            ThreadStart delegado = new ThreadStart(CorrerProceso);
            /*HILO*/
            hilo = new Thread(delegado);
            /*INICIO DE HILO*/ 
            hilo.Start();
            #endregion
            //------------------------------------------------
        }   
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region Proceso del hilo
        /// <summary>
        /// Todas las tareas que se realizan dentro del hilo
        /// </summary>
        private void CorrerProceso()
        {
            int contador_prueba = 0;
            #region CONTADOR DE BARRILES 1, 2 Y 3
            /*Esta variable sirve para almacenar los contadores de cada linea de barriles*/
            int[] contador_barriles = new int[4];
            #endregion

            //------------------------------------------------
            #region LECTURA DE MEMORIA DE LOGO Y DIBUJO DE HMI
            #region LECTURA DE MEMORIA DE LOGO
            try
            {
                proceso.LeerBloque512();
                f.escribirDisplay(" LECTURA: " +
                                      proceso.Client.PduSizeNegotiated.ToString());
            }
            catch (Exception ex)
            {
                f.escribirDisplay("ErrorCheck: " + ex.Message);
            }
            #endregion

            #region INICIA EL ARRAY DE BYTES CON VALORES DE LA LECTURA
            for (int i = 0; i < 512; i++)
            {
                array[i] = proceso.buffer512[i];
            }
            #endregion

            #region DIBUJA HMI
            f.dibujaEVA123_1();
            f.dibujaEVA123_2();
            f.dibujaEVA123_3();
            f.dibujaEVA5_1();
            f.dibujaEVA5_2();
            f.dibujaEVA5_3();
            f.dibujaDetector1();
            f.dibujaDetector2();
            f.dibujaDetector3();
            f.dibujaAlarmaCO2();
            f.dibujaAlarmaCA();
            f.dibujaAlarmaPuerta();
            f.dibujaAlarmaFusible();
            f.dibujaLitros();
            #endregion
            #endregion
            //------------------------------------------------


            //------------------------------------------------
            #region HILO ABIERTO MIENTRAS CONECTADO A LOGO
            while (proceso.EstaConectado() && Conectado)
            {
                #region BUCLE DEL HILO    
                /*Lectura de memoria del Logo*/
                proceso.LeerBloque512();
                #region LECTURA DE MEMORIA DE LOGO
                try
                {
                    proceso.LeerBloque512();
                    f.escribirDisplay(" LECTURA OK: " + proceso.Client.ExecutionTime.ToString() + "ms " +
                                          proceso.Client.PduSizeNegotiated.ToString());
                }
                catch (Exception ex)
                {
                    f.escribirDisplay("ErrorCheck: " + ex.Message);
                }
                #endregion

                #region REALIZA EL RECUENTO DE BARRILES DE CADA UNA DE LAS LÍNEAS
                #region CONTADOR DE LINEA 1
                contador_barriles[0] = (proceso.buffer512[121]) |
                                        (proceso.buffer512[120] << 8) |
                                         (proceso.buffer512[119] << 16) |
                                          (proceso.buffer512[118] << 24);
                #endregion
                #region CONTADOR DE LINEA 2
                contador_barriles[1] = (proceso.buffer512[125]) |
                                        (proceso.buffer512[124] << 8) |
                                         (proceso.buffer512[123] << 16) |
                                          (proceso.buffer512[122] << 24);
                #endregion
                #region CONTADOR DE LINEA 3
                contador_barriles[2] = (proceso.buffer512[129]) |
                                        (proceso.buffer512[128] << 8) |
                                         (proceso.buffer512[127] << 16) |
                                          (proceso.buffer512[126] << 24);
                #endregion
                f.dibujaConsumo(contador_prueba);
                #endregion

                #region comprueba - electroválvulas 1,2 y 3
                if (array[214] != proceso.buffer512[214] ||
                    array[215] != proceso.buffer512[215] ||
                    array[216] != proceso.buffer512[216])
                {
                    f.dibujaEVA123_1();
                    array[214] = proceso.buffer512[214];
                    array[215] = proceso.buffer512[215];
                    array[216] = proceso.buffer512[216];
                }
                #endregion

                #region comprueba - electroválvulas 11,12 y 13
                if (array[224] != proceso.buffer512[224] ||
                    array[225] != proceso.buffer512[225] ||
                    array[226] != proceso.buffer512[226])
                {
                    f.dibujaEVA123_2();
                    array[224] = proceso.buffer512[224];
                    array[225] = proceso.buffer512[225];
                    array[226] = proceso.buffer512[226];
                }
                #endregion

                #region comprueba - electroválvulas 21,22 y 23
                if (array[234] != proceso.buffer512[234] ||
                    array[235] != proceso.buffer512[235] ||
                    array[236] != proceso.buffer512[236])
                {
                    f.dibujaEVA123_3();
                    array[234] = proceso.buffer512[234];
                    array[235] = proceso.buffer512[235];
                    array[236] = proceso.buffer512[236];
                }
                #endregion

                #region comprueba - electroválvulas 5
                if (array[310] != proceso.buffer512[310])
                {
                    f.dibujaEVA5_1();
                    array[310] = proceso.buffer512[310];
                }
                #endregion

                #region comprueba - electroválvulas 15
                if (array[320] != proceso.buffer512[320])
                {
                    f.dibujaEVA5_2();
                    array[320] = proceso.buffer512[320];
                }
                #endregion

                #region comprueba - electroválvulas 25
                if (array[330] != proceso.buffer512[330])
                {
                    f.dibujaEVA5_3();
                    array[330] = proceso.buffer512[330];
                }
                #endregion

                #region comprueba - detectores1
                if (array[210] != proceso.buffer512[210])
                {
                    f.dibujaDetector1();
                    array[210] = proceso.buffer512[210];
                }
                #endregion

                #region comprueba - detectores2
                if (array[220] != proceso.buffer512[220])
                {
                    f.dibujaDetector2();
                    array[220] = proceso.buffer512[220];
                }
                #endregion

                #region comprueba - detectores3
                if (array[230] != proceso.buffer512[230])
                {
                    f.dibujaDetector3();
                    array[230] = proceso.buffer512[230];
                }
                #endregion

                #region comprueba - alarma co2
                if (array[208] != proceso.buffer512[208])
                {
                    f.dibujaAlarmaCO2();
                    array[208] = proceso.buffer512[208];
                }
                #endregion

                #region comprueba - alarma ca
                if (array[255] != proceso.buffer512[255])
                {
                    f.dibujaAlarmaCA();
                    array[255] = proceso.buffer512[255];
                }
                #endregion

                #region comprueba - alarma puerta
                if (array[209] != proceso.buffer512[209])
                {
                    f.dibujaAlarmaPuerta();
                    array[209] = proceso.buffer512[209];
                }
                #endregion

                #region comprueba - alarma fusible
                if (array[250] != proceso.buffer512[250])
                {
                    f.dibujaAlarmaFusible();
                    array[250] = proceso.buffer512[250];
                }
                #endregion

                #region DIBUJA LITROS
                f.dibujaLitros();
                #endregion

                #region ACTUALIZA EL ARRAY DE BYTES CON VALORES DE LA LECTURA
                for (int i = 0; i < 512; i++)
                {
                    array[i] = proceso.buffer512[i];
                }
                #endregion

                #region COMPRUEBA SI EXISTE UN REGISTRO DE CONSUMO EN EL DÍA ACTUAL  Y LO GRABA
                /*Comprobar y grabar consumos*/
                /*
                DateTime fechaActual = DateTime.Today;
                int _fecha = (fechaActual.Year * 10000) +
                             (fechaActual.Month * 100) +
                             (fechaActual.Day + contador_prueba);
                if (_fecha > fecha)
                {
                    if (!sql.comprobarConsumo(_fecha))
                    {
                        int total = (contador_barriles[0] + contador_barriles[1] + contador_barriles[2] + contador_prueba * 100);
                        Consumos consumo = 
                            new Consumos(total, total-sql.recuperarConsumo(), fechaActual.Day, fechaActual.Month, fechaActual.Year);
                        MessageBox.Show(sql.recuperarConsumo().ToString());
                        sql.grabarConsumo(consumo);
                        fecha = _fecha;
                    }
                }
                */
                #endregion

                /*Duerme el hilo x segundos*/
                Thread.Sleep(500);
                //contador_prueba++;
                #endregion
            }
            #endregion
            //------------------------------------------------


            //------------------------------------------------
            #region FIN DEL HILO
            /*Detiene y elimina el hilo*/
            hilo.Abort();
            #endregion
            //------------------------------------------------
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region PROPIEDADES
        public bool Conectado { get => conectado; set => conectado = value; }
        #endregion
        /*################################################################*/
    }
}
