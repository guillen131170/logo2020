﻿using logo2020.CORE;
using logo2020.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.APPLICATION
{
    public class ProcesosConSQL
    {
        #region ATRIBUTOS
        private SqlTrans trans;
        #endregion


        #region CONSTRUCTOR
        /// <summary>
        /// Crea un objeto ProcesosConSQL inicializando un objeto SqlTrans
        /// 'trans' es un objeto para operar con db
        /// </summary>
        public ProcesosConSQL() { trans = new SqlTrans(); } 
        #endregion


        public bool comprobarConsumo(int f)
        {
            return trans.comprobar(f);
        }


        public bool grabarConsumo(Consumos c)
        {
            return trans.grabar(c.Accumulate, c.Amount, c._Day, c._Mounth, c._Year, c._Date, c.Pds);
        }


        public int recuperarConsumo()
        {
            return trans.recuperar();
        }
    }
}
