﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User2020.CORE
{
    /// <summary>
    /// ESTA CLASE SERÁN LOS DESTINATARIOS DE CORREOS ELECTRÓNICOS
    /// PARA LAS NOTIFICACIONES
    /// DE LAS ALARMAS
    /// 
    /// atributos:
    ///     - id - identificador de la dirrección de correo
    ///     - name - nombre del propietario de la dirrección de correo
    ///     - email - cuenta de correo electrónico
    ///     - level - indica el nivel del usuario y sirve para indicar
    ///               a quién se dirige el correo y a quién no
    ///               (VA A DEPENDER DE LA ALARMA DE QUE SE TARTE)
    /// </summary>   
    public class User
    {
        /*################################################################*/
        #region atributos     
        /// <summary>
        /// identificador de la dirrección de correo
        /// </summary>
        private int id;

        /// <summary>
        /// nombre del propietario de la dirrección de correo
        /// </summary>
        private string name;

        /// <summary>
        /// cuenta de correo electrónico
        /// </summary>
        private string email;

        /// <summary>
        /// número de nivel de la dirrección de correo
        /// </summary>
        private int level;
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region /* constructores */

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public User() { }

        /// <summary>
        /// Constructor por parámetros (4 - todos)
        /// </summary>
        public User(int id, string email, string name, int level)
        {
            Id = id;
            Email = email;
            Name = name;
            Level = level;
        }

        /// <summary>
        /// Constructor por parámetros (3 - email,name)
        /// </summary>
        public User(string email, string name, int level)
        {
            Email = email;
            Name = name;
            Level = level;
        }

        /// <summary>
        /// Constructor copia
        /// </summary>
        public User(User o)
        {
            Id = o.Id;
            Email = o.Email;
            Name = o.Name;
            Level = o.Level;
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region propiedades
        [Required]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Required]
        [StringLength(120)]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Required]
        [EmailAddress]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [Required]
        [Range(0, 3)]
        public int Level
        {
            get { return level; }
            set { level = value; }
        }
        #endregion
        /*################################################################*/
    }
}
