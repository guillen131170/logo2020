﻿using logo2020.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.DAL
{
    public class SqlTrans
    {
        #region ATRIBUTOS
        /// <summary>
        /// Cadena de conexión
        /// </summary>
        private string cadenaconexion =
            "server=DESKTOP-2M3VDIP\\SQLEXPRESS; database=pfc_2020 ; integrated security = true";    
        /// <summary>
        /// Objeto para CONECTAR con db
        /// </summary>
        public SqlConnection conexion;
        /// <summary>
        /// Objeto para CONSULTAR con db
        /// </summary>
        SqlCommand cmd;
        /// <summary>
        /// Objeto que guarada RESULTADO  de las consultas
        /// </summary>
        private SqlDataReader dr;
        private TextBox display;
        #endregion


        #region CONSTRUCTOR
        /// <summary>
        /// Constructor por defecto - sin parámetros
        /// </summary>
        public SqlTrans() 
        {
            try
            {
                conexion = new SqlConnection(CadenaConexion);
            }
            catch (Exception ex)
            {
                display.Text = "ErrorCheck: " + ex.Message;
            }
        }

        /// <summary>
        /// Constructor con 1 parámetro
        /// </summary>
        /// <param name="_display">cuadro de texto que mostrará los mensajes</param>
        public SqlTrans(TextBox _display)
        {
            display = _display;
            try
            {
                conexion = new SqlConnection(CadenaConexion);
            }
            catch (Exception ex)
            {
                display.Text = "ErrorCheck: " + ex.Message;
            }
        }
        #endregion


        #region COMPRUEBA SI HAY REGISTRADO UN CONSUMO EN LA FECHA INDICADA    
        /// <summary>
        /// Comprueba si hay algún consumo registrado
        /// en la fecha indicada en el parámetro
        /// </summary>
        /// <param name="_fecha">fecha a comprobar</param>
        /// <returns>verdadero o falso</returns>
        public bool comprobar(int _fecha)
        {
            bool valor = false;

            try
            {
                #region PREPARA LA CONSULTA A DB
                conexion.Open();
                string query =
                    "select * from Consumos " +
                    "where _date = @FECHA";
                cmd = new SqlCommand(query, conexion);
                cmd.Parameters.Add(new SqlParameter("FECHA", _fecha));
                #endregion
                if ((Int32)cmd.ExecuteNonQuery() > 0) valor = true;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("ErrorCheck: " + ex.Message);
                display.Text = "ErrorCheck: " + ex.Message;
            }
            finally
            {
                conexion.Close();
            }
            return valor;
        }
        #endregion


        #region GRABA UN CONSUMO EN LA FECHA INDICADA    
        /// <summary>
        /// Graba un consumo
        /// en la fecha indicada en el parámetro
        /// </summary>
        /// <param name="_fecha">fecha a grabar</param>
        /// <returns>verdadero o falso</returns>
        public bool grabar(int _Accumulate, int _Amount, int _Day, int _Month, int _Year, int _Date, int _Pds)
        {
            bool valor = false;
            try
            {
                #region PREPARA LA CONSULTA A DB
                conexion.Open();
                string query =
                    "INSERT INTO Consumos(accumulate,amount,_day,_month,_year,_date,pds) " +
                    "VALUES(@Accumulate,@Amount,@Day,@Month,@Year,@Date,@Pds)";
                cmd = new SqlCommand(query, conexion);
                cmd.Parameters.Add(new SqlParameter("Accumulate", _Accumulate));
                cmd.Parameters.Add(new SqlParameter("Amount", _Amount));
                cmd.Parameters.Add(new SqlParameter("Day", _Day));
                cmd.Parameters.Add(new SqlParameter("Month", _Month));
                cmd.Parameters.Add(new SqlParameter("Year", _Year));
                cmd.Parameters.Add(new SqlParameter("Date", _Date));
                cmd.Parameters.Add(new SqlParameter("Pds", _Pds));
                #endregion
                if ((Int32)cmd.ExecuteNonQuery() > 0) valor = true;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("ErrorCheck: " + ex.Message);
                display.Text = "ErrorCheck: " + ex.Message;
            }
            finally
            {
                conexion.Close();
            }
            return valor;
        }
        #endregion


        #region RECUPERA EL ÚLTIMO CONSUMO REGISTRADO
        /// <summary>
        /// Recupera el último consumo de la db
        /// </summary>
        /// <param name="_fecha">fecha a comprobar</param>
        /// <returns>verdadero o falso</returns>
        public int recuperar()
        {
            int resultado = 0;

            try
            {
                #region PREPARA LA CONSULTA A DB
                conexion.Open();
                string query =
                    "SELECT MAX(accumulate) " +
                    "FROM Consumos";
                cmd = new SqlCommand(query, conexion);
                #endregion
                resultado = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                //MessageBox.Show("ErrorCheck: " + ex.Message);
                display.Text = "ErrorCheck: " + ex.Message;
            }
            finally
            {
                conexion.Close();
            }
            return resultado;
        }
        #endregion


        #region ABRIR CONEXIÓN CON DB
        /// <summary>
        /// Función para conectar con base de datos
        /// </summary>
        public void Conectar()
        {
            try
            {
                this.conexion.Open();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("ErrorConnect: " + ex);
                display.Text = "ErrorCheck: " + ex.Message;
            }
        }
        #endregion


        #region CERRAR CONEXIÓN CON DB
        /// <summary>
        /// Función para desconectar con base de datos
        /// </summary>
        public void Desconectar()
        {
            try
            {
                this.conexion.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("ErrorDisconnect: " + ex);
                display.Text = "ErrorCheck: " + ex.Message;
            }
        }
        #endregion


        #region PROPIEDADES     
        /// <summary>
        /// Cadena de conexión para la base de datos 'PFC_2020'
        /// </summary>
        public string CadenaConexion
        {
            get { return cadenaconexion; }
        }
        #endregion
    }
}
