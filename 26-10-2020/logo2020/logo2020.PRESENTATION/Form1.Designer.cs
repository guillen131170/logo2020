﻿namespace logo2020.PRESENTATION
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.cabecera = new System.Windows.Forms.Panel();
            this.textpdu = new System.Windows.Forms.TextBox();
            this.textip = new System.Windows.Forms.TextBox();
            this.panelcabcentral = new System.Windows.Forms.Panel();
            this.PanelConnect = new System.Windows.Forms.Panel();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.iptextlocal = new System.Windows.Forms.TextBox();
            this.labelonoff = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.slotText = new System.Windows.Forms.TextBox();
            this.rackText = new System.Windows.Forms.TextBox();
            this.ipText = new System.Windows.Forms.TextBox();
            this.dnsText = new System.Windows.Forms.TextBox();
            this.panelsubcab = new System.Windows.Forms.Panel();
            this.panelAlarmas = new System.Windows.Forms.Panel();
            this.labelFUresultado = new System.Windows.Forms.Label();
            this.labelCA = new System.Windows.Forms.Label();
            this.labelFU = new System.Windows.Forms.Label();
            this.labelGas = new System.Windows.Forms.Label();
            this.labelPUresultado = new System.Windows.Forms.Label();
            this.labelCO2resultado = new System.Windows.Forms.Label();
            this.labelPU = new System.Windows.Forms.Label();
            this.labelCAresultado = new System.Windows.Forms.Label();
            this.textDump = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.registro = new System.Windows.Forms.Label();
            this.acumulado3 = new System.Windows.Forms.Label();
            this.panel = new System.Windows.Forms.Panel();
            this.panelHMI = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.vendido3 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.p31b3 = new System.Windows.Forms.Panel();
            this.p32b3 = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.p31b2 = new System.Windows.Forms.Panel();
            this.p32b2 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.p31b1 = new System.Windows.Forms.Panel();
            this.p32b1 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.subpanel3b = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.subpanel3 = new System.Windows.Forms.Panel();
            this.progressBarB3 = new System.Windows.Forms.ProgressBar();
            this.labelbombilla3 = new System.Windows.Forms.Label();
            this.b3_1 = new System.Windows.Forms.Label();
            this.b3_3 = new System.Windows.Forms.Label();
            this.b3_2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.vendido1 = new System.Windows.Forms.Label();
            this.acumulado1 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.p11b3 = new System.Windows.Forms.Panel();
            this.p12b3 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.p11b2 = new System.Windows.Forms.Panel();
            this.p12b2 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.p11b1 = new System.Windows.Forms.Panel();
            this.p12b1 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.subpanel1 = new System.Windows.Forms.Panel();
            this.progressBarB1 = new System.Windows.Forms.ProgressBar();
            this.labelbombilla1 = new System.Windows.Forms.Label();
            this.subpanel1b = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.b1_1 = new System.Windows.Forms.Label();
            this.b1_2 = new System.Windows.Forms.Label();
            this.b1_3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.vendido2 = new System.Windows.Forms.Label();
            this.acumulado2 = new System.Windows.Forms.Label();
            this.p21b3 = new System.Windows.Forms.Panel();
            this.p22b3 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.p21b2 = new System.Windows.Forms.Panel();
            this.p22b2 = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.p21b1 = new System.Windows.Forms.Panel();
            this.p22b1 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.subpanel2b = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.subpanel2 = new System.Windows.Forms.Panel();
            this.progressBarB2 = new System.Windows.Forms.ProgressBar();
            this.labelbombilla2 = new System.Windows.Forms.Label();
            this.b2_3 = new System.Windows.Forms.Label();
            this.b2_1 = new System.Windows.Forms.Label();
            this.b2_2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.timer1_jump1 = new System.Windows.Forms.Timer(this.components);
            this.timer2_jump2 = new System.Windows.Forms.Timer(this.components);
            this.timer3_jump3 = new System.Windows.Forms.Timer(this.components);
            this.timer1_refresh1 = new System.Windows.Forms.Timer(this.components);
            this.timer2_refresh2 = new System.Windows.Forms.Timer(this.components);
            this.timer3_refresh3 = new System.Windows.Forms.Timer(this.components);
            this.panel4 = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.timered = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.inicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarSesiónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administradorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.escribirEnLogoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enviarDatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.baseDeDatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarConexiónBDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alarmasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Q25 = new System.Windows.Forms.PictureBox();
            this.Q24 = new System.Windows.Forms.PictureBox();
            this.Q23 = new System.Windows.Forms.PictureBox();
            this.Q22 = new System.Windows.Forms.PictureBox();
            this.Q21 = new System.Windows.Forms.PictureBox();
            this.Q15 = new System.Windows.Forms.PictureBox();
            this.Q14 = new System.Windows.Forms.PictureBox();
            this.Q13 = new System.Windows.Forms.PictureBox();
            this.Q12 = new System.Windows.Forms.PictureBox();
            this.Q11 = new System.Windows.Forms.PictureBox();
            this.Q5 = new System.Windows.Forms.PictureBox();
            this.Q4 = new System.Windows.Forms.PictureBox();
            this.Q3 = new System.Windows.Forms.PictureBox();
            this.Q2 = new System.Windows.Forms.PictureBox();
            this.Q1 = new System.Windows.Forms.PictureBox();
            this.FA_3 = new System.Windows.Forms.PictureBox();
            this.FB_3 = new System.Windows.Forms.PictureBox();
            this.v3_1 = new System.Windows.Forms.PictureBox();
            this.v3_2 = new System.Windows.Forms.PictureBox();
            this.v3_3 = new System.Windows.Forms.PictureBox();
            this.v3_paso = new System.Windows.Forms.PictureBox();
            this.df3 = new System.Windows.Forms.PictureBox();
            this.run3 = new System.Windows.Forms.Button();
            this.stop3 = new System.Windows.Forms.Button();
            this.picB31 = new System.Windows.Forms.PictureBox();
            this.picB32 = new System.Windows.Forms.PictureBox();
            this.picB33 = new System.Windows.Forms.PictureBox();
            this.jump3 = new System.Windows.Forms.Button();
            this.refresh3 = new System.Windows.Forms.Button();
            this.picparomarcha3 = new System.Windows.Forms.PictureBox();
            this.picbombilla3 = new System.Windows.Forms.PictureBox();
            this.picB13 = new System.Windows.Forms.PictureBox();
            this.picB12 = new System.Windows.Forms.PictureBox();
            this.picB11 = new System.Windows.Forms.PictureBox();
            this.run1 = new System.Windows.Forms.Button();
            this.stop1 = new System.Windows.Forms.Button();
            this.jump1 = new System.Windows.Forms.Button();
            this.refresh1 = new System.Windows.Forms.Button();
            this.picparomarcha1 = new System.Windows.Forms.PictureBox();
            this.picbombilla1 = new System.Windows.Forms.PictureBox();
            this.FA_1 = new System.Windows.Forms.PictureBox();
            this.v1_1 = new System.Windows.Forms.PictureBox();
            this.FB_1 = new System.Windows.Forms.PictureBox();
            this.v1_2 = new System.Windows.Forms.PictureBox();
            this.v1_paso = new System.Windows.Forms.PictureBox();
            this.v1_3 = new System.Windows.Forms.PictureBox();
            this.df1 = new System.Windows.Forms.PictureBox();
            this.FA_2 = new System.Windows.Forms.PictureBox();
            this.FB_2 = new System.Windows.Forms.PictureBox();
            this.v2_1 = new System.Windows.Forms.PictureBox();
            this.v2_2 = new System.Windows.Forms.PictureBox();
            this.v2_3 = new System.Windows.Forms.PictureBox();
            this.v2_paso = new System.Windows.Forms.PictureBox();
            this.df2 = new System.Windows.Forms.PictureBox();
            this.run2 = new System.Windows.Forms.Button();
            this.stop2 = new System.Windows.Forms.Button();
            this.picB23 = new System.Windows.Forms.PictureBox();
            this.picB22 = new System.Windows.Forms.PictureBox();
            this.picB21 = new System.Windows.Forms.PictureBox();
            this.refresh2 = new System.Windows.Forms.Button();
            this.jump2 = new System.Windows.Forms.Button();
            this.picparomarcha2 = new System.Windows.Forms.PictureBox();
            this.picbombilla2 = new System.Windows.Forms.PictureBox();
            this.picCO2 = new System.Windows.Forms.PictureBox();
            this.picFU = new System.Windows.Forms.PictureBox();
            this.picCA = new System.Windows.Forms.PictureBox();
            this.picPU = new System.Windows.Forms.PictureBox();
            this.pictureBoxBoton = new System.Windows.Forms.PictureBox();
            this.off = new System.Windows.Forms.Button();
            this.on = new System.Windows.Forms.Button();
            this.pictureon = new System.Windows.Forms.PictureBox();
            this.picturepc = new System.Windows.Forms.PictureBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.cabecera.SuspendLayout();
            this.panelcabcentral.SuspendLayout();
            this.PanelConnect.SuspendLayout();
            this.panelsubcab.SuspendLayout();
            this.panelAlarmas.SuspendLayout();
            this.panel.SuspendLayout();
            this.panelHMI.SuspendLayout();
            this.panel3.SuspendLayout();
            this.p31b3.SuspendLayout();
            this.p32b3.SuspendLayout();
            this.p31b2.SuspendLayout();
            this.p32b2.SuspendLayout();
            this.p31b1.SuspendLayout();
            this.p32b1.SuspendLayout();
            this.subpanel3b.SuspendLayout();
            this.subpanel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.p11b3.SuspendLayout();
            this.p12b3.SuspendLayout();
            this.p11b2.SuspendLayout();
            this.p12b2.SuspendLayout();
            this.p11b1.SuspendLayout();
            this.p12b1.SuspendLayout();
            this.subpanel1.SuspendLayout();
            this.subpanel1b.SuspendLayout();
            this.panel2.SuspendLayout();
            this.p21b3.SuspendLayout();
            this.p22b3.SuspendLayout();
            this.p21b2.SuspendLayout();
            this.p22b2.SuspendLayout();
            this.p21b1.SuspendLayout();
            this.p22b1.SuspendLayout();
            this.subpanel2b.SuspendLayout();
            this.subpanel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Q25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_paso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.df3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_paso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.df1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_paso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.df2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCO2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBoton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picturepc)).BeginInit();
            this.SuspendLayout();
            // 
            // cabecera
            // 
            this.cabecera.BackColor = System.Drawing.SystemColors.Control;
            this.cabecera.Controls.Add(this.textpdu);
            this.cabecera.Controls.Add(this.textip);
            this.cabecera.Controls.Add(this.panelcabcentral);
            this.cabecera.Controls.Add(this.pictureon);
            this.cabecera.Controls.Add(this.picturepc);
            this.cabecera.Location = new System.Drawing.Point(12, 32);
            this.cabecera.Name = "cabecera";
            this.cabecera.Size = new System.Drawing.Size(712, 140);
            this.cabecera.TabIndex = 0;
            // 
            // textpdu
            // 
            this.textpdu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(213)))), ((int)(((byte)(48)))));
            this.textpdu.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textpdu.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textpdu.ForeColor = System.Drawing.Color.Blue;
            this.textpdu.Location = new System.Drawing.Point(15, 72);
            this.textpdu.Name = "textpdu";
            this.textpdu.ReadOnly = true;
            this.textpdu.Size = new System.Drawing.Size(67, 13);
            this.textpdu.TabIndex = 23;
            // 
            // textip
            // 
            this.textip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(213)))), ((int)(((byte)(48)))));
            this.textip.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textip.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textip.ForeColor = System.Drawing.Color.Blue;
            this.textip.Location = new System.Drawing.Point(15, 57);
            this.textip.Name = "textip";
            this.textip.ReadOnly = true;
            this.textip.Size = new System.Drawing.Size(67, 13);
            this.textip.TabIndex = 22;
            // 
            // panelcabcentral
            // 
            this.panelcabcentral.BackColor = System.Drawing.Color.Sienna;
            this.panelcabcentral.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelcabcentral.Controls.Add(this.PanelConnect);
            this.panelcabcentral.Location = new System.Drawing.Point(148, 3);
            this.panelcabcentral.Name = "panelcabcentral";
            this.panelcabcentral.Size = new System.Drawing.Size(561, 133);
            this.panelcabcentral.TabIndex = 1;
            // 
            // PanelConnect
            // 
            this.PanelConnect.BackColor = System.Drawing.Color.CadetBlue;
            this.PanelConnect.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelConnect.Controls.Add(this.label53);
            this.PanelConnect.Controls.Add(this.label52);
            this.PanelConnect.Controls.Add(this.pictureBoxBoton);
            this.PanelConnect.Controls.Add(this.iptextlocal);
            this.PanelConnect.Controls.Add(this.labelonoff);
            this.PanelConnect.Controls.Add(this.progressBar1);
            this.PanelConnect.Controls.Add(this.off);
            this.PanelConnect.Controls.Add(this.on);
            this.PanelConnect.Controls.Add(this.label5);
            this.PanelConnect.Controls.Add(this.label4);
            this.PanelConnect.Controls.Add(this.label3);
            this.PanelConnect.Controls.Add(this.label2);
            this.PanelConnect.Controls.Add(this.slotText);
            this.PanelConnect.Controls.Add(this.rackText);
            this.PanelConnect.Controls.Add(this.ipText);
            this.PanelConnect.Controls.Add(this.dnsText);
            this.PanelConnect.Location = new System.Drawing.Point(3, 2);
            this.PanelConnect.Name = "PanelConnect";
            this.PanelConnect.Size = new System.Drawing.Size(553, 127);
            this.PanelConnect.TabIndex = 14;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(8, 99);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(217, 15);
            this.label53.TabIndex = 52;
            this.label53.Text = "CONEXIÓN PC - PLC LOGO SIEMENS";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(214, 42);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(48, 18);
            this.label52.TabIndex = 51;
            this.label52.Text = "IP-pc";
            // 
            // iptextlocal
            // 
            this.iptextlocal.BackColor = System.Drawing.SystemColors.Window;
            this.iptextlocal.Enabled = false;
            this.iptextlocal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iptextlocal.ForeColor = System.Drawing.Color.Black;
            this.iptextlocal.Location = new System.Drawing.Point(268, 39);
            this.iptextlocal.Name = "iptextlocal";
            this.iptextlocal.ReadOnly = true;
            this.iptextlocal.Size = new System.Drawing.Size(110, 24);
            this.iptextlocal.TabIndex = 50;
            // 
            // labelonoff
            // 
            this.labelonoff.AutoSize = true;
            this.labelonoff.Font = new System.Drawing.Font("Consolas", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelonoff.ForeColor = System.Drawing.Color.Black;
            this.labelonoff.Location = new System.Drawing.Point(426, 2);
            this.labelonoff.Name = "labelonoff";
            this.labelonoff.Size = new System.Drawing.Size(51, 28);
            this.labelonoff.TabIndex = 17;
            this.labelonoff.Text = "OFF";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(10, 117);
            this.progressBar1.MarqueeAnimationSpeed = 20;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(532, 5);
            this.progressBar1.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(159, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "SLOT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(51, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "RACK";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(27, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "IP-logo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "DNS-Host";
            // 
            // slotText
            // 
            this.slotText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slotText.Location = new System.Drawing.Point(205, 68);
            this.slotText.Name = "slotText";
            this.slotText.Size = new System.Drawing.Size(49, 24);
            this.slotText.TabIndex = 4;
            this.slotText.Text = "0";
            // 
            // rackText
            // 
            this.rackText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rackText.Location = new System.Drawing.Point(97, 68);
            this.rackText.Name = "rackText";
            this.rackText.Size = new System.Drawing.Size(49, 24);
            this.rackText.TabIndex = 3;
            this.rackText.Text = "1";
            // 
            // ipText
            // 
            this.ipText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipText.Location = new System.Drawing.Point(97, 38);
            this.ipText.Name = "ipText";
            this.ipText.Size = new System.Drawing.Size(110, 24);
            this.ipText.TabIndex = 2;
            this.ipText.Text = "192.168.0.3";
            // 
            // dnsText
            // 
            this.dnsText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dnsText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dnsText.Location = new System.Drawing.Point(97, 7);
            this.dnsText.Name = "dnsText";
            this.dnsText.Size = new System.Drawing.Size(281, 24);
            this.dnsText.TabIndex = 1;
            // 
            // panelsubcab
            // 
            this.panelsubcab.BackColor = System.Drawing.Color.Sienna;
            this.panelsubcab.Controls.Add(this.panelAlarmas);
            this.panelsubcab.Controls.Add(this.textDump);
            this.panelsubcab.Location = new System.Drawing.Point(723, 35);
            this.panelsubcab.Name = "panelsubcab";
            this.panelsubcab.Size = new System.Drawing.Size(543, 133);
            this.panelsubcab.TabIndex = 4;
            // 
            // panelAlarmas
            // 
            this.panelAlarmas.BackColor = System.Drawing.Color.CadetBlue;
            this.panelAlarmas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelAlarmas.Controls.Add(this.picCO2);
            this.panelAlarmas.Controls.Add(this.picFU);
            this.panelAlarmas.Controls.Add(this.picCA);
            this.panelAlarmas.Controls.Add(this.picPU);
            this.panelAlarmas.Controls.Add(this.labelFUresultado);
            this.panelAlarmas.Controls.Add(this.labelCA);
            this.panelAlarmas.Controls.Add(this.labelFU);
            this.panelAlarmas.Controls.Add(this.labelGas);
            this.panelAlarmas.Controls.Add(this.labelPUresultado);
            this.panelAlarmas.Controls.Add(this.labelCO2resultado);
            this.panelAlarmas.Controls.Add(this.labelPU);
            this.panelAlarmas.Controls.Add(this.labelCAresultado);
            this.panelAlarmas.Location = new System.Drawing.Point(291, 3);
            this.panelAlarmas.Name = "panelAlarmas";
            this.panelAlarmas.Size = new System.Drawing.Size(249, 128);
            this.panelAlarmas.TabIndex = 45;
            // 
            // labelFUresultado
            // 
            this.labelFUresultado.AutoSize = true;
            this.labelFUresultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFUresultado.Location = new System.Drawing.Point(3, 106);
            this.labelFUresultado.Name = "labelFUresultado";
            this.labelFUresultado.Size = new System.Drawing.Size(14, 13);
            this.labelFUresultado.TabIndex = 44;
            this.labelFUresultado.Text = "_";
            this.labelFUresultado.Visible = false;
            // 
            // labelCA
            // 
            this.labelCA.AutoSize = true;
            this.labelCA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCA.Location = new System.Drawing.Point(3, 9);
            this.labelCA.Name = "labelCA";
            this.labelCA.Size = new System.Drawing.Size(66, 26);
            this.labelCA.TabIndex = 37;
            this.labelCA.Text = "Compresor\r\nAire";
            this.labelCA.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelFU
            // 
            this.labelFU.AutoSize = true;
            this.labelFU.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFU.Location = new System.Drawing.Point(12, 73);
            this.labelFU.Name = "labelFU";
            this.labelFU.Size = new System.Drawing.Size(57, 26);
            this.labelFU.TabIndex = 43;
            this.labelFU.Text = "Botella 2\r\nCO2";
            this.labelFU.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelFU.Visible = false;
            // 
            // labelGas
            // 
            this.labelGas.AutoSize = true;
            this.labelGas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGas.Location = new System.Drawing.Point(143, 9);
            this.labelGas.Name = "labelGas";
            this.labelGas.Size = new System.Drawing.Size(46, 26);
            this.labelGas.TabIndex = 34;
            this.labelGas.Text = "Botella\r\nCO2";
            this.labelGas.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPUresultado
            // 
            this.labelPUresultado.AutoSize = true;
            this.labelPUresultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPUresultado.Location = new System.Drawing.Point(132, 106);
            this.labelPUresultado.Name = "labelPUresultado";
            this.labelPUresultado.Size = new System.Drawing.Size(14, 13);
            this.labelPUresultado.TabIndex = 41;
            this.labelPUresultado.Text = "_";
            // 
            // labelCO2resultado
            // 
            this.labelCO2resultado.AutoSize = true;
            this.labelCO2resultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCO2resultado.Location = new System.Drawing.Point(132, 45);
            this.labelCO2resultado.Name = "labelCO2resultado";
            this.labelCO2resultado.Size = new System.Drawing.Size(14, 13);
            this.labelCO2resultado.TabIndex = 35;
            this.labelCO2resultado.Text = "_";
            // 
            // labelPU
            // 
            this.labelPU.AutoSize = true;
            this.labelPU.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPU.Location = new System.Drawing.Point(140, 72);
            this.labelPU.Name = "labelPU";
            this.labelPU.Size = new System.Drawing.Size(49, 26);
            this.labelPU.TabIndex = 40;
            this.labelPU.Text = "Puerta\r\nCámara";
            this.labelPU.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelCAresultado
            // 
            this.labelCAresultado.AutoSize = true;
            this.labelCAresultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCAresultado.Location = new System.Drawing.Point(3, 43);
            this.labelCAresultado.Name = "labelCAresultado";
            this.labelCAresultado.Size = new System.Drawing.Size(14, 13);
            this.labelCAresultado.TabIndex = 38;
            this.labelCAresultado.Text = "_";
            // 
            // textDump
            // 
            this.textDump.BackColor = System.Drawing.Color.White;
            this.textDump.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDump.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDump.ForeColor = System.Drawing.Color.Blue;
            this.textDump.Location = new System.Drawing.Point(3, 4);
            this.textDump.MaxLength = 0;
            this.textDump.Multiline = true;
            this.textDump.Name = "textDump";
            this.textDump.ReadOnly = true;
            this.textDump.Size = new System.Drawing.Size(286, 65);
            this.textDump.TabIndex = 49;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(231, 279);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(137, 20);
            this.label35.TabIndex = 48;
            this.label35.Text = "TOTAL VENDIDO";
            // 
            // registro
            // 
            this.registro.AutoSize = true;
            this.registro.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.registro.Location = new System.Drawing.Point(231, 279);
            this.registro.Name = "registro";
            this.registro.Size = new System.Drawing.Size(137, 20);
            this.registro.TabIndex = 47;
            this.registro.Text = "TOTAL VENDIDO";
            // 
            // acumulado3
            // 
            this.acumulado3.AutoSize = true;
            this.acumulado3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.acumulado3.Location = new System.Drawing.Point(234, 305);
            this.acumulado3.Name = "acumulado3";
            this.acumulado3.Size = new System.Drawing.Size(18, 20);
            this.acumulado3.TabIndex = 46;
            this.acumulado3.Text = "0";
            // 
            // panel
            // 
            this.panel.Controls.Add(this.panelHMI);
            this.panel.Location = new System.Drawing.Point(12, 246);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1260, 423);
            this.panel.TabIndex = 5;
            // 
            // panelHMI
            // 
            this.panelHMI.Controls.Add(this.panel3);
            this.panelHMI.Controls.Add(this.panel1);
            this.panelHMI.Controls.Add(this.panel2);
            this.panelHMI.Location = new System.Drawing.Point(3, 4);
            this.panelHMI.Name = "panelHMI";
            this.panelHMI.Size = new System.Drawing.Size(1254, 416);
            this.panelHMI.TabIndex = 33;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Sienna;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.vendido3);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.label35);
            this.panel3.Controls.Add(this.acumulado3);
            this.panel3.Controls.Add(this.label25);
            this.panel3.Controls.Add(this.p31b3);
            this.panel3.Controls.Add(this.p31b2);
            this.panel3.Controls.Add(this.p31b1);
            this.panel3.Controls.Add(this.subpanel3b);
            this.panel3.Controls.Add(this.subpanel3);
            this.panel3.Controls.Add(this.b3_1);
            this.panel3.Controls.Add(this.b3_3);
            this.panel3.Controls.Add(this.b3_2);
            this.panel3.Location = new System.Drawing.Point(839, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(412, 408);
            this.panel3.TabIndex = 9;
            // 
            // vendido3
            // 
            this.vendido3.AutoSize = true;
            this.vendido3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vendido3.Location = new System.Drawing.Point(234, 325);
            this.vendido3.Name = "vendido3";
            this.vendido3.Size = new System.Drawing.Size(18, 20);
            this.vendido3.TabIndex = 51;
            this.vendido3.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Sienna;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(279, 366);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 18);
            this.label24.TabIndex = 45;
            this.label24.Text = "LINEA";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Sienna;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(324, 322);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(69, 73);
            this.label25.TabIndex = 46;
            this.label25.Text = "3";
            // 
            // p31b3
            // 
            this.p31b3.BackColor = System.Drawing.Color.Silver;
            this.p31b3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p31b3.Controls.Add(this.p32b3);
            this.p31b3.Location = new System.Drawing.Point(158, 300);
            this.p31b3.Name = "p31b3";
            this.p31b3.Size = new System.Drawing.Size(70, 100);
            this.p31b3.TabIndex = 44;
            // 
            // p32b3
            // 
            this.p32b3.BackColor = System.Drawing.Color.Gold;
            this.p32b3.Controls.Add(this.label33);
            this.p32b3.Location = new System.Drawing.Point(4, 4);
            this.p32b3.Name = "p32b3";
            this.p32b3.Size = new System.Drawing.Size(60, 90);
            this.p32b3.TabIndex = 34;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(4, 32);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(52, 26);
            this.label33.TabIndex = 39;
            this.label33.Text = "BARRIL\r\n3";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p31b2
            // 
            this.p31b2.BackColor = System.Drawing.Color.Silver;
            this.p31b2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p31b2.Controls.Add(this.p32b2);
            this.p31b2.Location = new System.Drawing.Point(82, 300);
            this.p31b2.Name = "p31b2";
            this.p31b2.Size = new System.Drawing.Size(70, 100);
            this.p31b2.TabIndex = 43;
            // 
            // p32b2
            // 
            this.p32b2.BackColor = System.Drawing.Color.Gold;
            this.p32b2.Controls.Add(this.label34);
            this.p32b2.Location = new System.Drawing.Point(4, 4);
            this.p32b2.Name = "p32b2";
            this.p32b2.Size = new System.Drawing.Size(60, 90);
            this.p32b2.TabIndex = 34;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(4, 32);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(52, 26);
            this.label34.TabIndex = 39;
            this.label34.Text = "BARRIL\r\n2";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p31b1
            // 
            this.p31b1.BackColor = System.Drawing.Color.Silver;
            this.p31b1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p31b1.Controls.Add(this.p32b1);
            this.p31b1.Location = new System.Drawing.Point(6, 300);
            this.p31b1.Name = "p31b1";
            this.p31b1.Size = new System.Drawing.Size(70, 100);
            this.p31b1.TabIndex = 42;
            // 
            // p32b1
            // 
            this.p32b1.BackColor = System.Drawing.Color.Gold;
            this.p32b1.Controls.Add(this.label30);
            this.p32b1.Location = new System.Drawing.Point(4, 4);
            this.p32b1.Name = "p32b1";
            this.p32b1.Size = new System.Drawing.Size(60, 90);
            this.p32b1.TabIndex = 34;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(4, 32);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(52, 26);
            this.label30.TabIndex = 38;
            this.label30.Text = "BARRIL\r\n1";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // subpanel3b
            // 
            this.subpanel3b.BackColor = System.Drawing.SystemColors.Control;
            this.subpanel3b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.subpanel3b.Controls.Add(this.FA_3);
            this.subpanel3b.Controls.Add(this.label23);
            this.subpanel3b.Controls.Add(this.FB_3);
            this.subpanel3b.Controls.Add(this.v3_1);
            this.subpanel3b.Controls.Add(this.v3_2);
            this.subpanel3b.Controls.Add(this.v3_3);
            this.subpanel3b.Controls.Add(this.v3_paso);
            this.subpanel3b.Controls.Add(this.label19);
            this.subpanel3b.Controls.Add(this.label22);
            this.subpanel3b.Controls.Add(this.label20);
            this.subpanel3b.Controls.Add(this.label21);
            this.subpanel3b.Controls.Add(this.df3);
            this.subpanel3b.Location = new System.Drawing.Point(3, 3);
            this.subpanel3b.Name = "subpanel3b";
            this.subpanel3b.Size = new System.Drawing.Size(209, 272);
            this.subpanel3b.TabIndex = 41;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(5, 76);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(65, 18);
            this.label23.TabIndex = 10;
            this.label23.Text = "Detector";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(113, 213);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(34, 36);
            this.label19.TabIndex = 14;
            this.label19.Text = "V23\r\nBa3";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(163, 213);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(43, 36);
            this.label22.TabIndex = 11;
            this.label22.Text = "V25\r\nPaso";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(59, 213);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(34, 36);
            this.label20.TabIndex = 13;
            this.label20.Text = "V22\r\nBa2";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(5, 213);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 36);
            this.label21.TabIndex = 12;
            this.label21.Text = "V21\r\nBa1";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // subpanel3
            // 
            this.subpanel3.BackColor = System.Drawing.Color.CadetBlue;
            this.subpanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.subpanel3.Controls.Add(this.progressBarB3);
            this.subpanel3.Controls.Add(this.run3);
            this.subpanel3.Controls.Add(this.stop3);
            this.subpanel3.Controls.Add(this.labelbombilla3);
            this.subpanel3.Controls.Add(this.picB31);
            this.subpanel3.Controls.Add(this.picB32);
            this.subpanel3.Controls.Add(this.picB33);
            this.subpanel3.Controls.Add(this.jump3);
            this.subpanel3.Controls.Add(this.refresh3);
            this.subpanel3.Controls.Add(this.picparomarcha3);
            this.subpanel3.Controls.Add(this.picbombilla3);
            this.subpanel3.Location = new System.Drawing.Point(215, 3);
            this.subpanel3.Name = "subpanel3";
            this.subpanel3.Size = new System.Drawing.Size(192, 272);
            this.subpanel3.TabIndex = 40;
            // 
            // progressBarB3
            // 
            this.progressBarB3.Location = new System.Drawing.Point(16, 188);
            this.progressBarB3.MarqueeAnimationSpeed = 20;
            this.progressBarB3.Name = "progressBarB3";
            this.progressBarB3.Size = new System.Drawing.Size(162, 11);
            this.progressBarB3.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBarB3.TabIndex = 47;
            this.progressBarB3.Visible = false;
            // 
            // labelbombilla3
            // 
            this.labelbombilla3.AutoSize = true;
            this.labelbombilla3.Location = new System.Drawing.Point(55, 16);
            this.labelbombilla3.Name = "labelbombilla3";
            this.labelbombilla3.Size = new System.Drawing.Size(35, 13);
            this.labelbombilla3.TabIndex = 42;
            this.labelbombilla3.Text = "label1";
            // 
            // b3_1
            // 
            this.b3_1.AutoSize = true;
            this.b3_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b3_1.Location = new System.Drawing.Point(8, 279);
            this.b3_1.Name = "b3_1";
            this.b3_1.Size = new System.Drawing.Size(16, 18);
            this.b3_1.TabIndex = 27;
            this.b3_1.Text = "0";
            this.b3_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // b3_3
            // 
            this.b3_3.AutoSize = true;
            this.b3_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b3_3.Location = new System.Drawing.Point(160, 279);
            this.b3_3.Name = "b3_3";
            this.b3_3.Size = new System.Drawing.Size(16, 18);
            this.b3_3.TabIndex = 29;
            this.b3_3.Text = "0";
            this.b3_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // b3_2
            // 
            this.b3_2.AutoSize = true;
            this.b3_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b3_2.Location = new System.Drawing.Point(84, 279);
            this.b3_2.Name = "b3_2";
            this.b3_2.Size = new System.Drawing.Size(16, 18);
            this.b3_2.TabIndex = 28;
            this.b3_2.Text = "0";
            this.b3_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Sienna;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.vendido1);
            this.panel1.Controls.Add(this.acumulado1);
            this.panel1.Controls.Add(this.label36);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.p11b3);
            this.panel1.Controls.Add(this.p11b2);
            this.panel1.Controls.Add(this.p11b1);
            this.panel1.Controls.Add(this.subpanel1);
            this.panel1.Controls.Add(this.subpanel1b);
            this.panel1.Controls.Add(this.b1_1);
            this.panel1.Controls.Add(this.b1_2);
            this.panel1.Controls.Add(this.b1_3);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(412, 408);
            this.panel1.TabIndex = 7;
            // 
            // vendido1
            // 
            this.vendido1.AutoSize = true;
            this.vendido1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vendido1.Location = new System.Drawing.Point(234, 324);
            this.vendido1.Name = "vendido1";
            this.vendido1.Size = new System.Drawing.Size(18, 20);
            this.vendido1.TabIndex = 50;
            this.vendido1.Text = "0";
            // 
            // acumulado1
            // 
            this.acumulado1.AutoSize = true;
            this.acumulado1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.acumulado1.Location = new System.Drawing.Point(234, 304);
            this.acumulado1.Name = "acumulado1";
            this.acumulado1.Size = new System.Drawing.Size(18, 20);
            this.acumulado1.TabIndex = 49;
            this.acumulado1.Text = "0";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(231, 279);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(137, 20);
            this.label36.TabIndex = 48;
            this.label36.Text = "TOTAL VENDIDO";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Sienna;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(289, 366);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 18);
            this.label11.TabIndex = 19;
            this.label11.Text = "LINEA";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Sienna;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(323, 322);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 73);
            this.label1.TabIndex = 36;
            this.label1.Text = "1";
            // 
            // p11b3
            // 
            this.p11b3.BackColor = System.Drawing.Color.Silver;
            this.p11b3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p11b3.Controls.Add(this.p12b3);
            this.p11b3.Location = new System.Drawing.Point(158, 299);
            this.p11b3.Name = "p11b3";
            this.p11b3.Size = new System.Drawing.Size(70, 100);
            this.p11b3.TabIndex = 35;
            // 
            // p12b3
            // 
            this.p12b3.BackColor = System.Drawing.Color.Gold;
            this.p12b3.Controls.Add(this.label28);
            this.p12b3.Location = new System.Drawing.Point(4, 4);
            this.p12b3.Name = "p12b3";
            this.p12b3.Size = new System.Drawing.Size(60, 90);
            this.p12b3.TabIndex = 34;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(4, 32);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(52, 26);
            this.label28.TabIndex = 38;
            this.label28.Text = "BARRIL\r\n3";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p11b2
            // 
            this.p11b2.BackColor = System.Drawing.Color.Silver;
            this.p11b2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p11b2.Controls.Add(this.p12b2);
            this.p11b2.Location = new System.Drawing.Point(82, 299);
            this.p11b2.Name = "p11b2";
            this.p11b2.Size = new System.Drawing.Size(70, 100);
            this.p11b2.TabIndex = 34;
            // 
            // p12b2
            // 
            this.p12b2.BackColor = System.Drawing.Color.Gold;
            this.p12b2.Controls.Add(this.label27);
            this.p12b2.Location = new System.Drawing.Point(4, 4);
            this.p12b2.Name = "p12b2";
            this.p12b2.Size = new System.Drawing.Size(60, 90);
            this.p12b2.TabIndex = 34;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(4, 32);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(52, 26);
            this.label27.TabIndex = 38;
            this.label27.Text = "BARRIL\r\n2";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p11b1
            // 
            this.p11b1.BackColor = System.Drawing.Color.Silver;
            this.p11b1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p11b1.Controls.Add(this.p12b1);
            this.p11b1.Location = new System.Drawing.Point(6, 299);
            this.p11b1.Name = "p11b1";
            this.p11b1.Size = new System.Drawing.Size(70, 100);
            this.p11b1.TabIndex = 33;
            // 
            // p12b1
            // 
            this.p12b1.BackColor = System.Drawing.Color.Gold;
            this.p12b1.Controls.Add(this.label26);
            this.p12b1.Location = new System.Drawing.Point(4, 4);
            this.p12b1.Name = "p12b1";
            this.p12b1.Size = new System.Drawing.Size(60, 90);
            this.p12b1.TabIndex = 34;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(4, 32);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 26);
            this.label26.TabIndex = 37;
            this.label26.Text = "BARRIL\r\n1";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // subpanel1
            // 
            this.subpanel1.BackColor = System.Drawing.Color.CadetBlue;
            this.subpanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.subpanel1.Controls.Add(this.progressBarB1);
            this.subpanel1.Controls.Add(this.picB13);
            this.subpanel1.Controls.Add(this.picB12);
            this.subpanel1.Controls.Add(this.picB11);
            this.subpanel1.Controls.Add(this.labelbombilla1);
            this.subpanel1.Controls.Add(this.run1);
            this.subpanel1.Controls.Add(this.stop1);
            this.subpanel1.Controls.Add(this.jump1);
            this.subpanel1.Controls.Add(this.refresh1);
            this.subpanel1.Controls.Add(this.picparomarcha1);
            this.subpanel1.Controls.Add(this.picbombilla1);
            this.subpanel1.Location = new System.Drawing.Point(215, 3);
            this.subpanel1.Name = "subpanel1";
            this.subpanel1.Size = new System.Drawing.Size(192, 272);
            this.subpanel1.TabIndex = 32;
            // 
            // progressBarB1
            // 
            this.progressBarB1.Location = new System.Drawing.Point(14, 188);
            this.progressBarB1.MarqueeAnimationSpeed = 20;
            this.progressBarB1.Name = "progressBarB1";
            this.progressBarB1.Size = new System.Drawing.Size(162, 11);
            this.progressBarB1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBarB1.TabIndex = 37;
            this.progressBarB1.Visible = false;
            // 
            // labelbombilla1
            // 
            this.labelbombilla1.AutoSize = true;
            this.labelbombilla1.Location = new System.Drawing.Point(54, 16);
            this.labelbombilla1.Name = "labelbombilla1";
            this.labelbombilla1.Size = new System.Drawing.Size(35, 13);
            this.labelbombilla1.TabIndex = 19;
            this.labelbombilla1.Text = "label1";
            // 
            // subpanel1b
            // 
            this.subpanel1b.BackColor = System.Drawing.SystemColors.Control;
            this.subpanel1b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.subpanel1b.Controls.Add(this.label6);
            this.subpanel1b.Controls.Add(this.FA_1);
            this.subpanel1b.Controls.Add(this.v1_1);
            this.subpanel1b.Controls.Add(this.FB_1);
            this.subpanel1b.Controls.Add(this.v1_2);
            this.subpanel1b.Controls.Add(this.v1_paso);
            this.subpanel1b.Controls.Add(this.v1_3);
            this.subpanel1b.Controls.Add(this.label7);
            this.subpanel1b.Controls.Add(this.label8);
            this.subpanel1b.Controls.Add(this.label9);
            this.subpanel1b.Controls.Add(this.label10);
            this.subpanel1b.Controls.Add(this.df1);
            this.subpanel1b.Location = new System.Drawing.Point(3, 3);
            this.subpanel1b.Name = "subpanel1b";
            this.subpanel1b.Size = new System.Drawing.Size(209, 272);
            this.subpanel1b.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(5, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Detector";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(163, 215);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 36);
            this.label7.TabIndex = 11;
            this.label7.Text = "V5\r\nPaso";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(5, 215);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 36);
            this.label8.TabIndex = 12;
            this.label8.Text = "V1\r\nBa1";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(59, 215);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 36);
            this.label9.TabIndex = 13;
            this.label9.Text = "V2\r\nBa2";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(113, 215);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 36);
            this.label10.TabIndex = 14;
            this.label10.Text = "V3\r\nBa3";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // b1_1
            // 
            this.b1_1.AutoSize = true;
            this.b1_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b1_1.Location = new System.Drawing.Point(8, 278);
            this.b1_1.Name = "b1_1";
            this.b1_1.Size = new System.Drawing.Size(16, 18);
            this.b1_1.TabIndex = 24;
            this.b1_1.Text = "0";
            this.b1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // b1_2
            // 
            this.b1_2.AutoSize = true;
            this.b1_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b1_2.Location = new System.Drawing.Point(84, 278);
            this.b1_2.Name = "b1_2";
            this.b1_2.Size = new System.Drawing.Size(16, 18);
            this.b1_2.TabIndex = 25;
            this.b1_2.Text = "0";
            this.b1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // b1_3
            // 
            this.b1_3.AutoSize = true;
            this.b1_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b1_3.Location = new System.Drawing.Point(160, 278);
            this.b1_3.Name = "b1_3";
            this.b1_3.Size = new System.Drawing.Size(16, 18);
            this.b1_3.TabIndex = 26;
            this.b1_3.Text = "0";
            this.b1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Sienna;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.vendido2);
            this.panel2.Controls.Add(this.acumulado2);
            this.panel2.Controls.Add(this.p21b3);
            this.panel2.Controls.Add(this.p21b2);
            this.panel2.Controls.Add(this.p21b1);
            this.panel2.Controls.Add(this.subpanel2b);
            this.panel2.Controls.Add(this.registro);
            this.panel2.Controls.Add(this.subpanel2);
            this.panel2.Controls.Add(this.b2_3);
            this.panel2.Controls.Add(this.b2_1);
            this.panel2.Controls.Add(this.b2_2);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Location = new System.Drawing.Point(421, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(412, 408);
            this.panel2.TabIndex = 8;
            // 
            // vendido2
            // 
            this.vendido2.AutoSize = true;
            this.vendido2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vendido2.Location = new System.Drawing.Point(234, 325);
            this.vendido2.Name = "vendido2";
            this.vendido2.Size = new System.Drawing.Size(18, 20);
            this.vendido2.TabIndex = 51;
            this.vendido2.Text = "0";
            // 
            // acumulado2
            // 
            this.acumulado2.AutoSize = true;
            this.acumulado2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.acumulado2.Location = new System.Drawing.Point(234, 305);
            this.acumulado2.Name = "acumulado2";
            this.acumulado2.Size = new System.Drawing.Size(18, 20);
            this.acumulado2.TabIndex = 48;
            this.acumulado2.Text = "0";
            // 
            // p21b3
            // 
            this.p21b3.BackColor = System.Drawing.Color.Silver;
            this.p21b3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p21b3.Controls.Add(this.p22b3);
            this.p21b3.Location = new System.Drawing.Point(158, 300);
            this.p21b3.Name = "p21b3";
            this.p21b3.Size = new System.Drawing.Size(70, 100);
            this.p21b3.TabIndex = 41;
            // 
            // p22b3
            // 
            this.p22b3.BackColor = System.Drawing.Color.Gold;
            this.p22b3.Controls.Add(this.label32);
            this.p22b3.Location = new System.Drawing.Point(4, 4);
            this.p22b3.Name = "p22b3";
            this.p22b3.Size = new System.Drawing.Size(60, 90);
            this.p22b3.TabIndex = 34;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(4, 32);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(52, 26);
            this.label32.TabIndex = 38;
            this.label32.Text = "BARRIL\r\n3";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p21b2
            // 
            this.p21b2.BackColor = System.Drawing.Color.Silver;
            this.p21b2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p21b2.Controls.Add(this.p22b2);
            this.p21b2.Location = new System.Drawing.Point(82, 300);
            this.p21b2.Name = "p21b2";
            this.p21b2.Size = new System.Drawing.Size(70, 100);
            this.p21b2.TabIndex = 40;
            // 
            // p22b2
            // 
            this.p22b2.BackColor = System.Drawing.Color.Gold;
            this.p22b2.Controls.Add(this.label31);
            this.p22b2.Location = new System.Drawing.Point(4, 4);
            this.p22b2.Name = "p22b2";
            this.p22b2.Size = new System.Drawing.Size(60, 90);
            this.p22b2.TabIndex = 34;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(4, 32);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(52, 26);
            this.label31.TabIndex = 38;
            this.label31.Text = "BARRIL\r\n2";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p21b1
            // 
            this.p21b1.BackColor = System.Drawing.Color.Silver;
            this.p21b1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p21b1.Controls.Add(this.p22b1);
            this.p21b1.Location = new System.Drawing.Point(6, 300);
            this.p21b1.Name = "p21b1";
            this.p21b1.Size = new System.Drawing.Size(70, 100);
            this.p21b1.TabIndex = 39;
            // 
            // p22b1
            // 
            this.p22b1.BackColor = System.Drawing.Color.Gold;
            this.p22b1.Controls.Add(this.label29);
            this.p22b1.Location = new System.Drawing.Point(4, 4);
            this.p22b1.Name = "p22b1";
            this.p22b1.Size = new System.Drawing.Size(60, 90);
            this.p22b1.TabIndex = 34;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(4, 32);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(52, 26);
            this.label29.TabIndex = 38;
            this.label29.Text = "BARRIL\r\n1";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // subpanel2b
            // 
            this.subpanel2b.BackColor = System.Drawing.SystemColors.Control;
            this.subpanel2b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.subpanel2b.Controls.Add(this.FA_2);
            this.subpanel2b.Controls.Add(this.label17);
            this.subpanel2b.Controls.Add(this.FB_2);
            this.subpanel2b.Controls.Add(this.v2_1);
            this.subpanel2b.Controls.Add(this.v2_2);
            this.subpanel2b.Controls.Add(this.v2_3);
            this.subpanel2b.Controls.Add(this.v2_paso);
            this.subpanel2b.Controls.Add(this.label13);
            this.subpanel2b.Controls.Add(this.label16);
            this.subpanel2b.Controls.Add(this.label14);
            this.subpanel2b.Controls.Add(this.label15);
            this.subpanel2b.Controls.Add(this.df2);
            this.subpanel2b.Location = new System.Drawing.Point(3, 3);
            this.subpanel2b.Name = "subpanel2b";
            this.subpanel2b.Size = new System.Drawing.Size(209, 272);
            this.subpanel2b.TabIndex = 38;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 18);
            this.label17.TabIndex = 10;
            this.label17.Text = "Detector";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(114, 212);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 36);
            this.label13.TabIndex = 14;
            this.label13.Text = "V13\r\nBa3";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(164, 212);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 36);
            this.label16.TabIndex = 11;
            this.label16.Text = "V15\r\nPaso";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(60, 212);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 36);
            this.label14.TabIndex = 13;
            this.label14.Text = "V12\r\nBa2";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(6, 212);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 36);
            this.label15.TabIndex = 12;
            this.label15.Text = "V11\r\nBa1";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // subpanel2
            // 
            this.subpanel2.BackColor = System.Drawing.Color.CadetBlue;
            this.subpanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.subpanel2.Controls.Add(this.progressBarB2);
            this.subpanel2.Controls.Add(this.run2);
            this.subpanel2.Controls.Add(this.stop2);
            this.subpanel2.Controls.Add(this.labelbombilla2);
            this.subpanel2.Controls.Add(this.picB23);
            this.subpanel2.Controls.Add(this.picB22);
            this.subpanel2.Controls.Add(this.picB21);
            this.subpanel2.Controls.Add(this.refresh2);
            this.subpanel2.Controls.Add(this.jump2);
            this.subpanel2.Controls.Add(this.picparomarcha2);
            this.subpanel2.Controls.Add(this.picbombilla2);
            this.subpanel2.Location = new System.Drawing.Point(215, 3);
            this.subpanel2.Name = "subpanel2";
            this.subpanel2.Size = new System.Drawing.Size(192, 272);
            this.subpanel2.TabIndex = 37;
            // 
            // progressBarB2
            // 
            this.progressBarB2.Location = new System.Drawing.Point(12, 188);
            this.progressBarB2.MarqueeAnimationSpeed = 20;
            this.progressBarB2.Name = "progressBarB2";
            this.progressBarB2.Size = new System.Drawing.Size(162, 11);
            this.progressBarB2.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBarB2.TabIndex = 44;
            this.progressBarB2.Visible = false;
            // 
            // labelbombilla2
            // 
            this.labelbombilla2.AutoSize = true;
            this.labelbombilla2.Location = new System.Drawing.Point(54, 16);
            this.labelbombilla2.Name = "labelbombilla2";
            this.labelbombilla2.Size = new System.Drawing.Size(35, 13);
            this.labelbombilla2.TabIndex = 39;
            this.labelbombilla2.Text = "label1";
            // 
            // b2_3
            // 
            this.b2_3.AutoSize = true;
            this.b2_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b2_3.Location = new System.Drawing.Point(160, 279);
            this.b2_3.Name = "b2_3";
            this.b2_3.Size = new System.Drawing.Size(16, 18);
            this.b2_3.TabIndex = 29;
            this.b2_3.Text = "0";
            this.b2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // b2_1
            // 
            this.b2_1.AutoSize = true;
            this.b2_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b2_1.Location = new System.Drawing.Point(8, 279);
            this.b2_1.Name = "b2_1";
            this.b2_1.Size = new System.Drawing.Size(16, 18);
            this.b2_1.TabIndex = 27;
            this.b2_1.Text = "0";
            this.b2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // b2_2
            // 
            this.b2_2.AutoSize = true;
            this.b2_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b2_2.Location = new System.Drawing.Point(84, 279);
            this.b2_2.Name = "b2_2";
            this.b2_2.Size = new System.Drawing.Size(16, 18);
            this.b2_2.TabIndex = 28;
            this.b2_2.Text = "0";
            this.b2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Sienna;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(279, 365);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 18);
            this.label12.TabIndex = 42;
            this.label12.Text = "LINEA";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Sienna;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(324, 321);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(69, 73);
            this.label18.TabIndex = 43;
            this.label18.Text = "2";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // timer1_jump1
            // 
            this.timer1_jump1.Interval = 1000;
            this.timer1_jump1.Tick += new System.EventHandler(this.timer1_jump1_Tick);
            // 
            // timer2_jump2
            // 
            this.timer2_jump2.Interval = 1000;
            this.timer2_jump2.Tick += new System.EventHandler(this.timer2_jump2_Tick);
            // 
            // timer3_jump3
            // 
            this.timer3_jump3.Interval = 1000;
            this.timer3_jump3.Tick += new System.EventHandler(this.timer3_jump3_Tick);
            // 
            // timer1_refresh1
            // 
            this.timer1_refresh1.Interval = 1000;
            this.timer1_refresh1.Tick += new System.EventHandler(this.timer1_refresh1_Tick);
            // 
            // timer2_refresh2
            // 
            this.timer2_refresh2.Interval = 1000;
            this.timer2_refresh2.Tick += new System.EventHandler(this.timer2_refresh2_Tick);
            // 
            // timer3_refresh3
            // 
            this.timer3_refresh3.Interval = 1000;
            this.timer3_refresh3.Tick += new System.EventHandler(this.timer3_refresh3_Tick);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Sienna;
            this.panel4.Controls.Add(this.checkBox1);
            this.panel4.Controls.Add(this.label41);
            this.panel4.Controls.Add(this.label40);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.label37);
            this.panel4.Controls.Add(this.Q5);
            this.panel4.Controls.Add(this.Q4);
            this.panel4.Controls.Add(this.Q3);
            this.panel4.Controls.Add(this.Q2);
            this.panel4.Controls.Add(this.Q1);
            this.panel4.Location = new System.Drawing.Point(12, 172);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(418, 76);
            this.panel4.TabIndex = 6;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(226, 5);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(27, 16);
            this.label41.TabIndex = 30;
            this.label41.Text = "Q5";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(174, 5);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(27, 16);
            this.label40.TabIndex = 29;
            this.label40.Text = "Q4";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(118, 5);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(27, 16);
            this.label39.TabIndex = 28;
            this.label39.Text = "Q3";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(64, 5);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(27, 16);
            this.label38.TabIndex = 27;
            this.label38.Text = "Q2";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(10, 5);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(27, 16);
            this.label37.TabIndex = 26;
            this.label37.Text = "Q1";
            // 
            // panel5
            // 
            this.panel5.AllowDrop = true;
            this.panel5.BackColor = System.Drawing.Color.Sienna;
            this.panel5.Controls.Add(this.checkBox2);
            this.panel5.Controls.Add(this.label42);
            this.panel5.Controls.Add(this.label43);
            this.panel5.Controls.Add(this.label44);
            this.panel5.Controls.Add(this.label45);
            this.panel5.Controls.Add(this.label46);
            this.panel5.Controls.Add(this.Q15);
            this.panel5.Controls.Add(this.Q14);
            this.panel5.Controls.Add(this.Q13);
            this.panel5.Controls.Add(this.Q12);
            this.panel5.Controls.Add(this.Q11);
            this.panel5.Location = new System.Drawing.Point(436, 172);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(412, 76);
            this.panel5.TabIndex = 7;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(226, 5);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(35, 16);
            this.label42.TabIndex = 30;
            this.label42.Text = "Q15";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(174, 5);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(35, 16);
            this.label43.TabIndex = 29;
            this.label43.Text = "Q14";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(118, 5);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(35, 16);
            this.label44.TabIndex = 28;
            this.label44.Text = "Q13";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(64, 5);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(35, 16);
            this.label45.TabIndex = 27;
            this.label45.Text = "Q12";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(10, 5);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(35, 16);
            this.label46.TabIndex = 26;
            this.label46.Text = "Q11";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Sienna;
            this.panel6.Controls.Add(this.checkBox3);
            this.panel6.Controls.Add(this.label47);
            this.panel6.Controls.Add(this.label48);
            this.panel6.Controls.Add(this.label49);
            this.panel6.Controls.Add(this.label50);
            this.panel6.Controls.Add(this.label51);
            this.panel6.Controls.Add(this.Q25);
            this.panel6.Controls.Add(this.Q24);
            this.panel6.Controls.Add(this.Q23);
            this.panel6.Controls.Add(this.Q22);
            this.panel6.Controls.Add(this.Q21);
            this.panel6.Location = new System.Drawing.Point(854, 172);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(412, 76);
            this.panel6.TabIndex = 8;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(226, 5);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(35, 16);
            this.label47.TabIndex = 30;
            this.label47.Text = "Q25";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(174, 5);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(35, 16);
            this.label48.TabIndex = 29;
            this.label48.Text = "Q24";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(118, 5);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(35, 16);
            this.label49.TabIndex = 28;
            this.label49.Text = "Q23";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(64, 5);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(35, 16);
            this.label50.TabIndex = 27;
            this.label50.Text = "Q22";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(10, 5);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(35, 16);
            this.label51.TabIndex = 26;
            this.label51.Text = "Q21";
            // 
            // timered
            // 
            this.timered.Interval = 1000;
            this.timered.Tick += new System.EventHandler(this.timered_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.White;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inicioToolStripMenuItem,
            this.administradorToolStripMenuItem,
            this.configuraciónToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1284, 29);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // checkBox1
            // 
            this.checkBox1.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox1.AutoSize = true;
            this.checkBox1.FlatAppearance.BorderSize = 0;
            this.checkBox1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.checkBox1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.checkBox1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.checkBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox1.Image = global::logo2020.PRESENTATION.Properties.Resources.int_offoff;
            this.checkBox1.Location = new System.Drawing.Point(377, 5);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(38, 66);
            this.checkBox1.TabIndex = 31;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // inicioToolStripMenuItem
            // 
            this.inicioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cerrarSesiónToolStripMenuItem});
            this.inicioToolStripMenuItem.Image = global::logo2020.PRESENTATION.Properties.Resources.home_icon;
            this.inicioToolStripMenuItem.Name = "inicioToolStripMenuItem";
            this.inicioToolStripMenuItem.Size = new System.Drawing.Size(81, 25);
            this.inicioToolStripMenuItem.Text = "Inicio";
            // 
            // cerrarSesiónToolStripMenuItem
            // 
            this.cerrarSesiónToolStripMenuItem.Image = global::logo2020.PRESENTATION.Properties.Resources.Users_Exit_icon;
            this.cerrarSesiónToolStripMenuItem.Name = "cerrarSesiónToolStripMenuItem";
            this.cerrarSesiónToolStripMenuItem.Size = new System.Drawing.Size(209, 26);
            this.cerrarSesiónToolStripMenuItem.Text = "Cerrar aplicación";
            this.cerrarSesiónToolStripMenuItem.Click += new System.EventHandler(this.cerrarSesiónToolStripMenuItem_Click);
            // 
            // administradorToolStripMenuItem
            // 
            this.administradorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.escribirEnLogoToolStripMenuItem});
            this.administradorToolStripMenuItem.Image = global::logo2020.PRESENTATION.Properties.Resources.Folder_Mac_icon;
            this.administradorToolStripMenuItem.Name = "administradorToolStripMenuItem";
            this.administradorToolStripMenuItem.Size = new System.Drawing.Size(148, 25);
            this.administradorToolStripMenuItem.Text = "Administrador";
            // 
            // escribirEnLogoToolStripMenuItem
            // 
            this.escribirEnLogoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enviarDatosToolStripMenuItem});
            this.escribirEnLogoToolStripMenuItem.Image = global::logo2020.PRESENTATION.Properties.Resources.Home_Server_icon;
            this.escribirEnLogoToolStripMenuItem.Name = "escribirEnLogoToolStripMenuItem";
            this.escribirEnLogoToolStripMenuItem.Size = new System.Drawing.Size(118, 26);
            this.escribirEnLogoToolStripMenuItem.Text = "Logo";
            // 
            // enviarDatosToolStripMenuItem
            // 
            this.enviarDatosToolStripMenuItem.Image = global::logo2020.PRESENTATION.Properties.Resources.Files_Edit_file_icon__1_;
            this.enviarDatosToolStripMenuItem.Name = "enviarDatosToolStripMenuItem";
            this.enviarDatosToolStripMenuItem.Size = new System.Drawing.Size(174, 26);
            this.enviarDatosToolStripMenuItem.Text = "Enviar datos";
            this.enviarDatosToolStripMenuItem.Click += new System.EventHandler(this.enviarDatosToolStripMenuItem_Click);
            // 
            // configuraciónToolStripMenuItem
            // 
            this.configuraciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.baseDeDatosToolStripMenuItem,
            this.alarmasToolStripMenuItem});
            this.configuraciónToolStripMenuItem.Image = global::logo2020.PRESENTATION.Properties.Resources.Very_Basic_Settings_Wrench_Filled_icon;
            this.configuraciónToolStripMenuItem.Name = "configuraciónToolStripMenuItem";
            this.configuraciónToolStripMenuItem.Size = new System.Drawing.Size(147, 25);
            this.configuraciónToolStripMenuItem.Text = "Configuración";
            // 
            // baseDeDatosToolStripMenuItem
            // 
            this.baseDeDatosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verificarConexiónBDToolStripMenuItem});
            this.baseDeDatosToolStripMenuItem.Image = global::logo2020.PRESENTATION.Properties.Resources.Database_icon;
            this.baseDeDatosToolStripMenuItem.Name = "baseDeDatosToolStripMenuItem";
            this.baseDeDatosToolStripMenuItem.Size = new System.Drawing.Size(184, 26);
            this.baseDeDatosToolStripMenuItem.Text = "Base de datos";
            // 
            // verificarConexiónBDToolStripMenuItem
            // 
            this.verificarConexiónBDToolStripMenuItem.Image = global::logo2020.PRESENTATION.Properties.Resources.Ethernet_Cable_icon;
            this.verificarConexiónBDToolStripMenuItem.Name = "verificarConexiónBDToolStripMenuItem";
            this.verificarConexiónBDToolStripMenuItem.Size = new System.Drawing.Size(247, 26);
            this.verificarConexiónBDToolStripMenuItem.Text = "Verificar Conexión BD";
            this.verificarConexiónBDToolStripMenuItem.Click += new System.EventHandler(this.verificarConexiónBDToolStripMenuItem_Click);
            // 
            // alarmasToolStripMenuItem
            // 
            this.alarmasToolStripMenuItem.Name = "alarmasToolStripMenuItem";
            this.alarmasToolStripMenuItem.Size = new System.Drawing.Size(184, 26);
            this.alarmasToolStripMenuItem.Text = "Alarmas";
            // 
            // Q25
            // 
            this.Q25.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q25.Location = new System.Drawing.Point(219, 24);
            this.Q25.Name = "Q25";
            this.Q25.Size = new System.Drawing.Size(48, 48);
            this.Q25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q25.TabIndex = 25;
            this.Q25.TabStop = false;
            // 
            // Q24
            // 
            this.Q24.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q24.Location = new System.Drawing.Point(165, 24);
            this.Q24.Name = "Q24";
            this.Q24.Size = new System.Drawing.Size(48, 48);
            this.Q24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q24.TabIndex = 24;
            this.Q24.TabStop = false;
            // 
            // Q23
            // 
            this.Q23.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q23.Location = new System.Drawing.Point(111, 24);
            this.Q23.Name = "Q23";
            this.Q23.Size = new System.Drawing.Size(48, 48);
            this.Q23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q23.TabIndex = 23;
            this.Q23.TabStop = false;
            // 
            // Q22
            // 
            this.Q22.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q22.Location = new System.Drawing.Point(57, 24);
            this.Q22.Name = "Q22";
            this.Q22.Size = new System.Drawing.Size(48, 48);
            this.Q22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q22.TabIndex = 22;
            this.Q22.TabStop = false;
            // 
            // Q21
            // 
            this.Q21.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q21.Location = new System.Drawing.Point(3, 24);
            this.Q21.Name = "Q21";
            this.Q21.Size = new System.Drawing.Size(48, 48);
            this.Q21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q21.TabIndex = 21;
            this.Q21.TabStop = false;
            // 
            // Q15
            // 
            this.Q15.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q15.Location = new System.Drawing.Point(219, 24);
            this.Q15.Name = "Q15";
            this.Q15.Size = new System.Drawing.Size(48, 48);
            this.Q15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q15.TabIndex = 25;
            this.Q15.TabStop = false;
            // 
            // Q14
            // 
            this.Q14.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q14.Location = new System.Drawing.Point(165, 24);
            this.Q14.Name = "Q14";
            this.Q14.Size = new System.Drawing.Size(48, 48);
            this.Q14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q14.TabIndex = 24;
            this.Q14.TabStop = false;
            // 
            // Q13
            // 
            this.Q13.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q13.Location = new System.Drawing.Point(111, 24);
            this.Q13.Name = "Q13";
            this.Q13.Size = new System.Drawing.Size(48, 48);
            this.Q13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q13.TabIndex = 23;
            this.Q13.TabStop = false;
            // 
            // Q12
            // 
            this.Q12.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q12.Location = new System.Drawing.Point(57, 24);
            this.Q12.Name = "Q12";
            this.Q12.Size = new System.Drawing.Size(48, 48);
            this.Q12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q12.TabIndex = 22;
            this.Q12.TabStop = false;
            // 
            // Q11
            // 
            this.Q11.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q11.Location = new System.Drawing.Point(3, 24);
            this.Q11.Name = "Q11";
            this.Q11.Size = new System.Drawing.Size(48, 48);
            this.Q11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q11.TabIndex = 21;
            this.Q11.TabStop = false;
            // 
            // Q5
            // 
            this.Q5.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q5.Location = new System.Drawing.Point(219, 24);
            this.Q5.Name = "Q5";
            this.Q5.Size = new System.Drawing.Size(48, 48);
            this.Q5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q5.TabIndex = 25;
            this.Q5.TabStop = false;
            // 
            // Q4
            // 
            this.Q4.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q4.Location = new System.Drawing.Point(165, 24);
            this.Q4.Name = "Q4";
            this.Q4.Size = new System.Drawing.Size(48, 48);
            this.Q4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q4.TabIndex = 24;
            this.Q4.TabStop = false;
            // 
            // Q3
            // 
            this.Q3.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q3.Location = new System.Drawing.Point(111, 24);
            this.Q3.Name = "Q3";
            this.Q3.Size = new System.Drawing.Size(48, 48);
            this.Q3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q3.TabIndex = 23;
            this.Q3.TabStop = false;
            // 
            // Q2
            // 
            this.Q2.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q2.Location = new System.Drawing.Point(57, 24);
            this.Q2.Name = "Q2";
            this.Q2.Size = new System.Drawing.Size(48, 48);
            this.Q2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q2.TabIndex = 22;
            this.Q2.TabStop = false;
            // 
            // Q1
            // 
            this.Q1.Image = global::logo2020.PRESENTATION.Properties.Resources.LUZ_ROJA_OFF;
            this.Q1.Location = new System.Drawing.Point(3, 24);
            this.Q1.Name = "Q1";
            this.Q1.Size = new System.Drawing.Size(48, 48);
            this.Q1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Q1.TabIndex = 21;
            this.Q1.TabStop = false;
            // 
            // FA_3
            // 
            this.FA_3.Image = ((System.Drawing.Image)(resources.GetObject("FA_3.Image")));
            this.FA_3.Location = new System.Drawing.Point(2, 100);
            this.FA_3.Name = "FA_3";
            this.FA_3.Size = new System.Drawing.Size(153, 63);
            this.FA_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FA_3.TabIndex = 32;
            this.FA_3.TabStop = false;
            // 
            // FB_3
            // 
            this.FB_3.Location = new System.Drawing.Point(110, 63);
            this.FB_3.Name = "FB_3";
            this.FB_3.Size = new System.Drawing.Size(96, 100);
            this.FB_3.TabIndex = 34;
            this.FB_3.TabStop = false;
            // 
            // v3_1
            // 
            this.v3_1.Image = ((System.Drawing.Image)(resources.GetObject("v3_1.Image")));
            this.v3_1.Location = new System.Drawing.Point(2, 164);
            this.v3_1.Name = "v3_1";
            this.v3_1.Size = new System.Drawing.Size(45, 46);
            this.v3_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v3_1.TabIndex = 1;
            this.v3_1.TabStop = false;
            // 
            // v3_2
            // 
            this.v3_2.Image = ((System.Drawing.Image)(resources.GetObject("v3_2.Image")));
            this.v3_2.Location = new System.Drawing.Point(56, 164);
            this.v3_2.Name = "v3_2";
            this.v3_2.Size = new System.Drawing.Size(45, 46);
            this.v3_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v3_2.TabIndex = 2;
            this.v3_2.TabStop = false;
            // 
            // v3_3
            // 
            this.v3_3.Image = ((System.Drawing.Image)(resources.GetObject("v3_3.Image")));
            this.v3_3.Location = new System.Drawing.Point(110, 164);
            this.v3_3.Name = "v3_3";
            this.v3_3.Size = new System.Drawing.Size(45, 46);
            this.v3_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v3_3.TabIndex = 3;
            this.v3_3.TabStop = false;
            // 
            // v3_paso
            // 
            this.v3_paso.Image = ((System.Drawing.Image)(resources.GetObject("v3_paso.Image")));
            this.v3_paso.Location = new System.Drawing.Point(161, 164);
            this.v3_paso.Name = "v3_paso";
            this.v3_paso.Size = new System.Drawing.Size(45, 46);
            this.v3_paso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v3_paso.TabIndex = 4;
            this.v3_paso.TabStop = false;
            // 
            // df3
            // 
            this.df3.Image = ((System.Drawing.Image)(resources.GetObject("df3.Image")));
            this.df3.Location = new System.Drawing.Point(37, 5);
            this.df3.Name = "df3";
            this.df3.Size = new System.Drawing.Size(96, 96);
            this.df3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.df3.TabIndex = 5;
            this.df3.TabStop = false;
            // 
            // run3
            // 
            this.run3.FlatAppearance.BorderSize = 0;
            this.run3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.run3.Image = ((System.Drawing.Image)(resources.GetObject("run3.Image")));
            this.run3.Location = new System.Drawing.Point(107, 78);
            this.run3.Name = "run3";
            this.run3.Size = new System.Drawing.Size(80, 32);
            this.run3.TabIndex = 16;
            this.run3.UseVisualStyleBackColor = true;
            this.run3.Click += new System.EventHandler(this.run3_Click_1);
            // 
            // stop3
            // 
            this.stop3.FlatAppearance.BorderSize = 0;
            this.stop3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stop3.Image = ((System.Drawing.Image)(resources.GetObject("stop3.Image")));
            this.stop3.Location = new System.Drawing.Point(107, 41);
            this.stop3.Name = "stop3";
            this.stop3.Size = new System.Drawing.Size(80, 32);
            this.stop3.TabIndex = 15;
            this.stop3.UseVisualStyleBackColor = true;
            this.stop3.Click += new System.EventHandler(this.stop3_Click_1);
            // 
            // picB31
            // 
            this.picB31.Location = new System.Drawing.Point(12, 122);
            this.picB31.Name = "picB31";
            this.picB31.Size = new System.Drawing.Size(48, 48);
            this.picB31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB31.TabIndex = 37;
            this.picB31.TabStop = false;
            // 
            // picB32
            // 
            this.picB32.Location = new System.Drawing.Point(66, 122);
            this.picB32.Name = "picB32";
            this.picB32.Size = new System.Drawing.Size(48, 48);
            this.picB32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB32.TabIndex = 38;
            this.picB32.TabStop = false;
            // 
            // picB33
            // 
            this.picB33.Location = new System.Drawing.Point(120, 122);
            this.picB33.Name = "picB33";
            this.picB33.Size = new System.Drawing.Size(48, 48);
            this.picB33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB33.TabIndex = 39;
            this.picB33.TabStop = false;
            // 
            // jump3
            // 
            this.jump3.FlatAppearance.BorderSize = 0;
            this.jump3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jump3.Image = ((System.Drawing.Image)(resources.GetObject("jump3.Image")));
            this.jump3.Location = new System.Drawing.Point(98, 176);
            this.jump3.Name = "jump3";
            this.jump3.Size = new System.Drawing.Size(80, 32);
            this.jump3.TabIndex = 17;
            this.jump3.UseVisualStyleBackColor = true;
            this.jump3.Click += new System.EventHandler(this.jump3_Click_1);
            // 
            // refresh3
            // 
            this.refresh3.FlatAppearance.BorderSize = 0;
            this.refresh3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refresh3.Image = ((System.Drawing.Image)(resources.GetObject("refresh3.Image")));
            this.refresh3.Location = new System.Drawing.Point(12, 176);
            this.refresh3.Name = "refresh3";
            this.refresh3.Size = new System.Drawing.Size(80, 32);
            this.refresh3.TabIndex = 18;
            this.refresh3.UseVisualStyleBackColor = true;
            this.refresh3.Click += new System.EventHandler(this.refresh3_Click_1);
            // 
            // picparomarcha3
            // 
            this.picparomarcha3.Location = new System.Drawing.Point(50, 32);
            this.picparomarcha3.Name = "picparomarcha3";
            this.picparomarcha3.Size = new System.Drawing.Size(67, 84);
            this.picparomarcha3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picparomarcha3.TabIndex = 40;
            this.picparomarcha3.TabStop = false;
            // 
            // picbombilla3
            // 
            this.picbombilla3.Location = new System.Drawing.Point(3, 48);
            this.picbombilla3.Name = "picbombilla3";
            this.picbombilla3.Size = new System.Drawing.Size(48, 48);
            this.picbombilla3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbombilla3.TabIndex = 41;
            this.picbombilla3.TabStop = false;
            // 
            // picB13
            // 
            this.picB13.Location = new System.Drawing.Point(124, 122);
            this.picB13.Name = "picB13";
            this.picB13.Size = new System.Drawing.Size(48, 48);
            this.picB13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB13.TabIndex = 22;
            this.picB13.TabStop = false;
            // 
            // picB12
            // 
            this.picB12.Location = new System.Drawing.Point(70, 122);
            this.picB12.Name = "picB12";
            this.picB12.Size = new System.Drawing.Size(48, 48);
            this.picB12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB12.TabIndex = 21;
            this.picB12.TabStop = false;
            // 
            // picB11
            // 
            this.picB11.Location = new System.Drawing.Point(16, 122);
            this.picB11.Name = "picB11";
            this.picB11.Size = new System.Drawing.Size(48, 48);
            this.picB11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB11.TabIndex = 20;
            this.picB11.TabStop = false;
            // 
            // run1
            // 
            this.run1.FlatAppearance.BorderSize = 0;
            this.run1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.run1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.run1.Image = ((System.Drawing.Image)(resources.GetObject("run1.Image")));
            this.run1.Location = new System.Drawing.Point(107, 78);
            this.run1.Name = "run1";
            this.run1.Size = new System.Drawing.Size(80, 32);
            this.run1.TabIndex = 16;
            this.run1.UseVisualStyleBackColor = true;
            this.run1.Click += new System.EventHandler(this.run1_Click);
            // 
            // stop1
            // 
            this.stop1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.stop1.FlatAppearance.BorderSize = 0;
            this.stop1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.stop1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.stop1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stop1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stop1.Image = ((System.Drawing.Image)(resources.GetObject("stop1.Image")));
            this.stop1.Location = new System.Drawing.Point(107, 41);
            this.stop1.Name = "stop1";
            this.stop1.Size = new System.Drawing.Size(80, 32);
            this.stop1.TabIndex = 15;
            this.stop1.UseVisualStyleBackColor = true;
            this.stop1.Click += new System.EventHandler(this.stop1_Click_1);
            // 
            // jump1
            // 
            this.jump1.FlatAppearance.BorderSize = 0;
            this.jump1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.jump1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jump1.Image = ((System.Drawing.Image)(resources.GetObject("jump1.Image")));
            this.jump1.Location = new System.Drawing.Point(97, 176);
            this.jump1.Name = "jump1";
            this.jump1.Size = new System.Drawing.Size(80, 32);
            this.jump1.TabIndex = 17;
            this.jump1.UseVisualStyleBackColor = true;
            this.jump1.Click += new System.EventHandler(this.jump1_Click_1);
            // 
            // refresh1
            // 
            this.refresh1.FlatAppearance.BorderSize = 0;
            this.refresh1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.refresh1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refresh1.Image = ((System.Drawing.Image)(resources.GetObject("refresh1.Image")));
            this.refresh1.Location = new System.Drawing.Point(14, 176);
            this.refresh1.Name = "refresh1";
            this.refresh1.Size = new System.Drawing.Size(80, 32);
            this.refresh1.TabIndex = 18;
            this.refresh1.UseVisualStyleBackColor = true;
            this.refresh1.Click += new System.EventHandler(this.refresh1_Click_1);
            // 
            // picparomarcha1
            // 
            this.picparomarcha1.Location = new System.Drawing.Point(50, 32);
            this.picparomarcha1.Name = "picparomarcha1";
            this.picparomarcha1.Size = new System.Drawing.Size(67, 84);
            this.picparomarcha1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picparomarcha1.TabIndex = 0;
            this.picparomarcha1.TabStop = false;
            // 
            // picbombilla1
            // 
            this.picbombilla1.Location = new System.Drawing.Point(3, 48);
            this.picbombilla1.Name = "picbombilla1";
            this.picbombilla1.Size = new System.Drawing.Size(48, 48);
            this.picbombilla1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbombilla1.TabIndex = 1;
            this.picbombilla1.TabStop = false;
            // 
            // FA_1
            // 
            this.FA_1.Image = ((System.Drawing.Image)(resources.GetObject("FA_1.Image")));
            this.FA_1.Location = new System.Drawing.Point(2, 100);
            this.FA_1.Name = "FA_1";
            this.FA_1.Size = new System.Drawing.Size(153, 63);
            this.FA_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FA_1.TabIndex = 30;
            this.FA_1.TabStop = false;
            // 
            // v1_1
            // 
            this.v1_1.Image = ((System.Drawing.Image)(resources.GetObject("v1_1.Image")));
            this.v1_1.Location = new System.Drawing.Point(2, 166);
            this.v1_1.Name = "v1_1";
            this.v1_1.Size = new System.Drawing.Size(45, 46);
            this.v1_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v1_1.TabIndex = 1;
            this.v1_1.TabStop = false;
            // 
            // FB_1
            // 
            this.FB_1.Location = new System.Drawing.Point(110, 63);
            this.FB_1.Name = "FB_1";
            this.FB_1.Size = new System.Drawing.Size(96, 100);
            this.FB_1.TabIndex = 29;
            this.FB_1.TabStop = false;
            // 
            // v1_2
            // 
            this.v1_2.Image = ((System.Drawing.Image)(resources.GetObject("v1_2.Image")));
            this.v1_2.Location = new System.Drawing.Point(56, 166);
            this.v1_2.Name = "v1_2";
            this.v1_2.Size = new System.Drawing.Size(45, 46);
            this.v1_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v1_2.TabIndex = 2;
            this.v1_2.TabStop = false;
            // 
            // v1_paso
            // 
            this.v1_paso.Image = ((System.Drawing.Image)(resources.GetObject("v1_paso.Image")));
            this.v1_paso.Location = new System.Drawing.Point(161, 166);
            this.v1_paso.Name = "v1_paso";
            this.v1_paso.Size = new System.Drawing.Size(45, 46);
            this.v1_paso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v1_paso.TabIndex = 4;
            this.v1_paso.TabStop = false;
            // 
            // v1_3
            // 
            this.v1_3.Image = ((System.Drawing.Image)(resources.GetObject("v1_3.Image")));
            this.v1_3.Location = new System.Drawing.Point(110, 166);
            this.v1_3.Name = "v1_3";
            this.v1_3.Size = new System.Drawing.Size(45, 46);
            this.v1_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v1_3.TabIndex = 3;
            this.v1_3.TabStop = false;
            // 
            // df1
            // 
            this.df1.Image = ((System.Drawing.Image)(resources.GetObject("df1.Image")));
            this.df1.Location = new System.Drawing.Point(37, 5);
            this.df1.Name = "df1";
            this.df1.Size = new System.Drawing.Size(96, 96);
            this.df1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.df1.TabIndex = 5;
            this.df1.TabStop = false;
            // 
            // FA_2
            // 
            this.FA_2.Image = ((System.Drawing.Image)(resources.GetObject("FA_2.Image")));
            this.FA_2.Location = new System.Drawing.Point(3, 99);
            this.FA_2.Name = "FA_2";
            this.FA_2.Size = new System.Drawing.Size(153, 63);
            this.FA_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FA_2.TabIndex = 31;
            this.FA_2.TabStop = false;
            // 
            // FB_2
            // 
            this.FB_2.Location = new System.Drawing.Point(111, 62);
            this.FB_2.Name = "FB_2";
            this.FB_2.Size = new System.Drawing.Size(96, 100);
            this.FB_2.TabIndex = 33;
            this.FB_2.TabStop = false;
            // 
            // v2_1
            // 
            this.v2_1.Image = ((System.Drawing.Image)(resources.GetObject("v2_1.Image")));
            this.v2_1.Location = new System.Drawing.Point(3, 163);
            this.v2_1.Name = "v2_1";
            this.v2_1.Size = new System.Drawing.Size(45, 46);
            this.v2_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v2_1.TabIndex = 1;
            this.v2_1.TabStop = false;
            // 
            // v2_2
            // 
            this.v2_2.Image = ((System.Drawing.Image)(resources.GetObject("v2_2.Image")));
            this.v2_2.Location = new System.Drawing.Point(57, 163);
            this.v2_2.Name = "v2_2";
            this.v2_2.Size = new System.Drawing.Size(45, 46);
            this.v2_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v2_2.TabIndex = 2;
            this.v2_2.TabStop = false;
            // 
            // v2_3
            // 
            this.v2_3.Image = ((System.Drawing.Image)(resources.GetObject("v2_3.Image")));
            this.v2_3.Location = new System.Drawing.Point(111, 163);
            this.v2_3.Name = "v2_3";
            this.v2_3.Size = new System.Drawing.Size(45, 46);
            this.v2_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v2_3.TabIndex = 3;
            this.v2_3.TabStop = false;
            // 
            // v2_paso
            // 
            this.v2_paso.Image = ((System.Drawing.Image)(resources.GetObject("v2_paso.Image")));
            this.v2_paso.Location = new System.Drawing.Point(162, 163);
            this.v2_paso.Name = "v2_paso";
            this.v2_paso.Size = new System.Drawing.Size(45, 46);
            this.v2_paso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.v2_paso.TabIndex = 4;
            this.v2_paso.TabStop = false;
            // 
            // df2
            // 
            this.df2.Image = ((System.Drawing.Image)(resources.GetObject("df2.Image")));
            this.df2.Location = new System.Drawing.Point(38, 4);
            this.df2.Name = "df2";
            this.df2.Size = new System.Drawing.Size(96, 96);
            this.df2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.df2.TabIndex = 5;
            this.df2.TabStop = false;
            // 
            // run2
            // 
            this.run2.FlatAppearance.BorderSize = 0;
            this.run2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.run2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.run2.Image = ((System.Drawing.Image)(resources.GetObject("run2.Image")));
            this.run2.Location = new System.Drawing.Point(107, 78);
            this.run2.Name = "run2";
            this.run2.Size = new System.Drawing.Size(80, 32);
            this.run2.TabIndex = 16;
            this.run2.UseVisualStyleBackColor = true;
            this.run2.Click += new System.EventHandler(this.run2_Click_1);
            // 
            // stop2
            // 
            this.stop2.FlatAppearance.BorderSize = 0;
            this.stop2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stop2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stop2.Image = ((System.Drawing.Image)(resources.GetObject("stop2.Image")));
            this.stop2.Location = new System.Drawing.Point(107, 41);
            this.stop2.Name = "stop2";
            this.stop2.Size = new System.Drawing.Size(80, 32);
            this.stop2.TabIndex = 15;
            this.stop2.UseVisualStyleBackColor = true;
            this.stop2.Click += new System.EventHandler(this.stop2_Click);
            // 
            // picB23
            // 
            this.picB23.Location = new System.Drawing.Point(120, 122);
            this.picB23.Name = "picB23";
            this.picB23.Size = new System.Drawing.Size(48, 48);
            this.picB23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB23.TabIndex = 36;
            this.picB23.TabStop = false;
            // 
            // picB22
            // 
            this.picB22.Location = new System.Drawing.Point(66, 122);
            this.picB22.Name = "picB22";
            this.picB22.Size = new System.Drawing.Size(48, 48);
            this.picB22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB22.TabIndex = 35;
            this.picB22.TabStop = false;
            // 
            // picB21
            // 
            this.picB21.Location = new System.Drawing.Point(12, 122);
            this.picB21.Name = "picB21";
            this.picB21.Size = new System.Drawing.Size(48, 48);
            this.picB21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picB21.TabIndex = 34;
            this.picB21.TabStop = false;
            // 
            // refresh2
            // 
            this.refresh2.FlatAppearance.BorderSize = 0;
            this.refresh2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.refresh2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refresh2.Image = ((System.Drawing.Image)(resources.GetObject("refresh2.Image")));
            this.refresh2.Location = new System.Drawing.Point(12, 176);
            this.refresh2.Name = "refresh2";
            this.refresh2.Size = new System.Drawing.Size(80, 32);
            this.refresh2.TabIndex = 18;
            this.refresh2.UseVisualStyleBackColor = true;
            this.refresh2.Click += new System.EventHandler(this.refresh2_Click_1);
            // 
            // jump2
            // 
            this.jump2.FlatAppearance.BorderSize = 0;
            this.jump2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.jump2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jump2.Image = ((System.Drawing.Image)(resources.GetObject("jump2.Image")));
            this.jump2.Location = new System.Drawing.Point(98, 176);
            this.jump2.Name = "jump2";
            this.jump2.Size = new System.Drawing.Size(80, 32);
            this.jump2.TabIndex = 17;
            this.jump2.UseVisualStyleBackColor = true;
            this.jump2.Click += new System.EventHandler(this.jump2_Click_1);
            // 
            // picparomarcha2
            // 
            this.picparomarcha2.Location = new System.Drawing.Point(50, 32);
            this.picparomarcha2.Name = "picparomarcha2";
            this.picparomarcha2.Size = new System.Drawing.Size(67, 84);
            this.picparomarcha2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picparomarcha2.TabIndex = 37;
            this.picparomarcha2.TabStop = false;
            // 
            // picbombilla2
            // 
            this.picbombilla2.Location = new System.Drawing.Point(3, 48);
            this.picbombilla2.Name = "picbombilla2";
            this.picbombilla2.Size = new System.Drawing.Size(48, 48);
            this.picbombilla2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbombilla2.TabIndex = 38;
            this.picbombilla2.TabStop = false;
            // 
            // picCO2
            // 
            this.picCO2.Location = new System.Drawing.Point(195, 8);
            this.picCO2.Name = "picCO2";
            this.picCO2.Size = new System.Drawing.Size(48, 48);
            this.picCO2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCO2.TabIndex = 33;
            this.picCO2.TabStop = false;
            // 
            // picFU
            // 
            this.picFU.Location = new System.Drawing.Point(75, 71);
            this.picFU.Name = "picFU";
            this.picFU.Size = new System.Drawing.Size(48, 48);
            this.picFU.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFU.TabIndex = 42;
            this.picFU.TabStop = false;
            this.picFU.Visible = false;
            // 
            // picCA
            // 
            this.picCA.Location = new System.Drawing.Point(75, 9);
            this.picCA.Name = "picCA";
            this.picCA.Size = new System.Drawing.Size(48, 48);
            this.picCA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCA.TabIndex = 36;
            this.picCA.TabStop = false;
            // 
            // picPU
            // 
            this.picPU.Location = new System.Drawing.Point(195, 70);
            this.picPU.Name = "picPU";
            this.picPU.Size = new System.Drawing.Size(48, 48);
            this.picPU.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPU.TabIndex = 39;
            this.picPU.TabStop = false;
            // 
            // pictureBoxBoton
            // 
            this.pictureBoxBoton.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxBoton.Image = global::logo2020.PRESENTATION.Properties.Resources.SIGNAL2;
            this.pictureBoxBoton.Location = new System.Drawing.Point(483, 3);
            this.pictureBoxBoton.Name = "pictureBoxBoton";
            this.pictureBoxBoton.Size = new System.Drawing.Size(59, 59);
            this.pictureBoxBoton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxBoton.TabIndex = 1;
            this.pictureBoxBoton.TabStop = false;
            // 
            // off
            // 
            this.off.Enabled = false;
            this.off.FlatAppearance.BorderSize = 0;
            this.off.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.off.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.off.ForeColor = System.Drawing.Color.Black;
            this.off.Image = global::logo2020.PRESENTATION.Properties.Resources.NEW_OFF;
            this.off.Location = new System.Drawing.Point(407, 66);
            this.off.Name = "off";
            this.off.Size = new System.Drawing.Size(135, 45);
            this.off.TabIndex = 16;
            this.off.UseVisualStyleBackColor = true;
            this.off.Click += new System.EventHandler(this.off_Click);
            // 
            // on
            // 
            this.on.FlatAppearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.on.FlatAppearance.BorderSize = 0;
            this.on.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Highlight;
            this.on.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Highlight;
            this.on.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.on.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.on.ForeColor = System.Drawing.Color.Black;
            this.on.Image = global::logo2020.PRESENTATION.Properties.Resources.NEW_ON;
            this.on.Location = new System.Drawing.Point(262, 66);
            this.on.Name = "on";
            this.on.Size = new System.Drawing.Size(135, 45);
            this.on.TabIndex = 10;
            this.on.UseVisualStyleBackColor = true;
            this.on.Click += new System.EventHandler(this.on_Click);
            // 
            // pictureon
            // 
            this.pictureon.Image = global::logo2020.PRESENTATION.Properties.Resources.plcoff;
            this.pictureon.Location = new System.Drawing.Point(3, 3);
            this.pictureon.Name = "pictureon";
            this.pictureon.Size = new System.Drawing.Size(144, 133);
            this.pictureon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureon.TabIndex = 13;
            this.pictureon.TabStop = false;
            // 
            // picturepc
            // 
            this.picturepc.BackColor = System.Drawing.Color.Black;
            this.picturepc.Image = global::logo2020.PRESENTATION.Properties.Resources.Laptop_OFF;
            this.picturepc.Location = new System.Drawing.Point(10, 6);
            this.picturepc.Name = "picturepc";
            this.picturepc.Size = new System.Drawing.Size(128, 127);
            this.picturepc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picturepc.TabIndex = 14;
            this.picturepc.TabStop = false;
            this.picturepc.Visible = false;
            // 
            // checkBox2
            // 
            this.checkBox2.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox2.AutoSize = true;
            this.checkBox2.FlatAppearance.BorderSize = 0;
            this.checkBox2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.checkBox2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.checkBox2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.checkBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox2.Image = global::logo2020.PRESENTATION.Properties.Resources.int_offoff;
            this.checkBox2.Location = new System.Drawing.Point(370, 5);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(38, 66);
            this.checkBox2.TabIndex = 32;
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox3.AutoSize = true;
            this.checkBox3.FlatAppearance.BorderSize = 0;
            this.checkBox3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.checkBox3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.checkBox3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.checkBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox3.Image = global::logo2020.PRESENTATION.Properties.Resources.int_offoff;
            this.checkBox3.Location = new System.Drawing.Point(370, 5);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(38, 66);
            this.checkBox3.TabIndex = 32;
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1284, 691);
            this.ControlBox = false;
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.panelsubcab);
            this.Controls.Add(this.cabecera);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CONEXIÓN: COMPUTER - LOGO SIEMENS";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.cabecera.ResumeLayout(false);
            this.cabecera.PerformLayout();
            this.panelcabcentral.ResumeLayout(false);
            this.PanelConnect.ResumeLayout(false);
            this.PanelConnect.PerformLayout();
            this.panelsubcab.ResumeLayout(false);
            this.panelsubcab.PerformLayout();
            this.panelAlarmas.ResumeLayout(false);
            this.panelAlarmas.PerformLayout();
            this.panel.ResumeLayout(false);
            this.panelHMI.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.p31b3.ResumeLayout(false);
            this.p32b3.ResumeLayout(false);
            this.p32b3.PerformLayout();
            this.p31b2.ResumeLayout(false);
            this.p32b2.ResumeLayout(false);
            this.p32b2.PerformLayout();
            this.p31b1.ResumeLayout(false);
            this.p32b1.ResumeLayout(false);
            this.p32b1.PerformLayout();
            this.subpanel3b.ResumeLayout(false);
            this.subpanel3b.PerformLayout();
            this.subpanel3.ResumeLayout(false);
            this.subpanel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.p11b3.ResumeLayout(false);
            this.p12b3.ResumeLayout(false);
            this.p12b3.PerformLayout();
            this.p11b2.ResumeLayout(false);
            this.p12b2.ResumeLayout(false);
            this.p12b2.PerformLayout();
            this.p11b1.ResumeLayout(false);
            this.p12b1.ResumeLayout(false);
            this.p12b1.PerformLayout();
            this.subpanel1.ResumeLayout(false);
            this.subpanel1.PerformLayout();
            this.subpanel1b.ResumeLayout(false);
            this.subpanel1b.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.p21b3.ResumeLayout(false);
            this.p22b3.ResumeLayout(false);
            this.p22b3.PerformLayout();
            this.p21b2.ResumeLayout(false);
            this.p22b2.ResumeLayout(false);
            this.p22b2.PerformLayout();
            this.p21b1.ResumeLayout(false);
            this.p22b1.ResumeLayout(false);
            this.p22b1.PerformLayout();
            this.subpanel2b.ResumeLayout(false);
            this.subpanel2b.PerformLayout();
            this.subpanel2.ResumeLayout(false);
            this.subpanel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Q25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v3_paso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.df3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_paso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v1_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.df1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FA_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FB_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v2_paso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.df2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picparomarcha2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbombilla2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCO2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBoton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picturepc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel cabecera;
        private System.Windows.Forms.PictureBox pictureon;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel panelcabcentral;
        private System.Windows.Forms.Button off;
        private System.Windows.Forms.Panel PanelConnect;
        private System.Windows.Forms.Button on;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox slotText;
        private System.Windows.Forms.TextBox rackText;
        private System.Windows.Forms.TextBox ipText;
        private System.Windows.Forms.TextBox dnsText;
        private System.Windows.Forms.TextBox textip;
        private System.Windows.Forms.PictureBox pictureBoxBoton;
        private System.Windows.Forms.Label labelonoff;
        private System.Windows.Forms.Panel panelsubcab;
        public System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Panel panelHMI;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel subpanel3b;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.PictureBox df3;
        private System.Windows.Forms.PictureBox FB_3;
        private System.Windows.Forms.PictureBox FA_3;
        private System.Windows.Forms.PictureBox v3_1;
        private System.Windows.Forms.Label b3_3;
        private System.Windows.Forms.PictureBox v3_2;
        private System.Windows.Forms.Label b3_2;
        private System.Windows.Forms.PictureBox v3_3;
        private System.Windows.Forms.Label b3_1;
        private System.Windows.Forms.PictureBox v3_paso;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel subpanel3;
        private System.Windows.Forms.Button run3;
        private System.Windows.Forms.Button stop3;
        private System.Windows.Forms.Label labelbombilla3;
        private System.Windows.Forms.PictureBox picbombilla3;
        private System.Windows.Forms.PictureBox picparomarcha3;
        private System.Windows.Forms.PictureBox picB31;
        private System.Windows.Forms.PictureBox picB32;
        private System.Windows.Forms.PictureBox picB33;
        private System.Windows.Forms.Button jump3;
        private System.Windows.Forms.Button refresh3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel subpanel2b;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox df2;
        private System.Windows.Forms.PictureBox FB_2;
        private System.Windows.Forms.PictureBox FA_2;
        private System.Windows.Forms.PictureBox v2_1;
        private System.Windows.Forms.Label b2_3;
        private System.Windows.Forms.PictureBox v2_2;
        private System.Windows.Forms.Label b2_2;
        private System.Windows.Forms.PictureBox v2_3;
        private System.Windows.Forms.Label b2_1;
        private System.Windows.Forms.PictureBox v2_paso;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel subpanel2;
        private System.Windows.Forms.Button run2;
        private System.Windows.Forms.Button stop2;
        private System.Windows.Forms.Label labelbombilla2;
        private System.Windows.Forms.PictureBox picbombilla2;
        private System.Windows.Forms.PictureBox picparomarcha2;
        private System.Windows.Forms.PictureBox picB23;
        private System.Windows.Forms.PictureBox picB22;
        private System.Windows.Forms.PictureBox picB21;
        private System.Windows.Forms.Button refresh2;
        private System.Windows.Forms.Button jump2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel subpanel1;
        private System.Windows.Forms.PictureBox picB13;
        private System.Windows.Forms.PictureBox picB12;
        private System.Windows.Forms.PictureBox picB11;
        private System.Windows.Forms.Label labelbombilla1;
        private System.Windows.Forms.Button run1;
        private System.Windows.Forms.Button stop1;
        private System.Windows.Forms.PictureBox picbombilla1;
        private System.Windows.Forms.PictureBox picparomarcha1;
        private System.Windows.Forms.Button jump1;
        private System.Windows.Forms.Button refresh1;
        private System.Windows.Forms.Panel subpanel1b;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox df1;
        private System.Windows.Forms.PictureBox FA_1;
        private System.Windows.Forms.PictureBox v1_1;
        private System.Windows.Forms.PictureBox FB_1;
        private System.Windows.Forms.PictureBox v1_2;
        private System.Windows.Forms.PictureBox v1_paso;
        private System.Windows.Forms.PictureBox v1_3;
        private System.Windows.Forms.Label b1_3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label b1_2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label b1_1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelFUresultado;
        private System.Windows.Forms.Label labelFU;
        private System.Windows.Forms.PictureBox picFU;
        private System.Windows.Forms.Label labelPUresultado;
        private System.Windows.Forms.Label labelPU;
        private System.Windows.Forms.PictureBox picPU;
        private System.Windows.Forms.Label labelCAresultado;
        private System.Windows.Forms.Label labelCA;
        private System.Windows.Forms.PictureBox picCA;
        private System.Windows.Forms.Label labelCO2resultado;
        private System.Windows.Forms.Label labelGas;
        private System.Windows.Forms.PictureBox picCO2;
        private System.Windows.Forms.Panel panelAlarmas;
        private System.Windows.Forms.Panel p11b1;
        private System.Windows.Forms.Panel p12b1;
        private System.Windows.Forms.Panel p11b2;
        private System.Windows.Forms.Panel p12b2;
        private System.Windows.Forms.Panel p11b3;
        private System.Windows.Forms.Panel p12b3;
        private System.Windows.Forms.Panel p31b3;
        private System.Windows.Forms.Panel p32b3;
        private System.Windows.Forms.Panel p31b2;
        private System.Windows.Forms.Panel p32b2;
        private System.Windows.Forms.Panel p31b1;
        private System.Windows.Forms.Panel p32b1;
        private System.Windows.Forms.Panel p21b3;
        private System.Windows.Forms.Panel p22b3;
        private System.Windows.Forms.Panel p21b2;
        private System.Windows.Forms.Panel p22b2;
        private System.Windows.Forms.Panel p21b1;
        private System.Windows.Forms.Panel p22b1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label registro;
        private System.Windows.Forms.Label acumulado3;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textDump;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Timer timer1_jump1;
        private System.Windows.Forms.Timer timer2_jump2;
        private System.Windows.Forms.Timer timer3_jump3;
        private System.Windows.Forms.Timer timer1_refresh1;
        private System.Windows.Forms.Timer timer2_refresh2;
        private System.Windows.Forms.Timer timer3_refresh3;
        private System.Windows.Forms.ProgressBar progressBarB1;
        private System.Windows.Forms.ProgressBar progressBarB3;
        private System.Windows.Forms.ProgressBar progressBarB2;
        private System.Windows.Forms.TextBox textpdu;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label acumulado1;
        private System.Windows.Forms.Label acumulado2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox Q5;
        private System.Windows.Forms.PictureBox Q4;
        private System.Windows.Forms.PictureBox Q3;
        private System.Windows.Forms.PictureBox Q2;
        private System.Windows.Forms.PictureBox Q1;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.PictureBox Q15;
        private System.Windows.Forms.PictureBox Q14;
        private System.Windows.Forms.PictureBox Q13;
        private System.Windows.Forms.PictureBox Q12;
        private System.Windows.Forms.PictureBox Q11;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.PictureBox Q25;
        private System.Windows.Forms.PictureBox Q24;
        private System.Windows.Forms.PictureBox Q23;
        private System.Windows.Forms.PictureBox Q22;
        private System.Windows.Forms.PictureBox Q21;
        private System.Windows.Forms.Timer timered;
        private System.Windows.Forms.PictureBox picturepc;
        private System.Windows.Forms.TextBox iptextlocal;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem inicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administradorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem escribirEnLogoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuraciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem baseDeDatosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alarmasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarSesiónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enviarDatosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verificarConexiónBDToolStripMenuItem;
        private System.Windows.Forms.Label vendido3;
        private System.Windows.Forms.Label vendido1;
        private System.Windows.Forms.Label vendido2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
    }
}

