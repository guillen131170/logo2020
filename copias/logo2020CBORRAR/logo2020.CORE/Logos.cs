﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.CORE
{

    /// <summary>
    /// ESTA CLASE SERÁN CADA UNO DE LOS AUTÓMATAS LOGO CON QUE CONECTAMOS
    /// 
    /// atributos:
    ///     - id - identificador del autómata
    ///     - ip - dirección IP del autómata
    ///     - dns - nombre de dominio asociado a la red del autómata
    ///     - rack - número de bloque o bastidor
    ///     - slot - número de ranura del bloque o bastidor
    /// </summary>   
    public class Logos
    {
        /*################################################################*/
        #region atributos     
        /// <summary>
        /// identificador del autómata
        /// </summary>
        private int id;

        /// <summary>
        /// dirección IP del autómata
        /// </summary>
        private string ip;

        /// <summary>
        /// nombre de dominio asociado a la red del autómata
        /// </summary>
        private string dns;

        /// <summary>
        /// número de bloque o bastidor
        /// </summary>
        private int rack;

        /// <summary>
        /// número de ranura del bloque o bastidor
        /// </summary>
        private int slot;
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region /* constructores */
        
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Logos() { }

        /// <summary>
        /// Constructor por parámetros (5 - todos)
        /// </summary>
        public Logos(int id, string dns, string ip, string rack, string slot)
        {
            Id = id;
            DNS = dns;
            IP = ip;
            Rack = Convert.ToInt32(rack);
            Slot = Convert.ToInt32(slot);
        }

        /// <summary>
        /// Constructor por parámetros (2 - dns,ip)
        /// </summary>
        public Logos(string dns, string ip)
        {
            Id = 99;
            DNS = dns;
            IP = ip;
            Rack = 1;
            Slot = 0;
        }

        /// <summary>
        /// Constructor copia
        /// </summary>
        public Logos(Logos o)
        {
            Id = o.Id;
            DNS = o.DNS;
            IP = o.IP;
            Rack = o.Rack;
            Rack = o.Rack;
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region propiedades
        [Required]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Required]
        public string IP
        {
            get { return ip; }
            set { ip = value; }
        }

        [Required]
        public string DNS
        {
            get { return dns; }
            set { dns = value; }
        }

        [Required]
        [Range(0, 9)]
        public int Slot
        {
            get { return slot; }
            set { slot = value; }
        }

        [Required]
        [Range(0, 9)]
        public int Rack
        {
            get { return rack; }
            set { rack = value; }
        }
        #endregion
        /*################################################################*/
    }
}
