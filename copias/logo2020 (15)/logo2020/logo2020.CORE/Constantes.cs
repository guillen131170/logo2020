﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.CORE
{
    public static class Constantes
    {
        /*
        public struct N_BARRILES_LINEA1_BYTE_1  {
            public int direccion;
            public int valor;
            public N_BARRILES_LINEA1_BYTE_1 (int d, int v) { direccion = d; valor = v; }
        }
        */
        #region CONSTANTES DEL CORREO ELECTRÓNICO
        public const string ADDRESS_MAIL_FROM = "intercambiadordebarriles@hotmail.com";
        public const string PASSWORD_MAIL_FROM = "Virtuino";
        public const int MAX_NTRY_SENDMAIL = 5;
        #endregion

        #region CONTADOR DE BARRILES DE CADA LÍNEA: 1, 2 Y 3
        #region LINEA 1 - 4 BYTES - DIRECCIONES DE MEMORIA: 118 - 119 - 120 - 121
        public const int N_BARRILES_LINEA1_BYTE_1 = 118;
        public const int N_BARRILES_LINEA1_BYTE_2 = 119;
        public const int N_BARRILES_LINEA1_BYTE_3 = 120;
        public const int N_BARRILES_LINEA1_BYTE_4 = 121;
        #endregion
        #region LINEA 2 - 4 BYTES - DIRECCIONES DE MEMORIA: 122 - 123 - 124 - 125
        public const int N_BARRILES_LINEA2_BYTE_1 = 122;
        public const int N_BARRILES_LINEA2_BYTE_2 = 123;
        public const int N_BARRILES_LINEA2_BYTE_3 = 124;
        public const int N_BARRILES_LINEA2_BYTE_4 = 125;
        #endregion
        #region LINEA 3 - 4 BYTES - DIRECCIONES DE MEMORIA: 126 - 127 - 128 - 129
        public const int N_BARRILES_LINEA3_BYTE_1 = 126;
        public const int N_BARRILES_LINEA3_BYTE_2 = 127;
        public const int N_BARRILES_LINEA3_BYTE_3 = 128;
        public const int N_BARRILES_LINEA3_BYTE_4 = 129;
        #endregion
        #endregion

        #region BARRILES ACTIVOS - 3 BYTES POR LINEA (BA1 BYTE 0 - BA2 BYTE 1 - BA3 BYTE 2) - 1 ACTIVO / 0 INACTIVO 
        #region LINEA 1 - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES - DIRECCIONES DE MEMORIA: 214 - 215 - 216
        public const int BARRILACTIVO_LINEA1_BYTE_1 = 214;
        public const int BARRILACTIVO_LINEA1_BYTE_2 = 215;
        public const int BARRILACTIVO_LINEA1_BYTE_3 = 216;
        #endregion
        #region LINEA 2 - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES - DIRECCIONES DE MEMORIA: 224 - 225 - 226
        public const int BARRILACTIVO_LINEA2_BYTE_1 = 224;
        public const int BARRILACTIVO_LINEA2_BYTE_2 = 225;
        public const int BARRILACTIVO_LINEA2_BYTE_3 = 226;
        #endregion
        #region LINEA 3 - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES - DIRECCIONES DE MEMORIA: 234 - 235 - 236
        public const int BARRILACTIVO_LINEA3_BYTE_1 = 234;
        public const int BARRILACTIVO_LINEA3_BYTE_2 = 235;
        public const int BARRILACTIVO_LINEA3_BYTE_3 = 236;
        #endregion
        #endregion

        #region BARRILES LLENOS - 1 BYTE POR BARRIL - 0 LLENO / OTRO VALOR VACIO 
        #region LINEA 1 - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES - DIRECCIONES DE MEMORIA: 211 - 212 - 213
        public const int N_BARRILESLLENOS_LINEA1_BYTE_1 = 211;
        public const int N_BARRILESLLENOS_LINEA1_BYTE_2 = 212;
        public const int N_BARRILESLLENOS_LINEA1_BYTE_3 = 213;
        #endregion
        #region LINEA 2 - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES - DIRECCIONES DE MEMORIA: 221 - 222 - 223
        public const int N_BARRILESLLENOS_LINEA2_BYTE_1 = 221;
        public const int N_BARRILESLLENOS_LINEA2_BYTE_2 = 222;
        public const int N_BARRILESLLENOS_LINEA2_BYTE_3 = 223;
        #endregion
        #region LINEA 3 - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES - DIRECCIONES DE MEMORIA: 231 - 232 - 233
        public const int N_BARRILESLLENOS_LINEA3_BYTE_1 = 231;
        public const int N_BARRILESLLENOS_LINEA3_BYTE_2 = 232;
        public const int N_BARRILESLLENOS_LINEA3_BYTE_3 = 233;
        #endregion
        #endregion

        #region ELECTROVALVULAS 5, 15 Y 25 - 1 BYTE POR ELECTROVALVULA - 0 ABIERTA / OTRO VALOR CERRADA 
        #region LINEA 1, 2 Y 3 - 1 BYTE POR ELECTROVALVULA - DIRECCIONES DE MEMORIA: 310 - 320 - 330
        public const int ELECTROVALVULA5_LINEA1_BYTE = 310;
        public const int ELECTROVALVULA15_LINEA2_BYTE = 320;
        public const int ELECTROVALVULA25_LINEA3_BYTE = 330;
        #endregion
        #endregion

        #region DETECTORES - 1 BYTE POR DETECTOR - 0 FLOTADOR UP / OTRO VALOR FLOTADOR DOWN 
        #region LINEA 1, 2 Y 3 - 1 BYTE POR DETECTOR - DIRECCIONES DE MEMORIA: 210 - 220 - 230
        public const int DETECTOR_LINEA1_BYTE = 210;
        public const int DETECTOR_LINEA2_BYTE = 220;
        public const int DETECTOR_LINEA3_BYTE = 230;
        #endregion
        #endregion

        #region ALARMAS - 1 BYTE POR ALARMA
        #region 1 BYTE - DIRECCION DE MEMORIA: 208 - 1 OK / OTRO VALOR ERROR
        public const int ALARMA_CO2_BYTE = 208;
        public const int VALOR_OK_ALARMA_CO2 = 1;
        #endregion
        #region 1 BYTE - DIRECCION DE MEMORIA: 255 - 1 OK / OTRO VALOR ERROR
        public const int ALARMA_COMPRESORAIRE_BYTE = 255;
        #endregion
        #region 1 BYTE - DIRECCION DE MEMORIA: 209 - 1 OK / OTRO VALOR ERROR
        public const int ALARMA_PUERTAANIERTA_BYTE = 209;
        #endregion
        #endregion
    }
}
