﻿using logo2020.APPLICATION;
using logo2020.CORE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.PRESENTATION
{
    public partial class FormEscribirEnLogo : Form
    {
        public ProcesosConLogo procesosconlogo;

        #region CREAR Y MOSTRAR EL FORMULARIO
        #region CREAR EL FORMULARIO
        public FormEscribirEnLogo(ProcesosConLogo p)
        {
            InitializeComponent();
            procesosconlogo = p;
        }
        #endregion

        #region CARGAR EL FORMULARIO
        private void FormEscribirEnLogo_Load(object sender, EventArgs e)
        {
            tipodato.SelectedIndex = 0;
        }
        #endregion 
        #endregion


        #region ENVIAR LA INFORMACIÓN Y ESCRIBIR EN LOGO
        private void admin_enviar_Click(object sender, EventArgs e)
        {
            int start = 0;
            int value = 0;
            Generales general = new Generales();

            try
            {
                start = Convert.ToInt32(admin_address.Text);
                value = Convert.ToInt32(admin_value.Text);
            }
            catch (Exception) { }

            if (!admin_address.Text.Equals("") && !admin_value.Text.Equals(""))
            {
                switch (tipodato.SelectedIndex)
                {
                    case 0: //BYTE
                        byte[] buffer_byte = new byte[1];
                        try
                        {
                            //buffer_byte = general.IntToArrayByte4(value);
                            procesosconlogo.writeByte(start, buffer_byte[0]);
                            MessageBox.Show("BYTE");
                        }
                        catch (Exception) { }
                        break;
                    case 1: //WORD
                        byte[] buffer_word = new byte[2];
                        try
                        {
                            buffer_word = general.IntToArrayByte2(value);
                            procesosconlogo.writeWord(start, buffer_word[0], buffer_word[1]);
                            MessageBox.Show("WORD");
                        }
                        catch (Exception) { }
                        break;
                    case 2: //DWORD
                        byte[] buffer_dword = new byte[4];
                        try
                        {
                            buffer_dword = general.IntToArrayByte4(value);
                            procesosconlogo.writeDword(start, buffer_dword[0], buffer_dword[1], buffer_dword[2], buffer_dword[3]);
                            MessageBox.Show("DWORD");
                        }
                        catch (Exception) { }
                        break;
                    default:
                        break;
                }
            }
        } 
        #endregion


        #region CERRAR ESTE FORMULARIO
        private void cerrarEscribirLogo_Click(object sender, EventArgs e)
        {
            this.Close();
        } 
        #endregion
    }
}
