﻿namespace logo2020.PRESENTATION
{
    partial class FormEscribirEnLogo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.admin = new System.Windows.Forms.Panel();
            this.label56 = new System.Windows.Forms.Label();
            this.tipodato = new System.Windows.Forms.ComboBox();
            this.label55 = new System.Windows.Forms.Label();
            this.admin_value = new System.Windows.Forms.TextBox();
            this.admin_address = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.cerrarEscribirLogo = new System.Windows.Forms.Button();
            this.admin_enviar = new System.Windows.Forms.Button();
            this.admin.SuspendLayout();
            this.SuspendLayout();
            // 
            // admin
            // 
            this.admin.BackColor = System.Drawing.Color.CadetBlue;
            this.admin.Controls.Add(this.cerrarEscribirLogo);
            this.admin.Controls.Add(this.label56);
            this.admin.Controls.Add(this.tipodato);
            this.admin.Controls.Add(this.admin_enviar);
            this.admin.Controls.Add(this.label55);
            this.admin.Controls.Add(this.admin_value);
            this.admin.Controls.Add(this.admin_address);
            this.admin.Controls.Add(this.label54);
            this.admin.Location = new System.Drawing.Point(12, 12);
            this.admin.Name = "admin";
            this.admin.Size = new System.Drawing.Size(496, 243);
            this.admin.TabIndex = 51;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(26, 29);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(104, 18);
            this.label56.TabIndex = 37;
            this.label56.Text = "TIPO DE DATO";
            // 
            // tipodato
            // 
            this.tipodato.AllowDrop = true;
            this.tipodato.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tipodato.FormattingEnabled = true;
            this.tipodato.Items.AddRange(new object[] {
            "BYTE",
            "WORD",
            "DWORD"});
            this.tipodato.Location = new System.Drawing.Point(136, 26);
            this.tipodato.Name = "tipodato";
            this.tipodato.Size = new System.Drawing.Size(85, 21);
            this.tipodato.TabIndex = 36;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(227, 108);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(48, 18);
            this.label55.TabIndex = 34;
            this.label55.Text = "VALOR";
            // 
            // admin_value
            // 
            this.admin_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.admin_value.Location = new System.Drawing.Point(172, 104);
            this.admin_value.Name = "admin_value";
            this.admin_value.Size = new System.Drawing.Size(49, 24);
            this.admin_value.TabIndex = 33;
            this.admin_value.Text = "1";
            // 
            // admin_address
            // 
            this.admin_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.admin_address.Location = new System.Drawing.Point(172, 74);
            this.admin_address.Name = "admin_address";
            this.admin_address.Size = new System.Drawing.Size(49, 24);
            this.admin_address.TabIndex = 31;
            this.admin_address.Text = "1";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(227, 78);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(144, 18);
            this.label54.TabIndex = 32;
            this.label54.Text = "DIRECCIÓN MEMORIA";
            // 
            // cerrarEscribirLogo
            // 
            this.cerrarEscribirLogo.FlatAppearance.BorderSize = 0;
            this.cerrarEscribirLogo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cerrarEscribirLogo.Image = global::logo2020.PRESENTATION.Properties.Resources.CERRAR;
            this.cerrarEscribirLogo.Location = new System.Drawing.Point(296, 169);
            this.cerrarEscribirLogo.Name = "cerrarEscribirLogo";
            this.cerrarEscribirLogo.Size = new System.Drawing.Size(75, 40);
            this.cerrarEscribirLogo.TabIndex = 38;
            this.cerrarEscribirLogo.UseVisualStyleBackColor = true;
            this.cerrarEscribirLogo.Click += new System.EventHandler(this.cerrarEscribirLogo_Click);
            // 
            // admin_enviar
            // 
            this.admin_enviar.FlatAppearance.BorderSize = 0;
            this.admin_enviar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.admin_enviar.Image = global::logo2020.PRESENTATION.Properties.Resources.enviar;
            this.admin_enviar.Location = new System.Drawing.Point(386, 164);
            this.admin_enviar.Name = "admin_enviar";
            this.admin_enviar.Size = new System.Drawing.Size(75, 50);
            this.admin_enviar.TabIndex = 35;
            this.admin_enviar.UseVisualStyleBackColor = true;
            this.admin_enviar.Click += new System.EventHandler(this.admin_enviar_Click);
            // 
            // FormEscribirEnLogo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Sienna;
            this.ClientSize = new System.Drawing.Size(520, 267);
            this.Controls.Add(this.admin);
            this.Name = "FormEscribirEnLogo";
            this.Text = "ESCRIBIR MEMORIA DE LOGO";
            this.Load += new System.EventHandler(this.FormEscribirEnLogo_Load);
            this.admin.ResumeLayout(false);
            this.admin.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel admin;
        private System.Windows.Forms.Button cerrarEscribirLogo;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.ComboBox tipodato;
        private System.Windows.Forms.Button admin_enviar;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox admin_value;
        private System.Windows.Forms.TextBox admin_address;
        private System.Windows.Forms.Label label54;
    }
}