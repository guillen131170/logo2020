﻿namespace logo2020.PRESENTATION
{
    partial class FormVerificarBD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.cerrarbd = new System.Windows.Forms.Button();
            this.probar = new System.Windows.Forms.Button();
            this.labelconectado = new System.Windows.Forms.Label();
            this.linea = new System.Windows.Forms.Label();
            this.pictureBoxbd = new System.Windows.Forms.PictureBox();
            this.pictureBoxpc = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxbd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxpc)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.cerrarbd);
            this.panel1.Controls.Add(this.probar);
            this.panel1.Controls.Add(this.labelconectado);
            this.panel1.Controls.Add(this.linea);
            this.panel1.Controls.Add(this.pictureBoxbd);
            this.panel1.Controls.Add(this.pictureBoxpc);
            this.panel1.Location = new System.Drawing.Point(7, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(743, 143);
            this.panel1.TabIndex = 0;
            // 
            // cerrarbd
            // 
            this.cerrarbd.FlatAppearance.BorderSize = 0;
            this.cerrarbd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cerrarbd.Image = global::logo2020.PRESENTATION.Properties.Resources.CERRAR;
            this.cerrarbd.Location = new System.Drawing.Point(379, 75);
            this.cerrarbd.Name = "cerrarbd";
            this.cerrarbd.Size = new System.Drawing.Size(75, 40);
            this.cerrarbd.TabIndex = 5;
            this.cerrarbd.UseVisualStyleBackColor = true;
            this.cerrarbd.Click += new System.EventHandler(this.cerrarbd_Click);
            // 
            // probar
            // 
            this.probar.FlatAppearance.BorderSize = 0;
            this.probar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.probar.Image = global::logo2020.PRESENTATION.Properties.Resources.PROBAR;
            this.probar.Location = new System.Drawing.Point(289, 75);
            this.probar.Name = "probar";
            this.probar.Size = new System.Drawing.Size(75, 40);
            this.probar.TabIndex = 4;
            this.probar.UseVisualStyleBackColor = true;
            this.probar.Click += new System.EventHandler(this.probar_Click);
            // 
            // labelconectado
            // 
            this.labelconectado.AutoSize = true;
            this.labelconectado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelconectado.Location = new System.Drawing.Point(159, 47);
            this.labelconectado.Name = "labelconectado";
            this.labelconectado.Size = new System.Drawing.Size(0, 20);
            this.labelconectado.TabIndex = 3;
            // 
            // linea
            // 
            this.linea.BackColor = System.Drawing.Color.Black;
            this.linea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linea.ForeColor = System.Drawing.Color.Black;
            this.linea.Location = new System.Drawing.Point(159, 67);
            this.linea.Name = "linea";
            this.linea.Size = new System.Drawing.Size(425, 5);
            this.linea.TabIndex = 2;
            this.linea.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBoxbd
            // 
            this.pictureBoxbd.Image = global::logo2020.PRESENTATION.Properties.Resources.bd_off;
            this.pictureBoxbd.Location = new System.Drawing.Point(590, 7);
            this.pictureBoxbd.Name = "pictureBoxbd";
            this.pictureBoxbd.Size = new System.Drawing.Size(128, 128);
            this.pictureBoxbd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxbd.TabIndex = 1;
            this.pictureBoxbd.TabStop = false;
            // 
            // pictureBoxpc
            // 
            this.pictureBoxpc.Image = global::logo2020.PRESENTATION.Properties.Resources.Laptop_OFF;
            this.pictureBoxpc.Location = new System.Drawing.Point(25, 7);
            this.pictureBoxpc.Name = "pictureBoxpc";
            this.pictureBoxpc.Size = new System.Drawing.Size(128, 128);
            this.pictureBoxpc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxpc.TabIndex = 0;
            this.pictureBoxpc.TabStop = false;
            // 
            // FormVerificarBD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(756, 157);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormVerificarBD";
            this.Text = "VERIFICA CONEXIÓN CON BASE DE DATOS";
            this.Load += new System.EventHandler(this.FormVerificarBD_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxbd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxpc)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBoxbd;
        private System.Windows.Forms.PictureBox pictureBoxpc;
        private System.Windows.Forms.Label linea;
        private System.Windows.Forms.Label labelconectado;
        private System.Windows.Forms.Button cerrarbd;
        private System.Windows.Forms.Button probar;
    }
}