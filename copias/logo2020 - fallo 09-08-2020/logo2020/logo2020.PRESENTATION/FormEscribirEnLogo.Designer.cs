﻿namespace logo2020.PRESENTATION
{
    partial class FormEscribirEnLogo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.admin = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.admin_logo = new System.Windows.Forms.TextBox();
            this.direccion = new System.Windows.Forms.ComboBox();
            this.nombre = new System.Windows.Forms.ComboBox();
            this.cerrarEscribirLogo = new System.Windows.Forms.Button();
            this.label56 = new System.Windows.Forms.Label();
            this.tipodato = new System.Windows.Forms.ComboBox();
            this.admin_enviar = new System.Windows.Forms.Button();
            this.label55 = new System.Windows.Forms.Label();
            this.admin_value = new System.Windows.Forms.TextBox();
            this.admin_address = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.admin.SuspendLayout();
            this.SuspendLayout();
            // 
            // admin
            // 
            this.admin.BackColor = System.Drawing.SystemColors.Control;
            this.admin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.admin.Controls.Add(this.label1);
            this.admin.Controls.Add(this.admin_logo);
            this.admin.Controls.Add(this.direccion);
            this.admin.Controls.Add(this.nombre);
            this.admin.Controls.Add(this.cerrarEscribirLogo);
            this.admin.Controls.Add(this.label56);
            this.admin.Controls.Add(this.tipodato);
            this.admin.Controls.Add(this.admin_enviar);
            this.admin.Controls.Add(this.label55);
            this.admin.Controls.Add(this.admin_value);
            this.admin.Controls.Add(this.admin_address);
            this.admin.Controls.Add(this.label54);
            this.admin.Location = new System.Drawing.Point(12, 12);
            this.admin.Name = "admin";
            this.admin.Size = new System.Drawing.Size(496, 243);
            this.admin.TabIndex = 51;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(330, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 18);
            this.label1.TabIndex = 42;
            this.label1.Text = "VALOR ACTUAL";
            // 
            // admin_logo
            // 
            this.admin_logo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.admin_logo.Location = new System.Drawing.Point(275, 101);
            this.admin_logo.Name = "admin_logo";
            this.admin_logo.ReadOnly = true;
            this.admin_logo.Size = new System.Drawing.Size(49, 24);
            this.admin_logo.TabIndex = 41;
            this.admin_logo.Text = "1";
            // 
            // direccion
            // 
            this.direccion.AllowDrop = true;
            this.direccion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.direccion.FormattingEnabled = true;
            this.direccion.Items.AddRange(new object[] {
            "0",
            "4",
            "8",
            "12",
            "16",
            "20",
            "24",
            "28",
            "32",
            "211",
            "212",
            "213",
            "221",
            "222",
            "223",
            "231",
            "232",
            "233"});
            this.direccion.Location = new System.Drawing.Point(16, 107);
            this.direccion.Name = "direccion";
            this.direccion.Size = new System.Drawing.Size(83, 21);
            this.direccion.TabIndex = 40;
            this.direccion.SelectionChangeCommitted += new System.EventHandler(this.direccion_SelectionChangeCommitted);
            // 
            // nombre
            // 
            this.nombre.AllowDrop = true;
            this.nombre.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.nombre.FormattingEnabled = true;
            this.nombre.Items.AddRange(new object[] {
            "LITROS BARRIL 1 GRIFO 1",
            "LITROS BARRIL 2 GRIFO 1",
            "LITROS BARRIL 3 GRIFO 1",
            "LITROS BARRIL 1 GRIFO 2",
            "LITROS BARRIL 2 GRIFO 2",
            "LITROS BARRIL 3 GRIFO 2",
            "LITROS BARRIL 1 GRIFO 3",
            "LITROS BARRIL 2 GRIFO 3",
            "LITROS BARRIL 3 GRIFO 3",
            "VACIO/LLENO BARRIL 1 GRIFO 1",
            "VACIO/LLENO BARRIL 2 GRIFO 1",
            "VACIO/LLENO BARRIL 3 GRIFO 1",
            "VACIO/LLENO BARRIL 1 GRIFO 2",
            "VACIO/LLENO BARRIL 2 GRIFO 2",
            "VACIO/LLENO BARRIL 3 GRIFO 2",
            "VACIO/LLENO BARRIL 1 GRIFO 3",
            "VACIO/LLENO BARRIL 2 GRIFO 3",
            "VACIO/LLENO BARRIL 3 GRIFO 3"});
            this.nombre.Location = new System.Drawing.Point(16, 54);
            this.nombre.Name = "nombre";
            this.nombre.Size = new System.Drawing.Size(233, 21);
            this.nombre.TabIndex = 39;
            this.nombre.SelectionChangeCommitted += new System.EventHandler(this.nombre_SelectionChangeCommitted);
            // 
            // cerrarEscribirLogo
            // 
            this.cerrarEscribirLogo.FlatAppearance.BorderSize = 0;
            this.cerrarEscribirLogo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cerrarEscribirLogo.Image = global::logo2020.PRESENTATION.Properties.Resources.CERRAR;
            this.cerrarEscribirLogo.Location = new System.Drawing.Point(326, 193);
            this.cerrarEscribirLogo.Name = "cerrarEscribirLogo";
            this.cerrarEscribirLogo.Size = new System.Drawing.Size(75, 40);
            this.cerrarEscribirLogo.TabIndex = 38;
            this.cerrarEscribirLogo.UseVisualStyleBackColor = true;
            this.cerrarEscribirLogo.Click += new System.EventHandler(this.cerrarEscribirLogo_Click);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(272, 22);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(104, 18);
            this.label56.TabIndex = 37;
            this.label56.Text = "TIPO DE DATO";
            // 
            // tipodato
            // 
            this.tipodato.AllowDrop = true;
            this.tipodato.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tipodato.FormattingEnabled = true;
            this.tipodato.Items.AddRange(new object[] {
            "BYTE",
            "WORD",
            "DWORD"});
            this.tipodato.Location = new System.Drawing.Point(382, 22);
            this.tipodato.Name = "tipodato";
            this.tipodato.Size = new System.Drawing.Size(85, 21);
            this.tipodato.TabIndex = 36;
            // 
            // admin_enviar
            // 
            this.admin_enviar.FlatAppearance.BorderSize = 0;
            this.admin_enviar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.admin_enviar.Image = global::logo2020.PRESENTATION.Properties.Resources.enviar;
            this.admin_enviar.Location = new System.Drawing.Point(416, 188);
            this.admin_enviar.Name = "admin_enviar";
            this.admin_enviar.Size = new System.Drawing.Size(75, 50);
            this.admin_enviar.TabIndex = 35;
            this.admin_enviar.UseVisualStyleBackColor = true;
            this.admin_enviar.Click += new System.EventHandler(this.admin_enviar_Click);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(330, 135);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(96, 18);
            this.label55.TabIndex = 34;
            this.label55.Text = "NUEVO VALOR";
            // 
            // admin_value
            // 
            this.admin_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.admin_value.Location = new System.Drawing.Point(275, 131);
            this.admin_value.Name = "admin_value";
            this.admin_value.Size = new System.Drawing.Size(49, 24);
            this.admin_value.TabIndex = 33;
            this.admin_value.Text = "0";
            // 
            // admin_address
            // 
            this.admin_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.admin_address.Location = new System.Drawing.Point(275, 71);
            this.admin_address.Name = "admin_address";
            this.admin_address.Size = new System.Drawing.Size(49, 24);
            this.admin_address.TabIndex = 31;
            this.admin_address.Text = "0";
            this.admin_address.TextChanged += new System.EventHandler(this.admin_address_TextChanged);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(330, 75);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(144, 18);
            this.label54.TabIndex = 32;
            this.label54.Text = "DIRECCIÓN MEMORIA";
            // 
            // FormEscribirEnLogo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(520, 267);
            this.ControlBox = false;
            this.Controls.Add(this.admin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormEscribirEnLogo";
            this.Text = "ESCRIBIR MEMORIA DE LOGO";
            this.Load += new System.EventHandler(this.FormEscribirEnLogo_Load);
            this.admin.ResumeLayout(false);
            this.admin.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel admin;
        private System.Windows.Forms.Button cerrarEscribirLogo;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.ComboBox tipodato;
        private System.Windows.Forms.Button admin_enviar;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox admin_value;
        private System.Windows.Forms.TextBox admin_address;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.ComboBox nombre;
        private System.Windows.Forms.ComboBox direccion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox admin_logo;
    }
}