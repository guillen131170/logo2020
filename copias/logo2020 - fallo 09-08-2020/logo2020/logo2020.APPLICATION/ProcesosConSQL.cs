﻿using logo2020.CORE;
using logo2020.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.APPLICATION
{
    public class ProcesosConSQL
    {
        #region ATRIBUTOS
        private SqlTrans transaccion;
        #endregion


        #region CONSTRUCTOR
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ProcesosConSQL() { transaccion = new SqlTrans(); }
        #endregion


        #region COMPRUEBA SI HAY REGISTRO GRABADO DE LA FECHA INDICADA
        /// <summary>
        /// Comprueba si hay algún consumo registrado
        /// en la fecha indicada en el parámetro
        /// </summary>
        /// <param name="f">fecha a comprobar</param>
        /// <returns>verdadero o falso</returns>
        public bool comprobarConsumo(int i)
        {
            return transaccion.comprobar(i);
        }
        #endregion


        #region COMPRUEBA LA TABLA ESTÁ VACÍA
        /// <summary>
        /// Comprueba si la tabla está vacía
        /// </summary>
        /// <returns></returns>
        public bool tablaVacia()
        {
            return transaccion.vacio();
        }
        #endregion


        #region GRABA UN REGISTRO EN BD
        public bool grabarConsumo(Consumos c)
        {

             return transaccion.grabar(c);
        }
        #endregion


        #region RECUPERA EL ULTIMO REGISTRO DE CONSUMOS
        /// <summary>
        /// Recupera la última fila/registro de la tabla Consumos
        /// </summary>
        /// <returns>Última fila/registro</returns>
        public Consumos recuperarConsumo()
        {
            return transaccion.recuperar();
        }
        #endregion


        #region OBTIENE LA CADENA DE CONEXION
        public string cadenaConexion()
        {
            return transaccion.GetConnectionString();
        }
        #endregion


        #region VERIFICA LA CONEXIÓN CON BASE DE DATOS
        public bool verificar()
        {
            return transaccion.VerificarConexión();
        }
        #endregion
    }
}
