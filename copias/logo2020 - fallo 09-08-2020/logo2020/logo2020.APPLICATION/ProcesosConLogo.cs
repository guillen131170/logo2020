﻿using logo2020.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.APPLICATION
{
    public class ProcesosConLogo
    {
        /*################################################################*/
        #region ATRIBUTOS DE LA CLASE
        /* Objeto Logo */
        private Logos logo;
        /* Objeto Sharp7 */
        private S7Client client = new S7Client();
        //private int sizeRead = 0;
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region BLOQUES DE DATOS
        private const int BLOCK1024 = 1024;
        private const int BLOCK512 = 512;
        private const int BLOCK256 = 256;
        private const int BLOCK128 = 128;
        private const int BLOCK64 = 64;
        private const int BLOCK32 = 32;
        private const int BLOCK16 = 16;
        private const int BLOCK8 = 8;
        private const int BLOCK4 = 4;
        private const int BLOCK3 = 3;
        private const int BLOCK2 = 2;
        private const int BLOCK1 = 1;

        private byte[] buffer1024 = new byte[BLOCK1024];
        public byte[] buffer512 = new byte[BLOCK512];
        private byte[] buffer256 = new byte[BLOCK256];
        private byte[] buffer128 = new byte[BLOCK128];
        private byte[] buffer64 = new byte[BLOCK64];
        private byte[] buffer32 = new byte[BLOCK32];
        private byte[] buffer16 = new byte[BLOCK16];
        private byte[] buffer8 = new byte[BLOCK8];
        private byte[] buffer4 = new byte[BLOCK4];
        private byte[] buffer3 = new byte[BLOCK3];
        private byte[] buffer2 = new byte[BLOCK2];
        private byte[] buffer1 = new byte[BLOCK1];
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region ESTRUCTURAS DE DATOS
        public int[] Area =
        {
            S7Consts.S7AreaPE,
            S7Consts.S7AreaPA,
            S7Consts.S7AreaMK,
            S7Consts.S7AreaDB,
            S7Consts.S7AreaCT,
            S7Consts.S7AreaTM
        };
        public int[] WordLen =
        {
            S7Consts.S7WLBit,
            S7Consts.S7WLByte,
            S7Consts.S7WLChar,
            S7Consts.S7WLWord,
            S7Consts.S7WLInt,
            S7Consts.S7WLDWord,
            S7Consts.S7WLDInt,
            S7Consts.S7WLReal,
            S7Consts.S7WLCounter,
            S7Consts.S7WLTimer
        };
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region CONSTRUCTORES DE LA CLASE        
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ProcesosConLogo() { }

        /// <summary>
        /// Constructor con 2 parámetros
        /// </summary>
        /// <param name="dns">Nombre de dominio de la dirección de red de Logo</param>
        /// <param name="ip">Dirección de red de Logo</param>
        public ProcesosConLogo(string dns, string ip)
        {
            logo = new Logos(dns, ip);
        }


        /// <summary>
        /// Constructor copia
        /// </summary>
        /// <param name="o">Objeto ProcesosConLogo que se va a copiar</param>
        public ProcesosConLogo(ProcesosConLogo o)
        {
            Logo = o.Logo;
            client = o.client;
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region CONECTAR/DESCONECTAR LOGO
        /// <summary>
        /// Conectar con Logo mediante IP
        /// </summary>
        /// <returns></returns>
        public int connectPlcIP()
        {
            return client.ConnectTo(logo.IP, logo.Rack, logo.Slot);
        }

        /// <summary>
        /// Conectar con Logo mediante DNS
        /// </summary>
        /// <returns></returns>
        public void connectPlcDNS()
        {
            try
            {
                IPHostEntry host = Dns.GetHostEntry(logo.DNS);
                if (host != null)
                {
                    foreach (IPAddress address in host.AddressList)
                    {
                        logo.IP = address.ToString();
                    }
                }
                connectPlcIP();
            }
            catch { }
        }

        /// <summary>
        /// Desconectar del Logo
        /// </summary>
        /// <returns></returns>
        public void disconnectPLc()
        {
            try
            {
                client.Disconnect();
            }
            catch { }
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region LECTURAS DEL PLC/LOGO
        #region LECTURA DE BLOQUE DE 512 BYTES
        /// <summary>
        /// Lee un bloque de 512 bytes del plc
        /// </summary>
        /// <returns>Result==0 OK o Result!=0 ERROR - </returns>
        public int LeerBloque512()
        {
            int Result = -1;
            try
            {
                /* Crear una instancia Multi Reader para la lectura */
                S7MultiVar Reader = new S7MultiVar(Client);
                /* Crea la sentencia de lectura */
                Reader.Add(Area[3], WordLen[1], 1, 0, BLOCK512, ref buffer512);
                /* Ejecuta la lectura */
                Result = Reader.Read();
            }
            catch (Exception)
            {
                
            }
            return Result;
        }
        #endregion


        #region ELECTROVÁLVULA 5 - PASO DE PRODUCTO - NORMALMENTE ABIERTA
        /// <summary>
        /// Lee el estado de la ELECTROVÁLVULA NÚMERO 5
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public int lecturaV5(int pos)
        {
            int Result = -1;
            int dir_memoria = pos;
            try
            {
                if (readBlock1(3, 1, 1, dir_memoria) == 0)
                    Result = Convert.ToInt32(buffer1[0]);
            }
            catch (Exception)
            {
                
            }
            return Result;
        }
        #endregion


        #region DETECTOR DE FASE - NORMALMENTE CERRADA
        /// <summary>
        /// Lee el estado del DETECTOR
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public int lecturaDetector(int pos)
        {
            int Result = -1;
            int dir_memoria = pos;
            try
            {
                if (readBlock1(3, 1, 1, dir_memoria) == 0)
                    Result = Convert.ToInt32(buffer1[0]);
            }
            catch (Exception)
            {

            }
            return Result;
        }
        #endregion


        #region BARRILES LLENOS
        /// <summary>
        /// Comprueba barriles llenos
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public int lecturaContenido(int pos)
        {
            int Result = -1;
            int dir_memoria = pos;
            try
            {
                if (readBlock1(3, 1, 1, dir_memoria) == 0)
                    Result = Convert.ToInt32(buffer1[0]);
            }
            catch (Exception)
            {

            }
            return Result;
        }
        #endregion


        #region NO UTILIZADO - BORRAR
        #region GRIFO ABIERTO/CERRADO o MARCHA/PARO - NO UTILIZADO
        /// <summary>
        /// Lee el estado del grifo abierto/cerrado o marcha/paro
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public int lecturaGrifo(int pos)
        {
            int dir_memoria = pos;
            readBlock1(3, 1, 1, dir_memoria);
            return Convert.ToInt32(buffer1[0]);
        }
        #endregion

        #region SIRVIENDO - NO UTILIZADO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public int lecturaSirviendo(int pos)
        {
            int dir_memoria = pos;
            readBlock1(3, 1, 1, dir_memoria);
            return Convert.ToInt32(buffer1[0]);
        }
        #endregion
        #endregion
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region PARO-MARCHA
        /*
         DIR MEMORIA: 310 grifo 1 (1 paro - 0 marcha)
         DIR MEMORIA: 320 grifo 2 (1 paro - 0 marcha)
         DIR MEMORIA: 330 grifo 3 (1 paro - 0 marcha)
        */
        #region PARO
        /// <summary>
        /// Paro grifo
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public int paro(int pos)
        {
            int Result = -1;
            int paro = 1;
            int dir_memoria = pos;
            try
            {
                writeByte(dir_memoria, paro);
                if (readBlock1(3, 1, 1, dir_memoria) == 0)
                    Result = Convert.ToInt32(buffer1[0]);
            }
            catch (Exception)
            {

            }
            return Result;
        }
        #endregion

        #region MARCHA
        /// <summary>
        /// Marcha grifo
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public int marcha(int pos)
        {
            int Result = -1;
            int marcha = 0;
            int dir_memoria = pos;
            try
            {
                writeByte(dir_memoria, marcha);
                if (readBlock1(3, 1, 1, dir_memoria) == 0)
                    Result = Convert.ToInt32(buffer1[0]);
            }
            catch (Exception)
            {

            }
            return Result;
        }
        #endregion

        #region LECTURA PARO-MARCHA
        /// <summary>
        /// Lectura de parp - marcha
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public int lercturaMarchaParo(int pos)
        {
            int Result = -1;
            int dir_memoria = pos;
            try
            {
                if (readBlock1(3, 1, 1, dir_memoria) == 0)
                    Result = Convert.ToInt32(buffer1[0]);
            }
            catch (Exception)
            {

            }
            return Result;
        }
        #endregion
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region MODO LIMPIEZA - NO SE UTILIZA
        /// <summary>
        /// DIR MEMORIA: 310 grifo 1 (1 paro - 0 marcha)
        /// DIR MEMORIA: 320 grifo 2 (1 paro - 0 marcha)
        /// DIR MEMORIA: 330 grifo 3 (1 paro - 0 marcha)
        /// </summary>
        /// <returns></returns>
        public bool modoLimpieza()
        {
            int paro = 1;
            int marcha = 0;
            int dir_memoria_g1 = 217;
            int dir_memoria_g2 = 227;
            int dir_memoria_g3 = 237;
            bool resultado = true;

            /*comprueba estado de los grifos: abierto - cerrado*/
            readBlock1(3, 1, 1, dir_memoria_g1);
            dir_memoria_g1 = buffer1[0];
            readBlock1(3, 1, 1, dir_memoria_g2);
            dir_memoria_g2 = buffer1[0];
            readBlock1(3, 1, 1, dir_memoria_g3);
            dir_memoria_g3 = buffer1[0];

            if ((dir_memoria_g1 == 0) || (dir_memoria_g2 == 0) || (dir_memoria_g3 == 0))
            {
                /*está abierto, lo cierra*/
                writeByte(310, paro);
                writeByte(320, paro);
                writeByte(330, paro);
                resultado = false;
            }
            else
            {
                /*está cerrado, lo abre*/
                writeByte(310, marcha);
                writeByte(320, marcha);
                writeByte(330, marcha);
                resultado = true;
            }

            return resultado;
        }
        #endregion
        /*################################################################*/


        #region COMPRUEBA GRIFO ABIERTO/CERRADO - NO SE UTILIZA
        /// <summary>
        /// DIR MEMORIA: 217 grifo 1 (1 paro - 0 marcha)
        /// DIR MEMORIA: 227 grifo 2 (1 paro - 0 marcha)
        /// DIR MEMORIA: 237 grifo 3 (1 paro - 0 marcha)
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public int grifo(int pos)
        {
            int paro = 1;
            int marcha = 0;
            int dir_memoria = pos;

            /*comprueba estado del grifo: abierto - cerrado*/
            readBlock1(3, 1, 1, dir_memoria);
            if (buffer1[0] == 0)
            {
                /*está cerrado, lo abre*/
                writeByte(dir_memoria, marcha);
            }
            else
            {
                /*está abierto, lo cierra*/
                writeByte(dir_memoria, paro);
            }

            readBlock1(3, 1, 1, dir_memoria);
            return Convert.ToInt32(buffer1[0]);
        }
        #endregion


        #region AVANCE BARRIL
        /// <summary>
        /// DIR AVANCE: 311 grifo 1 (escribir 1 y después 0)
        /// DIR AVANCE: 321 grifo 2 (escribir 1 y después 0)
        /// DIR AVANCE: 331 grifo 3 (escribir 1 y después 0)
        /// </summary>
        /// <param name="pos"></param>
        public void avance(int pos)
        {
            int dir_memoria = pos;

            writeByte(dir_memoria, 1);
            writeByte(dir_memoria, 0);
        }
        #endregion


        #region REPOSICIÓN BARRILES
        /// <summary>
        /// DIR AVANCE: 313 grifo 1 (escribir 1 y después 0)
        /// DIR AVANCE: 323 grifo 2 (escribir 1 y después 0)
        /// DIR AVANCE: 333 grifo 3 (escribir 1 y después 0)
        /// </summary>
        /// <param name="pos"></param>
        public void reponer(int pos)
        {
            int dir_memoria = pos;

            writeByte(dir_memoria, 1);
            writeByte(dir_memoria, 0);
        }
        #endregion


        #region BARRIL ACTIVO
        /// <summary>
        /// Comprueba el barril activo - LEE 3 BYTES DESDE LA DIRECCIÓN DADA
        ///          DIR MEMORIA: 214 grifo 1 -> 214 - 215 - 216
        ///          DIR MEMORIA: 224 grifo 2 -> 224 - 225 - 226
        ///          DIR MEMORIA: 234 grifo 3 -> 234 - 235 - 236
        ///          
        ///          byte 0 es barril 1
        ///          byte 1 es barril 2
        ///          byte 2 es barril 3
        ///          
        ///          Valor 1 - ACTIVO
        ///          Valor 0 - INACTIVO   
        /// </summary>
        /// <param name="memoria"></param>
        /// <returns>DEVUELVE EL BARRIL QUE ESTÁ ACTIVO: 1, 2 o 3</returns>
        public int activo(int memoria)
        {
            int resultado = 0;
            int dir_memoria = memoria;
            readBlock3(3, 1, 1, dir_memoria);

            if (buffer3[0] == 1)
            {
                resultado = 1;
            }
            else if (buffer3[1] == 1)
            {
                resultado = 2;
            }
            else if (buffer3[2] == 1)
            {
                resultado = 3;
            }
            return resultado;
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region LECTURA GENERAL DE BLOQUES DE DATOS EN PLC
        #region LECTURA DE 512 BYTES - NO SE UTILIZA
        /* Lee un bloque de 512 bytes en bytes */
        public int readBlock512(int area, int Longitud, int DBNumber, int Offset)
        {
            /* Crear instancia Multi Reader */
            S7MultiVar Reader = new S7MultiVar(Client);
            /* Crea la sentencia */
            Reader.Add(Area[area], WordLen[Longitud], DBNumber, Offset, BLOCK512, ref buffer512);
            /* Ejecuta y lee */
            int Result = Reader.Read();
            return Result;
        }
        #endregion

        #region LECTURA DE 256 BYTES - NO SE UTILIZA
        /* Lee un bloque de 256 bytes en bytes */
        public int readBlock256(int area, int Longitud, int DBNumber, int Offset)
        {
            /* Crear instancia Multi Reader */
            S7MultiVar Reader = new S7MultiVar(Client);
            /* Crea la sentencia */
            Reader.Add(Area[area], WordLen[Longitud], DBNumber, Offset, BLOCK256, ref buffer256);
            /* Ejecuta y lee */
            int Result = Reader.Read();
            return Result;
        }
        #endregion

        #region LECTURA DE 16 BYTES - NO SE UTILIZA
        /* Lee un bloque de 16 bytes en bytes */
        public int readBlock16(int area, int Longitud, int DBNumber, int Offset)
        {
            /* Crear instancia Multi Reader */
            S7MultiVar Reader = new S7MultiVar(Client);
            /* Crea la sentencia */
            Reader.Add(Area[area], WordLen[Longitud], DBNumber, Offset, BLOCK16, ref buffer16);
            /* Ejecuta y lee */
            int Result = Reader.Read();
            return Result;
        }
        #endregion

        #region LECTURA DE 4 BYTES - NO SE UTILIZA
        /* Lee un registro de 4 byte: DWORD */
        public int readBlock4(int area, int Longitud, int DBNumber, int Offset)
        {
            /* Crear instancia Multi Reader */
            S7MultiVar Reader = new S7MultiVar(Client);
            /* Crea la sentencia */
            Reader.Add(Area[area], WordLen[Longitud], DBNumber, Offset, BLOCK4, ref buffer4);
            /* Ejecuta y lee */
            int Result = Reader.Read();
            return Result;
        }
        #endregion

        #region LECTURA DE 2 BYTES - NO SE UTILIZA
        /* Lee un registro de 2 byte: WORD */
        public int readBlock2(int area, int Longitud, int DBNumber, int Offset)
        {
            /* Crear instancia Multi Reader */
            S7MultiVar Reader = new S7MultiVar(Client);
            /* Crea la sentencia */
            Reader.Add(Area[area], WordLen[Longitud], DBNumber, Offset, BLOCK2, ref buffer2);
            /* Ejecuta y lee */
            int Result = Reader.Read();
            return Result;
        }
        #endregion

        #region LECTURA DE 3 BYTES
        /// <summary>
        /// Lee 3 BYTES del plc
        /// 
        /// byte 0 es barril 1
        /// byte 1 es barril 2
        /// byte 2 es barril 3
        /// 
        /// Sirve para obtener el barril activo
        /// 
        /// Valor 1 - ACTIVO
        /// Valor 0 - INACTIO
        /// </summary>
        /// <param name="area"></param>
        /// <param name="Longitud"></param>
        /// <param name="DBNumber"></param>
        /// <param name="Offset"></param>
        /// <returns></returns>
        public int readBlock3(int area, int Longitud, int DBNumber, int Offset)
        {
            int Result = -1;
            try
            {
                /* Crear instancia Multi Reader */
                S7MultiVar Reader = new S7MultiVar(Client);
                /* Crea la sentencia */
                Reader.Add(Area[area], WordLen[Longitud], DBNumber, Offset, BLOCK3, ref buffer3);
                /* Ejecuta y lee */
                Result = Reader.Read();
            }
            catch (Exception)
            {

            }
            return Result;
        }
        #endregion

        #region LECTURA DE 1 BYTE
        /// <summary>
        ///  Lee 1 BYTE del plc
        /// </summary>
        /// <param name="area"></param>
        /// <param name="Longitud"></param>
        /// <param name="DBNumber"></param>
        /// <param name="Offset"></param>
        /// <returns></returns>
        public int readBlock1(int area, int Longitud, int DBNumber, int Offset)
        {
            int Result = -1;
            try
            {
                /* Crear instancia Multi Reader */
                S7MultiVar Reader = new S7MultiVar(Client);
                /* Crea la sentencia */
                Reader.Add(Area[area], WordLen[Longitud], DBNumber, Offset, BLOCK1, ref buffer1);
                /* Ejecuta y lee */
                Result = Reader.Read();
            }
            catch (Exception)
            {
                
            }
            return Result;
        }
        #endregion
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region ESCRITURA GENERAL DE BLOQUES DE DATOS EN PLC
        #region ESCRITURA DE UN DWORD
        /// <summary>
        /// Escribe en el plc un DWORD
        /// </summary>
        /// <param name="start">Posición de memoria en la que escribe</param>
        /// <param name="byte1">byte 1 del DWORD</param>
        /// <param name="byte2">byte 2 del DWORD</param>
        /// <param name="byte3">byte 3 del DWORD</param>
        /// <param name="byte4">byte 4 del DWORD</param>
        public void writeDword(int start, int byte1, int byte2, int byte3, int byte4)
        {
            try
            {
                // Multi Writer Instance
                S7MultiVar Writer = new S7MultiVar(Client);
                byte[] DB_A = new byte[4];
                DB_A[0] = Convert.ToByte(byte1);
                DB_A[1] = Convert.ToByte(byte2);
                DB_A[2] = Convert.ToByte(byte3);
                DB_A[3] = Convert.ToByte(byte4);
                int DBNumber_A = 1;
                Writer.Add(S7Consts.S7AreaDB, S7Consts.S7WLByte, DBNumber_A,
                                start, 4, ref DB_A);
                int Result = Writer.Write();
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region ESCRITURA DE UN WORD
        /// <summary>
        /// Escribe en el plc un WORD
        /// </summary>
        /// <param name="start">Posición de memoria en la que escribe</param>
        /// <param name="byte1">byte 1 del WORD</param>
        /// <param name="byte2">byte 2 del WORD</param>
        public void writeWord(int start, int byte1, int byte2)
        {
            try
            {
                // Multi Writer Instance
                S7MultiVar Writer = new S7MultiVar(Client);
                byte[] DB_A = new byte[2];
                DB_A[0] = Convert.ToByte(byte1);
                DB_A[1] = Convert.ToByte(byte2);
                int DBNumber_A = 1;
                Writer.Add(S7Consts.S7AreaDB, S7Consts.S7WLByte, DBNumber_A,
                                start, 2, ref DB_A);
                int Result = Writer.Write();
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region ESCRITURA DE UN BYTE      
        /// <summary>
        /// Escribe en el plc un BYTE
        /// </summary>
        /// <param name="start">Posición de memoria en la que escribe</param>
        /// <param name="valor">byte 1 del BYTE</param>
        public void writeByte(int start, int valor)
        {
            try
            {
                // Multi Writer Instance
                S7MultiVar Writer = new S7MultiVar(Client);
                byte[] DB_A = new byte[1];
                DB_A[0] = Convert.ToByte(valor);
                int DBNumber_A = 1;
                Writer.Add(S7Consts.S7AreaDB, S7Consts.S7WLByte, DBNumber_A,
                                start, 1, ref DB_A);
                int Result = Writer.Write();
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region MÉTODOS GENERALES DE LA CLASE
        /* Leer resultados */
        public string LeerResultado()
        {
            return Client.PduSizeNegotiated.ToString();
        }

        /* Leer resultados */
        public bool EstaConectado()
        {
            return Client.Connected;
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region PROPIEDADES DE LA CLASE
        /// <summary>
        /// Propiedad de Objeto S7Client
        /// </summary>
        public S7Client Client
        {
            get { return client; }
        }

        /// <summary>
        /// Propiedadad de Objeto Logo
        /// </summary>
        public Logos Logo
        {
            get { return logo; }
            set { logo = value; }
        }
        #endregion
        /*################################################################*/
    }
}
